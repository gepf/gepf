<?php

/**
 * @var Controller $this
 */

use Gepf\Core\Controller\Controller;

$data = $this->getData();
$request = $this->getRequest();
$response = $this->getResponse();

$data->set('title', 'Start');
$data->set('header_1', 'gepf');
$data->set('header_2', 'good enough php framework');

$data->set(
    'main_p1',
    'gepf is meant to be a self running php application in which content, ' .
    'application logic and style are strictly separated.',
);
$data->set(
    'main_p2',
    'gepf comes with typical application components like user management, ' .
    'routing, database connectivity and much more.',
);
