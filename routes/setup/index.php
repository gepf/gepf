<?php

/** @var Controller $this */

use Gepf\Core\Controller\Controller;
use Gepf\Service\Form\Form;
use Gepf\Service\Security\AuthMethod\AuthMethodInterface;
use Gepf\Service\Security\Login;

$data = $this->getData();
$request = $this->getRequest();
$security = $this->getSecurity();

$security->checkAuth();

$data->set('auth', $security->isAuthenticated());
$data->set('loginStatus', $security->getLoginStatus());
$data->set('formSubmission', $security->getLoginStatus());

$data->set('allowed_auth_methods', $security->getAvailableUserProviders()); //  ['db', 'map', 'pam'] );
$data->set('username', $security->getUsername());

$form = Form::createFromEntityClass(Login::class);
$form->add(AuthMethodInterface::KEY_USERNAME);
$form->add(AuthMethodInterface::KEY_PASSWORD);

$data->set('form', $form->getFields());
