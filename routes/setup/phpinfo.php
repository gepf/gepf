<?php


/** @var Controller $this */

use Gepf\Core\Controller\Controller;

ob_start();
phpinfo();
$html = ob_get_clean();

/// Delete styles from output
$html = preg_replace('#(\n?<style[^>]*?>.*?</style[^>]*?>)|(\n?<style[^>]*?/>)#is', '', $html);
$html = preg_replace('#(\n?<head[^>]*?>.*?</head[^>]*?>)|(\n?<head[^>]*?/>)#is', '', $html);
/// Delete DOCTYPE from output
$html = preg_replace('/<!DOCTYPE html PUBLIC.*?>/is', '', $html);
/// Delete body and html tags
$html = preg_replace('/<html.*?>.*?<body.*?>/is', '', $html);
$html = preg_replace('/.<\/body><\/html>/is', '', $html);

$data = $this->getData();
$data->set('phpinfo', $html);
