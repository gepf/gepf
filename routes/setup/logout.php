<?php

/** @var Controller $this */

use Gepf\Core\Controller\Controller;

$security = $this->getSecurity();
$security->removeAuth();

$this->redirect('./');
