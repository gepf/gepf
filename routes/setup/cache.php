<?php

/** @var Controller $this */

use Gepf\Core\Controller\Controller;
use Gepf\Core\Util\Markup;

$data = $this->getData();
$data->set(
    'cache',
    Markup::export(opcache_get_status(), [
        'div# ' => [
            'h1' => '#',
            'div' => '$',
        ],
        'div' => [
            'h2' => '#',
            'div($?arr)' => ['table' => '$'],
            'div($?!arr)' => '$',
        ],
        'tr' => [
            'td_1' => '#',
            'td_2' => '$',
        ],
    ]),
);
