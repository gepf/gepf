<?php

/** @var Controller $this */

use Gepf\Core\Controller\Controller;
use Gepf\Core\Negotiation\Response\Response;

$this->getRequest()->headers->getAccept()->setPreferredValue(Response::CONTENT_TYPE_XML);

$data = $this->getData();
$data->set('test', 'Hello World');
