<?php

/**
 * Convenient way to test cache via Web.
 * @see \components\Cache\CacheTest for actual cache testing
 *
 * @var Controller $this
 */

use Gepf\Cache\Adapter\PhpFileAdapter;
use Gepf\Core\Controller\Controller;

$exampleArray = [
    'this' => 'is',
    'an' => [
        'example',
        'array',
    ],
];

// TODO clear cache beforehand, test retrieval of cache file also

// Test json
$cache = new PhpFileAdapter(PhpFileAdapter::STRATEGY_JSON);
$jsonResult = $cache->get('evaluation.cache.json', function () use ($exampleArray) {
    return $exampleArray;
});

// Test serialize
$cache = new PhpFileAdapter(PhpFileAdapter::STRATEGY_SERIALIZATION);
$serializeResult = $cache->get('evaluation.cache.serialize', function () use ($exampleArray) {
    return $exampleArray;
});

echo md5(json_encode($jsonResult, JSON_THROW_ON_ERROR)) . "<br />\n";
echo md5(json_encode($serializeResult, JSON_THROW_ON_ERROR));
