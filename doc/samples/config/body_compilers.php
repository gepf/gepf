<?php

/*| AVAILABLE BODY COMPILERS CONFIG
 *|---------------------------------
 *| As for content-type like text/html
*/

use Gepf\Core\Negotiation\Response\Response;

return [
    Response::CONTENT_TYPE_PLAIN => Gepf\Core\View\Plain::class,
    Response::CONTENT_TYPE_JSON => Gepf\Core\View\Json::class,
    Response::CONTENT_TYPE_HTML => Gepf\Lite\Engine::class,
    Response::CONTENT_TYPE_XML => Gepf\Core\View\XmlViewCompiler::class,
    Response::CONTENT_TYPE_ANY => Gepf\Bridge\Twig\Twig::class,
];
