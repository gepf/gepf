<?php /** @noinspection SpellCheckingInspection */

return [
    'class' => Gepf\Service\Security\Security::class,
    'config' => [
        'user_providers' => [
            'map' => Gepf\Service\Security\UserProvider\MapProvider::class,
            'pam' => Gepf\Service\Security\UserProvider\PamProvider::class,
            //'db'
        ],
        'auth_methods' => [
            'form-based' => Gepf\Service\Security\AuthMethod\FormBased::class,
        ],
        'allowed_auth_methods' => [
            'user-map',
            'session-component',
            'linux-pam',
        ],
        'user_map' => [
            // default admin:admin
            'admin' => '$2y$12$.0XfXR05onc0sMUyQlruQuhIj8TKlmwG/ZNQLOpXoHqGa9nUvbPny',
        ],
        'session_allowed_users' => ['admin'],
        'session_allowed_groups' => ['admin'],
        'linux_pam_allowed_users' => ['root'],
        'linux_pam_allowed_groups' => ['root', 'sudo', 'www-data'],
    ],
];
