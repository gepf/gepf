<?php

return [
    'class' => Gepf\Service\Debug\Debug::class,
    'config' => [
        'initialMemory' => null,
        'memoryLimit' => null,
    ],
];
