<?php

return [
    'class' => Gepf\Service\Editorial::class,
    'config' => [
        'backend' => Gepf\Service\Editorial::BACKEND_JSON,
    ],
];
