<?php
/**
 * @noinspection PhpUnused
 * @noinspection UnknownInspectionInspection
 */

use Gepf\Service\Debug\Debug;

function dump($mixed): void
{
    echo (new Debug())->dump($mixed);
}


function getDump($mixed): string
{
    return (new Debug())->dump($mixed);
}
