<?php

use Gepf\Core\Environment\Environment;

function isProduction(): bool
{
    return $_ENV[Environment::KEY_MODE] === Environment::MODE_PROD;
}

function isDevelopment(): bool
{
    return $_ENV[Environment::KEY_MODE] === Environment::MODE_DEV;
}

function isTest(): bool
{
    if (!isset($_ENV[Environment::KEY_MODE])) {
        return false;
    }

    return $_ENV[Environment::KEY_MODE] === Environment::MODE_TEST;
}

function isVendor(): bool
{
    return $_ENV[Environment::KEY_IS_VENDOR];
}

function isErrorReporting(): bool
{
    if (strtolower(getenv('ERROR_REPORTING')) === 'true') {
        return true;
    }

    if (isDevelopment()) {
        return true;
    }

    return false;
}

function getSrcDir(): bool
{
    return $_ENV[Environment::KEY_SOURCE];
}

function getConfDir(): string
{
    return getenv('CONF_DIR') ?: 'config';
}

function getTmpDir(): string
{
    return isTest() ? 'tmp/__test' : 'tmp';
}

function getVarDir(): string
{
    return isTest() ? 'var/__test' : 'var';
}
