<?php

/** @var Controller $this */

use Gepf\Core\Command\CommandManager;
use Gepf\Core\Controller\Controller;

$name = CommandManager::getInput('Name');


print "
____________
< Hallo $name >
 ------------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
";
