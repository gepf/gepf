<?php /** @noinspection PhpUnhandledExceptionInspection */

namespace components\Service;

use Gepf\Core\Kernel\Task\Boilerplate\Boilerplate;
use Gepf\Core\Log\Logger;
use Gepf\Test\FunctionalTestCase;
use Psr\Log\LogLevel;

class SecurityTest extends FunctionalTestCase
{
    protected function setUp(): void
    {
        $securityConfig = require 'doc/samples/config/services/00_security.php';

        //parent::setUp();
        self::createKernel();
        static::$kernel->workToAndExecute(Boilerplate::TASK_CHECK_RUNTIME);
        static::$container->getConfig()->addService($securityConfig);
        static::$kernel->bootToLogicStage();
    }

    public function testLogin(): void
    {
        /** @var Logger $logger */
        $logger = static::$container->getLogger();
        $security = static::$container->getSecurity();
        $security->checkAuth();
        $result = $security->login();

        $this->assertFalse($result);

        $this->assertSame('Attempt to login with empty username', current($logger->getLevel(LogLevel::INFO))->getMessage());
    }
}
