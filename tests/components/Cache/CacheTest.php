<?php

namespace components\Cache;

use Gepf\Cache\Adapter\PhpFileAdapter;
use Gepf\Test\FunctionalTestCase;

class CacheTest extends FunctionalTestCase
{
    public static function getTestItems(): array
    {
        return [
            ['string'],
            [42],
            [42.42],
            [null],
            [true],
            [['I' => 'am', 'an' => ['example', 'array']]],
            [(object)['scalar_property' => 'value', 'array_property' => ['1', '2']]],
        ];
    }

    /**
     * @dataProvider getTestItems
     */
    public function testVarExport(mixed $content): void
    {
        $this->test($content, PhpFileAdapter::STRATEGY_VAR_EXPORT);
    }

    private function test(mixed $content, string $strategy): void
    {
        $cache = new PhpFileAdapter($strategy);
        $result = $cache->get('cache.key', function () use ($content) {
            return $content;
        });

        self::assertSame($result, $content);
    }

    /**
     * @dataProvider getTestItems
     */
    public function testJson(mixed $content): void
    {
        $this->test($content, PhpFileAdapter::STRATEGY_JSON);
    }

    /**
     * @dataProvider getTestItems
     */
    public function testSerialization(mixed $content): void
    {
        $this->test($content, PhpFileAdapter::STRATEGY_SERIALIZATION);
    }
}
