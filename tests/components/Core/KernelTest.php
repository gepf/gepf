<?php

namespace components\Core;

use Gepf\Core\Kernel\Task\Boilerplate\SubscribeEvents;
use Gepf\Test\KernelTestCase;

class KernelTest extends KernelTestCase
{
    public function testStartAndStopOfKernel(): void
    {
        // Test this does not throw exceptions
        static::$kernel->workToAndExecute(SubscribeEvents::class);
        static::$kernel->reboot();

        $this->expectNotToPerformAssertions();
    }


    // TODO prepare test/framework for complete kernel run
//    public function testCompleteKernelRun(): void
//    {
//        static::$kernel->finish();
//    }
}
