<?php

use Gepf\Core\Autoload;

chdir(__DIR__ . '/../');

// writing session mock might be necessary, not sure...
//$_SESSION = [];

try {
    // because of the workings of phpunit, session_start will cause an exception when it is called later
    session_start();
} catch (Exception $exception) {
    // try tests anyway
}

require 'components/Core/Autoload.php';
spl_autoload_register([Autoload::class, Autoload::getLoader(false)]);
