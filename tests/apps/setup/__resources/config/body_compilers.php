<?php

use Gepf\Core\Negotiation\Response\Response;

return [
    Response::CONTENT_TYPE_HTML => Gepf\Bridge\Twig\Twig::class,
];
