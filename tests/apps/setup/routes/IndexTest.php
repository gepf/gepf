<?php /** @noinspection PhpUnhandledExceptionInspection */

namespace apps\setup\routes;

use Gepf\Test\HtmlTestCase;

class IndexTest extends HtmlTestCase
{
    protected function setUp(): void
    {
        self::createLocalClient([
            'CONF_DIR' => 'tests/components/apps/__resources/config',
            'ERROR_REPORTING' => 'TRUE',
        ]);
    }

    public function testGuestAccess(): void
    {
        $this->request('/setup');
        $this->assertResponseIsSuccessful();
    }

    public function testUserAccess(): void
    {
        $this->request('/setup');
        $this->assertResponseIsSuccessful();

        $this->form('/setup', [
            'user[email]' => 'admin',
            'user[password]' => 'admin',
        ]);
        $this->assertResponseIsSuccessful();
//		$this->assertFormSuccessful();
//		$this->assertUserLoggedIn('admin');
    }
}
