<?php /** @noinspection PhpUnhandledExceptionInspection */

namespace apps\welcome\routes;

use Gepf\Test\HtmlTestCase;

class IndexTest extends HtmlTestCase
{
    public function testGuestAccess(): void
    {
        $this->request('/welcome');
        $this->assertResponseIsSuccessful();
    }
}
