<?php

use Gepf\Core\Autoload;
use Gepf\Core\Kernel\Kernel;

/*| GET STATS FOR PERFORMANCE EVALUATION |*/
define('RU_START', getrusage());
define('WALL_CLOCK_START', microtime(true));

umask(0002);

/*| IMMEDIATE RETURN IN CASE OF PING... |*/
$path = $_SERVER['PATH_INFO'] ?? $_SERVER['REQUEST_URI'] ?? null;
if ($path === '/__ping') {
    echo 'pong';
    exit;
}

$isVendor = is_dir('vendor/gepf/gepf');

/*| LOAD |*/
require 'components/Core/Autoload.php';
spl_autoload_register([Autoload::class, Autoload::getLoader($isVendor)]);

// .env can be set via environment variable
$dotEnv = getenv('GEPF_ENV_FILE') ?: '.env';

Kernel::spawn($dotEnv, $isVendor)->boot()->finish();
