<?php

namespace Psr\Container;

/**
 * Polyfill, for composer less instances
 *
 * @author https://www.php-fig.org/personnel/
 */
interface ContainerInterface
{
    /** @throws NotFoundExceptionInterface|ContainerExceptionInterface */
    public function get(string $id): mixed;

    public function has(string $id): bool;
}
