<?php

namespace Psr\Container;

/**
 * Polyfill, for composer less instances
 *
 * @author https://www.php-fig.org/personnel/
 */
interface ContainerExceptionInterface extends \Throwable
{

}
