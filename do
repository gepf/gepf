#!/usr/bin/env php
<?php

if (getcwd() !== __DIR__) {
    chdir(__DIR__);
}

require __DIR__ . '/index.php';
