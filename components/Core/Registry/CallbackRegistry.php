<?php

namespace Gepf\Core\Registry;

use Gepf\Core\AssociativeCollection;

class CallbackRegistry extends AssociativeCollection
{
    public function current(): callable
    {
        return parent::current();
    }
}
