<?php

namespace Gepf\Core\Registry;

use Gepf\Core\DependencyInjection\KernelService;
use Gepf\Core\Negotiation\Request\Request;
use Gepf\Core\Negotiation\Response\Response;

class Registry extends KernelService
{
    private Data $data;
    private AssetRegistry $assets;
    private CallbackRegistry $callbacks;
    private Request $request;
    private Response $response;
    private ?string $dump = null;

    public function __construct(Data $data = null, AssetRegistry $assets = null, CallbackRegistry $callbacks = null)
    {
        $this->data = $data ?? new Data();
        $this->assets = $assets ?? new AssetRegistry();
        $this->callbacks = $callbacks ?? new CallbackRegistry();
    }

    public function getAssetRegistry(): AssetRegistry
    {
        return $this->assets;
    }

    public function getData(): Data
    {
        return $this->data;
    }

    public function getCallbacks(): CallbackRegistry
    {
        return $this->callbacks;
    }

    public function getDump(): ?string
    {
        return $this->dump;
    }

    public function setDump(?string $dump): void
    {
        $this->dump = $dump;
    }

    public function getRequest(): Request
    {
        return $this->request;
    }

    public function setRequest(Request $request): void
    {
        $this->request = $request;
    }

    public function getResponse(): Response
    {
        return $this->response;
    }

    public function setResponse(Response $response): void
    {
        $this->response = $response;
    }


    // shortcuts
    public function get(string $key): mixed
    {
        return $this->getData()->get($key);
    }

    public function set(string $key, mixed $value): bool
    {
        return $this->getData()->set($key, $value);
    }
}
