<?php

namespace Gepf\Core\Registry;

use Gepf\Core\Collection;
use Gepf\Service\Asset\Item\AbstractAsset;
use Gepf\Service\Asset\Item\AssetCollectionTrait;

class AssetRegistry extends Collection
{
    use AssetCollectionTrait;

    public function current(): AbstractAsset
    {
        return parent::current();
    }
}
