<?php

namespace Gepf\Core\Registry;

use Gepf\Core\AssociativeCollection;

class Data extends AssociativeCollection
{
    public function getRegister(): array
    {
        return $this->toArray();
    }
}
