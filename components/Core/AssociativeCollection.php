<?php

namespace Gepf\Core;

use ReturnTypeWillChange;

class AssociativeCollection implements \Iterator
{
    protected array $collection = [];
    private array $keys = [];
    private int $position = 0;
    private int $count = 0;


//    public function hasValue(mixed $value): bool
//    {
//        return \in_array($value, $this->collection, true);
//    }

    public function hasKey(string $key): bool
    {
        return \in_array($key, $this->keys, true);
    }

    public function get(string $key): mixed
    {
        if (!$this->hasKey($key)) {
            throw new \RuntimeException('Key not found: ' . $key);
        }

        return $this->collection[array_search($key, $this->keys, true)];
    }

    public function set(string $key, mixed $value): bool
    {
        if ($this->hasKey($key)) {
            throw new \RuntimeException('Key for AssociativeCollection already exists: ' . $key);
        }

        $this->collection[$this->count] = $value;
        $this->keys[$this->count] = $key;
        $this->count++;

        return true;
    }

    public function remove($value): bool
    {
        $position = array_search($value, $this->collection, true);
        if ($position !== false) {
            unset($this->collection[$position], $this->keys[$position]);

            $this->count--;

            return true;
        }

        return false;
    }

    public function toArray(): array
    {
        $associativeArray = [];
        foreach ($this->collection as $n => $item) {
            $associativeArray[$this->getKeyForPosition($n)] = $item;
        }

        return $associativeArray;
    }

    #[ReturnTypeWillChange]
    public function current()
    {
        return $this->collection[$this->position];
    }

    public function next(): void
    {
        $this->position++;
    }

    public function key(): int
    {
        return $this->getKeyForPosition($this->position);
    }

    public function valid(): bool
    {
        return isset($this->collection[$this->position]);
    }

    public function rewind(): void
    {
        $this->position = 0;
    }

    public function search($value): ?string
    {
        $this->rewind();
        foreach ($this->collection as $n => $item) {
            if ($value === $item) {
                return $this->getKeyForPosition($n);
            }
        }
        return null;
    }

    private function getKeyForPosition(int $position)
    {
        return $this->keys[$position];
    }
}
