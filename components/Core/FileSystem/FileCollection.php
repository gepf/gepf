<?php

namespace Gepf\Core\FileSystem;

use Gepf\Core\Collection;

class FileCollection extends Collection
{
    public static function createFilesFromStrings(array $filePaths): self
    {
        $self = new self();
        foreach ($filePaths as $filePath) {
            $self->add(new File($filePath));
        }
        $self->rewind();

        return $self;
    }

    public function current(): FileEntity
    {
        return parent::current();
    }

    public function addFile(File $file): void
    {
        $this->add($file);
    }

    public function addDirectory(Directory $file): void
    {
        $this->add($file);
    }
}
