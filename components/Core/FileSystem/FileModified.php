<?php

namespace Gepf\Core\FileSystem;

use DateTimeImmutable;

class FileModified extends DateTimeImmutable
{
    public function __construct(private readonly FileEntity $file, $timezone = null)
    {
        $parsableTimestamp = '@' . filemtime($file->getPathname());
        parent::__construct($parsableTimestamp, $timezone);
    }

    public function __toString(): string
    {
        return $this->getAtom();
    }

    public function getFile(): FileEntity
    {
        return $this->file;
    }

    public function getAtom(): string
    {
        return $this->format(self::ATOM);
    }

    public function getSqlTimestamp(): string
    {
        return $this->format('Y-m-d H:i:s');
    }
}
