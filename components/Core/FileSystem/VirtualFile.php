<?php

namespace Gepf\Core\FileSystem;

use Gepf\Core\FileSystem\Util\FileSystem;

class VirtualFile extends AbstractFile
{
    public function toArray(): array
    {
        return [
            'path' => $this->getPathname(),
            'content' => $this->getContent(),
            'md5' => $this->getMd5(),
        ];
    }

    public function getId(): string
    {
        return $this->getMd5();
    }

    public function getMd5(): string
    {
        if (!isset($this->md5)) {
            $this->setMd5(md5($this->getContent()));
        }

        return $this->getMd5();
    }

    public function persist(): File
    {
        FileSystem::putWithoutCache($this->getPathname(), $this->getContent());
        return new File($this->getPathname());
    }
}
