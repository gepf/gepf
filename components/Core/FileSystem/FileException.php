<?php

namespace Gepf\Core\FileSystem;

class FileException extends \RuntimeException
{
    public const TYPE_CANNOT_WRITE = 1;
    public const TYPE_CANNOT_MAKE_DIR = 2;
    public const TYPE_IS_DIR_NOT_FILE = 3;
    public const TYPE_IS_FILE_NOT_DIR = 4;
    public const TYPE_FILE_NOT_FOUND = 5;
    public const TYPE_DIRECTORY_NOT_FOUND = 6;


    /**
     * Exception messages with %s being the path of subject
     */
    public const MESSAGES = [
        self::TYPE_CANNOT_WRITE => 'Cannot write file "%s".',
        self::TYPE_CANNOT_MAKE_DIR => 'Cannot create directory "%s".',
        self::TYPE_IS_DIR_NOT_FILE => 'Provided path "%s" is a directory. Use Directory class instead.',
        self::TYPE_FILE_NOT_FOUND => 'File not found "%s".',
        self::TYPE_DIRECTORY_NOT_FOUND => 'Directory not found: "%s".',
    ];

    public function __construct(string $path, int $type, \Throwable $previous = null, int $code = 0)
    {
        $message = sprintf(self::MESSAGES[$type], $path);
        parent::__construct($message, $code, $previous);
    }
}
