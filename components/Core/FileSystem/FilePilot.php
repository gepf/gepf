<?php

namespace Gepf\Core\FileSystem;

use Gepf\Core\Util\Text;

class FilePilot
{
    private string $currentPath;

    public function __construct(string $entryPoint)
    {
        $this->setCurrentPath($entryPoint);
    }

    public static function createFromString(string $entryPoint): self
    {
        return new self($entryPoint);
    }

    public static function createFromFile(FileEntity $file): self
    {
        return new self($file->getPathname());
    }

    public function getFile(): File
    {
        return new File($this->getCurrentPath());
    }

    public function getDirectory(): Directory
    {
        return new Directory($this->getCurrentPath());
    }

    public function getCurrentPath(): string
    {
        return $this->currentPath;
    }

    public function canGoUp(): bool
    {
        return str_contains($this->getCurrentPath(), '/');
    }

    public function goUp(): self
    {
        $newPath = Text::pop('/', $this->getCurrentPath());
        $this->setCurrentPath($newPath);

        return $this;
    }

    public function goTo(string $relativePath): self
    {
        $newPath = $this->getCurrentPath() . '/' . ltrim($relativePath, '/');
        $this->setCurrentPath($newPath);

        return $this;
    }


    public function check(string $relativePath): bool
    {
        return file_exists($this->getCurrentPath() . '/' . ltrim($relativePath, '/'));
    }


    private function setCurrentPath(string $newPath): void
    {
        $this->checkAbsolutePath($newPath);
        $this->currentPath = rtrim($newPath, '/');
    }

    private function checkAbsolutePath(string $path): void
    {
        if (!file_exists($path)) {
            throw new FileException($path, FileException::TYPE_FILE_NOT_FOUND);
        }
    }
}
