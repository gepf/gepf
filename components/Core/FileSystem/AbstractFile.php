<?php

namespace Gepf\Core\FileSystem;

abstract class AbstractFile extends FileEntity
{
    protected string $content;
    protected string $md5;

    abstract public function getMd5();

    public function setMd5(string $md5): void
    {
        $this->md5 = $md5;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content): void
    {
        $this->content = $content;
    }
}
