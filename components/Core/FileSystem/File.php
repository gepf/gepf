<?php

namespace Gepf\Core\FileSystem;

use Gepf\Core\FileSystem\Util\Directory;
use Gepf\Core\FileSystem\Util\FileSystem;
use Gepf\Core\FileSystem\Util\TmpFileSystem;

class File extends AbstractFile
{
    private string $id;
    private string $initializedWithPath;

    public function __construct(string $pathname)
    {
        if (!is_readable($pathname)) {
            throw new FileException($pathname, FileException::TYPE_FILE_NOT_FOUND);
        }

        if (is_dir($pathname)) {
            throw new FileException($pathname, FileException::TYPE_IS_DIR_NOT_FILE);
        }

        $this->initializedWithPath = $pathname;
        parent::__construct($pathname);
    }

    public static function createCopyFrom(string $source, string $target): self
    {
        return (new self($source))->createCopyWithSubDirs($target);
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'path' => $this->getPathname(),
            'cache' => $this->getCachePath(),
            'content' => $this->getContent(),
            'md5' => $this->getMd5(),
            'modified' => [
                'timezone' => $this->getModifiedTime()->getTimezone(),
                'unix' => $this->getModifiedTime()->getTimestamp(),
                'sql_timestamp' => $this->getModifiedTime()->getTimestamp(),
            ],
        ];
    }

    public function getInitializedWithPath(): string
    {
        return $this->initializedWithPath;
    }

    public function getCachePath(): ?string
    {
        $createdPath = TmpFileSystem::createCachePath($this);
        return TmpFileSystem::exists($createdPath) ? $createdPath : null;
    }

    public function getContent(): string
    {
        if (isset($this->content)) {
            return parent::getContent();
        }

        if ($this->isCached()) {
            $this->setContent(TmpFileSystem::nativelyRequirePhpFileContent($this->getCachePath()));
            return parent::getContent();
        }

        $this->setContent(FileSystem::getUnCachedContent($this->getPathname()));

        return parent::getContent();
    }

    /** Files are automatically cached when processed via FileSystem */
    public function isCached(): bool
    {
        return (bool)$this->getCachePath();
    }

    public function getMd5(): string
    {
        if (!isset($this->md5)) {
            $this->md5 = md5_file($this->getPathname());
        }

        return $this->md5;
    }

    /**
     * "foo/bar/file.tar.gz" will get ID like "foo_bar_file_tar_gz_<filemtime()>"
     */
    public function getId(): string
    {
        if (!isset($this->id)) {
            $this->id = str_replace(['/', '.'], '_', $this->getInitializedWithPath()) . '_' . $this->getModifiedTime()->getTimestamp();
        }

        return $this->id;
    }


    public function createCopyWithSubDirs(string $targetPath): self
    {
        Directory::ensureForFile($targetPath);
        return $this->createCopy($targetPath);
    }


    public function createCopy(string $targetPath): self
    {
        if (!copy($this->getPathname(), $targetPath)) {
            throw new FileException($targetPath, FileException::TYPE_CANNOT_WRITE);
        }

        return new File($targetPath);
    }
}
