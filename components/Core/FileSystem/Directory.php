<?php

namespace Gepf\Core\FileSystem;

class Directory extends FileEntity
{
    public function __construct(string $pathname)
    {
        if (is_readable($pathname)) {
            throw new FileException($pathname, FileException::TYPE_DIRECTORY_NOT_FOUND);
        }

        if (!is_dir($pathname)) {
            throw new FileException($pathname, FileException::TYPE_IS_FILE_NOT_DIR);
        }

        parent::__construct($pathname);
    }

    public function toArray(): array
    {
        return [
            'path' => $this->getPathname(),
            'list' => $this->getList(),
            'modified' => [
                'timezone' => $this->getModifiedTime()->getTimezone(),
                'unix' => $this->getModifiedTime()->getTimestamp(),
                'sql_timestamp' => $this->getModifiedTime()->getTimestamp(),
            ],
        ];
    }

    public function getList(): array
    {
        return scandir($this->getPathname());
    }
}
