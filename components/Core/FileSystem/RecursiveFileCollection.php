<?php

namespace Gepf\Core\FileSystem;

class RecursiveFileCollection
{
    /** @var File[]|RecursiveFileCollection[] */
    private array $paths = [];


    public function __construct(array $paths, ?string $root = null)
    {
        foreach ($paths as $path) {

            if (\in_array($path, ['.', '..'])) {
                continue;
            }

            if ($root) {
                $path = $root . '/' . $path;
            }

            if (!file_exists($path)) {
                throw new FileException($path, FileException::TYPE_FILE_NOT_FOUND);
            }

            if (is_dir($path)) {
                $files = scandir($path);
                $this->paths[] = new RecursiveFileCollection($files, $path);
            }

            if (is_file($path)) {
                $this->paths[] = new File($path);
            }
        }
    }


    public function __sleep()
    {
        return $this->toArray();
    }


    public function toArray(): array
    {
        return self::convertToArray($this->paths);
    }


    private static function convertToArray(array $paths): array
    {
        $tree = [];
        foreach ($paths as $path) {
            $tree[] = self::pathToArray($path);
        }

        return $tree;
    }


    private static function pathToArray(File|RecursiveFileCollection $path): array
    {
        if ($path instanceof File) {
            return $path->toArray();
        }

        return self::convertToArray($path->getPaths());
    }


    public function getPaths(): array
    {
        return $this->paths;
    }


    /**
     * @return File[]
     */
    public function getFiles(): array
    {
        $files = [];
        $fileArrays = [];
        foreach ($this->getPaths() as $path) {
            if ($path instanceof File) {
                $files[] = $path;
            }

            if ($path instanceof self) {
                $fileArrays[] = $path->getFiles();
            }
        }

        return array_merge($files, ...$fileArrays);
    }


//    public function getLastModified(): \DateTimeImmutable
//    {
//        $lastModified = null;
//        foreach ($this->paths as $path) {
//
//            // TODO Test this. For recursion, do $path->getLastModifiedTime()
//            //   or refactor getLastModifiedTime() to getModifiedTime and
//            //   introduce shared interface
//            $modified = $path->getModifiedTime();
//
//            if ($modified > $lastModified) {
//                $lastModified = $modified;
//            }
//        }
//
//        if ($lastModified === null) {
//            throw new \LogicException('Could not determine last modified');
//        }
//
//        return $lastModified;
//    }
}
