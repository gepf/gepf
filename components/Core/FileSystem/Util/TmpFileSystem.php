<?php

namespace Gepf\Core\FileSystem\Util;

use Gepf\Core\FileSystem\File;

class TmpFileSystem
{
    public static function exists(string $pathname): bool
    {
        return file_exists(getTmpDir() . '/' . $pathname);
    }

    public static function put(string $pathname, $data): void
    {
        Directory::ensureForFile(getTmpDir() . '/' . $pathname);
        file_put_contents(getTmpDir() . '/' . $pathname, $data);
    }

    public static function get(string $pathname): File
    {
        return new File(getTmpDir() . '/' . $pathname);
    }

    /**
     * this will work under the condition that file has a bare return statement,
     * like: <?php return 'Stuff';
     */
    public static function nativelyRequirePhpFileContent(string $pathname): mixed
    {
        return require getTmpDir() . '/' . $pathname;
    }

    public static function cacheFile(File $file): void
    {
        Directory::ensureForFile(getTmpDir() . '/' . self::createCachePath($file));

        // Delete all Files with appended timestamp
        array_map('unlink', glob(getTmpDir() . '/files/' . $file->getPathname() . '.*.php'));

        // RegEx required, since addcslashes() masks quotes that are already masked, so does str_replace("'", "\'" ...)
        $maskedContent = preg_replace('/(?<!\\\\)(\')/m', '\\\'', $file->getContent());

        file_put_contents(
            getTmpDir() . '/' . self::createCachePath($file),
            '<?php return \'' . $maskedContent . "';\n",
        );
    }

    public static function createCachePath(File $file): string
    {
        return 'files/' . $file->getPathname() . '.' . $file->getModifiedTime()->getTimestamp() . '.php';
    }
}
