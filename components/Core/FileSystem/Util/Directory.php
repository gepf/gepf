<?php

namespace Gepf\Core\FileSystem\Util;

use Gepf\Core\FileSystem\FileException;
use Gepf\Core\Util\Text;

class Directory
{
    public static function ensureForFile(string $path, int $umask = 0770): bool
    {
        if (is_file($path)) {
            return true;
        }

        $directory = Text::pop('/', $path);

        return self::ensure($directory, $umask);
    }

    public static function ensure(string $directory, int $umask = 0770): bool
    {
        if (is_dir($directory)) {
            return true;
        }

        $oldUmask = umask(0);

        if (!mkdir($directory, $umask, true) && !is_dir($directory)) {
            throw new FileException($directory, FileException::TYPE_CANNOT_MAKE_DIR);
        }

        umask($oldUmask);
        return true;
    }

    public static function list(string $directory, ?string $extension = null): array
    {
        $results = [];
        foreach (scandir($directory) as $file) {

            if ($file === '.' || $file === '..') {
                continue;
            }

            $result = self::checkPath($directory, $file, $extension);

            if ($result) {
                $results = $results ? array_merge($results, $result) : $result;
            }
        }

        return $results;
    }

    public static function checkPath(string $directory, string $file, ?string $extension = null): array|false
    {
        if (is_dir($directory . '/' . $file)) {
            return self::list($directory . '/' . $file, $extension);
        }

        if ($extension) {
            $fileExt = Text::get(-1, '.', $file);
            if ($fileExt !== $extension) {
                return false;
            }
        }

        return [$directory . '/' . $file];
    }

    /**
     * Not recursive
     *
     * @return string[]
     */
    public static function listDirs(string $directory): array
    {
        return array_filter(scandir($directory), static function (string $file) {
            return $file !== '.' && $file !== '..' && !is_file($file);
        });
    }

    /**
     * @noinspection PhpUnused
     */
    public static function isWritableRecursive(string $directory): bool
    {
        if (!file_exists($directory)) {
            throw new FileException('Directory %s not found', $directory);
        }

        if (is_dir($directory)) {
            if (!is_writable($directory)) {
                return false;
            }
            foreach (scandir($directory) as $object) {
                if ($object !== '.' && $object !== '..' && !self::isWritableRecursive($directory . '/' . $object)) {
                    return false;
                }
            }
        }

        return is_writable($directory);
    }
}
