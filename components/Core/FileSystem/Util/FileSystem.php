<?php

namespace Gepf\Core\FileSystem\Util;

use Gepf\Core\FileSystem\File;

class FileSystem
{
    public static function getContent(string $pathname): string
    {
        return self::get($pathname)->getContent();
    }

    public static function get(string $pathname): File
    {
        $file = new File($pathname);

        if ($file->isCached()) {
            return $file;
        }

        TmpFileSystem::cacheFile($file);

        return $file;
    }

    public static function getUnCachedContent(string $pathname): string
    {
        return file_get_contents($pathname);
    }

    public static function put(string $pathname, $data): void
    {
        Directory::ensureForFile($pathname);
        file_put_contents($pathname, $data);

        TmpFileSystem::cacheFile(new File($pathname));
    }

    public static function putWithoutCache(string $pathname, $data): void
    {
        Directory::ensureForFile($pathname);
        file_put_contents($pathname, $data);
    }
}
