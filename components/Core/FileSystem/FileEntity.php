<?php

namespace Gepf\Core\FileSystem;

abstract class FileEntity extends \SplFileInfo
{
    private FileModified $modified;

    public function __sleep(): array
    {
        return $this->toArray();
    }

    abstract public function toArray(): array;

    public function getModifiedTime(): \DateTimeImmutable
    {
        if (!isset($this->modified)) {
            /** @noinspection PhpUnhandledExceptionInspection */
            $this->modified = new FileModified($this);
        }

        return $this->modified;
    }

    public function getPilot(): FilePilot
    {
        return FilePilot::createFromFile($this);
    }
}
