<?php /** @noinspection DuplicatedCode, PhpUnused */

namespace Gepf\Core;

class Autoload
{
    public static function getLoader(bool $isVendor): string
    {
        if ($isVendor) {
            set_include_path(get_include_path() . ':vendor/gepf/gepf');
        }

        return $isVendor ? 'loadAsVendor' : 'loadAsApp';
    }

    public static function loadAsVendor(string $className): void
    {
        $path = str_replace('\\', '/', $className);
        $pathParts = explode('/', $path);
        $pathWithoutVendor = trim(str_replace($pathParts[0], '', $path), '/');

        // COMPONENTS
        if ($pathParts[0] === 'Gepf') {
            require_once 'components/' . str_replace('Gepf/', '', $path) . '.php';

            return;
        }

        // APP
        if (file_exists('app/' . $pathWithoutVendor . '.php')) {
            require_once 'app/' . $pathWithoutVendor . '.php';
        }

        // POLYFILL
        if (stream_resolve_include_path("polyfill/$path.php")) {
            require_once "polyfill/$path.php";
        }
    }

    public static function loadAsApp(string $className): void
    {
        $path = str_replace('\\', '/', $className);
        $pathParts = explode('/', $path);
        $pathWithoutVendor = trim(str_replace($pathParts[0], '', $path), '/');

        // COMPONENTS
        if ($pathParts[0] === 'Gepf') {
            require_once 'components/' . str_replace('Gepf/', '', $path) . '.php';

            return;
        }

        // BUNDLES
        if (file_exists('bundles/' . strtolower($pathParts[0]) . '/app/' . $pathWithoutVendor . '.php')) {
            require_once 'bundles/' . strtolower($pathParts[0]) . '/app/' . $pathWithoutVendor . '.php';

            return;
        }

        // APPS
        if (file_exists('apps/' . $path . '.php')) {
            require_once 'apps/' . $path . '.php';
        }

        // POLYFILL
        if (file_exists('polyfill/' . $path . '.php')) {
            require_once 'polyfill/' . $path . '.php';
        }

        // TESTS
        if (file_exists('tests/' . $path . '.php')) {
            require_once 'tests/' . $path . '.php';
        }
    }
}
