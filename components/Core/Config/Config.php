<?php

namespace Gepf\Core\Config;

use Gepf\Core\DependencyInjection\ReadOnlyServiceInterface;
use Gepf\Service\ServiceConfigInterface;

class Config implements ReadOnlyServiceInterface
{
    use ConfigTrait;

    private ?string $templateEngine;
    private array $services = [];
    private array $appConfigs = [];
    private array $customSettings;
    private array $bodyCompilers;
    private string $profiler;
    private ?string $defaultApp = null;
    private bool $bufferDump;


    public function getDefaultApp(): ?string
    {
        return $this->defaultApp;
    }

    public function setDefaultApp(?string $defaultApp): void
    {
        $this->defaultApp = $defaultApp;
    }

    public function isBufferDump(): bool
    {
        return $this->bufferDump;
    }

    public function setBufferDump(bool $bufferDump): void
    {
        $this->bufferDump = $bufferDump;
    }

    public function getTemplateEngine(): ?string
    {
        return $this->templateEngine;
    }

    public function setTemplateEngine(string $templateEngine): void
    {
        $this->templateEngine = $templateEngine;
    }

    public function getService(string $key): ServiceConfigInterface
    {
        return $this->services[$key];
    }

    /**
     * @return ServiceConfigInterface[]
     */
    public function getServices(): array
    {
        return $this->services;
    }

    public function setServices(array $services): void
    {
        $this->services = $services;
    }

    public function addService(array|ServiceConfigInterface $serviceConfig): void
    {
        if (\is_array($serviceConfig)) {
            $serviceConfig = $this->createConfigFromArray($serviceConfig);
        }

        /** @var ServiceConfigInterface $serviceConfig */
        $this->services[$serviceConfig->getName()] = $serviceConfig;
    }

    public function getAppConfigs(): array
    {
        return $this->appConfigs;
    }

    public function getAppConfig($key)
    {
        return $this->appConfigs[$key] ?? null;
    }

    public function addAppConfig(array $appConfig): void
    {
        $this->appConfigs = array_merge($this->appConfigs, $appConfig);
    }

    public function getBodyCompilers(): array
    {
        return $this->bodyCompilers;
    }

    public function setBodyCompiler(string $key, string $value): void
    {
        $this->bodyCompilers[$key] = $value;
    }

    public function setBodyCompilers(array $bodyCompilers): void
    {
        $this->bodyCompilers = $bodyCompilers;
    }

    public function get(string $key): mixed
    {
        return $this->customSettings[$key] ?? null;
    }
}
