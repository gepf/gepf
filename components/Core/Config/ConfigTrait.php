<?php

namespace Gepf\Core\Config;

use Gepf\Service\ServiceConfig;

trait ConfigTrait
{
    private function createConfigFromArray(array $serviceSettings): ServiceConfig
    {
        return new ServiceConfig(
            $serviceSettings['class'],
            $serviceSettings['config'],
            $serviceSettings['interface'] ?? null,
        );
    }
}
