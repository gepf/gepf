<?php

namespace Gepf\Core\Event;

use Gepf\Core\Collection;

class EventCollection extends Collection
{
    public function addEvent(Event $event): void
    {
        $this->add($event);
    }

    public function findByNameAndTempus(string $name, int $tempus): ?\Generator
    {
        foreach ($this->collection as $item) {
            if ($item->getName() !== $name) {
                continue;
            }

            if ($item->getTempus() !== $tempus) {
                continue;
            }

            yield $item;
        }
    }

    public function findOneBy(string $name, int $tempus, int $priority): ?Event
    {
        foreach ($this->collection as $item) {
            if ($item->getName() !== $name) {
                continue;
            }

            if ($item->getTempus() !== $tempus) {
                continue;
            }

            if ($item->getPriority() !== $priority) {
                continue;
            }

            return $item;
        }

        return null;
    }
}
