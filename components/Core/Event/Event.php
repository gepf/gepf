<?php

namespace Gepf\Core\Event;

use Gepf\Core\StringCollection;

class Event
{
    public const TEMPUS_PRE = 1;
    public const TEMPUS_POST = 2;

    private StringCollection $listeners;

    public function __construct(
        private readonly string $name,
        private readonly int $tempus,
        private readonly int $priority = 0,
    ) {
        $this->listeners = new StringCollection();
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getTempus(): int
    {
        return $this->tempus;
    }

    public function getPriority(): int
    {
        return $this->priority;
    }

    public function getListeners(): StringCollection
    {
        return $this->listeners;
    }

    public function addListener(string $listener): void
    {
        $this->listeners->addString($listener);
    }
}
