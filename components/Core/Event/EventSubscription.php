<?php

namespace Gepf\Core\Event;

class EventSubscription
{
    private string $listener;
    private string $eventName;
    private int $tempus;
    private int $priority;

    public function __construct(string $listener, string $eventName, int $tempus, int $priority = 0)
    {
        $this->listener = $listener;
        $this->eventName = $eventName;
        $this->tempus = $tempus;
        $this->priority = $priority;
    }

    public function getListener(): string
    {
        return $this->listener;
    }

    public function getEventName(): string
    {
        return $this->eventName;
    }

    public function getTempus(): int
    {
        return $this->tempus;
    }

    public function getPriority(): int
    {
        return $this->priority;
    }
}
