<?php

namespace Gepf\Core\Event;

use Gepf\Core\DependencyInjection\KernelService;

class EventManager extends KernelService
{
    private EventCollection $events;

    public function __construct()
    {
        $this->events = new EventCollection();
    }


//	public function createEventsFromServiceConfigs(): void
//	{
//		foreach ($this->container->getServiceConfigs() as $config) {
//			foreach ($config->getEventSubscribers() as $eventSubscriber) {
//				$this->addEvent($eventSubscriber);
//			}
//		}
//	}


    /** @return Event[] */
    public function findEvents(string $name, int $tempus): iterable
    {
        return $this->events->findByNameAndTempus($name, $tempus);
    }


    public function subscribe(EventSubscription $subscription): void
    {
        $existingEvent = $this->events->findOneBy(
            $subscription->getEventName(),
            $subscription->getTempus(),
            $subscription->getPriority()
        );

        if ($existingEvent) {
            $existingEvent->addListener($subscription->getListener());

            return;
        }

        $newEvent = new Event($subscription->getEventName(), $subscription->getTempus(), $subscription->getPriority());
        $newEvent->addListener($subscription->getListener());

        $this->addEvent($newEvent);
    }


    public function addEvent(Event $event): void
    {
        $this->events->addEvent($event);
    }


//    public function getEvents(): EventCollection
//    {
//        $this->events->rewind();
//
//        return $this->events;
//    }
}
