<?php

namespace Gepf\Core\Event;

abstract class AbstractEventListener
{
    abstract public function execute(): void;
}
