<?php

namespace Gepf\Core\Negotiation\Request;

use Gepf\Core\DependencyInjection\ParameterBag\ParameterBag;
use Gepf\Core\DependencyInjection\ReadOnlyServiceInterface;
use Gepf\Core\Runtime\Runtime;

/**
 * Request Object contains entire HttpRequest, or – if called differently than via http – polyfills for such
 * \-
 *  - body      $_REQUEST        combined set of $_GET and $_POST
 *  - server    $_SERVER
 *  - cli       $_SERVER[argv]
 *  - headers   $_SERVER         subset of server
 *  - cookies   $_COOKIE
 *  - files     $_FILES
 */
readonly class Request implements ReadOnlyServiceInterface
{
    public string $method;

    public ParameterBag $body;
    public ParameterBag $server;
    public RequestHeaderBag $headers;
    public ParameterBag $cookies;
    public ParameterBag $files;

    /** Use for custom parameters */
    public ParameterBag $parameter;

    private ?string $path;


    public static function createFromGlobals(): self
    {
        $request = new self();
        $request->body = new ParameterBag($_REQUEST);
        $request->server = new ParameterBag($_SERVER);
        $request->cookies = new ParameterBag($_COOKIE);
        $request->files = new ParameterBag($_FILES);
        $request->parameter = new ParameterBag();

        $request->method = $_SERVER['REQUEST_METHOD'] ?? 'GET';
        $request->path = $request->detectPath($_SERVER);

        $request->headers = RequestHeaderBag::createSelf($_SERVER);

        return $request;
    }


    /**
     * @throws \RuntimeException
     */
    private function detectPath(array $server): ?string
    {
        $path = $server['PATH_INFO'] ?? $server['REQUEST_URI'] ?? $server['argv'][1] ?? null;

        if ($path === null && Runtime::SERVER_API_CONSOLE === PHP_SAPI) {
            $path = 'gepf:list-commands';
        }

        if ($path === null) {
            throw new \RuntimeException('Path could not be perceived');
        }

        return trim($path, '/');
    }


    public function getBody(): ParameterBag
    {
        return $this->body;
    }


    public function getPath(): string
    {
        return $this->path;
    }


    public function getLocale()
    {
        return $this->headers->getLanguage()->get() ?? 'en';
    }


    /**
     * Shortcut for body->get()
     *
     * For example in case of posted form, it will be a submitted value
     */
    public function get(mixed $key): mixed
    {
        return $this->body->get($key);
    }
}
