<?php

namespace Gepf\Core\Negotiation\Request;

use Gepf\Core\Negotiation\Http\Header\Header;
use Gepf\Core\Negotiation\Http\Header\HeaderBag;
use Gepf\Core\Negotiation\Http\Header\HeaderUtils;

class RequestHeaderBag extends HeaderBag
{
    private const HEADER_FALLBACKS = [
        'Accept' => 'text/html',
        'Accept-Language' => 'en',
    ];


    /* $server as known from $_SERVER */
    public static function createSelf(array $server): self
    {
        $httpServerRows = HeaderUtils::getHttpRows($server);
        $httpServerRows['Accept'] ??= self::HEADER_FALLBACKS['Accept'];
        $httpServerRows['Accept-Language'] ??= self::HEADER_FALLBACKS['Accept-Language'];

        $headers = [];
        foreach ($httpServerRows as $key => $row) {
            $headers[$key] = new Header($row);
        }

        return new self($headers);
    }


    public function getAccept(): Header
    {
        return $this->get('Accept');
    }

    public function getLanguage(): Header
    {
        return $this->get('Accept-Language');
    }
}
