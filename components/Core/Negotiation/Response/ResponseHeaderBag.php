<?php

namespace Gepf\Core\Negotiation\Response;

use Gepf\Core\Negotiation\Http\Header\Header;
use Gepf\Core\Negotiation\Http\Header\HeaderBag;

class ResponseHeaderBag extends HeaderBag
{
//    public function getContentLanguage(): Header
//    {
//        return $this->get('content-language');
//    }
//
//    public function setContentLanguage(string $contentLanguage): void
//    {
//        $this->add('content-language', new Header($contentLanguage));
//    }

    public function getContentType(): ?Header
    {
        return $this->get('content-type');
    }

    public function setContentType(string $contentType): void
    {
        $this->add('content-type', new Header($contentType));
    }
}
