<?php

namespace Gepf\Core\Negotiation\Response;

use Gepf\Core\DependencyInjection\KernelService;

class Response extends KernelService
{
    use ResponseStatusTrait;

    public const CONTENT_TYPE_PLAIN = 'text/plain';
    public const CONTENT_TYPE_JSON = 'application/json';
    public const CONTENT_TYPE_HTML = 'text/html';
    public const CONTENT_TYPE_XML = 'application/xml';
    public const CONTENT_TYPE_ANY = '*/*';

    private ?string $body = null;
    private ?string $redirect = null;
    public ResponseHeaderBag $headers;
    private ResponseStatus $status;

    public function __construct()
    {
        $this->headers = new ResponseHeaderBag();
        $this->status = new ResponseStatus();
    }

    public function getBody(): ?string
    {
        return $this->body;
    }

    public function setBody(?string $body): void
    {
        $this->body = $body;
    }

    public function getHeaders(): ResponseHeaderBag
    {
        return $this->headers;
    }

    public function getRedirect(): ?string
    {
        return $this->redirect;
    }

    public function setRedirect(string $redirect): void
    {
        $this->redirect = $redirect;
    }

    public function getStatus(): ResponseStatus
    {
        return $this->status;
    }
}
