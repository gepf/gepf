<?php

namespace Gepf\Core\Negotiation\Response;

class ResponseStatus
{
    public const STATUS_OK = 'OK';
    public const STATUS_NOT_FOUND = 'Not Found';
    public const STATUS_METHOD_NOT_ALLOWED = 'Method Not Allowed';
    public const STATUS_INTERNAL_SERVER_ERROR = 'Internal Server Error';

    public const CODES = [
        200 => self::STATUS_OK,
        404 => self::STATUS_NOT_FOUND,
        405 => self::STATUS_METHOD_NOT_ALLOWED,
        500 => self::STATUS_INTERNAL_SERVER_ERROR,
    ];

    private int $code;
    private string $message;
    private string $reason;
    private array $backtrace = [];

    public function getCode(): int
    {
        return $this->code;
    }

    final public function setCode(int $code): void
    {
        $this->code = $code;
    }

    /**
     * If message property is uninitialized, standard message for current status code is being looked up
     */
    public function getMessage(): string
    {
        if (!isset($this->message) && !isset($this->code)) {
            throw new \LogicException('No response status message or code are set');
        }

        if (!isset($this->message)) {
            if (!isset(self::CODES[$this->code])) {
                throw new \LogicException('Attempted to get standard message for unknown response code: ' . $this->code);
            }

            return self::CODES[$this->code];
        }

        return $this->message;
    }

    /**
     * Standard messages can be overwritten, like 'path/to/site Not Found' instead of 'Not Found'.
     * They should suit the status code though.
     */
    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

    public function getReason(): string
    {
        return $this->reason;
    }

    public function setReason(string $reason): void
    {
        $this->reason = $reason;
    }

    public function getTrace(): array
    {
        return $this->backtrace;
    }

    public function setBacktrace(array $backtrace): void
    {
        $this->backtrace = $backtrace;
    }

    public function isCodeSet(): bool
    {
        return isset($this->code);
    }

    public function isStatusOk(): bool
    {
        // If status not set yet, it is okay
        if (!$this->isCodeSet()) {
            return true;
        }

        // If the first char of status code is not 4 or 5, it is okay
        $code = (string)$this->getCode();
        if (!in_array($code[0], ['4', '5'], true)) {
            return true;
        }

        return false;
    }
}
