<?php

namespace Gepf\Core\Negotiation\Response;

trait ResponseStatusTrait
{
    public function getCode(): int
    {
        return $this->getStatus()->getCode();
    }

    final public function setCode(int $code): void
    {
        $this->getStatus()->setCode($code);
    }

    public function getMessage(): string
    {
        return $this->getStatus()->getMessage();
    }

    final public function setMessage(string $message): void
    {
        $this->getStatus()->setMessage($message);
    }

    public function getReason(): string
    {
        return $this->getStatus()->getReason();
    }

    final public function setReason(string $reason): void
    {
        $this->getStatus()->setReason($reason);
    }

    public function getBacktrace(): array
    {
        return $this->getStatus()->getTrace();
    }

    final public function setBackTrace(array $backTrace): void
    {
        $this->getStatus()->setBacktrace($backTrace);
    }

    public function isCodeSet(): bool
    {
        return $this->getStatus()->isCodeSet();
    }

    public function isStatusOk(): bool
    {
        return $this->getStatus()->isStatusOk();
    }
}
