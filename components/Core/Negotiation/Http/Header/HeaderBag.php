<?php

namespace Gepf\Core\Negotiation\Http\Header;

use Gepf\Core\DependencyInjection\ParameterBag\ParameterBag;

class HeaderBag extends ParameterBag
{
    public function add(string $key, Header $header): void
    {
        $this->parameter[$key] = $header;
    }
}
