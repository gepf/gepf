<?php

namespace Gepf\Core\Negotiation\Http\Header;

use Gepf\Core\Exception\InvalidValueException;

class Header
{
    private string $plain;
    private ?string $preferredValue = null;
    private array $values;

    public function __construct(string $plain)
    {
        $this->plain = $plain;
        $this->values = HeaderUtils::deserialize($plain);
    }

    public function __toString()
    {
        return $this->plain;
    }

    public function get(int $n = null)
    {
        if ($n === null) {
            return $this->preferredValue ?? $this->getValues()[0];
        }

        return $this->getValues()[$n];
    }

    public function getValues(): array
    {
        return $this->values;
    }

    public function setPreferredValue(string $value): void
    {
        if (!in_array($value, $this->getValues(), true)) {
            throw new InvalidValueException($value, InvalidValueException::TYPE_VALUE, $this->getValues());
        }

        $this->preferredValue = $value;
    }
}
