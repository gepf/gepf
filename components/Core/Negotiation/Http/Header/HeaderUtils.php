<?php

namespace Gepf\Core\Negotiation\Http\Header;

use Gepf\Core\Util\Text;

class HeaderUtils
{
    public static function getHttpRows(array $server): array
    {
        $httpRows = [];
        foreach ($server as $serverKey => $serverValue) {
            if (str_starts_with($serverKey, 'HTTP_')) {
                $humanizedKey = str_replace('HTTP_', '', $serverKey);
                $humanizedKey = Text::humanize($humanizedKey, '-');

                $httpRows[$humanizedKey] = $serverValue;
            }
        }

        return $httpRows;
    }

    public static function deserialize(string $headerLine): array
    {
        return array_map(
            static fn(string $value) => $value,
            explode(',', str_replace(';', ',', $headerLine))
        );
    }
}
