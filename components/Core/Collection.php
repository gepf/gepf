<?php

namespace Gepf\Core;

use ReturnTypeWillChange;

class Collection implements \Iterator, \Countable
{
    protected array $collection;
    private int $position = 0;


    public function __construct(array $collection = [])
    {
        $this->collection = $collection;
    }


    public function has(mixed $item): bool
    {
        return $this->search($item) !== null;
    }

    public function get(int $key)
    {
        return $this->collection[$key];
    }

    public function add(mixed $mixed): void
    {
        $this->collection[] = $mixed;
    }

    public function remove($mixed): void
    {
        $key = array_search($mixed, $this->collection, true);
        if ($key !== false) {
            unset($this->collection[$key]);
        }

        // re-index, to make sure that start position after rewind can be 0
        $this->collection = array_values($this->collection);
    }

    public function toArray(): array
    {
        return $this->collection;
    }

    #[ReturnTypeWillChange]
    public function current()
    {
        return $this->collection[$this->position];
    }

    public function next(): void
    {
        ++$this->position;
    }

    public function key(): int
    {
        return $this->position;
    }

    public function hasItems(): bool
    {
        return (bool)$this->collection;
    }

    public function count(): int
    {
        return \count($this->collection);
    }

    public function valid(): bool
    {
        return isset($this->collection[$this->position]);
    }

    public function rewind(): void
    {
        $this->position = 0;
    }

    public function search($value): ?int
    {
        $this->rewind();

        foreach ($this->collection as $n => $collectionItem) {
            if ($collectionItem === $value) {
                return $n;
            }
        }
        return null;
    }
}
