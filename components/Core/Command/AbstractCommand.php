<?php

namespace Gepf\Core\Command;

abstract class AbstractCommand
{
    abstract public function execute(): int;

    abstract public static function getName(): string;

    public function getDescription(): ?string
    {
        return null;
    }
}
