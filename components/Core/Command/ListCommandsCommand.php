<?php

namespace Gepf\Core\Command;

use Gepf\Core\FileSystem\Util\Directory;

class ListCommandsCommand extends AbstractCommand
{
    public function __construct(
        private readonly CommandManager $manager
    ) {}


    public static function getName(): string
    {
        return 'gepf:list-commands';
    }

    public function execute(): int
    {
        $cmdSrc = getSrcDir() . 'commands';

        if (file_exists($cmdSrc)) {
            foreach ($this->convertPathsToCommands(Directory::list($cmdSrc), $cmdSrc . '/') as $item) {
                print "$item\n";
            }
        }

        foreach ($this->manager->getCollectedCommands() as $command) {
            print $command::getName() . "\n";
        }

        return 0;
    }

    private function convertPathsToCommands(array $files, string $root): array
    {
        return array_map(static fn(string $path) => str_replace([$root, '.php', '/'], ['', '', ':'], $path), $files);
    }
}
