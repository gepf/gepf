<?php

namespace Gepf\Core\Command;

trait CommandManagerTrait
{
    public static function getInput(string $name): string
    {
        return trim(readline("\n> $name: "));
    }

    // Single lines as array items
    public static function getInputText(string $name, string $endCondition = ':q'): array
    {
        print "\n quit capture by entering $endCondition";

        do {
            $cmd = trim(readline("\n> $name: "));
            readline_add_history($cmd);
        } while ($cmd !== $endCondition);

        $text = readline_list_history();
        readline_clear_history();

        return $text;
    }

    public static function getOptionalArgument($n = 1): ?string
    {
        // +1, because 0 is 'do' and 1 is the command
        return $_SERVER['argv'][$n + 1] ?? null;
    }
}
