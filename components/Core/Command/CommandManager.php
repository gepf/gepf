<?php

namespace Gepf\Core\Command;

use Gepf\Core\DependencyInjection\Container\Container;
use Gepf\Core\DependencyInjection\KernelService;

class CommandManager extends KernelService
{
    private array $commands;

    use CommandManagerTrait;

    public function __construct()
    {
        // Set system commands
        $this->commands['gepf:list-commands'] = ListCommandsCommand::class;
    }


    public function register(string $commandClassName): void
    {
        if (!method_exists($commandClassName, 'getName')) {
            throw new \LogicException();
        }

        $this->commands[$commandClassName::getName()] = $commandClassName;
    }


    public function execute(string $path, Container $container): int
    {
        if (!isset($this->commands[$path])) {
            throw new CommandNotFoundException($path);
        }

        return $container->wire($this->commands[$path])->execute();
    }


    public function getCollectedCommands(): array
    {
        return $this->commands;
    }


    public function has(string $path): bool
    {
        return isset($this->commands[$path]);
    }
}
