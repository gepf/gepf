<?php

namespace Gepf\Core;

use Gepf\Core\Config\Config;
use Gepf\Core\DependencyInjection\KernelService;
use Gepf\Core\Negotiation\Request\Request;

class Locale extends KernelService
{
    private string $language;

    public function __construct(?string $language)
    {
        $this->language = $language ?? 'en';
    }

    public static function create(Request $request, Config $config): self
    {
        $language = $request->headers->getLanguage()->get() ?? $config->get('default_locale') ?? 'en';

        return new self($language);
    }

    public function getLanguage(): string
    {
        return $this->language;
    }
}
