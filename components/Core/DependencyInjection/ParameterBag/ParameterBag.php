<?php

namespace Gepf\Core\DependencyInjection\ParameterBag;

class ParameterBag
{
    protected array $parameter;

    public function __construct(array $data = [])
    {
        $this->parameter = $data;
    }

    public function get(string $index)
    {
        return $this->parameter[$index] ?? null;
    }

    public function getAll(): array
    {
        return $this->parameter;
    }
}
