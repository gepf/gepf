<?php

namespace Gepf\Core\DependencyInjection;

use Gepf\Core\Event\EventSubscription;
use Gepf\Service\Asset\Item\AssetCollection;
use Gepf\Service\ServiceConfig;

interface ConfigurableServiceInterface extends ServiceInterface
{
    public function setConfig(ServiceConfig $serviceConfig): void;

    public static function getCallbackNames(): array;

    public static function getAssets(): ?AssetCollection;

    /** @return EventSubscription[] */
    public static function getSubscribedEvents(): array;
}
