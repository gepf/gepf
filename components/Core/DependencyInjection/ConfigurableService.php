<?php

namespace Gepf\Core\DependencyInjection;

use Gepf\Core\Event\EventSubscription;
use Gepf\Service\Asset\Item\AssetCollection;
use Gepf\Service\ServiceConfig;

abstract class ConfigurableService extends AbstractService implements ConfigurableServiceInterface
{
    protected ServiceConfig $serviceConfig;

    public function getServiceConfig(): ServiceConfig
    {
        return $this->serviceConfig;
    }

    public function setConfig(ServiceConfig $serviceConfig): void
    {
        $this->serviceConfig = $serviceConfig;
    }

    public static function getCallbackNames(): array
    {
        return [];
    }

    public static function getAssets(): ?AssetCollection
    {
        return null;
    }

    /** @return EventSubscription[] */
    public static function getSubscribedEvents(): array
    {
        return [];
    }
}
