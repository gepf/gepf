<?php

namespace Gepf\Core\DependencyInjection\Container;

use Gepf\Core\Command\CommandManager;
use Gepf\Core\Config\Config;
use Gepf\Core\DependencyInjection\PriorityEnum;
use Gepf\Core\FileManager;
use Gepf\Core\Kernel\Task\KernelTask;
use Gepf\Core\Locale;
use Gepf\Core\Log\Logger;
use Gepf\Core\Negotiation\Request\Request;
use Gepf\Core\Negotiation\Response\Response;
use Gepf\Core\Registry\Registry;
use Gepf\Core\Routing\CommandRouter;
use Gepf\Core\Routing\FallbackRouter;
use Gepf\Core\Routing\IndexRouter\IndexRouter;
use Gepf\Core\Routing\RouterInterface;
use Gepf\Core\Session\Session;
use Gepf\Core\View\Plain;
use Gepf\Core\View\ViewCompilerInterface;
use Psr\Log\LoggerInterface;

/**
 * This will create a container with a minimum set of services that are
 * required by the kernel to run successfully with input and output.
 *
 * All these services can be found in the Gepf\Core namespace
 * Services from the Gepf\Service namespace however must be activated/
 * configured in config/services folder.
 *
 * Note that 'mandatory' means that they have to be registered, but not
 * necessarily loaded. For example, depending on PHP_SAPI the kernel
 * will either load IndexRouter or CommandRouter.
 * @see \Gepf\Core\DependencyInjection\KernelServiceInterface::canLoad
 */
class RegisterMandatoryServicesTask extends KernelTask
{
    public function __construct(private readonly Container $container) {}

    public function __invoke(): void
    {
        $this->container->registerItem(Response::class);
        $this->container->registerInterfaceItem(LoggerInterface::class, Logger::class);
        $this->container->registerInterfaceItem(RouterInterface::class, CommandRouter::class);
        $this->container->registerInterfaceItem(RouterInterface::class, IndexRouter::class);
        $this->container->registerInterfaceItem(RouterInterface::class, FallbackRouter::class, PriorityEnum::FALLBACK);
        $this->container->registerInterfaceItem(ViewCompilerInterface::class, Plain::class);

        $this->container->addInitializedService(Config::class, new Config());
        $this->container->addInitializedService(Request::class, Request::createFromGlobals());

        $this->container->registerItem(CommandManager::class);
        $this->container->registerItem(Registry::class);
        $this->container->registerItem(Session::class);
        $this->container->registerItem(Locale::class);
        $this->container->registerItem(FileManager::class);
    }
}
