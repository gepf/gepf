<?php

namespace Gepf\Core\DependencyInjection\Container\ContainerItem;

use Gepf\Core\DependencyInjection\Container\Container;
use Gepf\Core\DependencyInjection\PriorityEnum;
use Gepf\Service\ServiceConfigInterface;

class InterfaceContainerItem extends AbstractContainerItem
{
    private PriorityEnum $priority;

    public function __construct(
        private readonly string $interface,
        string $serviceName,
        Container $container,
        PriorityEnum $priority = PriorityEnum::ABSTAIN,
        bool $initialized = false,
        ?ServiceConfigInterface $config = null,
    ) {
        parent::__construct(
            serviceName: $serviceName,
            container: $container,
            initialized: $initialized,
            config: $config,
        );

        $this->priority = $priority;
    }

    public function getInterface(): string
    {
        return $this->interface;
    }

    public function getPriority(): PriorityEnum
    {
        return $this->priority;
    }

    public function setPriority(PriorityEnum $priority): void
    {
        $this->priority = $priority;
    }

    /**
     * @throws \ReflectionException
     */
    public function initialize(): void
    {
        $serviceClassName = $this->getServiceName();
        $this->setService($this->getContainer()->wire($serviceClassName));
    }
}
