<?php

namespace Gepf\Core\DependencyInjection\Container\ContainerItem;

use Gepf\Core\DependencyInjection\ConfigurableService;

class ContainerItem extends AbstractContainerItem
{
    /**
     * @throws \ReflectionException
     */
    public function initialize(): void
    {
        $serviceName = $this->getServiceName();

        if ($this->checkIfUnQualifiedKernelService()) {
            return;
        }

        $service = $this->getContainer()->wire($serviceName);
        if ($service instanceof ConfigurableService) {
            $service->setConfig($this->getConfig());
        }

        $this->setService($service);
    }
}
