<?php

namespace Gepf\Core\DependencyInjection\Container\ContainerItem;

use Gepf\Core\DependencyInjection\ConfigurableService;
use Gepf\Core\DependencyInjection\Container\Container;
use Gepf\Core\DependencyInjection\Container\ContainerException;
use Gepf\Core\DependencyInjection\KernelServiceInterface;
use Gepf\Core\DependencyInjection\PriorityEnum;
use Gepf\Core\DependencyInjection\ServiceInterface;
use Gepf\Service\ServiceConfigInterface;

abstract class AbstractContainerItem
{
    protected readonly ServiceInterface $service;
    private readonly \WeakReference $container;

    public function __construct(
        private readonly string $serviceName,
        Container $container,
        private bool $initialized = false,
        private readonly ?ServiceConfigInterface $config = null,
    ) {
        $this->container = \WeakReference::create($container);
    }

    public function getService(): ServiceInterface
    {
        if (!$this->isInitialized()) {
            $this->initialize();
        }

        if (!isset($this->service)) {
            throw new ContainerException('Initialization of service failed with unknown error ' . $this->getServiceName());
        }

        return $this->service;
    }

    public function setService(ServiceInterface $service): void
    {
        $this->service = $service;
        $this->setInitialized(true);
    }

    public function isInitialized(): bool
    {
        return $this->initialized;
    }

    public function getPriority(): PriorityEnum
    {
        return PriorityEnum::ABSTAIN;
    }

    public function canInitialize(): bool
    {
        if ($this->isInitialized()) {
            return true;
        }

        if (is_subclass_of($this->getServiceName(), ConfigurableService::class)) {
            return true;
        }

        if ($this->checkIfUnQualifiedKernelService()) {
            return false;
        }

        return true;
    }

    abstract public function initialize(): void;

    public function getServiceName(): string
    {
        return $this->serviceName;
    }

    public function setInitialized(bool $initialized): void
    {
        $this->initialized = $initialized;
    }

    protected function checkIfUnQualifiedKernelService(string $className = null): bool
    {
        // if container item is a package, an explicit item must be provided
        $className = $className ?? $this->getServiceName();

        if (!is_subclass_of($className, KernelServiceInterface::class)) {
            return false;
        }

        if ($className::canLoad($this->getContainer())) {
            return false;
        }

        return true;
    }

    public function getContainer(): Container
    {
        return $this->container->get();
    }

    public function getConfig(): ?ServiceConfigInterface
    {
        return $this->config;
    }
}
