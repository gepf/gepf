<?php

namespace Gepf\Core\DependencyInjection\Container\ContainerItem;

use Gepf\Core\DependencyInjection\PriorityEnum;

class ContainerPackage extends AbstractContainerItem
{
    /** @var InterfaceContainerItem[] */
    private array $items;

    public static function createFromItem(InterfaceContainerItem $item): self
    {
        $package = new self(
            serviceName: $item->getInterface(),
            container: $item->getContainer(),
        );

        $package->add($item);

        return $package;
    }

    public function add(InterfaceContainerItem $item): void
    {
        $this->items[$item->getServiceName()] = $item;
    }

    public function initialize(): void
    {
        if ($this->isInitialized()) {
            throw new \LogicException('Liable service for interface already found');
        }

        $fallback = null;
        $abstain = null;
        $should = null;
        $hit = false;
        foreach ($this->items as $item) {
            $className = $item->getServiceName();
            if ($this->checkIfUnQualifiedKernelService($className)) {
                continue;
            }

            if (!$item->canInitialize()) {
                continue;
            }

            if ($item->getPriority() === PriorityEnum::MUST) {
                $this->setService($item->getService());
                $hit = true;
                break;
            }

            if ($item->getPriority() === PriorityEnum::SHOULD) {
                $should = $item;
                continue;
            }

            if ($item->getPriority() === PriorityEnum::ABSTAIN) {
                $abstain = $item;
                continue;
            }

            if ($item->getPriority() === PriorityEnum::FALLBACK) {
                $fallback = $item;
            }

            // TODO log multiple services eligible for interface in current scenario
        }

        if (!$hit) {
            if (!$should && !$abstain && $fallback) {
                $this->setService($fallback->getService());
            }

            if (!$should && $abstain) {
                $this->setService($abstain->getService());
            }

            if ($should) {
                $this->setService($should->getService());
            }
        }

        if (!$this->isInitialized()) {
            throw new \LogicException('No service found for interface: ' . $this->getServiceName());
        }
    }

    public function canInitialize(): bool
    {
        foreach ($this->items as $item) {
            if ($item->canInitialize()) {
                return true;
            }
        }

        return false;
    }
}
