<?php /** @noinspection EfferentObjectCouplingInspection, PhpIncompatibleReturnTypeInspection */

namespace Gepf\Core\DependencyInjection\Container;

use Gepf\Core\Command\CommandManager;
use Gepf\Core\Config\Config;
use Gepf\Core\DependencyInjection\Container\ContainerItem\InterfaceContainerItem;
use Gepf\Core\Environment\Environment;
use Gepf\Core\Event\EventManager;
use Gepf\Core\FileManager;
use Gepf\Core\Locale;
use Gepf\Core\Negotiation\Request\Request;
use Gepf\Core\Negotiation\Response\Response;
use Gepf\Core\Registry\Data;
use Gepf\Core\Registry\Registry;
use Gepf\Core\Routing\RouterInterface;
use Gepf\Core\Runtime\Runtime;
use Gepf\Core\Session\Session;
use Gepf\Core\View\ViewCompilerInterface;
use Gepf\Service\Profiler\ProfilerInterface;
use Gepf\Service\Security\Security;
use Gepf\Service\Sql\SqlManager;
use Psr\Log\LoggerInterface;

trait ContainerTrait
{
    public function getEnvironment(): Environment
    {
        return $this->get(Environment::class);
    }

    public function getRuntime(): Runtime
    {
        return $this->get(Runtime::class);
    }

    public function getConfig(): Config
    {
        return $this->get(Config::class);
    }

    public function getEventManager(): EventManager
    {
        return $this->get(EventManager::class);
    }

    public function getCommandManager(): CommandManager
    {
        return $this->get(CommandManager::class);
    }

    public function getData(): Data
    {
        /** @noinspection PhpPossiblePolymorphicInvocationInspection */
        return $this->get(Registry::class)->getData();
    }

    public function getRegistry(): Registry
    {
        return $this->get(Registry::class);
    }

    public function getRequest(): Request
    {
        return $this->get(Request::class);
    }

    public function getResponse(): Response
    {
        return $this->get(Response::class);
    }

    public function getSession(): Session
    {
        return $this->get(Session::class);
    }

    public function getLogger(): LoggerInterface
    {
        return $this->get(LoggerInterface::class);
    }

    public function getRouter(): RouterInterface
    {
        return $this->get(RouterInterface::class);
    }

    public function getLocale(): Locale
    {
        return $this->get(Locale::class);
    }

    public function getFileManager(): FileManager
    {
        return $this->get(FileManager::class);
    }

    public function getViewCompiler(): ViewCompilerInterface
    {
        return $this->get(ViewCompilerInterface::class);
    }

    public function setViewCompiler(ViewCompilerInterface $viewCompiler): void
    {
        $containerItem = new InterfaceContainerItem(
            interface: ViewCompilerInterface::class,
            serviceName: $viewCompiler::class,
            container: $this,
        );
        $containerItem->setService($viewCompiler);
        $this->register($containerItem);
    }

    // Getter for optional services (for convenience)

    public function getProfiler(): ProfilerInterface
    {
        $profiler = $this->get(ProfilerInterface::class);
        if (!$profiler instanceof ProfilerInterface) {
            throw new \LogicException();
        }

        return $profiler;
    }

    public function getQueryBuilder(): SqlManager
    {
        $sql = $this->get(SqlManager::class);
        if (!$sql instanceof SqlManager) {
            throw new \LogicException();
        }

        return $sql;
    }

    public function getSecurity(): Security
    {
        $security = $this->get(Security::class);
        if (!$security instanceof Security) {
            throw new \LogicException();
        }

        return $security;
    }
}
