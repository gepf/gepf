<?php

namespace Gepf\Core\DependencyInjection\Container;

use Gepf\Core\DependencyInjection\ServiceInterface;
use Psr\Container\NotFoundExceptionInterface;

class NotFoundException extends \InvalidArgumentException implements NotFoundExceptionInterface
{
    public function __construct(string $className, int $code = 0, ?\Throwable $previous = null)
    {
        $message = "Service '$className' could not be found in service container. " . $this->furtherReasoning($className);

        parent::__construct($message, $code, $previous);
    }

    private function furtherReasoning(string $className): string
    {
        if (is_subclass_of($className, ServiceInterface::class)) {
            return 'Class exists and is eligible for autoloader though. It also implements ServiceInterface.';
        }

        if (class_exists($className)) {
            return 'Class exists and is eligible for autoloader though.';
        }

        return 'Class cannot be detected by autoloader.';
    }
}
