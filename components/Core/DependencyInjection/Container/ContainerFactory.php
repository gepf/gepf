<?php

namespace Gepf\Core\DependencyInjection\Container;

use Gepf\Core\Environment\Environment;
use Gepf\Core\Event\EventManager;

class ContainerFactory
{
    public static function create(Environment $environment): Container
    {
        $container = new Container();

        // These two services are necessary for kernel and task worker to run
        $container->addInitializedService(Environment::class, $environment);
        $container->addInitializedService(EventManager::class, new EventManager());

        return $container;
    }
}
