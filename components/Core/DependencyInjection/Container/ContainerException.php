<?php

namespace Gepf\Core\DependencyInjection\Container;

use Psr\Container\ContainerExceptionInterface;

class ContainerException extends \RuntimeException implements ContainerExceptionInterface
{

}