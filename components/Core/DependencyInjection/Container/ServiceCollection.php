<?php

namespace Gepf\Core\DependencyInjection\Container;

use Gepf\Core\AssociativeCollection;
use Gepf\Core\DependencyInjection\Container\ContainerItem\AbstractContainerItem;

class ServiceCollection extends AssociativeCollection
{
    public function current(): AbstractContainerItem
    {
        return parent::current();
    }
}
