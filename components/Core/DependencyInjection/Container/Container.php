<?php

namespace Gepf\Core\DependencyInjection\Container;

use Gepf\Core\DependencyInjection\Container\ContainerItem\AbstractContainerItem;
use Gepf\Core\DependencyInjection\Container\ContainerItem\ContainerItem;
use Gepf\Core\DependencyInjection\Container\ContainerItem\ContainerPackage;
use Gepf\Core\DependencyInjection\Container\ContainerItem\InterfaceContainerItem;
use Gepf\Core\DependencyInjection\PriorityEnum;
use Gepf\Core\DependencyInjection\ServiceInterface;
use Psr\Container\ContainerInterface;

class Container implements ContainerInterface
{
    use ContainerTrait;

    private ServiceCollection $services;


    public function __construct()
    {
        $this->services = new ServiceCollection();
    }


    public function registerItem(string $serviceName): void
    {
        $this->register(new ContainerItem(serviceName: $serviceName, container: $this));
    }


    public function registerInterfaceItem(
        string $interface,
        string $serviceName,
        PriorityEnum $priority = PriorityEnum::ABSTAIN,
    ): void {
        $this->register(
            new InterfaceContainerItem(
                interface: $interface,
                serviceName: $serviceName,
                container: $this,
                priority: $priority,
            ),
        );
    }


    public function register(AbstractContainerItem $containerItem): void
    {

        $this->services->set($containerItem->getServiceName(), $containerItem);

        if ($containerItem instanceof InterfaceContainerItem) {
            $name = $containerItem->getInterface();
            if ($this->has($name)) {
                $package = $this->getItem($name);
                if (!$package instanceof ContainerPackage) {
                    throw new \LogicException(
                        'Service with same name of an interface '
                        . ' detected that is not a Container Package: ' . $name,
                    );
                }

                $package->add($containerItem);
                return;
            }

            $package = ContainerPackage::createFromItem($containerItem);
            $this->services->set($package->getServiceName(), $package);
        }
    }


    public function addInitializedService(string $serviceName, ServiceInterface $service): void
    {
        $registryItem = new ContainerItem(serviceName: $serviceName, container: $this, initialized: true);
        $registryItem->setService($service);

        $this->services->set($serviceName, $registryItem);
    }


    public function getItem(string $serviceName): AbstractContainerItem
    {
        if (!$this->services->hasKey($serviceName)) {
            throw new NotFoundException($serviceName);
        }

        return $this->services->get($serviceName);
    }


    public function get(string $id): ServiceInterface
    {
        return $this->getItem($id)->getService();
    }


    public function getOptional(string $serviceName): ?ServiceInterface
    {
        return $this->services->hasKey($serviceName) ? $this->services->get($serviceName) : null;
    }


    public function isInitialized(string $serviceName): bool
    {
        return $this->getItem($serviceName)->isInitialized();
    }


    public function canInitialize(string $serviceName): bool
    {
        return $this->getItem($serviceName)->canInitialize();
    }


    public function has(string $id): bool
    {
        return $this->services->hasKey($id);
    }


    public function getServices(): ServiceCollection
    {
        $this->services->rewind();
        return $this->services;
    }


    public function wire(string $className): object
    {
        $class = new \ReflectionClass($className);
        $constructor = $class->getConstructor();

        $services = [];
        if ($constructor) {
            foreach ($constructor->getParameters() as $parameter) {
                if ($parameter->getName() === 'container') {
                    $services[] = $this;
                    continue;
                }

                if ($parameter->allowsNull()) {
                    $services[] = $this->getOptional($parameter->getType()?->getName());
                    continue;
                }

                $services[] = $this->get($parameter->getType()?->getName());
            }
        }

        return new $className(...$services);
    }
}
