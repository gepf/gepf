<?php

namespace Gepf\Core\DependencyInjection;

abstract class AbstractService
{
    /** @return string[] */
    public static function getCommands(): array
    {
        return [];
    }
}
