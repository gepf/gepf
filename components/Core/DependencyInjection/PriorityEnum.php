<?php

namespace Gepf\Core\DependencyInjection;

enum PriorityEnum
{
    case FALLBACK;
    case ABSTAIN;
    case SHOULD;
    case MUST;
}
