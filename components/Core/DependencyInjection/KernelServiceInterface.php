<?php

namespace Gepf\Core\DependencyInjection;

use Gepf\Core\DependencyInjection\Container\Container;

interface KernelServiceInterface extends ServiceInterface
{
    public static function canLoad(Container $container): bool;
}
