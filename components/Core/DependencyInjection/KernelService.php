<?php

namespace Gepf\Core\DependencyInjection;

use Gepf\Core\DependencyInjection\Container\Container;

abstract class KernelService extends AbstractService implements KernelServiceInterface
{
    public static function canLoad(Container $container): bool
    {
        return true;
    }
}
