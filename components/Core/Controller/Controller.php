<?php

namespace Gepf\Core\Controller;

use Gepf\Core\DependencyInjection\Container\Container;
use Gepf\Core\DependencyInjection\ServiceInterface;

class Controller
{
    use ControllerTrait;

    private Container $container;


    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * @throws \ReflectionException
     */
    public function run(string $file): void
    {
        require_once $file;

        $declaredClasses = get_declared_classes();
        $lastClass = end($declaredClasses);

        if (is_subclass_of($lastClass, InvokableController::class)) {
            $reflection = new \ReflectionMethod($lastClass, '__invoke');

            $services = [];
            foreach ($reflection->getParameters() as $parameter) {
                $services[] = $this->container->get($parameter->getType()?->getName());
            }

            $controller = new $lastClass(...$services);
            $controller();
        }
    }

    public function redirect(string $path): void
    {
        $this->container->getResponse()->setRedirect($path);
    }

    public function get($index): ServiceInterface
    {
        return $this->container->get($index);
    }
}
