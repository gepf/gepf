<?php

namespace Gepf\Core\Controller\Exception;

class ControllerException extends \Exception
{
    private int $statusCode;


    public function __construct(int $statusCode, string $message = null, \Throwable $previous = null, int $code = 0)
    {
        $this->statusCode = $statusCode;

        parent::__construct($message, $code, $previous);
    }


    public function getStatusCode(): int
    {
        return $this->statusCode;
    }
}
