<?php

namespace Gepf\Core\Controller;

interface InvokableController
{
    public function __invoke();
}
