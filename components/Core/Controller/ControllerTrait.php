<?php /** @noinspection PhpIncompatibleReturnTypeInspection */

namespace Gepf\Core\Controller;

use Gepf\Core\Config\Config;
use Gepf\Core\Environment\Environment;
use Gepf\Core\Log\Logger;
use Gepf\Core\Negotiation\Request\Request;
use Gepf\Core\Negotiation\Response\Response;
use Gepf\Core\Registry\Data;
use Gepf\Core\Registry\Registry;
use Gepf\Core\Runtime\Runtime;
use Gepf\Core\Session\Session;
use Gepf\Service\Editorial;
use Gepf\Service\RestOrm\RestOrm;
use Gepf\Service\Security\Security;

trait ControllerTrait
{
    public function getEnvironment(): Environment
    {
        return $this->container->get(Environment::class);
    }

    public function getRuntimeManager(): Runtime
    {
        return $this->container->get(Runtime::class);
    }

    public function getConfig(): Config
    {
        return $this->container->get(Config::class);
    }

    public function getRegistry(): Registry
    {
        return $this->container->get(Registry::class);
    }

    public function getData(): Data
    {
        return $this->getRegistry()->getData();
    }

    public function getRequest(): Request
    {
        return $this->container->get(Request::class);
    }

    public function getResponse(): Response
    {
        return $this->container->get(Response::class);
    }

    public function getSession(): Session
    {
        return $this->container->get(Session::class);
    }

    public function getSecurity(): Security
    {
        return $this->container->get(Security::class);
    }

    public function getEditorial(): Editorial
    {
        return $this->container->get(Editorial::class);
    }

    public function getLogger(): Logger
    {
        return $this->container->get(Logger::class);
    }


    // Optional services

    public function getOrm(): RestOrm
    {
        return $this->container->get(RestOrm::class);
    }
}
