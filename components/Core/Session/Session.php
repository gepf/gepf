<?php

namespace Gepf\Core\Session;

use Gepf\Core\DependencyInjection\KernelService;
use Gepf\Core\DependencyInjection\ParameterBag\ParameterBag;

/**
 * @see \Gepf\Core\Kernel\Task\Boilerplate\Boilerplate::startSession
 */
class Session extends KernelService
{
    public const TOKEN_KEY = '_csrf_token';
    public const QUEUE_KEY = 'queue';
    public const REFERER_KEY = 'referer';

    private string $id;

    /** CSRF token */
    private string $token;

    /** A bag for $_SERVER */
    private ParameterBag $onRequestParameters;
    private array $queuedParams; // = [];
    private ?string $referer;

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function setToken(string $token): void
    {
        $this->token = $token;
    }

    public function getOnRequestParameters(): ParameterBag
    {
        return $this->onRequestParameters;
    }

    public function setOnRequestParameters(ParameterBag $onRequestParameters): void
    {
        $this->onRequestParameters = $onRequestParameters;
    }

    public function getQueuedParams(): array
    {
        return $this->queuedParams;
    }

    /** @see \Gepf\Core\Kernel\Task\Boilerplate\Boilerplate::startSession */
    final public function setQueuedParams(array $params): void
    {
        $this->queuedParams = $params;
    }

    public function addQueuedParam(string $scope, $param): void
    {
        $this->queuedParams[$scope][] = $param;
    }

    public function flushQueuedParams(): array
    {
        $cache = $this->queuedParams;
        $this->queuedParams = [];

        return $cache;
    }

    public function getReferer(): ?string
    {
        return $this->referer;
    }

    public function setReferer(?string $referer): void
    {
        // This can happen, because paths are usually trimmed
        if ($referer === '') {
            $referer = '/';
        }

        $this->referer = $referer;
    }
}
