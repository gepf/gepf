<?php

namespace Gepf\Core;

abstract class Enum
{
    protected string $value;


    public function __construct(string $value)
    {
        if (!self::isValid($value)) {
            throw new \UnexpectedValueException("Value '$value' is not part of the enum " . static::class);
        }

        $this->value = $value;
    }


    abstract public static function getAllowedValues(): array;


    public static function isValid(string $value): bool
    {
        return in_array($value, static::getAllowedValues(), true);
    }


    public function getValue(): string
    {
        return $this->value;
    }
}
