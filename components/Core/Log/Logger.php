<?php

namespace Gepf\Core\Log;

use Gepf\Core\DependencyInjection\KernelService;
use Gepf\Core\FileSystem\Util\Directory;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;

class Logger extends KernelService implements LoggerInterface
{
    public const EXCEPTION_KEYWORD = 'exception';

    public const LOG_IN_PRODUCTION = [
        LogLevel::EMERGENCY,
        LogLevel::ALERT,
        LogLevel::CRITICAL,
        LogLevel::ERROR,
        LogLevel::WARNING,
    ];


    /** @var LogEntry[] */
    private array $logs;


    public function emergency(string|\Stringable $message, array $context = []): void
    {
        $this->add(LogLevel::EMERGENCY, $message, $context);
    }


    public function alert(string|\Stringable $message, array $context = []): void
    {
        $this->add(LogLevel::ALERT, $message, $context);
    }


    public function critical(string|\Stringable $message, array $context = []): void
    {
        $this->add(LogLevel::CRITICAL, $message, $context);
    }


    public function error(string|\Stringable $message, array $context = []): void
    {
        $this->add(LogLevel::ERROR, $message, $context);
    }


    public function warning(string|\Stringable $message, array $context = []): void
    {
        $this->add(LogLevel::WARNING, $message, $context);
    }


    public function notice(string|\Stringable $message, array $context = []): void
    {
        $this->add(LogLevel::NOTICE, $message, $context);
    }


    public function info(string|\Stringable $message, array $context = []): void
    {
        $this->add(LogLevel::INFO, $message, $context);
    }


    public function debug(string|\Stringable $message, array $context = []): void
    {
        $this->add(LogLevel::DEBUG, $message, $context);
    }


    public function log(mixed $level, string|\Stringable $message, array $context = []): void
    {
        $this->add($level, $message, $context);
    }


    private function add(mixed $level, string $message, array $context): void
    {
        $exception = $context[self::EXCEPTION_KEYWORD] ?? null;

        if (isProduction() && \in_array($level, self::LOG_IN_PRODUCTION, true)) {
            $this->writeToFile($level, $message, $exception);
        }

        if (isDevelopment()) {
            $this->writeToFile($level, $message, $exception);
        }

        $this->logs[] = new LogEntry($message, $context, $level);
    }


    public function getAll(): array
    {
        return $this->logs ?? [];
    }


    public function getLevel(string $level): array
    {
        return array_filter($this->getAll(), static function (LogEntry $logEntry) use ($level) {
            return $level === $logEntry->getLevel();
        });
    }


    private function writeToFile(mixed $level, string $message, ?\Exception $exception = null): void
    {
        Directory::ensureForFile('var/log/log.log');
        $file = fopen('var/log/log.log', 'ab');

        $logLine = sprintf(
            "[%s] [%s] %s\n",
            strtoupper(str_pad($level, 9)),
            (new \DateTimeImmutable())->format('Y-m-d H:i:s'),
            $message,
        );

        fwrite($file, $logLine);

        if ($exception) {
            fwrite($file, str_pad('    -->', 12) . $exception->getTraceAsString() . "\n");
        }

        fclose($file);
    }
}
