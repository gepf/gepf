<?php

namespace Gepf\Core\Log;

use Psr\Log\LogLevel;

class LogEntry
{
    private string $message;
    private array $context;
    private mixed $level;


    public function __construct(string $message, array $context = [], mixed $level = LogLevel::INFO)
    {
        $this->message = $message;
        $this->context = $context;
        $this->level = $level;
    }

    public function __toString(): string
    {
        return "$this->level: $this->message";
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function getContext(): array
    {
        return $this->context;
    }

    public function getLevel(): mixed
    {
        return $this->level;
    }
}
