<?php

namespace Gepf\Core\View;

use Gepf\Core\Config\Config;
use Gepf\Core\Config\ConfigException;
use Gepf\Core\DependencyInjection\Container\Container;
use Gepf\Core\DependencyInjection\Container\ContainerItem\InterfaceContainerItem;
use Gepf\Core\DependencyInjection\PriorityEnum;
use Gepf\Core\Exception\DependencyException;
use Gepf\Core\Kernel\Task\KernelTask;
use Psr\Log\LoggerInterface;

class DetermineCompilerTask extends KernelTask
{
    public function __construct(
        private readonly LoggerInterface $logger,
        private readonly Config $config,
        private readonly Container $container,
    ) {}

    public function __invoke(): void
    {
        if ($this->container->isInitialized(ViewCompilerInterface::class)) {
            return;
        }

        $request = $this->container->getRequest();
        $response = $this->container->getResponse();
        $contentType = $request->headers->getAccept()->get();

        $compilers = $this->config->getBodyCompilers();
        $compilerClass = $compilers[$contentType] ?? null;
        if (!$compilerClass) {
            throw new ConfigException("No compiler found, for: $contentType");
        }

        if (!$this->container->has($compilerClass) && !$response->isStatusOk()) {
            $this->logger->debug('Error occurred early, using fallback ViewCompiler');
            $response->setBody(ViewCompilerUtil::getFallbackErrorBody($response->getStatus()));
            $compilerClass = Plain::class;
        }

        if (!$this->container->has($compilerClass)) {
            throw new ConfigException(
                "Configured ViewCompiler $compilerClass is missing, - " .
                'you might have forgotten to explicitly add it in /config/services',
            );
        }

        $compiler = $this->container->get($compilerClass);

        if (!$compiler instanceof ViewCompilerInterface) {
            throw new DependencyException(
                $compiler::class,
                DependencyException::TYPE_CLASS,
                'Configured compiler does not implement' . ViewCompilerInterface::class,
            );
        }

        $item = $this->container->getItem($compilerClass);

        if (!$item instanceof InterfaceContainerItem) {
            throw new DependencyException(
                $item::class,
                DependencyException::TYPE_CLASS,
                'Configured service item does not implement' . InterfaceContainerItem::class,
            );
        }

        $item->setPriority(PriorityEnum::MUST);
    }
}
