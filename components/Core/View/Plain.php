<?php

namespace Gepf\Core\View;

use Gepf\Core\Registry\Registry;
use Gepf\Core\Routing\ViewTemplateFile;

class Plain implements ViewCompilerInterface
{
    public function compile(Registry $registry, ?ViewTemplateFile $view): string
    {
        return $registry->getResponse()->getBody() ?? 'No content';
    }

    public function requiresTemplate(): bool
    {
        return false;
    }
}
