<?php

namespace Gepf\Core\View;

use Gepf\Core\Negotiation\Response\ResponseStatus;
use Gepf\Service\Debug\Debug;

class ViewCompilerUtil
{
    // 'Pure' exception is important for external view compilers (Twig/Smarty),
    // or when exception happens very early
    public static function getFallbackErrorBody(ViewCompilerException|\Exception|ResponseStatus $e): string
    {
        $title = $e instanceof ResponseStatus
            ? "{$e->getCode()} {$e->getMessage()}"
            : '500 Internal Server Error';

        $body = '<!DOCTYPE html>' . PHP_EOL;
        $body .= '<html lang="en"><head><title>' . $title . '</title></head><body>';
        $body .= '<h1>' . $title . '</h1>';

        if (isErrorReporting()) {
            $body .= $e instanceof ResponseStatus ? "<p>{$e->getReason()}</p>" : "<p>{$e->getMessage()}</p>";
            $body .= "<p>in {$e->getFile()}:{$e->getLine()}</p>";
            $body .= '<br /><hr /><h2>Backtrace</h2>';
            $body .= (new Debug())->dump($e->getTrace());
        }

        $body .= '</body></html>';

        return $body;
    }
}
