<?php

namespace Gepf\Core\View;

use Gepf\Core\Registry\Registry;
use Gepf\Core\Routing\ViewTemplateFile;
use Gepf\Core\Util\Property;

class XmlViewCompiler implements ViewCompilerInterface
{
    public function compile(Registry $registry, ?ViewTemplateFile $view): string
    {
        $out = "<?xml version=\"1.0\"?>\n<values>";
        $this->renderData($registry->getData()->getRegister(), $out);
        $out .= '</values>';

        return $out;
    }

    public function requiresTemplate(): bool
    {
        return false;
    }


    private function renderData(array|object $data, string &$out): void
    {
        if (\is_object($data)) {
            $serializedData = [];
            foreach (Property::getPropertiesOfClass($data) as $propertyName) {
                $serializedData[$propertyName] = Property::access($data, $propertyName);
            }
            $data = $serializedData;
        }

        foreach ($data as $key => $value) {
            $key = is_numeric($key) ? \gettype($value) : $key;
            $out .= "<$key>";
            $this->parseValue($value, $out);
            $out .= "</$key>";
        }
    }


    private function parseValue(mixed $value, string &$out): void
    {
        if (is_scalar($value)) {
            $out .= $value;
            return;
        }

        if ($value === null) {
            return;
        }

        $this->renderData($value, $out);
    }
}
