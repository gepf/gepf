<?php

namespace Gepf\Core\View;

use Gepf\Core\FileSystem\File;
use Gepf\Core\FileSystem\VirtualFile;

abstract class AbstractTemplate
{
    public function __construct(private readonly File|VirtualFile $file) {}

    public function getFile(): File|VirtualFile
    {
        return $this->file;
    }

    public function getUniqueId(): string
    {
        return $this->file->getId();
    }
}
