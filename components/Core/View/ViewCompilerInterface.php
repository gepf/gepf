<?php

namespace Gepf\Core\View;

use Gepf\Core\DependencyInjection\ServiceInterface;
use Gepf\Core\Registry\Registry;
use Gepf\Core\Routing\ViewTemplateFile;

interface ViewCompilerInterface extends ServiceInterface
{
    /** @throws ViewCompilerException */
    public function compile(Registry $registry, ?ViewTemplateFile $view): string;

    public function requiresTemplate(): bool;
}
