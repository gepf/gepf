<?php

namespace Gepf\Core\Runtime;

use Gepf\Core\DependencyInjection\ReadOnlyServiceInterface;
use Gepf\Core\Runtime\App\AbstractApp;
use Gepf\Core\Runtime\App\SingleApp;

readonly class Runtime implements ReadOnlyServiceInterface
{
    public const MODE_APP = 1;
    public const MODE_BUNDLE = 2;
    public const MODE_VENDOR = 3;
    public const SERVER_API_CONSOLE = 'cli';

    public function __construct(
        private array $apps,
        public ?string $resolvedPath,
        private AbstractApp $app,
    ) {}

    public function getApp(): AbstractApp
    {
        return $this->app;
    }

    /**
     * Resolved path does not include app name
     */
    public function getResolvedPath(): ?string
    {
        return $this->resolvedPath;
    }

    public static function isCommandLine(): bool
    {
        return self::SERVER_API_CONSOLE === PHP_SAPI;
    }

    public static function isHttp(): bool
    {
        return self::SERVER_API_CONSOLE !== PHP_SAPI;
    }

    public function getMode(): ?int
    {
        return $this->getApp()->getType();
    }

    /** @return AbstractApp[] */
    public function getApps(): array
    {
        return $this->apps;
    }

    public function getSingleApp(): ?SingleApp
    {
        return $this->apps[basename(getcwd())] ?? null;
    }
}
