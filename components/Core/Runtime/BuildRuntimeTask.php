<?php

namespace Gepf\Core\Runtime;

use Gepf\Core\Config\Config;
use Gepf\Core\DependencyInjection\Container\Container;
use Gepf\Core\Kernel\Task\KernelTask;
use Gepf\Core\Negotiation\Request\Request;

class BuildRuntimeTask extends KernelTask
{
    public function __construct(
        private readonly Config $config,
        private readonly Request $request,
        private readonly Container $container,
    ) {}

    public function __invoke(): void
    {
        $runtime = (new RuntimeFactory())->createRuntime($this->config, $this->request);
        $this->container->addInitializedService(Runtime::class, $runtime);
    }
}
