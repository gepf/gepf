<?php

namespace Gepf\Core\Runtime;

use Gepf\Core\Config\Config;
use Gepf\Core\Config\ConfigException;
use Gepf\Core\FileSystem\Util\Directory;
use Gepf\Core\Negotiation\Request\Request;
use Gepf\Core\Runtime\App\AbstractApp;
use Gepf\Core\Runtime\App\Bundle;
use Gepf\Core\Runtime\App\SingleApp;
use Gepf\Core\Runtime\App\SourcedApp;
use Gepf\Core\StaticPaths;
use Gepf\Core\Util\Text;

class RuntimeFactory
{
    public function createRuntime(Config $config, Request $request): Runtime
    {
        $apps = $this->detectApps();

        $contextStart = Text::first('/', $request->getPath());
        $app = $this->getApp($contextStart, $config->getDefaultApp(), $apps);
        $resolvedPath = $this->resolvePath($request->getPath(), $app->getName());

        return new Runtime($apps, $resolvedPath, $app);
    }


    /** @return AbstractApp[] */
    private function detectApps(): array
    {
        $apps = [];

        $this->appendSourcedAppToApps($apps, 'apps');
        $this->appendSourcedAppToApps($apps, 'commands');
        $this->appendSourcedAppToApps($apps, 'routes');
        $this->appendSourcedAppToApps($apps, 'templates');

        if (is_dir('bundles')) {
            foreach (Directory::listDirs('bundles') as $bundle) {
                $apps[$bundle] = new Bundle($bundle);
            }
        }

        if (isVendor()) {
            $singleAppName = basename(getcwd());
            $apps[$singleAppName] = new SingleApp($singleAppName);
        }

        return $apps;
    }

    private function appendSourcedAppToApps(array &$apps, string $dir): void
    {
        if (is_dir($dir)) {
            foreach (Directory::listDirs($dir) as $app) {
                $name = strtolower($app);
                $apps[$name] = new SourcedApp($name);
            }
        }
    }

    /**
     * Resolves the name of the app being used
     *
     * It will use the first atom of context (eg 'foo' of 'foo/bar/http' or 'foo:bar:command') to search if
     * a folder of same name exists in bundle, route, template folder, etc. If nothing is found, it will
     * return the default app name.
     */
    private function getApp($contextStart, ?string $defaultApp, $apps): AbstractApp
    {
        if ($contextStart && is_dir(StaticPaths::BUNDLE_FOLDER . '/' . $contextStart)) {
            return $apps[$contextStart];
        }

        if (isVendor()) {
            return $apps[basename(getcwd())];
        }

        if ($contextStart && is_dir(StaticPaths::ROUTE_FOLDER . '/' . $contextStart)) {
            return $apps[$contextStart];
        }

        // search templates only when protocol is http
        if ($contextStart
            && Runtime::isHttp()
            && is_dir(StaticPaths::TEMPLATE_FOLDER . '/' . $contextStart)
        ) {
            return $apps[$contextStart];
        }

        if (!$defaultApp) {
            throw new ConfigException('No default app configured, required in non-vendor mode');
        }

        return $apps[$defaultApp];
    }

    private function resolvePath(?string $path, ?string $app): ?string
    {
        if ($app === Text::get(0, '/', $path)) {
            return Text::shift('/', $path);
        }

        return $path;
    }
}
