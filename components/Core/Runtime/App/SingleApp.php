<?php

namespace Gepf\Core\Runtime\App;

/*
 * A 'single app' is an app that uses gepf as a vendor, like
 * - app
 *     + Entity
 *     + ...
 * - routes
 *     + start
 *     + ...
 * - vendor
 *     + gepf
 */

use Gepf\Core\Runtime\Runtime;

readonly class SingleApp extends AbstractApp
{
    public function getType(): int
    {
        return Runtime::MODE_VENDOR;
    }
}
