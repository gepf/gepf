<?php

namespace Gepf\Core\Runtime\App;

readonly abstract class AbstractApp
{
    public function __construct(private string $name) {}


    public function getName(): string
    {
        return $this->name;
    }

    abstract public function getType(): int;
}
