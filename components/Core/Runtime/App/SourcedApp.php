<?php

namespace Gepf\Core\Runtime\App;

/*
 * A 'sourced app' is an app that is located directly in the gepf tree, like:
 * - apps/
 *     + MyApp/
 * - routes/
 *     + my-app/
 * - templates/
 *     + my-app/
 */

use Gepf\Core\Runtime\Runtime;

readonly class SourcedApp extends AbstractApp
{
    public function getType(): int
    {
        return Runtime::MODE_APP;
    }
}
