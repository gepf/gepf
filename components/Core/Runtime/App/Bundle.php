<?php

namespace Gepf\Core\Runtime\App;

use Gepf\Core\Runtime\Runtime;

readonly class Bundle extends AbstractApp
{
    public function getType(): int
    {
        return Runtime::MODE_BUNDLE;
    }
}
