<?php

namespace Gepf\Core\Routing;

use Gepf\Core\DependencyInjection\Container\Container;
use Gepf\Core\DependencyInjection\KernelService;

class FallbackRouter extends KernelService implements RouterInterface
{
    private int $statusCode = 200;

    public function getRoute(): Route
    {
        return new Route(
            path: "/$this->statusCode",
        );
    }

    public function createRoute(): void
    {
        // I am the fallback router. I return the same route all the time
    }

    public function setExceptionRoute(int $statusCode): void
    {
        $this->statusCode = $statusCode;
    }

    public function run(Container $container): void
    {
        // Nothing to do here
    }
}
