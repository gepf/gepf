<?php

namespace Gepf\Core\Routing\IndexRouter;

use Gepf\Core\Controller\Controller;
use Gepf\Core\DependencyInjection\Container\Container;
use Gepf\Core\Routing\AbstractRouter;
use Gepf\Core\Routing\Exception\RouteNotFoundException;
use Gepf\Core\Routing\Exception\RouterRuntimeException;
use Gepf\Core\Routing\Route;
use Gepf\Core\Routing\ViewTemplateFile;
use Gepf\Core\Runtime\Runtime;

class IndexRouter extends AbstractRouter
{
    use IndexRouterTrait;

    private readonly ?array $table;

    public static function canLoad(Container $container): bool
    {
        if (!$container->has(Runtime::class)) {
            return false;
        }

        return Runtime::isHttp();
    }

    public function createRoute(): void
    {
        $this->setLogicRoot($this->resolveLogicRoot());
        $this->setTemplateRoot($this->resolveTemplateRoot());

        $path = $this->runtime->getResolvedPath();
        $endpoint = $this->resolveEndpoint($path);
        $compilerTemplate = $this->resolveCompilerTemplatePath($path);

        if (!$endpoint && !$compilerTemplate) {
            throw new RouteNotFoundException($path ?? '/');
        }

        $indexes = $this->findIndexes($path, $endpoint);

        $this->route = new Route(
            path: $path,
            endpoint: $endpoint,
            indexes: $indexes,
            compilerTemplate: $compilerTemplate
        );
    }

    private function resolveEndpoint(?string $path): ?string
    {
        $searchPath = $path
            ? $this->getLogicRoot() . '/' . $path
            : $this->getLogicRoot();

        return $this->findFileForPath($searchPath, 'php');
    }

    private function resolveCompilerTemplatePath(?string $path): ?ViewTemplateFile
    {
        $searchPath = $path
            ? $this->getTemplateRoot() . '/' . $path
            : $this->getTemplateRoot();

        $compilerTemplatePath = $this->findFileForPath($searchPath, $this->getTemplateExtension());

        return $compilerTemplatePath
            ? new ViewTemplateFile($compilerTemplatePath, $this->getTemplateRoot())
            : null;
    }

    public function run(Container $container): void
    {
        $controller = new Controller($container);

        foreach ($this->getRoute()->getIndexes() as $index) {
            if (!file_exists($index)) {
                throw new RouterRuntimeException("Index not found: $index");
            }
            $controller->run($index);
        }

        $endpoint = $this->getRoute()->getEndpoint();
        if ($endpoint && $endpoint !== 'index.php') {
            $controller->run($endpoint);
        }
    }


    public function setExceptionRoute(int $statusCode): void
    {
        $exceptionRouter = new ExceptionRouter($this->config, $this->runtime);
        $this->route = $exceptionRouter->createExceptionRoute($statusCode);
    }
}
