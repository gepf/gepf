<?php

namespace Gepf\Core\Routing\IndexRouter;

use Gepf\Core\Config\Config;
use Gepf\Core\Negotiation\Response\Response;
use Gepf\Core\Routing\Route;
use Gepf\Core\Routing\ViewTemplateFile;
use Gepf\Core\Runtime\Runtime;
use Gepf\Lite\Engine;

class ExceptionRouter
{
    use IndexRouterTrait;

    public function __construct(private readonly Config $config, private readonly Runtime $runtime) {}

    public function createExceptionRoute(int $statusCode): Route
    {
        // If exception happens too early, the bundle owned exception route can not be served
        $this->setLogicRoot($this->resolveLogicRoot());
        $this->setTemplateRoot($this->resolveTemplateRoot());

        $endpoint = $this->findFileForPath($this->getLogicRoot() . '/exceptions/' . $statusCode, 'php');
        $compilerTemplate = $this->getExceptionTemplate($statusCode);

        return new Route(
            path: 'exceptions/' . $statusCode,
            endpoint: $endpoint,
            compilerTemplate: $compilerTemplate,
        );
    }


    /**
     * Searches for a provided exception template. If none is found, it returns the default template for specified
     * exception code, or - if that one does not exist either - a fallback exception template
     */
    private function getExceptionTemplate(int $statusCode): ViewTemplateFile
    {
        $file = $this->findFileForPath($this->getTemplateRoot() . '/exceptions/' . $statusCode, $this->getTemplateExtension());
        if ($file) {
            return new ViewTemplateFile($file, $this->getTemplateRoot());
        }

        // Fallback, if no custom exception template is found
        $file = file_exists('templates/exceptions/' . $statusCode . '.html')
            ? 'templates/exceptions/' . $statusCode . '.html'
            : 'templates/exceptions/xxx.html';

        if (!file_exists($file) && isVendor()) {
            $file = file_exists('vendor/gepf/gepf/templates/exceptions/' . $statusCode . '.html')
                ? 'vendor/gepf/gepf/templates/exceptions/' . $statusCode . '.html'
                : 'vendor/gepf/gepf/templates/exceptions/xxx.html';
        }

        $this->useFallbackCompiler();

        return new ViewTemplateFile($file, 'templates/exceptions/');
    }


    /**
     * In case of an exception and if no user defined template is available, use LITE for Gepf provided templates
     */
    protected function useFallbackCompiler(): void
    {
        $this->config->setBodyCompiler(Response::CONTENT_TYPE_HTML, Engine::class);
    }
}
