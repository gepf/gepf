<?php

namespace Gepf\Core\Routing\IndexRouter;

use Gepf\Core\Routing\ViewTemplateFile;
use Gepf\Core\Runtime\Runtime;
use Gepf\Core\StaticPaths;

trait IndexRouterTrait
{
    private readonly string $logicRoot;
    private readonly string $templateRoot;
    private readonly string $templateExtension;

    public function getTemplateRoot(): string
    {
        return $this->templateRoot;
    }

    public function setTemplateRoot(string $templateRoot): void
    {
        $this->templateRoot = $templateRoot;
    }

    public function getTemplateExtension(): string
    {
        return $this->templateExtension ?? ViewTemplateFile::DEFAULT_TEMPLATE_EXTENSION;
    }

    public function setTemplateExtension(string $templateExtension): void
    {
        $this->templateExtension = $templateExtension;
    }

    private function resolveLogicRoot(): string
    {
        $runtime = $this->runtime;

        if ($runtime->getMode() === Runtime::MODE_BUNDLE) {
            return StaticPaths::BUNDLE_FOLDER . '/' . $runtime->getApp()->getName() . '/' . StaticPaths::ROUTE_FOLDER;
        }

        if ($runtime->getMode() === Runtime::MODE_APP) {
            return StaticPaths::ROUTE_FOLDER . '/' . $runtime->getApp()->getName();
        }

        if ($runtime->getMode() === Runtime::MODE_VENDOR) {
            return StaticPaths::ROUTE_FOLDER;
        }

        throw new \RuntimeException("Unknown runtime mode: {$this->runtime->getMode()}");
    }

    private function resolveTemplateRoot(): string
    {
        $runtime = $this->runtime;

        if ($runtime->getMode() === Runtime::MODE_BUNDLE) {
            return StaticPaths::BUNDLE_FOLDER . '/' . $runtime->getApp()->getName() . '/' . ViewTemplateFile::DEFAULT_TEMPLATE_ROOT;
        }

        if ($runtime->getMode() === Runtime::MODE_APP) {
            return ViewTemplateFile::DEFAULT_TEMPLATE_ROOT . '/' . $runtime->getApp()->getName();
        }

        if ($runtime->getMode() === Runtime::MODE_VENDOR) {
            return ViewTemplateFile::DEFAULT_TEMPLATE_ROOT;
        }

        throw new \RuntimeException("Unknown runtime mode: {$runtime->getMode()}");
    }

    private function findIndexes(?string $path, ?string $endpoint): array
    {
        $pathParts = $path ? explode('/', $path) : [''];
        $indexPath = '';
        $indexes = [];

        // check for 'root index'
        if ($pathParts /*&& $this->endpointIsIndex*/) { // should be 'endpoint is root index'
            $file = $this->findFileForPath($this->getLogicRoot(), 'php');

            if ($file === $endpoint) {
                array_pop($pathParts);
            }

            if ($file && $file !== $endpoint) {
                $indexes[] = $file;
            }
        }

        foreach ($pathParts as $directory) {
            $indexPath = $directory ? $indexPath . '/' . $directory : '';

            $file = $this->findIndexForPath($this->getLogicRoot() . '/' . $indexPath, 'php');
            if ($file) {
                $indexes[] = $file;
            }
        }

        return $indexes;
    }

    /**
     * A file for a path can be either 'path.*' or 'path/index.*'
     */
    private function findFileForPath(string $path, string $extension): ?string
    {
        if (file_exists($path . '.' . $extension)) {
            return $path . '.' . $extension;
        }

        $index = $this->findIndexForPath($path, $extension);
        if ($index) {
            return $index;
        }

        return null;
    }

    private function findIndexForPath(string $path, string $extension): ?string
    {
        if (file_exists($path . '/index.' . $extension)) {
            return trim($path . '/index.' . $extension, '/');
        }

        return null;
    }

    public function getLogicRoot(): string
    {
        return $this->logicRoot;
    }

    public function setLogicRoot(string $logicRoot): void
    {
        $this->logicRoot = $logicRoot;
    }
}
