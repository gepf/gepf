<?php

namespace Gepf\Core\Routing;

use Gepf\Core\FileSystem\File;

class ViewTemplateFile
{
    public const DEFAULT_TEMPLATE_ROOT = 'templates';
    public const DEFAULT_TEMPLATE_EXTENSION = 'html';

    private File $file;
    private string $root;
    private string $relativePath;

    public function __construct(string $path, string $root)
    {
        $this->file = new File($path);
        $this->root = $root;
        $this->relativePath = str_replace($root, '', $path);
    }

    public function getFile(): File
    {
        return $this->file;
    }

    public function getRoot(): string
    {
        return $this->root;
    }

    public function getRelativePath(): array|string
    {
        return $this->relativePath;
    }
}
