<?php

namespace Gepf\Core\Routing;

use Gepf\Core\DependencyInjection\Container\Container;
use Gepf\Core\Routing\Exception\RouteNotFoundException;
use Gepf\Core\Runtime\Runtime;

class CommandRouter extends AbstractRouter
{
    public static function canLoad(Container $container): bool
    {
        return Runtime::isCommandLine();
    }


    public function createRoute(): void
    {
        $this->route = new Route(
            path: $this->runtime->getResolvedPath(),
        );
    }

    public function setExceptionRoute(int $statusCode): void
    {
        $this->route = new Route(
            endpoint: $this->runtime->getApp()->getName() . '/exceptions/' . $statusCode,
        );
    }


    /**
     * @throws RouteNotFoundException
     */
    private function resolveEndpoint(string $command): string
    {
        $path = str_replace(':', '/', $command);

        if (file_exists('commands/' . $path . '.php')) {
            return 'commands/' . $path . '.php';
        }

        if (file_exists('vendor/gepf/commands/' . $path . '.php')) {
            return 'vendor/gepf/commands/' . $path . '.php';
        }

        throw new RouteNotFoundException($command);
    }


    public function run(Container $container): void
    {
        $commandManager = $container->getCommandManager();
        if ($commandManager->has($this->route->getPath())) {
            $commandManager->execute($this->route->getPath(), $container);
            return;
        }

        $file = $this->resolveEndpoint($this->route->getPath());

        require_once $file;
    }
}
