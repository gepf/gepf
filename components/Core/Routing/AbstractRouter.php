<?php

namespace Gepf\Core\Routing;

use Gepf\Core\Config\Config;
use Gepf\Core\DependencyInjection\KernelService;
use Gepf\Core\Runtime\Runtime;

abstract class AbstractRouter extends KernelService implements RouterInterface
{
    protected Route $route;

    public function __construct(
        protected readonly Config $config,
        protected Runtime $runtime,
    ) {}


    public function getRoute(): Route
    {
        return $this->route;
    }
}
