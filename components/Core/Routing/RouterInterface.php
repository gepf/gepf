<?php

namespace Gepf\Core\Routing;

use Gepf\Core\DependencyInjection\Container\Container;
use Gepf\Core\DependencyInjection\KernelServiceInterface;
use Gepf\Core\Routing\Exception\RouteNotFoundException;

interface RouterInterface extends KernelServiceInterface
{
    public function getRoute(): Route;

    /** @throws RouteNotFoundException */
    public function createRoute(): void;

    public function setExceptionRoute(int $statusCode): void;

    /** @throws RouteNotFoundException */
    public function run(Container $container): void;
}
