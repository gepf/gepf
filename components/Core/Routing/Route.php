<?php

namespace Gepf\Core\Routing;

readonly class Route
{
    /**
     * @param null|string $path - is what you see client-side, like 'path/to/site' or 'path:to:command'
     * @param null|string $endpoint - is the internal path, like 'routes/welcome/index.php' or 'commands/test/cowsay.php'
     * @param array $indexes -are the paths to all index.php files that come before the endpoint
     */
    public function __construct(
        private ?string $path = null,
        private ?string $endpoint = null,
        private array $indexes = [],
        private ?ViewTemplateFile $compilerTemplate = null,
    ) {}


    public function getPath(): ?string
    {
        return $this->path;
    }

    public function getEndpoint(): ?string
    {
        return $this->endpoint;
    }

    public function getIndexes(): array
    {
        return $this->indexes;
    }

    public function getCompilerTemplate(): ?ViewTemplateFile
    {
        return $this->compilerTemplate;
    }
}
