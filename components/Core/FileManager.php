<?php

namespace Gepf\Core;

use Gepf\Core\DependencyInjection\KernelService;
use Gepf\Core\FileSystem\File;
use Gepf\Core\FileSystem\Util\FileSystem;
use Gepf\Core\Runtime\Runtime;

class FileManager extends KernelService
{
    public function __construct(private readonly Runtime $runtime) {}

    public function get(string $filename): File
    {
        return FileSystem::get($this->createPath($filename));
    }

    private function createPath(string $filename): string
    {
        if ($this->runtime->getMode() === Runtime::MODE_BUNDLE) {
            return 'bundles/' . $this->runtime->getApp()->getName() . '/' . $filename;
        }

        return $filename;
    }
}
