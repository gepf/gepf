<?php

namespace Gepf\Core\Util;

class Naming
{
    public static function camelCaseSubject($name): string
    {
        return lcfirst(static::upperCamelCaseSubject($name));
    }


    public static function upperCamelCaseSubject($name): string
    {
        $name = is_object($name) ? get_class($name) : $name;

        $lastSlashPosition = strrpos($name, '\\');

        return $lastSlashPosition !== false ? substr($name, $lastSlashPosition + 1) : $name;
    }


    public static function underscoreSubject($name): string
    {
        return static::underscore(static::camelCaseSubject($name));
    }


    public static function underscore(string $name): string
    {
        $name = preg_replace('#(?<!_)[A-Z]#', '_\0', $name);
        $name = mb_strtolower($name);

        return trim($name, '_');
    }


    public static function humanize(string $text): string
    {
        return ucfirst(strtolower(trim(preg_replace(['/([A-Z])/', '/[_\s]+/'], ['_$1', ' '], $text))));
    }


    public static function snakeCase(string $text): string
    {
        return strtolower(trim(preg_replace(['/([A-Z])/', '/[_\s]+/'], ['_$1', '-'], $text)));
    }
}
