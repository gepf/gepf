<?php

namespace Gepf\Core\Util;

class Text
{
    public static function get(int $position, string $delimiter, string $subject): string
    {
        $subjectParts = explode($delimiter, $subject);

        return current(array_values(\array_slice($subjectParts, $position)));
    }


    public static function first(string $delimiter, string $subject): string
    {
        return self::get(0, $delimiter, $subject);
    }


    // remove last element
    public static function pop(string $delimiter, string $subject): string
    {
        $subjectParts = explode($delimiter, $subject);
        unset($subjectParts[\count($subjectParts) - 1]);

        return implode($delimiter, $subjectParts);
    }


    // remove first element
    public static function shift(string $delimiter, string $subject): string
    {
        $subjectParts = explode($delimiter, $subject);
        unset($subjectParts[0]);

        return implode($delimiter, $subjectParts);
    }


    public static function humanize(string $subject, string $specialCharsReplace = ' '): string
    {
        $specialChars = ['_', '-', ':'];
        $humanized = $subject;
        $humanized = str_replace($specialChars, ' ', $humanized);
        $humanized = strtolower($humanized);
        $humanized = ucwords($humanized);

        return str_replace(' ', $specialCharsReplace, $humanized);
    }


    public static function snakeCase(string $subject): string
    {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $subject, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = $match === strtoupper($match) ? strtolower($match) : lcfirst($match);
        }
        return implode('_', $ret);
    }


    /**
     * Use with caution.
     * Will transform a string 'true' to (bool) true
     * and a string '12345' to (int) 12345.
     *
     * It won't transform to float though. Float is bad luck.
     */
    public static function guessAndConvertToAppropriateType(string $subject): string|int|bool
    {
        if (ctype_digit($subject)) {
            return (int)$subject;
        }

        if (strtolower($subject) === 'true') {
            return true;
        }

        if (strtolower($subject) === 'false') {
            return false;
        }

        return $subject;
    }


    public static function getStringBetween(string $haystack, string $start, string $end): string
    {
        [$startPosition, $length] = self::getPositionsBetween($haystack, $start, $end);

        return substr($haystack, $startPosition, $length);
    }


    public static function replaceBetween(string $haystack, string $start, string $end): string
    {
        [$startPosition, $length] = self::getPositionsBetween($haystack, $start, $end);

        return substr_replace($haystack, '', $startPosition, $length);
    }


    private static function getPositionsBetween(string $haystack, string $start, string $end): array
    {
        $startPosition = strpos($haystack, $start);

        if ($startPosition === false) {
            throw new \RuntimeException('provided starting string not found');
        }

        $startPosition += \strlen($start);
        $length = strpos($haystack, $end, $startPosition) - $startPosition;

        return [$startPosition, $length];
    }
}
