<?php

namespace Gepf\Core\Util;

class Property
{
    public static function access(object $class, string $propertyName, array $args = []): mixed
    {
        $getter = 'get' . ucfirst($propertyName);

        if (method_exists($class, $getter)) {
            return $class->$getter(...$args);
        }

        throw new PropertyException($class, $propertyName);
    }


    public static function exists(object $class, string $propertyName): bool
    {
        $getter = 'get' . ucfirst($propertyName);
        return method_exists($class, $getter);
    }


    public static function setValue(object $class, string $propertyName, mixed $value): void
    {
        $setter = 'set' . ucfirst($propertyName);

        if (method_exists($class, $setter)) {
            $class->$setter($value);

            return;
        }

        throw new PropertyException($class, $propertyName);
    }


    /**
     * Returns all public properties and all non-public properties that have a getter for a class.
     * WeakReferences are omitted to prevent errors
     */
    public static function getPropertiesOfClass(object $object): array
    {
        $reflectionClass = new \ReflectionClass($object);
        $publicProperties = $reflectionClass->getProperties(\ReflectionProperty::IS_PUBLIC);
        $nonPublicProperties = $reflectionClass->getProperties(\ReflectionProperty::IS_PRIVATE | \ReflectionProperty::IS_PROTECTED);

        return array_merge(
            array_map(static fn(\ReflectionProperty $property) => $property->getName(), $publicProperties),
            array_map(static fn(\ReflectionProperty $property) => $property->getName(),
                array_filter(
                    $nonPublicProperties,
                    static fn(\ReflectionProperty $property) => $property->getType()?->getName() !== \WeakReference::class
                        && method_exists($object, 'get' . ucfirst($property->getName())),
                ),
            ),
        );
    }


    /**
     * @throws \ReflectionException
     */
    public static function constantKeyForValue(string $class, int|string $constantsValue, string $replace = null): string
    {
        $reflectedClass = new \ReflectionClass($class);
        $constants = array_flip($reflectedClass->getConstants());

        $target = $constants[$constantsValue];

        if ($replace) {
            $croppedTarget = str_replace($replace, '', $target);

            if ($croppedTarget === $target) {
                throw new \LogicException("could not find the replace string '$replace' for constant '$target'");
            }

            return $croppedTarget;
        }

        return $target;
    }
}


class PropertyException extends \RuntimeException
{
    public function __construct(object $class, string $property, \Throwable $previous = null, int $code = 0)
    {
        $message = sprintf('Property "%s" not found in object "%s".', $property, $class::class);
        parent::__construct($message, $code, $previous);
    }
}
