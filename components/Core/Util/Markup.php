<?php

namespace Gepf\Core\Util;

class Markup
{
    public static function parseAndGet(string $string, string $searchStart, string $searchEnd = 'regex:$'): ?string
    {
        $searchStartRegex = str_contains($searchStart, 'regex:');
        $searchEndRegex = str_contains($searchEnd, 'regex:');

        $newSearchStart = $searchStartRegex ? str_replace('regex:', '', $searchStart) : preg_quote($searchStart, null);
        $newSearchEnd = $searchEndRegex ? str_replace('regex:', '', $searchEnd) : preg_quote($searchEnd, null);

        preg_match("/$newSearchStart([\s\S]*?)$newSearchEnd/", $string, $find);

        return $find[0] ?? null;
    }


    public static function parseAndReplace(string $string, string $searchStart, string $searchEnd = 'regex:$'): string
    {
        $searchStartRegex = str_contains($searchStart, 'regex:');
        $searchEndRegex = str_contains($searchEnd, 'regex:');

        $newSearchStart = $searchStartRegex ? str_replace('regex:', '', $searchStart) : preg_quote($searchStart, null);
        $newSearchEnd = $searchEndRegex ? str_replace('regex:', '', $searchEnd) : preg_quote($searchEnd, null);

        return preg_replace("/$newSearchStart([\s\S]*?)$newSearchEnd/", '', $string);
    }


    public static function translateMlStructure(array $structure): array
    {
        $mlStructure = [];
        foreach ($structure as $parentTag => $childTag) {

            if (is_int($parentTag)) {
                throw new \RuntimeException('Structure must be associative');
            }

            $children = is_array($childTag) ? self::translateMlStructure($childTag) : $childTag;

            $options = self::parseAndGet($parentTag, '[', ']');
            $tagString = self::parseAndReplace($parentTag, '[', ']');

            $classes = self::parseAndGet($tagString, '.');
            $tagString = self::parseAndReplace($tagString, '.');

            $ifCase = self::parseAndGet($tagString, '(', ')');
            $tagString = self::parseAndReplace($tagString, '(', ')');

            $id = self::parseAndGet($tagString, '#');
            $tagString = self::parseAndReplace($tagString, '#');
            $tagString = self::parseAndReplace($tagString, '_');

            $mlLevel = [
                'ifCase' => $ifCase,
                'name' => $tagString,
                'id' => $id,
                'classes' => $classes,
                'options' => $options,
                'children' => $children,
            ];

            $mlStructure[] = $mlLevel;
        }

        return $mlStructure;
    }


    public static function buildMlElement($key, $val, array $structure = [], $valWasArr = false): string
    {
        $ob = '';
        $id = null;
        $classes = null;
        $options = null;

        foreach ($structure as $scope) {

            if ($scope['ifCase']) {
                if ($scope['ifCase'] === '($?!arr)' && $valWasArr) {
                    continue;
                }
                if ($scope['ifCase'] === '($?arr)' && !$valWasArr) {
                    continue;
                }
            }

            if ($scope['options']) {
                $options = $scope['options'];
                $options = str_replace(['[', ']', '=', ' '], ['', '', '="', '" '], $options);
                $options = ' ' . $options . '"';
            }

            if ($scope['classes']) {
                $classes = $scope['classes'];
                $classes = str_replace('.', ' ', $classes);
                $classes = trim($classes);
                $classes = ' class="' . $classes . '"';
            }

            if ($scope['id']) {
                $id = $scope['id'];
                $id = str_replace(['#$', '#'], ['$', $key], $id);
                $id = ' id="' . $id . '"';
            }

            $content = is_array($scope['children']) ? self::buildMlElement($key, $val, $scope['children'], $valWasArr) : $scope['children'];
            $content = str_replace('#', $key, $content);

            self::processValue($val, $id, $classes, $options, $content);

            $ob .= '<' . $scope['name'];

            if ($id) {
                $ob .= $id;
            }
            if ($classes) {
                $ob .= $classes;
            }
            if ($options) {
                $ob .= $options;
            }

            $ob .= '>' . "\r\n";
            $ob .= $content . "\r\n";
            $ob .= '</' . $scope['name'] . '>' . "\r\n";
        }

        return $ob;
    }


    private static function processValue($val, &$id, &$classes, &$options, &$content): void
    {
        if (is_array($val)) {
            foreach ($val as $arrKey => $current) {
                $id = str_replace('$' . ($arrKey + 1), $current, $id);
                $classes = str_replace('$' . ($arrKey + 1), $current, $classes);
                $options = str_replace('$' . ($arrKey + 1), $current, $options);
                $content = str_replace('$' . ($arrKey + 1), $current, $content);
            }

            return;
        }


        if (is_bool($val)) {
            $val = $val ? 'true' : 'false';
        }

        $id = str_replace('$', $val, $id);
        $classes = str_replace('$', $val, $classes);
        $options = str_replace('$', $val, $options);
        $content = str_replace('$', $val, $content);
    }


    public static function export($values, ?array $structure = null): string
    {
        if (!$structure) {
            $structure = [
                'div#' => [
                    'div_1.reference' => '#',
                    'div_2.value' => '$',
                ],
            ];
        }

        // Get the first key of structure array since this will be the structure for our current scope
        $key = key($structure);

        // Wrap current structure in new array, because translateMlStructure() needs it that way
        $currentStructure[][$key] = $structure[$key];

        // Translate 'short-hand-array' into something machine readable

        $currentStructure = self::translateMlStructure($currentStructure[0]);

        unset($structure[$key]);

        $ob = '';

        foreach ($values as $key => $val) {

            $valWasArr = false;
            if (is_array($val) && !isset($val[0])) {
                $valWasArr = true;
                $val = self::export($val, $structure);
            }

            $ob .= self::buildMlElement($key, $val, $currentStructure, $valWasArr);
        }

        return $ob;
    }
}
