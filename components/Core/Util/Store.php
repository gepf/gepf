<?php

namespace Gepf\Core\Util;

use Gepf\Core\FileSystem\Util\Directory;
use Gepf\Core\FileSystem\Util\TmpFileSystem;

/**
 * Store is not the same as a cache, since it can be used as a database.
 * A cache on the other hand, shall never be used as a database.
 *
 * Like the cache, the store uses included php files. That way, the store can take advantage of the opcache
 */
class Store
{
    /**
     * @throws StoreException
     */
    public static function get(string $key): mixed
    {
        $path = str_replace('.', '/', $key);
        if (file_exists(getTmpDir() . '/store/' . $path . '.php')) {
            return TmpFileSystem::nativelyRequirePhpFileContent('store/' . $path . '.php');
        }

        throw new StoreException($key);
    }


    public static function getOptional(string $key, mixed $default = null): mixed
    {
        $path = str_replace('.', '/', $key);
        if (file_exists(getTmpDir() . '/store/' . $path . '.php')) {
            return TmpFileSystem::nativelyRequirePhpFileContent('store/' . $path . '.php');
        }

        return $default;
    }


    public static function set(string $key, $val): void
    {
        $path = str_replace('.', '/', $key);

        $directory = Text::pop('/', $path);
        $mixedAsString = var_export($val, true);

        Directory::ensure(getTmpDir() . '/store/' . $directory);

        $store = null;

        if (file_exists(getTmpDir() . '/store/' . $path . '.php')) {
            require getTmpDir() . '/store/' . $path . '.php';
        }

        if ($val !== $store) {
            file_put_contents(
                getTmpDir() . '/store/' . $path . '.php',
                '<?php' . ' return ' . $mixedAsString . ';'
            );
        }
    }


    public static function delete(string $key): ?bool
    {
        $path = str_replace('.', '/', $key);
        if (!file_exists(getTmpDir() . '/store/' . $path . '.php')) {
            return null;
        }

        return unlink(getTmpDir() . '/store/' . $path . '.php');
    }
}


class StoreException extends \Exception
{
    public function __construct(string $key, \Throwable $previous = null, int $code = 0)
    {
        $message = sprintf('Key "%s" not found.', $key);
        parent::__construct($message, $code, $previous);
    }
}
