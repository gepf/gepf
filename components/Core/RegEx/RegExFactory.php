<?php

namespace Gepf\Core\RegEx;

class RegExFactory
{
    public const KEY_MATCH = 'match';
    public const KEY_POS = 'pos';
    public const KEY_GROUPS = 'groups';

    public static function getMatches(string $pattern, string $subject): array
    {
        preg_match_all('/' . $pattern . '/', $subject, $matches);

        return $matches[0];
    }

    public static function getCaptureGroups(string $pattern, string $subject): array
    {
        preg_match_all('/' . $pattern . '/', $subject, $matches, PREG_SET_ORDER);

        $result = [];
        foreach ($matches as $n => $match) {
            foreach ($match as $group => $capture) {

                // pcre puts the general match always in the '0-item'
                if ($group === 0) {
                    $result[$n][self::KEY_MATCH] = $capture;
                    continue;
                }

                if (\is_int($group)) {
                    continue;
                }

                $result[$n][self::KEY_GROUPS][$group] = $capture;
            }
        }

        return $result;
    }

    public static function getFirstOccurrenceCaptureGroups(string $pattern, string $subject): ?RegExMatch
    {
        preg_match('/' . $pattern . '/', $subject, $matches, PREG_OFFSET_CAPTURE);

        $result = [];
        foreach ($matches as $n => $match) {
            // pcre puts the general match always in the '0-item'
            if ($n === 0) {
                $result[self::KEY_MATCH] = $match[0];
                $result[self::KEY_POS] = $match[1];

                continue;
            }

            if (\is_int($n)) {
                continue;
            }

            $result[self::KEY_GROUPS][$n] = new RegExGroupItem($n, $match[0], $match[1]);
        }

        if (!$result) {
            return null;
        }

        return new RegExMatch($result[self::KEY_MATCH], $result[self::KEY_POS], $result[self::KEY_GROUPS]);
    }
}
