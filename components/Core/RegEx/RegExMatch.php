<?php

namespace Gepf\Core\RegEx;

readonly class RegExMatch
{
    public function __construct(
        private string $match,
        private int $pos,
        private array $groups,
    ) {}

    public function getMatch(): string
    {
        return $this->match;
    }

    public function getPos(): int
    {
        return $this->pos;
    }

    public function getGroups(): array
    {
        return $this->groups;
    }

    public function has(string $groupName): bool
    {
        return isset($this->groups[$groupName]);
    }

    public function get(string $groupName): RegExGroupItem
    {
        return $this->groups[$groupName];
    }

    public function getValue(string $groupName): string
    {
        return $this->groups[$groupName]->getValue();
    }
}
