<?php

namespace Gepf\Core\RegEx;

readonly class RegExGroupItem
{
    public function __construct(
        private string $name,
        private string $value,
        private int $pos,
    ) {}

    public function getName(): string
    {
        return $this->name;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function getPos(): int
    {
        return $this->pos;
    }
}
