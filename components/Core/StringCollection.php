<?php

namespace Gepf\Core;

class StringCollection extends Collection
{
    public function current(): string
    {
        return parent::current();
    }

    public function get(int $key): string
    {
        return parent::get($key);
    }

    public function addString(string $value): void
    {
        $this->add($value);
    }
}
