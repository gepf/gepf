<?php

namespace Gepf\Core\Environment;

use Gepf\Core\FileSystem\Util\FileSystem;
use Gepf\Core\Util\Text;

class EnvironmentFactory
{
    public static function createFromDotEnv(string $dotEnvPath, bool $isVendor): Environment
    {
        try {
            $envFile = FileSystem::getContent($dotEnvPath);
            $envLines = explode(PHP_EOL, $envFile);
        } catch (\Exception) {
            $envLines = [
                'MODE=production',
            ];
        }

        $variables = self::populateFromDotEnv($envLines);
        $variables[Environment::KEY_IS_VENDOR] = $isVendor;
        $variables[Environment::KEY_SOURCE] = $isVendor ? 'vendor/gepf/gepf' : '';

        $_ENV = array_merge($_ENV, $variables);

        return new Environment($dotEnvPath, $variables);
    }


    private static function populateFromDotEnv(array $envLines): array
    {
        $envVars = [];
        foreach ($envLines as $envLine) {

            if (empty($envLine)) {
                continue;
            }

            $envVariable = explode('=', $envLine);

            if (\count($envVariable) !== 2) {
                throw new \RuntimeException('Malformed ENV file');
            }

            $key = strtoupper($envVariable[0]);
            $value = $envVariable[1];

            $envVars[$key] = Text::guessAndConvertToAppropriateType($value);
        }

        self::setRequiredValues($envVars);

        return $envVars;
    }


    private static function setRequiredValues(array &$envVars): void
    {
        $envVars[Environment::KEY_MODE] = $envVars[Environment::KEY_MODE] ?? Environment::MODE_PROD;
    }
}
