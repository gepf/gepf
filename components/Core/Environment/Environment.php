<?php

namespace Gepf\Core\Environment;

use Gepf\Core\DependencyInjection\ReadOnlyServiceInterface;

readonly class Environment implements ReadOnlyServiceInterface
{
    public const MODE_PROD = 'production';
    public const MODE_DEV = 'development';
    public const MODE_TEST = 'test';
    public const KEY_MODE = 'MODE';
    public const KEY_IS_VENDOR = 'GEPF_IS_VENDOR';
    public const KEY_SOURCE = 'GEPF_SOURCE';


    public function __construct(
        private string $dotEnvPath,
        private array $variables,
    ) {}

    public function getDotEnvPath(): ?string
    {
        return $this->dotEnvPath;
    }

    public function getServerApi(): string
    {
        return PHP_SAPI;
    }

    public function isDevelopment(): bool
    {
        return $this->getMode() === self::MODE_DEV;
    }

    public function getMode(): string
    {
        return $this->get('MODE');
    }

    public function get(string $key)
    {
        return $this->variables[$key] ?? null;
    }

    public function isProduction(): bool
    {
        return $this->getMode() === self::MODE_PROD;
    }
}
