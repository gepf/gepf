<?php

namespace Gepf\Core;

class StaticPaths
{
    public const BUNDLE_FOLDER = 'bundles';
    public const ROUTE_FOLDER = 'routes';
    public const TEMPLATE_FOLDER = 'templates';
}
