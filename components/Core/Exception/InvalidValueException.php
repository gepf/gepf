<?php

namespace Gepf\Core\Exception;

class InvalidValueException extends AbstractMissingException
{
    public const TYPE_VALUE = 1;

    public function __construct(string $missingEntity, int $type, array $allowedValues = null)
    {
        $message = $allowedValues
            ? 'Invalid value: ' . $missingEntity . ' ; Allowed values are: ' . implode(', ', $allowedValues)
            : null;

        parent::__construct($missingEntity, $type, $message);
    }
}
