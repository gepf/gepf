<?php

namespace Gepf\Core\Exception;

class WrongReferenceException extends \Exception
{
    public function __construct(int $code = 0, \Throwable $previous = null)
    {
        parent::__construct('Wrong WeakReference used', $code, $previous);
    }
}
