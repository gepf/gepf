<?php

namespace Gepf\Core\Exception;

use Gepf\Core\Util\Property;

abstract class AbstractMissingException extends \LogicException
{
    protected int $type;

    public function __construct(string $missingEntity, int $type, string $customMessage = null, \Throwable $previous = null, int $code = 0)
    {
        $this->type = $type;

        $message = $customMessage ?? 'Not found: ' . $this->getExceptionType() . ': ' . $missingEntity;

        parent::__construct($message, $code, $previous);
    }


    public function getExceptionType(): string
    {
        try {
            $type = Property::constantKeyForValue(get_class($this), $this->type);
        } catch (\ReflectionException) {
            return "Unknown type: $this->type";
        }

        return $type;
    }
}
