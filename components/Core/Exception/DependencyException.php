<?php

namespace Gepf\Core\Exception;

class DependencyException extends AbstractMissingException
{
    public const TYPE_CLASS = 1;
    public const TYPE_EXTENSION = 2;
}
