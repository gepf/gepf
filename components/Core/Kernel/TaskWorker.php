<?php

namespace Gepf\Core\Kernel;

use Gepf\Core\Controller\Exception\ControllerException;
use Gepf\Core\DependencyInjection\Container\Container;
use Gepf\Core\Event\Event;
use Gepf\Core\Kernel\Task\Boilerplate\Boilerplate;
use Gepf\Core\Kernel\Task\CompileView\CompileView;
use Gepf\Core\Kernel\Task\KernelStage;
use Gepf\Core\Kernel\Task\Logic\Logic;
use Gepf\Core\Kernel\Task\Request\Request;
use Gepf\Core\Kernel\Task\Response\Response;
use Gepf\Core\Log\Logger;
use Gepf\Core\Routing\RouterInterface;
use Gepf\Core\StringCollection;

class TaskWorker
{
    public const STAGE_BOILERPLATE = Boilerplate::class;
    public const STAGE_REQUEST = Request::class;
    public const STAGE_LOGIC = Logic::class;
    public const STAGE_VIEW = CompileView::class;
    public const STAGE_RESPONSE = Response::class;

    public const STAGES = [
        self::STAGE_BOILERPLATE,
        self::STAGE_REQUEST,
        self::STAGE_LOGIC,
        self::STAGE_VIEW,
        self::STAGE_RESPONSE,
    ];

    private StringCollection $remainingStages;
    private StringCollection $completedTasks;


    public function __construct(private readonly Container $container)
    {
        $this->remainingStages = new StringCollection(self::STAGES);
        $this->completedTasks = new StringCollection();
    }


    public function isStopOnFirstError(): bool
    {
        return $this->container->getEnvironment()->get('STOP_ON_FIRST_ERROR') ?? false;
    }


    /**
     * @throws \Exception when env 'STOP_ON_FIRST_ERROR' is set
     */
    public function workToAndExecute(string $stopAfter = null): void
    {
        $finishedStages = [];
        foreach ($this->getRemainingStages() as $stageClassName) {

            $this->dispatch(Event::TEMPUS_PRE, $stageClassName);

            /** @var KernelStage $stage */
            $stage = new $stageClassName($this->container);
            foreach ($stage->getTasks() as $taskName) {

                if ($this->getCompletedTasks()->has($taskName)) {
                    continue;
                }

                $this->dispatch(Event::TEMPUS_PRE, $taskName);

                try {
                    $task = $this->container->wire($taskName);
                    $task();
                } catch (\Exception $e) {
                    $this->wrapError($e);
                }

                $this->dispatch(Event::TEMPUS_POST, $taskName);

                $this->getCompletedTasks()->addString($taskName);

                if ($stopAfter === $taskName) {
                    break 2;
                }
            }

            $this->dispatch(Event::TEMPUS_POST, $stageClassName);
            $finishedStages[] = $stageClassName;
            if ($stopAfter === $stageClassName) {
                break;
            }
        }

        foreach ($finishedStages as $finishedStage) {
            $this->getRemainingStages()->remove($finishedStage);
        }
    }


    /**
     * @throws \Exception when env 'STOP_ON_FIRST_ERROR' is set
     */
    private function dispatch(int $tempus, string $name): void
    {
        $eventManager = $this->container->getEventManager();
        $subscribedEvents = $eventManager->findEvents($name, $tempus);

        foreach ($subscribedEvents as $event) {
            foreach ($event->getListeners() as $listener) {
                try {
                    $this->container->wire($listener)->execute();
                } catch (\Exception $e) {
                    $this->wrapError($e);
                }
            }
        }
    }


    /**
     * @throws \Exception
     */
    private function wrapError(\Exception $e): void
    {
        if ($this->isStopOnFirstError()) {
            throw $e;
        }

        $responseCode = $e instanceof ControllerException ? $e->getStatusCode() : 500;
        $this->container->getLogger()->error($e->getMessage(), [Logger::EXCEPTION_KEYWORD => $e]);
        $this->container->getResponse()->setCode($responseCode);
        $this->container->getResponse()->setReason($e->getMessage());
        $this->container->getResponse()->setBackTrace($e->getTrace());
        $this->container->getResponse()->setBody($e->getMessage());

        if ($this->container->canInitialize(RouterInterface::class)) {
            $this->container->getRouter()->setExceptionRoute($responseCode);
        }
    }


    public function getRemainingStages(): StringCollection
    {
        return $this->remainingStages;
    }

    public function getCompletedTasks(): StringCollection
    {
        return $this->completedTasks;
    }
}
