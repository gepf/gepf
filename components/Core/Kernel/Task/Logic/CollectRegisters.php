<?php

namespace Gepf\Core\Kernel\Task\Logic;

use Gepf\Core\DependencyInjection\ConfigurableServiceInterface;
use Gepf\Core\DependencyInjection\Container\Container;
use Gepf\Core\DependencyInjection\Container\ContainerItem\AbstractContainerItem;
use Gepf\Core\Kernel\Task\KernelTask;

class CollectRegisters extends KernelTask
{
    public function __construct(private readonly Container $container) {}

    public function __invoke(): void
    {
        $this->container->getRegistry()->setRequest($this->container->getRequest());
        $this->container->getRegistry()->setResponse($this->container->getResponse());
        $this->iterateServices();
    }


    private function iterateServices(): void
    {
        foreach ($this->container->getServices() as $item) {
            $this->buildCallbackRegistry($item);
            $this->buildAssetRegistry($item);
        }
    }


    private function buildCallbackRegistry(AbstractContainerItem $item): void
    {
        $service = $item->isInitialized() ? $item->getService() : $item->getServiceName();

        if (!method_exists($service, 'getCallbackNames')) {
            return;
        }

        if (!$service::getCallbackNames()) {
            return;
        }

        if (!$item->isInitialized()) {
            // Service must be initialized, in order to have direct callbacks
            // TODO find a way to initialize services when callback is demanded only
            $service = $item->getService();
        }

        if (!$service instanceof ConfigurableServiceInterface) {
            return;
        }

        foreach ($service::getCallbackNames() as $callbackName) {
            $this->container->getRegistry()->getCallbacks()->set($callbackName, $service->$callbackName());
        }
    }


    private function buildAssetRegistry(AbstractContainerItem $item): void
    {
        $serviceAsStringOrClass = $item->isInitialized() ? $item->getService() : $item->getServiceName();

        if (!method_exists($serviceAsStringOrClass, 'getAssets')) {
            return;
        }

        if (!$serviceAsStringOrClass::getAssets()) {
            return;
        }

        foreach ($serviceAsStringOrClass::getAssets() as $asset) {
            $this->container->getRegistry()->getAssetRegistry()->addAsset($asset);
        }
    }
}
