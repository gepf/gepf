<?php

namespace Gepf\Core\Kernel\Task\Logic;

use Gepf\Core\Kernel\Task\KernelStage;

class Logic extends KernelStage
{
    public const TASK_COLLECT_REGISTERS = CollectRegisters::class;
    public const TASK_RUN_LOGIC = RunLogic::class;


    public const TASKS = [
        self::TASK_COLLECT_REGISTERS,
        self::TASK_RUN_LOGIC,
    ];


    public function getTasks(): array
    {
        return self::TASKS;
    }
}
