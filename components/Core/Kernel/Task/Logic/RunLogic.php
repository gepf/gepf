<?php

namespace Gepf\Core\Kernel\Task\Logic;

use Gepf\Core\DependencyInjection\Container\Container;
use Gepf\Core\Kernel\Task\KernelTask;
use Gepf\Core\Routing\Exception\RouteNotFoundException;
use Gepf\Core\Runtime\Runtime;

class RunLogic extends KernelTask
{
    public function __construct(private readonly Container $container) {}

    /**
     * @throws RouteNotFoundException
     */
    public function __invoke(): void
    {
        $isUseBufferDump = $this->checkIsUseBufferDump();

        if (!$this->container->getResponse()->isStatusOk()) {
            // Oh, we messed up already!
            $this->container->getRouter()->setExceptionRoute($this->container->getResponse()->getCode());

            return;
        }

        try {
            $this->container->getRouter()->createRoute();
        } catch (RouteNotFoundException $e) {
            $this->container->getResponse()->setCode(404);
            $this->container->getResponse()->setReason($e->getMessage());
            $this->container->getRouter()->setExceptionRoute(404);

            return;
        }

        if ($isUseBufferDump) {
            ob_start();
        }

        // TODO as of now, CommandRouter can emit RouteNotFoundException only
        //  in run() method, but this should happen in createRoute() already
        try {
            $this->container->getRouter()->run($this->container);
        } catch (RouteNotFoundException $routeNotFoundException) {
            $this->container->getResponse()->setCode(404);
            $this->container->getResponse()->setReason($routeNotFoundException->getMessage());
            $this->container->getRouter()->setExceptionRoute(404);

            return;
        } catch (\Exception $e) {
            $this->collectDump($isUseBufferDump);
            throw $e;
        }

        $this->collectDump($isUseBufferDump);

        if (!$this->container->getResponse()->isCodeSet()) {
            $this->container->getResponse()->setCode(200);
        }
    }


    private function checkIsUseBufferDump(): bool
    {
        if (Runtime::isCommandLine()) {
            return false;
        }

        return $this->container->getConfig()->isBufferDump();
    }


    private function collectDump(bool $isUseBufferDump): void
    {
        $dump = $isUseBufferDump ? ob_get_clean() : null;
        $this->container->getRegistry()->setDump($dump);
    }
}
