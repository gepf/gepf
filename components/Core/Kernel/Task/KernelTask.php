<?php

namespace Gepf\Core\Kernel\Task;

abstract class KernelTask
{
    abstract public function __invoke(): void;
}
