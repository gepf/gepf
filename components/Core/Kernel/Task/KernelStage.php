<?php

namespace Gepf\Core\Kernel\Task;

use Gepf\Core\DependencyInjection\Container\Container;

abstract class KernelStage
{
    protected Container $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    abstract public function getTasks(): array;
}
