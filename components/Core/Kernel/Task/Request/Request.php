<?php

namespace Gepf\Core\Kernel\Task\Request;

use Gepf\Core\Kernel\Task\KernelStage;

class Request extends KernelStage
{
    public const TASK_START_SESSION = StartSession::class;


    public const TASKS = [
        self::TASK_START_SESSION,
    ];


    public function getTasks(): array
    {
        return self::TASKS;
    }
}
