<?php

namespace Gepf\Core\Kernel\Task\Request;

use Gepf\Core\DependencyInjection\ParameterBag\ParameterBag;
use Gepf\Core\Kernel\Task\KernelTask;
use Gepf\Core\Session\Session;

/**
 * TODO transform this KernelTask to optional Service
 *   also @see writeSession
 */
class StartSession extends KernelTask
{
    public function __construct(private readonly Session $session) {}

    /**
     * @throws \Exception
     */
    public function __invoke(): void
    {
        if (session_status() === PHP_SESSION_NONE) {
            try {
                session_start();
            } catch (\Exception $e) {
                $this->createSession();
                throw $e;
            }
        }

        $this->createSession();
    }

    private function createSession(): void
    {
        $token = $_SESSION[Session::TOKEN_KEY] ?? base64_encode(random_bytes(16));

        $session = $this->session;
        $session->setId(session_id());
        $session->setToken($token);
        $session->setOnRequestParameters(new ParameterBag($_SESSION ?? []));
        $session->setReferer($_SESSION[Session::REFERER_KEY] ?? null);
        $session->setQueuedParams($_SESSION[Session::QUEUE_KEY] ?? []);
    }
}
