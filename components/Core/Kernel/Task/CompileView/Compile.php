<?php

namespace Gepf\Core\Kernel\Task\CompileView;

use Gepf\Core\Kernel\Task\KernelTask;
use Gepf\Core\Negotiation\Response\Response;
use Gepf\Core\Registry\Registry;
use Gepf\Core\Routing\RouterInterface;
use Gepf\Core\View\ViewCompilerException;
use Gepf\Core\View\ViewCompilerInterface;
use Gepf\Core\View\ViewCompilerUtil;
use Psr\Log\LoggerInterface;

class Compile extends KernelTask
{
    public function __construct(
        private readonly RouterInterface $router,
        private readonly Response $response,
        private readonly Registry $registry,
        private readonly LoggerInterface $logger,
        private readonly ViewCompilerInterface $viewCompiler,
    ) {}


    public function __invoke(): void
    {
        $template = $this->router->getRoute()->getCompilerTemplate();

        if (!$template && $this->viewCompiler->requiresTemplate()) {
            throw new ViewCompilerException(
                "Could not find template for {$this->router->getRoute()->getPath()}",
            );
        }

        try {
            $responseBody = $this->viewCompiler->compile($this->registry, $template);
            $this->response->setBody($responseBody);
        } catch (ViewCompilerException|\Exception $e) {
            $this->setFallbackErrorResponse($e);
        }
    }


    private function setFallbackErrorResponse(\Exception $e): void
    {
        $this->logger->error($e->getMessage());
        $this->response->setCode(500);
        $this->response->setReason($e->getMessage());
        $this->response->setBody(ViewCompilerUtil::getFallbackErrorBody($e));
    }
}
