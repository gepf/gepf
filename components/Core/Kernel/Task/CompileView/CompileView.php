<?php

namespace Gepf\Core\Kernel\Task\CompileView;

use Gepf\Core\Kernel\Task\KernelStage;
use Gepf\Core\View\DetermineCompilerTask;

class CompileView extends KernelStage
{
    public const TASK_DETERMINE_COMPILER = DetermineCompilerTask::class;
    public const TASK_COMPILE = Compile::class;


    public const TASKS = [
        self::TASK_DETERMINE_COMPILER,
        self::TASK_COMPILE,
    ];


    public function getTasks(): array
    {
        return self::TASKS;
    }
}
