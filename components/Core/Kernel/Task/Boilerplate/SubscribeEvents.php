<?php

namespace Gepf\Core\Kernel\Task\Boilerplate;

use Gepf\Core\DependencyInjection\ConfigurableServiceInterface;
use Gepf\Core\DependencyInjection\Container\Container;
use Gepf\Core\DependencyInjection\Container\ContainerItem\AbstractContainerItem;
use Gepf\Core\Event\Event;
use Gepf\Core\Event\EventSubscription;
use Gepf\Core\Kernel\Task\KernelTask;

class SubscribeEvents extends KernelTask
{
    public function __construct(private readonly Container $container) {}

    public function __invoke(): void
    {
        $eventManager = $this->container->getEventManager();
        foreach ($this->container->getServices() as $item) {
            // try without autoloader (instanceof) first, then use is_subclass_of for uninitialized services (string)
            if ($this->checkIfConfigurableService($item)) {
                /** @noinspection PhpUndefinedMethodInspection */
                foreach ($item->getServiceName()::getSubscribedEvents() as $event) {
                    if (!$this->checkIfSubscribedMissed($event)) {
                        throw new \RuntimeException('Event ' . $event->getListener() . ' can not be registered before ' . self::class);
                    }

                    $eventManager->subscribe($event);
                }
            }
        }
    }


    private function checkIfSubscribedMissed(EventSubscription $eventSubscription): bool
    {
        $thisEventPosition = current(array_keys(Boilerplate::TASKS, self::class));
        $invalidEvents = \array_slice(Boilerplate::TASKS, 0, $thisEventPosition);

        if (\in_array($eventSubscription->getEventName(), $invalidEvents, true)) {
            return false;
        }

        if ($eventSubscription->getEventName() === Boilerplate::class && $eventSubscription->getTempus() === Event::TEMPUS_PRE) {
            return false;
        }

        if ($eventSubscription->getEventName() === self::class && $eventSubscription->getTempus() === Event::TEMPUS_PRE) {
            return false;
        }

        return true;
    }


    private function checkIfConfigurableService(AbstractContainerItem $item): bool
    {
        if ($item->isInitialized() && $item->getService() instanceof ConfigurableServiceInterface) {
            return true;
        }

        if (is_subclass_of($item->getServiceName(), ConfigurableServiceInterface::class)) {
            return true;
        }

        return false;
    }
}
