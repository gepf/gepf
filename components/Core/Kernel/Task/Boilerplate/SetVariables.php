<?php

namespace Gepf\Core\Kernel\Task\Boilerplate;

use Gepf\Core\Config\Config;
use Gepf\Core\Environment\Environment;
use Gepf\Core\Kernel\Task\KernelTask;
use Gepf\Core\Routing\IndexRouter\IndexRouter;
use Gepf\Core\Routing\RouterInterface;

class SetVariables extends KernelTask
{
    public function __construct(
        private readonly RouterInterface $router,
        private readonly Config $config,
    ) {}

    public function __invoke(): void
    {
        if (\in_array($_ENV[Environment::KEY_MODE], [Environment::MODE_DEV, Environment::MODE_TEST], true)) {
            error_reporting(E_ALL);
            ini_set('display_errors', 1);
        }

        // TODO config trait with getTemplateExtension, for easier refactoring if a config option deprecates
        if ($this->router instanceof IndexRouter && $this->config->getAppConfig('template_extension')) {
            $this->router->setTemplateExtension($this->config->getAppConfig('template_extension'));
        }
    }
}
