<?php

namespace Gepf\Core\Kernel\Task\Boilerplate;

use Gepf\Core\DependencyInjection\Container\RegisterMandatoryServicesTask;
use Gepf\Core\Kernel\Task\KernelStage;
use Gepf\Core\Runtime\BuildRuntimeTask;

class Boilerplate extends KernelStage
{
    public const TASK_REGISTER_MANDATORY_SERVICES = RegisterMandatoryServicesTask::class;
    public const TASK_LOAD_CONFIG = LoadConfig::class;
    public const TASK_CHECK_RUNTIME = BuildRuntimeTask::class;
    public const TASK_LOAD_SERVICES = LoadServices::class;
    public const TASK_LOAD_APP_CONFIGS = LoadAppConfigs::class;
    public const TASK_SUBSCRIBE_EVENTS = SubscribeEvents::class;
    public const TASK_SET_COMMANDS = SetCommands::class;
    public const TASK_SET_VARIABLES = SetVariables::class;


    public const TASKS = [
        self::TASK_REGISTER_MANDATORY_SERVICES,
        self::TASK_LOAD_CONFIG,
        self::TASK_CHECK_RUNTIME,
        self::TASK_LOAD_SERVICES,
        self::TASK_LOAD_APP_CONFIGS,
        self::TASK_SUBSCRIBE_EVENTS,
        self::TASK_SET_COMMANDS,
        self::TASK_SET_VARIABLES,
    ];


    public function getTasks(): array
    {
        return self::TASKS;
    }
}
