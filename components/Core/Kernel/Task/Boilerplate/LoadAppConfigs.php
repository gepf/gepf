<?php

namespace Gepf\Core\Kernel\Task\Boilerplate;

use Gepf\Core\Config\Config;
use Gepf\Core\Config\ConfigException;
use Gepf\Core\Kernel\Task\KernelTask;
use Gepf\Core\Runtime\Runtime;

class LoadAppConfigs extends KernelTask
{
    public function __construct(
        private readonly Runtime $runtime,
        private readonly Config $config,
    ) {}

    /**
     * @throws ConfigException
     */
    public function __invoke(): void
    {
        if (!is_dir('bundles/' . $this->runtime->getApp()->getName() . '/config')) {
            return;
        }

        $appConfigs = array_diff(scandir('bundles/' . $this->runtime->getApp()->getName() . '/config'), ['..', '.']);

        foreach ($appConfigs as $appConfig) {
            if (is_dir('bundles/' . $this->runtime->getApp()->getName() . '/config/' . $appConfig)) {
                throw new ConfigException('Recursive configuration for apps not supported yet');
            }

            $configContent = require 'bundles/' . $this->runtime->getApp()->getName() . '/config/' . $appConfig;

            if (!\is_array($configContent)) {
                throw new ConfigException(
                    'Malformed app configuration: \'bundles/' . $this->runtime->getApp()->getName()
                    . '/config/' . $appConfig . '\' - Config must be a php file that returns an array',
                );
            }

            $this->config->addAppConfig($configContent);
        }
    }
}
