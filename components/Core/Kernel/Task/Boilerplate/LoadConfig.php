<?php

namespace Gepf\Core\Kernel\Task\Boilerplate;

use Gepf\Core\Config\Config;
use Gepf\Core\FileSystem\Util\FileSystem;
use Gepf\Core\Kernel\Task\KernelTask;
use Gepf\Core\Negotiation\Response\Response;
use Gepf\Core\View;
use Gepf\Lite\Engine;
use Gepf\Lite\LiteConfig;
use Gepf\Service;

class LoadConfig extends KernelTask
{
    public function __construct(private readonly Config $config) {}

    public function __invoke(): void
    {
        $filesystem = new FileSystem();

        $generalFile = getConfDir() . '/gepf.php';
        $general = file_exists($generalFile) ? include $generalFile : [];

        $config = $this->config;
        $config->setDefaultApp($general['default_app'] ?? !isVendor() ? 'welcome' : null);
        $config->setBodyCompilers($this->getBodyCompilers());
        $config->setBufferDump($general['buffer_dump'] ?? true);

        $serviceConfigs = $this->getServiceConfigs();
        foreach ($serviceConfigs as $serviceConfig) {
            $config->addService(include getConfDir() . '/services/' . $serviceConfig);
        }

        // Default set of services, if none are configured
        if (!$serviceConfigs) {
            $config->addService(new LiteConfig());
            $config->addService(['class' => Service\Asset\AssetService::class, 'config' => []]);
        }
    }


    private function getBodyCompilers(): array
    {
        $compilers = getConfDir() . '/body_compilers.php';
        if (file_exists($compilers)) {
            return include $compilers;
        }

        //Default set of compilers, if none are configured
        return [
            Response::CONTENT_TYPE_PLAIN => View\Plain::class,
            Response::CONTENT_TYPE_JSON => View\Json::class,
            Response::CONTENT_TYPE_HTML => Engine::class,
            Response::CONTENT_TYPE_XML => View\XmlViewCompiler::class,
            Response::CONTENT_TYPE_ANY => View\Plain::class,
        ];
    }


    private function getServiceConfigs(): array
    {
        $services = getConfDir() . '/services';
        if (is_dir($services)) {
            return array_diff(scandir($services), ['..', '.']);
        }

        return [];
    }
}
