<?php

namespace Gepf\Core\Kernel\Task\Boilerplate;

use Gepf\Core\DependencyInjection\ConfigurableService;
use Gepf\Core\DependencyInjection\Container\Container;
use Gepf\Core\DependencyInjection\KernelServiceInterface;
use Gepf\Core\Kernel\Task\KernelTask;

class SetCommands extends KernelTask
{
    public function __construct(private readonly Container $container) {}

    public function __invoke(): void
    {
        $commandManager = $this->container->getCommandManager();
        foreach ($this->container->getServices() as $item) {
            $className = $item->getServiceName();
            if (is_subclass_of(
                $className,
                KernelServiceInterface::class,
                is_subclass_of($className, ConfigurableService::class),
            )) {
                foreach ($className::getCommands() as $command) {
                    $commandManager->register($command);
                }
            }
        }
    }
}
