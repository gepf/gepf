<?php

namespace Gepf\Core\Kernel\Task\Boilerplate;

use Gepf\Core\Config\Config;
use Gepf\Core\Config\ConfigException;
use Gepf\Core\DependencyInjection\ConfigurableService;
use Gepf\Core\DependencyInjection\Container\Container;
use Gepf\Core\DependencyInjection\Container\ContainerItem\ContainerItem;
use Gepf\Core\DependencyInjection\Container\ContainerItem\InterfaceContainerItem;
use Gepf\Core\Kernel\Task\KernelTask;
use Gepf\Service\ServiceConfigInterface;

class LoadServices extends KernelTask
{
    public function __construct(
        private readonly Config $config,
        private readonly Container $container,
    ) {}

    /**
     * @throws ConfigException
     */
    public function __invoke(): void
    {
        foreach ($this->config->getServices() as $serviceSettings) {
            $serviceClassName = $serviceSettings->getClass();

            if (!class_exists($serviceClassName)) {
                throw new ConfigException('Configured Service ' . $serviceClassName . 'does not exist');
            }

            if (!is_subclass_of($serviceClassName, ConfigurableService::class)) {
                throw new ConfigException($serviceClassName . ' does not implement ' . ConfigurableService::class);
            }

            $this->containerRegister($serviceSettings, $serviceClassName);
        }
    }


    private function containerRegister(ServiceConfigInterface $serviceSettings, string $serviceClassName): void
    {
        if (method_exists($serviceSettings, 'getInterface') && $serviceSettings->getInterface()) {
            $this->container->register(
                new InterfaceContainerItem(
                    interface: $serviceSettings->getInterface(),
                    serviceName: $serviceClassName,
                    container: $this->container,
                    config: $serviceSettings,
                ),
            );

            return;
        }

        $this->container->register(
            new ContainerItem(
                serviceName: $serviceClassName,
                container: $this->container,
                config: $serviceSettings,
            ),
        );
    }
}
