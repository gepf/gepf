<?php

namespace Gepf\Core\Kernel\Task\Response;

use Gepf\Core\Kernel\Task\KernelTask;
use Gepf\Core\Negotiation\Request\Request;

class PopulateResponse extends KernelTask
{
    public function __construct(
        private readonly \Gepf\Core\Negotiation\Response\Response $response,
        private readonly Request $request,
    ) {}

    public function __invoke(): void
    {
        $this->response->getHeaders()->setContentType($this->request->headers->getAccept()->get());
    }
}
