<?php

namespace Gepf\Core\Kernel\Task\Response;

use Gepf\Core\Kernel\Task\KernelStage;

class Response extends KernelStage
{
    public const TASK_WRITE_SESSION = WriteSession::class;
    public const TASK_REDIRECT = RedirectIfDemanded::class;
    public const TASK_POPULATE_RESPONSE = PopulateResponse::class;
    public const TASK_PRINT = PrintBody::class;

    public const TASKS = [
        self::TASK_WRITE_SESSION,
        self::TASK_REDIRECT,
        self::TASK_POPULATE_RESPONSE,
        self::TASK_PRINT,
    ];

    public function getTasks(): array
    {
        return self::TASKS;
    }
}
