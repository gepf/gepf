<?php

namespace Gepf\Core\Kernel\Task\Response;

use Gepf\Core\Kernel\Task\KernelTask;
use Gepf\Core\Log\Logger;
use Gepf\Core\Runtime\Runtime;

class PrintBody extends KernelTask
{
    public function __construct(
        private readonly Logger $logger,
        private readonly \Gepf\Core\Negotiation\Response\Response $response,
    ) {}

    public function __invoke(): void
    {
        http_response_code($this->response->getCode());

        if (!headers_sent()) {
            header('Content-Type: ' . $this->response->getHeaders()->getContentType());
        } else {
            $this->logger->warning('HTTP Headers sent out too early');
        }

        print $this->response->getBody();

        if (Runtime::isCommandLine() && $this->response->getCode() !== 200) {
            print "{$this->response->getCode()} - {$this->response->getMessage()}\n";
            print "{$this->response->getReason()}\n";
        }
    }
}
