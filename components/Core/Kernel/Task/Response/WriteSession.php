<?php

namespace Gepf\Core\Kernel\Task\Response;

use Gepf\Core\Kernel\Task\KernelTask;
use Gepf\Core\Kernel\Task\Request\StartSession;
use Gepf\Core\Negotiation\Request\Request;
use Gepf\Core\Session\Session;

/**
 * TODO transform this KernelTask to optional Service
 *   also @see StartSession
 */
class WriteSession extends KernelTask
{
    public function __construct(
        private readonly Session $session,
        private readonly \Gepf\Core\Negotiation\Response\Response $response,
        private readonly Request $request,
    ) {}

    public function __invoke(): void
    {
        $session = $this->session;

        // If no redirect is demanded, set current path as referer
        if (!$this->response->getRedirect()) {
            $session->setReferer($this->request->getPath());
        }

        $_SESSION[Session::TOKEN_KEY] = $session->getToken();
        $_SESSION[Session::QUEUE_KEY] = $session->getQueuedParams();
        $_SESSION[Session::REFERER_KEY] = $session->getReferer();
    }
}
