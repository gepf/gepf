<?php

namespace Gepf\Core\Kernel\Task\Response;

use Gepf\Core\Kernel\Task\KernelTask;

class RedirectIfDemanded extends KernelTask
{
    public function __construct(private readonly \Gepf\Core\Negotiation\Response\Response $response) {}

    public function __invoke(): void
    {
        if (!$this->response->getRedirect()) {
            return;
        }

        header('Location: ' . $this->response->getRedirect(), true, 303);
        die();
    }
}
