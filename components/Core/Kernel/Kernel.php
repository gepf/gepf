<?php /** @noinspection PhpUnhandledExceptionInspection */

namespace Gepf\Core\Kernel;

use Gepf\Core\DependencyInjection\Container\Container;
use Gepf\Core\DependencyInjection\Container\ContainerFactory;
use Gepf\Core\Environment\Environment;
use Gepf\Core\Environment\EnvironmentFactory;
use Gepf\Core\Kernel\Task\Request\Request;

class Kernel
{
    private ?Container $serviceContainer;
    private ?TaskWorker $worker;
    private Environment $environment;

    private bool $booted = false;


    public function __construct(
        private readonly string $pathToDotEnv = '.env',
        private readonly bool $isVendor = false,
    ) {}


    public static function spawn(string $pathToDotEnv = '.env', bool $isVendor = false): self
    {
        return new self($pathToDotEnv, $isVendor);
    }


    // Cannot reboot with different .env
    private function bootImmutables(Environment $environment): void
    {
        if ($environment->getMode() === Environment::MODE_DEV) {
            require_once 'functions/dump.php';
        }

        // Flush write folders for test environment
        if ($environment->getMode() === Environment::MODE_TEST) {
            $this->recursivelyRemoveDir(getTmpDir());
            $this->recursivelyRemoveDir(getVarDir());
        }
    }


    public function boot(): self
    {
        require_once 'functions/env.php';

        if (!isset($this->environment)) {
            $this->environment = EnvironmentFactory::createFromDotEnv($this->pathToDotEnv, $this->isVendor);
            $this->bootImmutables($this->environment);
        }

        // vendor/composer must be sorted before instantiating service container,
        // because it might provide the Psr ContainerInterface
        $disableComposer = $this->environment->get('DISABLE_COMPOSER_AUTOLOAD');
        if (!$disableComposer && file_exists('vendor/autoload.php')) {
            require_once 'vendor/autoload.php';
        }

        $this->serviceContainer = ContainerFactory::create($this->environment);
        $this->worker = new TaskWorker($this->serviceContainer);
        $this->booted = true;

        return $this;
    }


    public function shutdown(): void
    {
        if ($this->booted === false) {
            return;
        }

        $this->booted = false;

        foreach ($this->getServiceContainer() as $service) {
            $service->shutdown();
            $service->setContainer(null);
        }

        $this->serviceContainer = null;
        $this->worker = null;
    }


    public function reboot(): void
    {
        $this->shutdown();
        $this->boot();
    }


    public function bootToLogicStage(): void
    {
        if (!$this->booted) {
            $this->boot();
        }

        $this->workToAndExecute(Request::class);
    }


    public function finish(): void
    {
        $this->workToAndExecute();
    }


    public function workToAndExecute(string $stageOrTask = null): void
    {
        try {
            $this->worker->workToAndExecute($stageOrTask);
        } catch (\Exception $e) {
            $this->lastResortEchoException($e);
        }
    }


    /**
     * The kernel can handle exceptions by itself.
     * This is the last resort for displaying errors in dev mode when
     * errors happen too early during kernel runtime.
     */
    private function lastResortEchoException(\Exception $e): void
    {
        $noLogger = false;
        try {
            $this->serviceContainer->getLogger()->error($e->getMessage());
        } /** @noinspection BadExceptionsProcessingInspection */ catch (\Exception $noLogger) {
            // Error will not be logged!
        }

        if (!\function_exists('isProduction')) {
            return;
        }

        if (!isProduction()) {
            echo "500 - {$e->getMessage()}<br />";

            if ($noLogger) {
                echo 'No logger found. In production, this error would go unnoticed';
            }

            if (\function_exists('dump')) {
                /** @noinspection ForgottenDebugOutputInspection */
                dump($e->getTrace());
            } else {
                echo '<pre>';
                /** @noinspection ForgottenDebugOutputInspection */
                print_r($e->getTrace());
                echo '</pre>';
            }
        }
    }


    public function getServiceContainer(): Container
    {
        return $this->serviceContainer;
    }


    /*
     * Poison cabinet. Use to delete write folders (tmp, var) in testing mode only!
     */
    private function recursivelyRemoveDir(string $dir): void
    {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object !== '.' && $object !== '..') {
                    if (is_dir($dir . DIRECTORY_SEPARATOR . $object) && !is_link($dir . '/' . $object)) {
                        $this->recursivelyRemoveDir($dir . DIRECTORY_SEPARATOR . $object);
                    } else {
                        unlink($dir . DIRECTORY_SEPARATOR . $object);
                    }
                }
            }
            rmdir($dir);
        }
    }
}
