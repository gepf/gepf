<?php

namespace Gepf\Lite;

interface RenderingOutputInterface
{
    public function getContent(): string;

    public function setContent(string $content): void;

    public function getVariables(): array;
}
