<?php

namespace Gepf\Lite\Exception;

use Gepf\Core\View\ViewCompilerException;

class LiteException extends ViewCompilerException
{

}
