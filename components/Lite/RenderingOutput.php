<?php

namespace Gepf\Lite;

class RenderingOutput implements RenderingOutputInterface
{
    use RenderingTrait;

    private string $content;


    public function __construct(private readonly Template $template)
    {
        $this->setContent($this->template->getContent());
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    public function getTemplate(): Template
    {
        return $this->template;
    }

    public function getVariables(): array
    {
        return [];
    }
}
