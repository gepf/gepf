<?php

namespace Gepf\Lite;

use Gepf\Core\View\ViewCompilerInterface;
use Gepf\Service\ServiceConfigInterface;

class LiteConfig implements ServiceConfigInterface
{
    public const EXPR_OPTIONAL = 1;
    public const EXPR_ORDER_IRRELEVANT = 2;

    public const FOREACH_EXPR_ARRAY = 10;
    public const FOREACH_EXPR_ITEM_KEY = 11;
    public const FOREACH_EXPR_ITEM_CONTENT = 12;
    public const FOREACH_EXPR_INSIDE_KEYWORD = 13;
    public const FOREACH_EXPR_ELEMENT_POINTER = 14;
    public const FOREACH_EXPR_SUBJECT_KEYWORD = 15;
    public const FOREACH_EXPR_KEY_KEYWORD = 16;
    public const FOREACH_EXPR_ITEM_KEYWORD = 17;

    // smarty equivalent (untested):
    //
    // \{foreach
    //   (
    //     ?=[0-9A-Za-z\s\=]+?from\=(
    //       ?<from>[\w]+
    //     )
    //   ){1}(
    //     ?=[0-9A-Za-z\s\=]+?item\=(
    //       ?<item>[\w]+
    //     )
    //   ){1}(
    //     ?=[0-9A-Za-z\s\=]+?key\=(
    //       ?<key>[\w]+
    //     )
    //   )?[
    //     0-9A-Za-za-z\s\=
    //   ]+?
    // \}
    //
    // \{%[\s]*?for[\s]+?(?:(?<key>[\w\d]+?)\,[\s]*?)?(?<item>[\w\d]+?){1}[\s]+?in[\s]+(?<array>[^%\{\}]+?){1}[\s]*?%\}

    private string $forEachStatement
        = '\{%[\s]*?for[\s]+?'            # opening and 'for' keyword
        . '(?:(?<key>[\w\d]+?)\,[\s]*?)?' # optional key, followed by comma (letters and digits allowed)
        . '(?<item>[\w\d]+?){1}'          # minimum one item (letters and digits allowed)
        . '[\s]+?in[\s]+'                 # 'in' keyword
        . '(?<iterable>[^%\{\}]+?){1}'    # minimum one iterable (all allowed, except '{', '}', '%')
        . '[\s]*?%\}';                    # closing


    // smarty equivalent (untested): '\{/foreach\}'
    private string $forEachDelimiter = '\{%[\s]*?endfor[\s]*?%\}';

    public function getForEachStatement(): string
    {
        return $this->forEachStatement;
    }



    // Corresponding smarty values in the comments
    private string $commentPrefix = '{#';   // {*
    private string $commentSuffix = '#}';   // *}
    private string $printPrefix = '{{';     // {
    private string $printSuffix = '}}';     // }
    private string $stmtPrefix = '{%';      // {
    private string $stmtSuffix = '%}';      // }
//	private string $varPrefix = '';         // $
    private string $objectChainSign = '.';  // ->

//	private ?string $foreachElementPointer = ',';    // null
//	private ?string $foreachInsideKeyword = 'in';    // null
//	private ?string $foreachKeyKeyword = null;       // key=
//	private ?string $foreachItemKeyword = null;      // item=
//	private ?string $foreachSubjectKeyword = null;   // from=

    private string $negativeCheckKeyword = 'not ';   // !
    private string $ifKeyword = 'if ';               // if
//	private string $endifKeyword = 'endif';          // /if
    private string $endforKeyword = 'endfor';        // /for

    private string $blockKeyword = 'block';         // block
    private string $endBlockKeyword = 'endblock';   // /block

//	private string $safeComparisonOp = 'is same as';
//	private string $safeNegativeComparisonOp = 'is not same as';
//	private string $comparisonOp = '==';
//	private string $negativeComparisonOp = '!=';

    // Example: 'for $itemContent in $array'
    private array $foreachFormalExpression = [
//		self::FOREACH_EXPR_ITEM_KEY . '.' . self::FOREACH_EXPR_ELEMENT_POINTER => [self::EXPR_OPTIONAL],
        self::FOREACH_EXPR_ITEM_KEY => [],
        self::FOREACH_EXPR_ITEM_CONTENT => [],
        self::FOREACH_EXPR_INSIDE_KEYWORD => [],
        self::FOREACH_EXPR_ARRAY => [],
    ];

//	[
//		self::FOREACH_EXPR_SUBJECT_KEYWORD . '.' . self::FOREACH_EXPR_ARRAY => [self::EXPR_ORDER_IRRELEVANT],
//		self::FOREACH_EXPR_KEY_KEYWORD . '.' . self::FOREACH_EXPR_ITEM_KEY => [self::EXPR_ORDER_IRRELEVANT, self::EXPR_OPTIONAL],
//		self::FOREACH_EXPR_ITEM_KEYWORD . '.' . self::FOREACH_EXPR_ITEM_CONTENT => [self::EXPR_ORDER_IRRELEVANT],
//	]

    private int $maxInheritance = 8;

    private array $keywords = ['if', 'is', 'not', 'same', 'as', '==', '!='];

    public function getClass(): string
    {
        return Engine::class;
    }

    public function getName(): string
    {
        return 'lite';
    }

    public function getInterface(): string
    {
        return ViewCompilerInterface::class;
    }

    public function getCommentPrefix(): string
    {
        return $this->commentPrefix;
    }

    public function getRegexCommentPrefix(): string
    {
        return preg_quote($this->getCommentPrefix(), null);
    }

//	public function setCommentPrefix(string $commentPrefix): void
//	{
//		$this->commentPrefix = $commentPrefix;
//	}

    public function getCommentSuffix(): string
    {
        return $this->commentSuffix;
    }

    public function getRegexCommentSuffix(): string
    {
        return preg_quote($this->getCommentSuffix(), null);
    }

//	public function setCommentSuffix(string $commentSuffix): void
//	{
//		$this->commentSuffix = $commentSuffix;
//	}

    public function getPrintPrefix(): string
    {
        return $this->printPrefix;
    }

    public function getRegexPrintPrefix(): string
    {
        return preg_quote($this->getPrintPrefix(), null);
    }

    public function setPrintPrefix(string $printPrefix): void
    {
        $this->printPrefix = $printPrefix;
    }

    public function getPrintSuffix(): string
    {
        return $this->printSuffix;
    }

    public function getRegexPrintSuffix(): string
    {
        return preg_quote($this->getPrintSuffix(), null);
    }

    public function setPrintSuffix(string $printSuffix): void
    {
        $this->printSuffix = $printSuffix;
    }

    public function getStmtPrefix(): string
    {
        return $this->stmtPrefix;
    }

    public function getRegexStmtPrefix(): string
    {
        return preg_quote($this->getStmtPrefix(), null);
    }

//	public function setStmtPrefix(string $stmtPrefix): void
//	{
//		$this->stmtPrefix = $stmtPrefix;
//	}

    public function getStmtSuffix(): string
    {
        return $this->stmtSuffix;
    }

    public function getRegexStmtSuffix(): string
    {
        return preg_quote($this->getStmtSuffix(), null);
    }

//	public function setStmtSuffix(string $stmtSuffix): void
//	{
//		$this->stmtSuffix = $stmtSuffix;
//	}

    public function getObjectChainSign(): string
    {
        return $this->objectChainSign;
    }

    public function setObjectChainSign(string $objectChainSign): void
    {
        $this->objectChainSign = $objectChainSign;
    }

    public function getNegativeCheckKeyword(): string
    {
        return $this->negativeCheckKeyword;
    }

    public function setNegativeCheckKeyword(string $negativeCheckKeyword): void
    {
        $this->negativeCheckKeyword = $negativeCheckKeyword;
    }

    public function getIfKeyword(): string
    {
        return $this->ifKeyword;
    }

    public function setIfKeyword(string $ifKeyword): void
    {
        $this->ifKeyword = $ifKeyword;
    }

    public function getBlockKeyword(): string
    {
        return $this->blockKeyword;
    }

    public function setBlockKeyword(string $blockKeyword): void
    {
        $this->blockKeyword = $blockKeyword;
    }

    public function getEndBlockKeyword(): string
    {
        return $this->endBlockKeyword;
    }

    public function setEndBlockKeyword(string $endBlockKeyword): void
    {
        $this->endBlockKeyword = $endBlockKeyword;
    }

    public function getKeywords(): array
    {
        return $this->keywords;
    }

    public function getForeachFormalExpression(): array
    {
        return $this->foreachFormalExpression;
    }

//	public function setForeachFormalExpression(array $foreachFormalExpression): void
//	{
//		$this->foreachFormalExpression = $foreachFormalExpression;
//	}

    public function getMaxInheritance(): int
    {
        return $this->maxInheritance;
    }

    public function setMaxInheritance(int $maxInheritance): void
    {
        $this->maxInheritance = $maxInheritance;
    }
}
