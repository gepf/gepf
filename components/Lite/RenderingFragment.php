<?php

namespace Gepf\Lite;

class RenderingFragment implements RenderingOutputInterface
{
    use RenderingTrait;

    public function __construct(private string $content, private readonly array $variables = []) {}

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    public function getVariables(): array
    {
        return $this->variables;
    }
}
