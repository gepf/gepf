<?php

namespace Gepf\Lite;

use Gepf\Core\FileSystem\File;
use Gepf\Core\FileSystem\VirtualFile;
use Gepf\Core\View\AbstractTemplate;
use Gepf\Lite\Block\Block;
use Gepf\Lite\Block\BlockCollection;
use Gepf\Lite\Exception\LiteException;

/**
 * Template explained by example (twig syntax)
 *
 * file b.html
 * ```
 * {% extends a.html %}
 * <h1>{{ greeting }}</h1>
 * ```
 *
 * content
 * -------
 * will be the raw pre compile content
 * ```
 * {% extends a.html %}
 * <h1>{{ greeting }}</h1>
 * ```
 *
 * resolvedValuesCompiledContent
 * -----------------------------
 * will be the html end result
 * ``<h1>Hello World</h1>``
 *
 * phpExpressionCompiledContent
 * ----------------------------
 * will be the generated php code
 * ```
 * $content = '<h1>' . $data->get('greeting') . '</h1>'
 * echo $content
 * ```
 *
 * parent
 * ------
 * will be another Template for b.html
 *
 * child
 * -----
 * this template has no child, but parent Template (b.html)
 * will have this Template (a.html) as a child
 */
class Template extends AbstractTemplate
{
    private readonly string $content;
    private readonly string $resolvedValuesCompiledContent;
    private readonly string $phpExpressionCompiledContent;
    private ?Template $parent;
    private ?Template $child;
    private BlockCollection $blocks;
    private BlockCollection $overwrittenBlocks;


    // METADATA

    private ?string $particularInherit = null;
    private bool $inherit = true;


    public function __construct(File|VirtualFile $file)
    {
        parent::__construct($file);
        $this->blocks = new BlockCollection();
        $this->overwrittenBlocks = new BlockCollection();
    }


    public function loadContent(): void
    {
        // This can happen, when lite engine tries to compile for both, 'php' and 'full' mode
        if (isset($this->content)) {
            return;
        }

        $this->content = $this->getFile()->getContent();
    }


    /**
     * Helper function to find parent in case template is supposed to inherit from parent
     */
    public function findParent(): ?string
    {
        $filePilot = $this->getFile()->getPilot()->goUp();

        // Reached starting point
        if ($this->getFile()->getBasename() === 'index.html' && !$filePilot->canGoUp()) {
            return null;
        }

        // Go up
        if ($this->getFile()->getBasename() === 'index.html') {
            $filePilot->goUp();
        }

        return $filePilot->check('index.html')
            ? $filePilot->goTo('index.html')->getCurrentPath()
            : null;
    }


    /**
     * Creates the template this template is supposed to inherit from
     */
    public function createInheritedTemplate(?string $path): ?Template
    {
        if (!$path) {
            return null;
        }

        if (!file_exists($path) && $this->getParticularInherit()) {
            throw new LiteException('particular demanded file for template inheritance not found');
        }

        $inheritedTemplate = null;
        if (file_exists($path)) {
            $inheritedTemplate = new self(new File($path));
            $inheritedTemplate->setChild($this);
        }
        return $inheritedTemplate;
    }


    public function getContent(): string
    {
        return $this->content;
    }

    public function getResolvedValuesCompiledContent(): string
    {
        return $this->resolvedValuesCompiledContent;
    }

    public function setResolvedValuesCompiledContent(string $content): void
    {
        $this->resolvedValuesCompiledContent = $content;
    }

    public function setPhpExpressionCompiledContent(string $content): void
    {
        $this->phpExpressionCompiledContent = $content;
    }

    public function getPhpExpressionCompiledContent(): string
    {
        return $this->phpExpressionCompiledContent;
    }

    public function hasParent(): bool
    {
        return (bool)$this->parent;
    }

    public function getParent(): Template
    {
        return $this->parent;
    }

    public function setParent(?Template $parent): void
    {
        $this->parent = $parent;
    }

    public function hasChild(): bool
    {
        if (!isset($this->child)) {
            return false;
        }

        return (bool)$this->getChild();
    }

    public function getChild(): Template
    {
        return $this->child;
    }

    public function setChild(?Template $child): void
    {
        $this->child = $child;
    }

    public function hasBlocks(): bool
    {
        return $this->blocks->hasItems();
    }

    public function getBlocks(): BlockCollection
    {
        $this->blocks->rewind();

        return $this->blocks;
    }

    public function addBlock(Block $block): void
    {
        $this->blocks->addBlock($block);
    }

    public function removeBlock(Block $block): void
    {
        $this->blocks->remove($block);
    }

    public function getOverwrittenBlocks(): BlockCollection
    {
        $this->overwrittenBlocks->rewind();

        return $this->overwrittenBlocks;
    }

    public function addOverwrittenBlock(Block $block): void
    {
        $this->overwrittenBlocks->addBlock($block);
    }

    public function getParticularInherit(): ?string
    {
        return $this->particularInherit;
    }

    public function setParticularInherit(string $particularInherit): void
    {
        $this->particularInherit = $particularInherit;
    }

    public function isInherit(): bool
    {
        return $this->inherit;
    }

    public function setInherit(bool $inherit): void
    {
        $this->inherit = $inherit;
    }
}
