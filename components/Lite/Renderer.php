<?php

namespace Gepf\Lite;

use Gepf\Core\Registry\Registry;
use Gepf\Lite\Block\BlockCompiler;
use Gepf\Lite\EntityCompiler\CommentCompiler;
use Gepf\Lite\EntityCompiler\ConditionCompiler;
use Gepf\Lite\EntityCompiler\EntityCompiler;
use Gepf\Lite\EntityCompiler\EntityCompilerCollection;
use Gepf\Lite\EntityCompiler\LoopCompiler;
use Gepf\Lite\EntityCompiler\VarCompiler;
use Gepf\Lite\Exception\LiteException;
use Psr\Log\LoggerInterface;

class Renderer
{
    public const PHP_RENDERING = 1;
    public const FULL_RENDERING = 2;

    public const ENTITY_COMPILERS = [
        CommentCompiler::class,
        ConditionCompiler::class,
        LoopCompiler::class,
        VarCompiler::class,
    ];

    private string $resolvedValuesOutput;
    private string $phpExpressionOutput;

    private BlockCompiler $blockCompiler;
    private EntityCompilerCollection $entityCompilers;

    private int $inheritanceDepth = 1;

    public function __construct(
        Registry $registry,
        LoggerInterface $logger,
        private readonly LiteConfig $config,
        private readonly MetaData $metaData = new MetaData(),
    ) {
        $this->blockCompiler = new BlockCompiler($config);
        $this->entityCompilers = new EntityCompilerCollection(
            self::ENTITY_COMPILERS,
            $this,
            $this->config,
            $registry,
            $logger,
        );
    }


    public function renderToPhp(Template $template): void
    {
        $template->loadContent();
        $renderingOutput = new RenderingOutput($template);

        $this->render($template, self::PHP_RENDERING, $renderingOutput);

        // masking quotes before compilers running will break condition parsing
        $renderingOutput->maskSingleQuotes();

        $phpFile = '<?php
        use Gepf\Core\Registry\Registry;
        /** @var Registry $registry */
        $data = $registry->getData();
        $callbacks = $registry->getCallbacks()->toArray();
        $request = $registry->getRequest();
        $response = $registry->getResponse();
        $content = \'' . $renderingOutput->getContent() . '\';
        echo $content;';

        $template->setPhpExpressionCompiledContent($phpFile);
        $this->phpExpressionOutput = $template->getPhpExpressionCompiledContent();
    }


    public function renderFully(Template $template): void
    {
        $template->loadContent();
        $renderingOutput = new RenderingOutput($template);

        $this->render($template, self::FULL_RENDERING, $renderingOutput);

        // RegEx required, since addcslashes() masks quotes that are already masked, so does str_replace("'", "\'" ...)
        $renderedContent = preg_replace('/(?<!\\\\)(\')/m', '\\\'', $renderingOutput->getContent());

        $template->setResolvedValuesCompiledContent($renderedContent);
        $this->resolvedValuesOutput = $template->getResolvedValuesCompiledContent();
    }


    private function render(Template $template, int $type, RenderingOutput $renderingOutput): void
    {
        $this->metaData->parse($template);
        $this->blockCompiler->compileToPhp($renderingOutput);

        if ($template->hasParent()) {
            $this->inheritanceDepth++;
            self::render($template->getParent(), $type, $renderingOutput);

            if ($this->inheritanceDepth === $this->config->getMaxInheritance()) {
                throw new LiteException('Inherit depth exceeds limit of ' . $this->config->getMaxInheritance());
            }

            // Only template without parent shall pass to logic compilation
            return;
        }

        $this->blockCompiler->mergeBlocks($template, $renderingOutput);

        $this->compileBody($type, $renderingOutput);
    }


    public function compileBody(int $type, RenderingOutput $renderingOutput): void
    {
        foreach ($this->entityCompilers as $compiler) {
            $this->compile($compiler, $type, $renderingOutput);
        }
    }


    public function compileStatement(RenderingFragment $fragment): void
    {
        // entityCompilers->toArray, to not mess with internal counter of @self::compileBody foreach loop
        foreach ($this->entityCompilers->toArray() as $compiler) {
            $compiler->compileFully($fragment);
        }
    }


    public function getResolvedValuesOutput(): string
    {
        return $this->resolvedValuesOutput;
    }


    public function getPhpExpressionOutput(): string
    {
        return $this->phpExpressionOutput;
    }


    private function compile(EntityCompiler $compiler, int $type, RenderingOutput $renderingOutput): void
    {
        switch ($type) {
            case self::PHP_RENDERING:
                $compiler->compileToPhp($renderingOutput);
                break;
            case self::FULL_RENDERING:
                $compiler->compileFully($renderingOutput);
        }
    }
}
