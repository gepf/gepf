<?php

/*|
 *| This class does nothing but type hinting.
 *| This would not be necessary if there was generics in php.
 */

namespace Gepf\Lite\Block;

use Gepf\Core\Collection;

class BlockCollection extends Collection
{
    public function get(int $key): Block
    {
        return parent::get($key);
    }

    public function current(): Block
    {
        return parent::current();
    }

    public function addBlock(Block $block): void
    {
        $this->add($block);
    }
}
