<?php

namespace Gepf\Lite\Block;

use Gepf\Lite\Exception\LiteException;
use Gepf\Lite\LiteConfig;
use Gepf\Lite\RenderingOutput;
use Gepf\Lite\Template;

readonly class BlockCompiler
{
    public function __construct(private LiteConfig $config) {}


    public function compileToPhp(RenderingOutput $renderingOutput): void
    {
        // Same behaviour
        $this->compileFully($renderingOutput);
    }


    public function compileFully(RenderingOutput $renderingOutput): void
    {
        foreach ($this->createBlocks($renderingOutput->getContent()) as $block) {
            $renderingOutput->getTemplate()->addBlock($block);
        }
    }


    public function createBlocks(string $templateBody): iterable
    {
        // Get standalone blocks: {% block name %} [...] {% endblock %}
        preg_match_all(
            '/'
            . $this->config->getRegexStmtPrefix() . ' ' . $this->config->getBlockKeyword()
            . ' [\s\S]*? '
            . $this->config->getRegexStmtSuffix()
            . '[\s\S]*?'
            . $this->config->getRegexStmtPrefix() . ' ' . $this->config->getEndBlockKeyword() . ' ' . $this->config->getRegexStmtSuffix()
            . '/',
            $templateBody,
            $rawBlockContents
        );

        foreach ($rawBlockContents[0] as $rawBlockContent) {
            yield $this->createBlock($rawBlockContent);
        }
    }


    public function createBlock(string $rawBlockContent): Block
    {
        preg_match_all(
            '/'
            . $this->config->getRegexStmtPrefix() . ' ' . $this->config->getBlockKeyword()
            . ' [\s\S]*? '
            . $this->config->getRegexStmtSuffix()
            . '/',
            $rawBlockContent,
            $blockOpening
        );

        $blockOpening = $blockOpening[0][0];
        $blockName = str_replace([$this->config->getStmtPrefix() . ' ' . $this->config->getBlockKeyword(), ' ' . $this->config->getStmtSuffix()],
            '',
            $blockOpening);
        $regexSafeBlockOpening = preg_quote($blockOpening, '/');
        $blockContent = str_replace(
            [$blockOpening, $this->config->getStmtPrefix() . ' ' . $this->config->getEndBlockKeyword() . ' ' . $this->config->getStmtSuffix()],
            '',
            $rawBlockContent,
        );

        $block = new Block();
        $block->setName($blockName);
        $block->setOpening($blockOpening);
        $block->setRegexSafeOpening($regexSafeBlockOpening);
        $block->setRawContent($rawBlockContent);
        $block->setContent($blockContent);

        return $block;
    }


    /** @throws LiteException */
    public function mergeBlocks(Template $template, RenderingOutput $renderingOutput): void
    {
        if (!$template->hasBlocks()) {
            return;
        }

        if ($template->hasChild()) {
            $this->replaceBlocks($template, $template->getChild());
        }

        foreach ($template->getBlocks() as $block) {
            $search = $this->getRawBlockContentForName($block->getName(), $template);
            $compiledTemplateContent = str_replace($search, $block->getContent(), $renderingOutput->getContent());

            if (!\is_string($compiledTemplateContent)) {
                throw new LiteException('preg_replace did not return string');
            }

            $renderingOutput->setContent($compiledTemplateContent);
        }
    }


    /*
     * Private section
     */

    private function replaceBlocks(Template $sourceTemplate, Template $childTemplate): void
    {
        if (!$childTemplate->hasBlocks()) {
            return;
        }

        foreach ($childTemplate->getBlocks() as $block) {
            $this->replaceBlock($sourceTemplate, $block);
        }
    }


    private function replaceBlock(Template $template, Block $newBlock): void
    {
        foreach ($template->getBlocks() as $block) {
            if ($block->getName() === $newBlock->getName()) {
                $template->addOverwrittenBlock($block);
                $template->removeBlock($block);
                $template->addBlock($newBlock);
            }
        }
    }


    /** @throws LiteException */
    private function getRawBlockContentForName(string $name, Template $template): string
    {
        foreach ($template->getOverwrittenBlocks() as $block) {
            if ($block->getName() === $name) {
                return $block->getRawContent();
            }
        }

        foreach ($template->getBlocks() as $block) {
            if ($block->getName() === $name) {
                return $block->getRawContent();
            }
        }

        throw new LiteException("Could not find block $name");
    }
}
