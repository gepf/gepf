<?php

namespace Gepf\Lite\Block;

class Block
{
    private string $name;
    private string $opening;
    private string $regexSafeOpening;
    private Block $parent;
//	private BlockCollection $children;
    private string $rawContent;
    private string $content;


    public function getName(): string
    {
        return $this->name;
    }

    final public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getOpening(): string
    {
        return $this->opening;
    }

    final public function setOpening(string $opening): void
    {
        $this->opening = $opening;
    }

    public function getRegexSafeOpening(): string
    {
        return $this->regexSafeOpening;
    }

    final public function setRegexSafeOpening(string $regexSafeOpening): void
    {
        $this->regexSafeOpening = $regexSafeOpening;
    }

    public function getParent(): Block
    {
        return $this->parent;
    }

    final public function setParent(Block $parent): void
    {
        $this->parent = $parent;
    }

//	public function getChildren(): BlockCollection
//	{
//		return $this->children;
//	}
//
//	public function addChildren(Block $child): void
//	{
//		$this->children->add($child);
//	}

    public function getRawContent(): string
    {
        return $this->rawContent;
    }

    final public function setRawContent(string $rawContent): void
    {
        $this->rawContent = $rawContent;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    final public function setContent(string $content): void
    {
        $this->content = $content;
    }
}
