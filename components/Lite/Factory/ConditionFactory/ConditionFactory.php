<?php

namespace Gepf\Lite\Factory\ConditionFactory;

use Gepf\Lite\Entity\AbstractStatement;
use Gepf\Lite\Entity\Condition;
use Gepf\Lite\Entity\StaticValue;
use Gepf\Lite\Exception\LiteException;
use Gepf\Lite\Factory\ConditionFactory\ConditionParts\AbstractPart;
use Gepf\Lite\Factory\ConditionFactory\ConditionParts\BracketPart;
use Gepf\Lite\Factory\ConditionFactory\ConditionParts\OperatorPart;
use Gepf\Lite\Factory\ConditionFactory\ConditionParts\StringPart;
use Gepf\Lite\Factory\ConditionFactory\ConditionParts\VariablePart;
use Gepf\Lite\Factory\VarFactory;

class ConditionFactory
{
    public static function createCondition(string $templateExpression, AbstractStatement $statement, VarFactory $varFactory): Condition
    {
        $chars = str_split($templateExpression);

        $subject = self::digestChars($chars);
        $subject = self::structureCondition($subject);
        return self::populateConditionGroup($subject, $varFactory, $statement);
    }

    public static function digestChars(array $chars): array
    {
        // DETECT STRINGS
        $subject = [];
        $lastChar = null;
        $singleQuote = false;
        $doubleQuote = false;
        foreach ($chars as $char) {

            // Start of single quote
            if ($char === "'" && $lastChar !== '\\' && !$doubleQuote && !$singleQuote) {
                $singleQuote = new StringPart(StringPart::ESCAPE_TYPE_SINGLE);
                $subject[] = $singleQuote;
                $lastChar = $char;
                continue;
            }

            // Inside single quote
            if ($singleQuote && ($char !== "'" || $lastChar === '\\')) {
                $singleQuote->addContent($char);
                $lastChar = $char;
                continue;
            }

            // End of single quote
            if ($char === "'" && $lastChar !== '\\' && $singleQuote) {
                $singleQuote = false;
                $lastChar = $char;
                continue;
            }

            // Start of double quote
            if ($char === '"' && $lastChar !== '\\' && !$doubleQuote && !$singleQuote) {
                $doubleQuote = new StringPart(StringPart::ESCAPE_TYPE_DOUBLE);
                $subject[] = $doubleQuote;
                $lastChar = $char;
                continue;
            }

            // Inside double quote
            if ($doubleQuote && ($char !== '"' || $lastChar === '\\')) {
                $doubleQuote->addContent($char);
                $lastChar = $char;
                continue;
            }

            // End of double quote
            if ($char === '"' && $lastChar !== '\\' && $doubleQuote) {
                $doubleQuote = false;
                $lastChar = $char;
                continue;
            }

            $subject[] = $char;
            $lastChar = $char;
        }

        // DETECT BRACKETS
        $originalSubject = $subject;
        $subject = [];
        $bracket = false;
        $depth = 0;
        foreach ($originalSubject as $char) {
            // Start bracket
            if ($char === '(' && !$bracket) {
                $bracket = new BracketPart();
                $subject[] = $bracket;
                continue;
            }

            // Inside bracket
            if ($bracket && $char === '(') {
                $bracket->addContent($char);
                $depth++;
                continue;
            }

            if ($bracket && $char === ')' && $depth > 0) {
                $bracket->addContent($char);
                $depth--;
                continue;
            }

            if ($bracket && !\in_array($char, ['(', ')'], true)) {
                $bracket->addContent($char);
                continue;
            }

            // End bracket
            if ($char === ')' && $bracket && $depth === 0) {
                $bracket->finalize();
                $bracket = false;
                continue;
            }

            $subject[] = $char;
        }

        // RID OF WHITESPACES
        $subject = array_filter($subject, static fn($char) => !\is_string($char) || !ctype_space($char));

        //DETECT LOGICAL OPERATORS
        $originalSubject = $subject;
        $subject = [];
        $skip = 0;
        foreach ($originalSubject as $pos => $char) {
            if ($skip) {
                $skip--;
                continue;
            }

            $next = $originalSubject[$pos + 1] ?? false;
            $nextNext = $originalSubject[$pos + 2] ?? false;
            $three = false;
            $two = false;

            if (\is_string($char) && \is_string($next) && \is_string($nextNext)) {
                $three = $char . $next . $nextNext;
            }

            if (\is_string($char) && \is_string($next)) {
                $two = $char . $next;
            }

            // 3 digit hit
            if (\in_array($three, Condition::ALLOWED_OPERATORS, true)) {
                $subject[] = new OperatorPart($three);
                $skip = 2;
                continue;
            }

            // 2 digit hit
            if (\in_array($two, Condition::ALLOWED_OPERATORS, true)) {
                $subject[] = new OperatorPart($two);
                $skip = 1;
                continue;
            }

            $subject[] = $char;
        }


        // DETECT VARIABLES, CALLBACKS, ETC.
        $originalSubject = $subject;
        $subject = [];
        $variable = false;
        foreach ($originalSubject as $char) {
            // Start of variable
            if (\is_string($char) && !$variable) {
                $variable = new VariablePart();
                $variable->addContent($char);
                $subject[] = $variable;
                continue;
            }

            // Inside variable
            if ($variable && \is_string($char)) {
                $variable->addContent($char);
                continue;
            }

            // End of variable
            if (!\is_string($char) && $variable) {

                if ($char instanceof BracketPart) {
                    $variable->setArguments($char);
                    $variable = false;

                    continue;
                }

                $variable = false;
            }

            $subject[] = $char;
        }

        return $subject;
    }

    public static function structureCondition(array $subjects): ConditionGroup
    {
        $conditionGroup = new ConditionGroup($subjects);

        // DIVIDE IN OR GROUPS
        if ($conditionGroup->hasOr()) {
            $orGroup = [];
            $orGroups = [];
            foreach ($conditionGroup->getContents() as $atom) {

                if (!$atom instanceof OperatorPart || $atom->getType() !== 'or') {
                    $orGroup[] = $atom;
                    continue;
                }

                $orGroups[] = $orGroup;
                $orGroup = [];
            }

            $conditionGroup = new ConditionGroup($orGroups);
        }

        // DIVIDE IN AND GROUPS
        if ($conditionGroup->hasAnd()) {
            $andGroup = [];
            $andGroups = [];
            foreach ($conditionGroup->getContents() as $atom) {

                if (!$atom instanceof OperatorPart || $atom->getType() !== 'and') {
                    $andGroup[] = $atom;
                    continue;
                }

                $andGroups[] = $andGroup;
                $andGroup = [];
            }

            $conditionGroup = new ConditionGroup($andGroups);
        }

        if (\count($conditionGroup->getContents()) !== 3) {
            throw new LiteException('Condition group must have exactly 3 atoms.');
        }

        return $conditionGroup;
    }

    public static function populateConditionGroup(
        ConditionGroup|AbstractPart $conditionGroup,
        VarFactory $varFactory,
        AbstractStatement $statement,
    ): Condition {
        $conditionParts = [];
        foreach ($conditionGroup->getContents() as $conditionPart) {
            if ($conditionPart instanceof VariablePart) {
                $conditionParts[] = $varFactory->create($statement, $conditionPart->getIdentifier());

                continue;
            }

            if ($conditionPart instanceof StringPart) {
                $conditionParts[] = new StaticValue($conditionPart->getChars(), $conditionPart->getEscapeType());

                continue;
            }

            if ($conditionPart instanceof BracketPart) {
                if (\count($conditionPart->getContents()) === 1) {
                    $conditionParts[] = new StaticValue($conditionPart->getContents()[0]->toString(), 1);

                    continue;
                }

                $conditionParts[] = self::populateConditionGroup($conditionPart, $varFactory, $statement);

                continue;
            }

            if ($conditionPart instanceof OperatorPart) {
                $conditionParts[] = $conditionPart->toString();

                continue;
            }

            throw new LiteException('Could not identify condition part: ' . $conditionPart::class);
        }

        if (!\in_array($conditionParts[1], Condition::ALLOWED_OPERATORS, true)) {
            throw new LiteException('Expected second part of condition to be operator');
        }

        if (\count($conditionParts) !== 3) {
            throw new LiteException('Condition group must have exactly 3 atoms.');
        }

        return new Condition($conditionParts[1], $conditionParts[0], $conditionParts[2]);
    }
}
