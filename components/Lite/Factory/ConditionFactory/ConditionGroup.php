<?php

namespace Gepf\Lite\Factory\ConditionFactory;

use Gepf\Lite\Factory\ConditionFactory\ConditionParts\OperatorPart;

readonly class ConditionGroup
{
    public function __construct(private array $contents) {}

    public function hasOr(): bool
    {
        foreach ($this->getContents() as $atom) {
            if ($atom instanceof OperatorPart && $atom->getType() === 'or') {
                return true;
            }
        }

        return false;
    }

    public function getContents(): array
    {
        return $this->contents;
    }

    public function hasAnd(): bool
    {
        foreach ($this->getContents() as $atom) {
            if ($atom instanceof OperatorPart && $atom->getType() === 'and') {
                return true;
            }
        }

        return false;
    }
}
