<?php

namespace Gepf\Lite\Factory\ConditionFactory\ConditionParts;

class AbstractPart
{
    protected array $contents = [];
    private string $chars = '';

    public function toString(): string
    {
        if ($this->getChars()) {
            return $this->getChars();
        }

        $collectedStrings = array_map(static fn($content) => \is_scalar($content) ? $content : $content->toString(), $this->contents);

        return implode('', $collectedStrings);
    }

    public function getChars(): string
    {
        return $this->chars;
    }

    public function setChars(string $chars): void
    {
        $this->chars = $chars;
    }

    public function getContents(): array
    {
        return $this->contents;
    }

    public function addContent(string|AbstractPart $content): void
    {
        $this->contents[] = $content;
    }

    public function addChar(string $char): void
    {
        $this->chars .= $char;
    }
}
