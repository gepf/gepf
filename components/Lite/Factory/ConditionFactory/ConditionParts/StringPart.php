<?php

namespace Gepf\Lite\Factory\ConditionFactory\ConditionParts;

class StringPart extends AbstractPart
{
    public const ESCAPE_TYPE_SINGLE = 1;
    public const ESCAPE_TYPE_DOUBLE = 2;

    public function __construct(private readonly int $escapeType) {}

    public function toString(): string
    {
        $escape = $this->getEscapeType() === self::ESCAPE_TYPE_SINGLE ? "'" : '"';

        return $escape . parent::toString() . $escape;
    }

    public function getEscapeType(): int
    {
        return $this->escapeType;
    }

    public function addContent(string|AbstractPart $content): void
    {
        parent::addContent($content);
        $this->addChar($content);
    }
}
