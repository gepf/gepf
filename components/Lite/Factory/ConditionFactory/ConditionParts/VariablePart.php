<?php

namespace Gepf\Lite\Factory\ConditionFactory\ConditionParts;

class VariablePart extends AbstractPart
{
    private string $arguments;

    public function addContent(string|AbstractPart $content): void
    {
        parent::addContent($content);
        $this->addChar($content);
    }

    public function getIdentifier(): string
    {
        if ($this->hasArguments()) {
            return $this->getChars() . $this->getArguments();
        }

        return $this->getChars();
    }

    public function hasArguments(): bool
    {
        return isset($this->arguments);
    }

    public function getArguments(): string
    {
        return $this->arguments;
    }

    public function setArguments(BracketPart $bracket): void
    {
        $this->arguments = $bracket->toString();
    }
}
