<?php

namespace Gepf\Lite\Factory\ConditionFactory\ConditionParts;

class OperatorPart extends AbstractPart
{
    public function __construct(string $type)
    {
        $this->setChars($type);
    }

    public function getType(): string
    {
        return $this->toString();
    }
}
