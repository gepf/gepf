<?php

namespace Gepf\Lite\Factory\ConditionFactory\ConditionParts;

use Gepf\Lite\Factory\ConditionFactory;

class BracketPart extends AbstractPart
{
    public function toString(): string
    {
        return '(' . parent::toString() . ')';
    }

    public function finalize(): void
    {
        $this->contents = ConditionFactory\ConditionFactory::digestChars($this->contents);
    }
}
