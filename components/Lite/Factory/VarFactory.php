<?php

namespace Gepf\Lite\Factory;

use Gepf\Core\Registry\Registry;
use Gepf\Core\Util\Property;
use Gepf\Lite\Entity\AbstractStatement;
use Gepf\Lite\Entity\Variable;
use Gepf\Lite\Entity\VariablePart;
use Gepf\Lite\Entity\VariableRegistryItem;
use Gepf\Lite\Exception\LiteException;
use Gepf\Lite\Exception\NotFoundException;
use Gepf\Lite\LiteConfig;

readonly class VarFactory
{
    public function __construct(
        private Registry $registry,
        private LiteConfig $config,
    ) {}


    //$template statement may look like '{{ get(42, 'abc').result }}'
    public function create(
        AbstractStatement $statement,
        string $variableIdentifier,
        array $inheritedVariables = [],
    ): Variable
    {
        // ['response', 'getCode']
        $parts = $this->getParts($variableIdentifier);

        // 'GLOBAL', 'DATA', 'CLOSURE' or 'SET_IN_TEMPLATE'
        $registryItem = $this->getRegistryItem($parts[0], $inheritedVariables);

        // 'SCALAR', 'OBJECT' or 'FUNCTION'
        $type = $this->guessType($registryItem, \count($parts) > 1);

        // remove first item, since it is the registry item
        array_shift($parts);

        // '$response->getCode()'
        $phpExpression = $this->replaceChainingSigns($parts, $registryItem);

        return new Variable(
            statement: $statement,
            chain: $variableIdentifier,
            parts: $parts,
            registryItem: $registryItem,
            type: $type,
            phpExpression: $phpExpression,
        );
    }


    /** @return VariablePart[] */
    private function getParts(string $chain): array
    {
        // save arguments for later; they will be reinserted after chain exploded
        // NOTE: strategy for masking dots only in arguments would have this RegEx: /(?:\().*?(\.).*?(?:\))/
        preg_match_all('/\((.*?)\)/', $chain, $matches);
        $arguments = $matches[0] ?? [];

        // replace arguments (brackets and everything between) with non-allowed mask (/), to prevent collision with chain sign
        $chainWithMaskedArgs = preg_replace('/\((.*?)\)/', '(/)', $chain);

        $partsWithMaskedArgs = explode($this->config->getObjectChainSign(), $chainWithMaskedArgs);

        // re-add arguments by iterating them, amount and order of arguments will match
        $variableParts = [];
        foreach ($partsWithMaskedArgs as $part) {
            if (!str_contains($part, '(/)')) {
                $variableParts[] = new VariablePart($part);
                continue;
            }

            $argumentString = array_shift($arguments);
            $argumentString = $this->trimArgumentString($argumentString);
            $variableParts[] = new VariablePart(str_replace('(/)', '', $part), $argumentString);
        }

        return $variableParts;
    }


    private function getRegistryItem(VariablePart $firstVarPart, array $inheritedVariables): VariableRegistryItem
    {
        if (isset($inheritedVariables[$firstVarPart->getName()])) {
            return new VariableRegistryItem(
                $firstVarPart->getName(),
                VariableRegistryItem::TYPE_SET_IN_TEMPLATE,
                $inheritedVariables[$firstVarPart->getName()],
                $firstVarPart->getArguments(),
            );
        }

        if ($this->registry->getData()->hasKey($firstVarPart->getName())) {
            return new VariableRegistryItem(
                $firstVarPart->getName(),
                VariableRegistryItem::TYPE_DATA,
                $this->registry->getData()->get($firstVarPart->getName()),
                $firstVarPart->getArguments(),
            );
        }

        if (Property::exists($this->registry, $firstVarPart->getName())) {
            return new VariableRegistryItem(
                $firstVarPart->getName(),
                VariableRegistryItem::TYPE_GLOBAL,
                Property::access($this->registry, $firstVarPart->getName()),
                $firstVarPart->getArguments(),
            );
        }

        if ($this->registry->getCallbacks()->hasKey($firstVarPart->getName())) {
            return new VariableRegistryItem(
                $firstVarPart->getName(),
                VariableRegistryItem::TYPE_CLOSURE,
                $this->registry->getCallbacks()->get($firstVarPart->getName()),
                $firstVarPart->getArguments(),
            );
        }

        throw new NotFoundException("{$firstVarPart->getName()} not found in value registry.");
    }


    private function guessType(VariableRegistryItem $registryItem, bool $hasChaining): int
    {
        if ($hasChaining && $registryItem->getType() === VariableRegistryItem::TYPE_CLOSURE) {
            throw new \RuntimeException('Chained template variables cannot be registered as callback');
        }

        if (!$hasChaining && $registryItem->getType() === VariableRegistryItem::TYPE_GLOBAL) {
            throw new \RuntimeException('Global registry items that are scalar can not be used in templates');
        }

        if ($registryItem->getType() === VariableRegistryItem::TYPE_CLOSURE) {
            return Variable::TYPE_FUNCTION;
        }

        return $hasChaining ? Variable::TYPE_OBJECT : Variable::TYPE_SCALAR;
    }


    /** @param VariablePart[] $varParts */
    private function replaceChainingSigns(array $varParts, VariableRegistryItem $registryItem): string
    {
        $parsedVar = match ($registryItem->getType()) {
            VariableRegistryItem::TYPE_GLOBAL => '$' . $registryItem->getName(),
            VariableRegistryItem::TYPE_DATA => '$data->get(\'' . $registryItem->getName() . '\')',
            VariableRegistryItem::TYPE_CLOSURE => $this->getCallbackPhpExpression($registryItem),
            VariableRegistryItem::TYPE_SET_IN_TEMPLATE => $registryItem->getContent(),
            default => throw new LiteException('Unknown registry item type'),
        };
        foreach ($varParts as $varPart) {
            // TODO argument support; getter or array item distinguishing
            $parsedVar .= '->' . $varPart->getGetter() . '()';
        }

        return $parsedVar;
    }


    private function trimArgumentString(string $argumentString): string
    {
        return str_replace(["\'", '(', ')'], ["'", '', ''], $argumentString);
    }

    private function getCallbackPhpExpression(VariableRegistryItem $registryItem): string
    {
        return
            '$callbacks[\'' . $registryItem->getName() . '\']('
            . implode(',', $registryItem->getArguments())
            . ')';
    }
}
