<?php

namespace Gepf\Lite\Factory;

use Gepf\Core\RegEx\RegExFactory;
use Gepf\Lite\Entity\AbstractStructuralStatement;
use Gepf\Lite\Entity\ConditionStatement;
use Gepf\Lite\Exception\LiteException;

class ControlStructureFactory
{
    public const TEST = '(?<before>[\s\S]*?)(?<statement>(?<if>{%[\s]*?if(?<condition>(?:"[\s\S]*?"|\'[\s\S] *?\'|[\s\S]*?)*?)%})|(?<endif>{%[\s]*?endif[\s]*?%}))(?<after>[\s\S]*$)';


    /** @return ConditionStatement[] */
    public function createStructure(
        string $content,
        ?AbstractStructuralStatement $parentOrNeighbour,
        ?AbstractStructuralStatement $parent,
        int $childOrNext,
    ): array {
        $nextOpeningOrClosing = RegExFactory::getFirstOccurrenceCaptureGroups(self::TEST, $content);

        if (!$nextOpeningOrClosing) {
            return [];
        }

        $statements = [];
        if ($nextOpeningOrClosing->getValue('if')) {

            $statement = new ConditionStatement($nextOpeningOrClosing->getValue('if'), '{%', '%}');
            $statement->setRegexMatch($nextOpeningOrClosing);

            if ($parent === null) {
                $statement->setParent(null);
            }

            $childrenAndNeighbours = $this->createStructure($nextOpeningOrClosing->getValue('after'), $statement, $parentOrNeighbour, 1);

            $neighbours = [];
            // $children = [];
            if ($childrenAndNeighbours) {
                foreach ($childrenAndNeighbours as $childOrNeighbour) {
                    if (!$childOrNeighbour->hasParent()) {
                        $neighbours[] = $childOrNeighbour;
                    }
//                    else {
//                        # children are omitted, since they have their parent set
//                        $children[] = $childOrNeighbour;
//                    }
                }

                $statements = array_merge($statements, $neighbours);
            }

            $statements[] = $statement;
        } elseif ($nextOpeningOrClosing->getValue('endif')) {

            if (!$parentOrNeighbour) {
                throw new LiteException('endif statement too early');
            }

            // endif followed by endif: break out
            if ($childOrNext === 2) {

                $subject = $parentOrNeighbour->getParent();
                $body = '';
                foreach ($subject->getChildren() as $child) {
                    $body .= $child->getRegexMatch()->getValue('before');
                    $body .= $child->getRegexMatch()->getValue('statement');
                    $body .= $child->getEndRegexMatch()->getValue('before');
                    $body .= $child->getEndRegexMatch()->getValue('endif');
                }

                $subject->setEndRegexMatch($nextOpeningOrClosing);
                $subject->setBody($body);

                return [];
            }

            if ($childOrNext === 1 && $parent === null) {
                $parentOrNeighbour->setParent(null);
            } elseif ($parent && !$parent->hasBody()) {
                $parentOrNeighbour->setParent($parent);
            } elseif ($parent && $parent->hasParent()) {
                $parentOrNeighbour->setParent($parent->getParent());
            }

            $parentOrNeighbour->setBody($nextOpeningOrClosing->getValue('before'));
            $parentOrNeighbour->setEndRegexMatch($nextOpeningOrClosing);

            $next = $this->createStructure($nextOpeningOrClosing->getValue('after'), $parentOrNeighbour, $parent, 2);
            $statements = array_merge($statements, $next);
        }

        return $statements;
    }
}