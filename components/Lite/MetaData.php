<?php

namespace Gepf\Lite;

class MetaData
{
    public function parse(Template $template): void
    {
        // content might not be loaded yet, if template is a parent of another template
        if ($template->hasChild()) {
            $template->loadContent();
        }

        $body = $template->getContent();

        if (str_contains($body, '{__do_not_inherit}')) {
            $template->setInherit(false);
            $body = str_replace('{__do_not_inherit}', '', $body);
        }

        // TODO particular inheritance
        if (str_contains($body, '{inherit')) {
            $template->setParticularInherit('');
        }

        if ($template->isInherit()) {
            $inheritPath = $template->getParticularInherit();
            if (!$inheritPath) {
                $inheritPath = $template->findParent();
            }

            $parent = $template->createInheritedTemplate($inheritPath);
            $template->setParent($parent);
        }

        // TODO find better way to mark inheritance
        //$template->setResolvedValuesCompiledContent($body);
    }
}
