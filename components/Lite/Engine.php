<?php

/*| LITE
 *|------
 *| Lite Is a Template Engine
*/

namespace Gepf\Lite;

use Gepf\Cache\Cache;
use Gepf\Cache\CacheInvalidator;
use Gepf\Cache\CacheInvalidatorFactory;
use Gepf\Core\DependencyInjection\ConfigurableService;
use Gepf\Core\DependencyInjection\Container\Container;
use Gepf\Core\FileSystem\File;
use Gepf\Core\FileSystem\FileCollection;
use Gepf\Core\FileSystem\Util\FileSystem;
use Gepf\Core\Registry\Registry;
use Gepf\Core\Routing\ViewTemplateFile;
use Gepf\Core\View\ViewCompilerInterface;
use Gepf\Service\ServiceConfigInterface;
use Psr\Log\LoggerInterface;

class Engine extends ConfigurableService implements ViewCompilerInterface
{
    private Renderer $renderer;
    private bool $useCache = false;

    public function __construct(Registry $registry, LoggerInterface $logger, LiteConfig $config = null)
    {
        $this->renderer = new Renderer($registry, $logger, $config ?? new LiteConfig());
    }


    /**
     * @param LiteConfig $serviceConfig
     */
    public static function create(Container $container, ServiceConfigInterface $serviceConfig): self
    {
        return new self($container->getRegistry(), $container->getLogger(), $serviceConfig);
    }


    public static function renderRightAway(Registry $registry, string $templatePath, LoggerInterface $logger): string
    {
        $template = new Template(new File($templatePath));

        $lite = new self($registry, $logger);
        $lite->renderer->renderFully($template);

        return $lite->renderer->getResolvedValuesOutput();
    }


    public function compile(Registry $registry, ?ViewTemplateFile $view): string
    {
        if (!$view) {
            throw new \RuntimeException('No template provided for lite');
        }

        $template = new Template($view->getFile());
        $targetPath = getTmpDir() . '/compile/' . $template->getUniqueId() . '.php';

        $phpResult = null;
        $cachedResult = null;
        $fullResult = null;

        if ($this->checkRenderToPhp()) {
            if (!file_exists($targetPath) || isDevelopment()) {
                $this->renderer->renderToPhp($template);
            }

            if (!file_exists($targetPath)) {
                FileSystem::put($targetPath, $this->renderer->getPhpExpressionOutput());
            }

            ob_start();
            include $targetPath;
            $phpResult = ob_get_clean();
        }

        if ($this->useCache) {
            $cacheKey = str_replace(['.', '/'], ['_', '.'], $template->getFile()->getPathname());
            $callback = function () use ($template) {
                $this->renderer->renderFully($template);
                return $this->renderer->getResolvedValuesOutput();
            };
            $cacheInvalidator = $this->createCacheInvalidator($template);
            $cache = new Cache();
            $cachedResult = $cache->get($cacheKey, $callback, $cacheInvalidator);
        }

        if ($this->checkRenderFully()) {
            $this->renderer->renderFully($template);
            $fullResult = $this->renderer->getResolvedValuesOutput();
        }

        if ($phpResult !== $fullResult && isDevelopment()) {
            FileSystem::put($targetPath, $this->renderer->getPhpExpressionOutput());
        }

        return $fullResult ?? $phpResult ?? $cachedResult;
    }


    public function requiresTemplate(): bool
    {
        return true;
    }


    private function checkRenderToPhp(): bool
    {
        // In dev mode, php results output (from output buffer) can be compared against full rendering
        if (isDevelopment()) {
            return true;
        }

        if (!$this->useCache) {
            return true;
        }

        return false;
    }


    private function checkRenderFully(): bool
    {
        // Full rendering during development allows detection of changed results when template is unchanged
        if (isDevelopment()) {
            return true;
        }

        return false;
    }


    private function createCacheInvalidator(Template $template): CacheInvalidator
    {
        $foldersToObserve = new FileCollection(['components', $template->getFile()->getPath()]);
        return CacheInvalidatorFactory::createExpiresAfter(604800, $foldersToObserve);
    }
}
