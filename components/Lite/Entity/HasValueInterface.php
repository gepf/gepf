<?php

namespace Gepf\Lite\Entity;

interface HasValueInterface
{
    public function getValue();

    public function getPhpExpression(): string;
}
