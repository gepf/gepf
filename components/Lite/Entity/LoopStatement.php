<?php

namespace Gepf\Lite\Entity;

class LoopStatement extends AbstractStatement
{
    public function __construct(
        string $masked,
        string $prefix,
        string $suffix,
        private readonly string $body,
        private readonly string $iterable,
        private readonly string $item,
        private readonly ?string $key = null,
    ) {
        $setInTemplateVariables = $key ? [$item, $key] : [$item];
        parent::__construct($masked, $prefix, $suffix, $setInTemplateVariables);
    }

    public function getKey(): ?string
    {
        return $this->key;
    }

    /* @deprecated different compiler must do this */
    public function getPhpBody(): string
    {
        return str_replace(['{{ ', '}}'], ['\' . $', ' . \''], $this->getBody());
    }

    public function getBody(): string
    {
        return $this->body;
    }

    // TODO will not work for callbacks, inherited values, etc.
    public function getPhpExpression(): string
    {
        return '$data->get(\'' . $this->getIterable() . '\') as $' . $this->getItem();
    }

    public function getIterable(): string
    {
        return $this->iterable;
    }

    public function getItem(): string
    {
        return $this->item;
    }

    public function getVariableIdentifiers(): array
    {
        return [$this->getIterable()];
    }
}
