<?php

namespace Gepf\Lite\Entity;

use Gepf\Lite\Exception\LiteException;

/**
 * Condition works with strict left-right principle.
 * This works, because cond1 AND cond2 AND cond3
 * becomes cond1 AND (cond2 AND cond3)
 *
 * Other examples:
 * var1 + var2 becomes cond != false
 */
class Condition extends AbstractOperation
{
    public const ALLOWED_OPERATORS = ['===', '==', '!==', '!=', '>=', '<=', '&&', '||', 'and', 'or'];

    /** @noinspection TypeUnsafeComparisonInspection */
    public function solve(): bool
    {
        $left = $this->getLeft()->getValue();
        $right = $this->getRight()->getValue();

        switch ($this->getOperator()) {
            case '===':
                return $left === $right;
            case '==':
                return $left == $right;
            case '!==':
                return $left !== $right;
            case '!=':
                return $left != $right;
            case '>=':
                return $left >= $right;
            case '<=':
                return $left <= $right;
            case 'and':
            case '&&':
                return $left && $right;
            case 'or':
            case '||':
                return $left || $right;
        }

        throw new LiteException('Wrong operator for condition used');
    }

    public function getValue(): bool
    {
        return $this->solve();
    }

    public function getPhpExpression(): string
    {
        $leftExpr = $this->getLeft()->getPhpExpression();
        $rightExpr = $this->getRight()->getPhpExpression();

        return "$leftExpr {$this->getOperator()} ($rightExpr)";
    }
}
