<?php

namespace Gepf\Lite\Entity;

class AbstractStatement
{
    public function __construct(
        private readonly string $masked,
        private readonly string $prefix,
        private readonly string $suffix,
        private readonly array $setInTemplateVariableNames = [],
    ) {}

    public function getMasked(): string
    {
        return $this->masked;
    }

    public function getUnmasked(): string
    {
        $preAndSuffix = [$this->getPrefix(), $this->getSuffix()];
        $removedSigns = str_replace($preAndSuffix, '', $this->getMasked());

        return trim($removedSigns);
    }

    public function getVariableIdentifiers(): array
    {
        return [];
    }

    public function getPrefix(): string
    {
        return $this->prefix;
    }

    public function getSuffix(): string
    {
        return $this->suffix;
    }

    public function hasSetInTemplateVariableName(string $search): bool
    {
        return \in_array($search, $this->setInTemplateVariableNames, true);
    }
}
