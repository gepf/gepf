<?php

namespace Gepf\Lite\Entity;

use Gepf\Lite\Exception\LiteException;

class MathResult extends AbstractOperation
{
    public const ALLOWED_OPERATORS = ['+', '-', '*', '/', '%'];

    public function getValue()
    {
        $left = $this->getLeft()->getValue();
        $right = $this->getRight()->getValue();

        switch ($this->getOperator()) {
            case '+':
                return $left + $right;
            case '-':
                return $left - $right;
            case '*':
                return $left * $right;
            case '/':
                return $left / $right;
            case '%':
                return $left % $right;
        }

        throw new LiteException('Wrong operator for math used');
    }

    public function getPhpExpression(): string
    {
        $leftExpr = $this->getLeft()->getPhpExpression();
        $rightExpr = $this->getRight()->getPhpExpression();

        return "$leftExpr {$this->getOperator()} ($rightExpr)";
    }
}
