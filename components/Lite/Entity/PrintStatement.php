<?php

/**
 * @noinspection PhpUnused
 * warnings disabled for now
 * TODO have variables work as operations in print statement
 * eg {{ foo - bar }}
 */

namespace Gepf\Lite\Entity;

/**
 * Only simple one-variable statements, eg {{ variableName }}, are supported
 */
class PrintStatement extends AbstractStatement
{
    /**
     * @var Variable[]
     */
    private array $variables;

    public const ALLOWED_OPERATORS = ['(', ')', '+', '-', '*', '/', '%', '==', '!=', '>=', '<=', '>', '<'];

    public function addVariable(Variable $variable): void
    {
        $this->variables[] = $variable;
    }

    public function getVariables(): array
    {
        return $this->variables;
    }

    public function getVariableIdentifiers(): array
    {
        return [$this->getUnmasked()];
    }
}
