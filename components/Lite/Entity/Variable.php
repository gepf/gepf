<?php

namespace Gepf\Lite\Entity;

use Gepf\Core\Util\Property;

class Variable implements HasValueInterface
{
    public const TYPE_SCALAR = 1;
    public const TYPE_OBJECT = 2;
    public const TYPE_FUNCTION = 3;

    private mixed $value;

    public function __construct(
        private readonly AbstractStatement $statement,
        private readonly string $chain,
        private readonly array $parts,
        private readonly VariableRegistryItem $registryItem,
        private readonly int $type,
        private readonly string $phpExpression,
        private readonly ?AbstractStatement $parentStatement = null,
    ) {}

    public function getStatement(): AbstractStatement
    {
        return $this->statement;
    }

    public function getChain(): string
    {
        return $this->chain;
    }

    public function getParts(): array
    {
        return $this->parts;
    }

    public function getRegistryItem(): VariableRegistryItem
    {
        return $this->registryItem;
    }

    public function getType(): int
    {
        return $this->type;
    }

    public function getPhpExpression(): string
    {
        return $this->phpExpression;
    }

    public function getParentStatement(): AbstractStatement
    {
        return $this->parentStatement;
    }

    public function resolveValue(): mixed
    {
        $currentObject = $this->getRegistryItem()->getValue();
        /** @var VariablePart $variablePart */
        foreach ($this->getParts() as $variablePart) {
            $currentObject = Property::access($currentObject, $variablePart->getName(), $variablePart->getArguments());
        }

        $this->value = $currentObject;

        return $this->value;
    }

    public function getValue(): mixed
    {
        return $this->value ?? $this->resolveValue();
    }
}
