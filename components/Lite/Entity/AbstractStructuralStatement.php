<?php

namespace Gepf\Lite\Entity;

use Gepf\Core\RegEx\RegExMatch;

class AbstractStructuralStatement extends AbstractStatement
{
    private string $body;

    private ?ConditionStatement $parent;

    /** @var ConditionStatement[] */
    private array $children;

    private RegExMatch $regexMatch;

    private RegExMatch $endRegexMatch;


    public function hasBody(): bool
    {
        return isset($this->body);
    }

    public function getBody(): string
    {
        return $this->body;
    }

    public function setBody(string $body): void
    {
        $this->body = $body;
    }

    public function hasParent(): bool
    {
        return isset($this->parent);
    }

    public function getParent(): AbstractStructuralStatement
    {
        return $this->parent;
    }

    public function setParent(?AbstractStructuralStatement $parent): void
    {
        $parent?->addChild($this);

        $this->parent = $parent;
    }

    public function addChild(AbstractStructuralStatement $child): void
    {
        $this->children[] = $child;
    }

    public function getChildren(): array
    {
        return $this->children;
    }

    public function getRegexMatch(): RegExMatch
    {
        return $this->regexMatch;
    }

    public function setRegexMatch(RegExMatch $regexMatch): void
    {
        $this->regexMatch = $regexMatch;
    }

    public function getEndRegexMatch(): RegExMatch
    {
        return $this->endRegexMatch;
    }

    public function setEndRegexMatch(RegExMatch $endRegexMatch): void
    {
        $this->endRegexMatch = $endRegexMatch;
    }
}
