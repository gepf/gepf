<?php

namespace Gepf\Lite\Entity;

class VariableRegistryItem
{
    public const TYPE_GLOBAL = 1;
    public const TYPE_DATA = 2;
    public const TYPE_CLOSURE = 3;
    public const TYPE_SET_IN_TEMPLATE = 4;

    public const TYPES = [
        self::TYPE_GLOBAL,
        self::TYPE_DATA,
        self::TYPE_CLOSURE,
        self::TYPE_SET_IN_TEMPLATE,
    ];

    public function __construct(
        private readonly string $name,
        private readonly int $type,
        private readonly mixed $content,
        private readonly array $arguments,
    ) {
        if (!\in_array($type, self::TYPES)) {
            throw new \LogicException('Unknown Type ' . $type . ' for ' . self::class);
        }
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getType(): int
    {
        return $this->type;
    }

    public function getContent(): mixed
    {
        return $this->content;
    }

    public function getArguments(): array
    {
        return $this->arguments;
    }

    public function getValue(): mixed
    {
        switch ($this->getType()) {
            case self::TYPE_DATA:
            case self::TYPE_GLOBAL:
            case self::TYPE_SET_IN_TEMPLATE:
                return $this->getContent();
            case self::TYPE_CLOSURE:
                return $this->getValueFromClosure();
        }

        throw new \LogicException('Unknown Type ' . $this->getType() . ' for ' . self::class);
    }


    private function getValueFromClosure(): mixed
    {
        $arguments = array_map(
            static fn(string $argument) => str_replace("'", '', $argument),
            $this->getArguments(),
        );
        return $this->getContent()(...$arguments);
    }
}
