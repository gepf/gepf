<?php

namespace Gepf\Lite\Entity;

use Gepf\Lite\Factory\ConditionFactory\ConditionParts\StringPart;

readonly class StaticValue implements HasValueInterface
{
    public function __construct(private string|int|float|bool|null $value, private int $originalEscapeType) {}

    public function getPhpExpression(): string
    {
        $escape = $this->getOriginalEscapeType() === StringPart::ESCAPE_TYPE_SINGLE ? "'" : '"';

        return $escape . $this->getValue() . $escape;
    }

    public function getOriginalEscapeType(): int
    {
        return $this->originalEscapeType;
    }

    public function getValue(): float|bool|int|string|null
    {
        return $this->value;
    }
}
