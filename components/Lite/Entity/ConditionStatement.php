<?php

namespace Gepf\Lite\Entity;

class ConditionStatement extends AbstractStructuralStatement
{
    private readonly Condition $condition;

    public function __construct(
        string $masked,
        string $prefix,
        string $suffix,
    ) {
        parent::__construct($masked, $prefix, $suffix);
    }

    /* @deprecated different compiler must do this */
    public function getPhpBody(): string
    {
        return str_replace(['{{ ', '}}'], ['\' . $', ' . \''], $this->getBody());
    }

    public function getPhpExpression(): string
    {
        return $this->getCondition()->getPhpExpression();
    }

    public function getCondition(): Condition
    {
        return $this->condition;
    }

    public function setCondition(Condition $condition): void
    {
        $this->condition = $condition;
    }

    public function getTemplateCondition(): string
    {
        return trim(str_replace('if', '', $this->getUnmasked()));
    }
}
