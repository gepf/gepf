<?php

namespace Gepf\Lite\Entity;

abstract class AbstractOperation implements HasValueInterface
{
    public function __construct(
        private readonly string $operator,
        private readonly Condition|Variable|MathResult|StaticValue $left,
        private readonly Condition|Variable|MathResult|StaticValue $right,
    ) {}

    public function getOperator(): string
    {
        return $this->operator;
    }

    public function getLeft(): Variable|Condition|MathResult|StaticValue
    {
        return $this->left;
    }

    public function getRight(): Variable|Condition|MathResult|StaticValue
    {
        return $this->right;
    }
}
