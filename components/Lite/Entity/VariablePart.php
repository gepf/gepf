<?php

namespace Gepf\Lite\Entity;

readonly class VariablePart
{
    public function __construct(
        private string $name,
        private ?string $arguments = null,
    ) {}

    public function getName(): string
    {
        return $this->name;
    }

    public function getArguments(): array
    {
        if (!$this->arguments) {
            return [];
        }

        return array_map(
            static fn(string $argument) => trim($argument),
            explode(',', $this->arguments),
        );
    }

    public function getGetter(): string
    {
        return 'get' . ucfirst($this->name);
    }
}
