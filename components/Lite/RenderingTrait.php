<?php

namespace Gepf\Lite;

trait RenderingTrait
{
    public function maskSingleQuotes(): void
    {
        // RegEx required, since addcslashes() masks quotes that
        // are already masked, so does str_replace("'", "\'" ...)
        $masked = preg_replace('/(?<!\\\\)(\')/m', '\\\'', $this->getContent());
        $this->setContent($masked);
    }

    public function replace(string|array $search, string|array $replace): void
    {
        $replaced = str_replace($search, $replace, $this->getContent());
        $this->setContent($replaced);
    }
}
