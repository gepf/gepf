<?php

namespace Gepf\Lite\EntityCompiler;

use Gepf\Core\RegEx\RegExFactory;
use Gepf\Core\Util\Text;
use Gepf\Lite\Entity\LoopStatement;
use Gepf\Lite\Factory\VarFactory;
use Gepf\Lite\RenderingFragment;
use Gepf\Lite\RenderingOutput;
use Gepf\Lite\RenderingOutputInterface;

class LoopCompiler extends EntityCompiler
{
    public function compileToPhp(RenderingOutput $renderingOutput): void
    {
        $parsedStatements = RegExFactory::getCaptureGroups($this->config->getForEachStatement(), $renderingOutput->getContent());

        foreach ($parsedStatements as $parsedStatement) {
            $loopStatement = $this->createLoopStatement($renderingOutput->getContent(), $parsedStatement);

            $replaced = Text::replaceBetween($renderingOutput->getContent(), $loopStatement->getMasked(), '{% endfor %}');
            $renderingOutput->setContent($replaced);

            $forEachStart = '\';foreach(' . $loopStatement->getPhpExpression() . '){ $content .= \'' . $loopStatement->getPhpBody();
            $renderingOutput->replace($loopStatement->getMasked(), $forEachStart);
        }

        $renderingOutput->replace('{% endfor %}', '\';} $content .= \'');
    }


    public function compileFully(RenderingOutputInterface $renderingOutput): void
    {
        $parsedStatements = RegExFactory::getCaptureGroups($this->config->getForEachStatement(), $renderingOutput->getContent());

        foreach ($parsedStatements as $parsedStatement) {
            $loopStatement = $this->createLoopStatement($renderingOutput->getContent(), $parsedStatement);

            $replaced = Text::replaceBetween($renderingOutput->getContent(), $loopStatement->getMasked(), '{% endfor %}');
            $renderingOutput->setContent($replaced);

            $iterated = $this->iterateLoop($loopStatement);
            $renderingOutput->replace($loopStatement->getMasked(), $iterated);
        }

        $renderingOutput->replace('{% endfor %}', '');
    }


    public function createLoopStatement(string $content, array $parsedStatement): LoopStatement
    {
        $body = Text::getStringBetween($content, $parsedStatement[RegExFactory::KEY_MATCH], '{% endfor %}');

        return new LoopStatement(
            $parsedStatement[RegExFactory::KEY_MATCH],
            $this->config->getStmtPrefix(),
            $this->config->getStmtSuffix(),
            $body,
            $parsedStatement[RegExFactory::KEY_GROUPS]['iterable'],
            $parsedStatement[RegExFactory::KEY_GROUPS]['item'],
            $parsedStatement[RegExFactory::KEY_GROUPS]['key'],
        );
    }


    public function iterateLoop(LoopStatement $statement): string
    {
        $factory = new VarFactory($this->registry, $this->config);
        $variable = $factory->create($statement, $statement->getIterable());

        $out = '';
        foreach ($variable->getValue() as $i => $iteration) {

            $var[$statement->getItem()] = $iteration;
            if ($statement->getKey()) {
                $var[$statement->getKey()] = $i;
            }

            $fragment = new RenderingFragment($statement->getBody(), $var);

            $this->renderer->compileStatement($fragment);
            $out .= $fragment->getContent();
        }

        return $out;
    }
}
