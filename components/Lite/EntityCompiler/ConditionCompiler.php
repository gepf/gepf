<?php

namespace Gepf\Lite\EntityCompiler;

use Gepf\Lite\Entity\ConditionStatement;
use Gepf\Lite\Factory\ConditionFactory\ConditionFactory;
use Gepf\Lite\Factory\ControlStructureFactory;
use Gepf\Lite\Factory\VarFactory;
use Gepf\Lite\RenderingOutput;
use Gepf\Lite\RenderingOutputInterface;

class ConditionCompiler extends EntityCompiler
{
    public function compileToPhp(RenderingOutput $renderingOutput): void
    {
        $factory = new ControlStructureFactory();
        $conditionStatements = $factory->createStructure($renderingOutput->getContent(), null, null, 2);

        foreach ($conditionStatements as $conditionStatement) {
            $conditionStatement = $this->setCondition($conditionStatement);
            $replaced = str_replace($conditionStatement->getBody(), '', $renderingOutput->getContent());
            $renderingOutput->setContent($replaced);
            $ifStart = '\';if(' . $conditionStatement->getPhpExpression() . '){ $content .= \'' . $conditionStatement->getPhpBody();
            $renderingOutput->replace($conditionStatement->getMasked(), $ifStart);
        }

        $renderingOutput->replace(['{% endif %}', '{% else %}'], ['\';} echo\'', '\';} else { echo\'']);
    }

    public function compileFully(RenderingOutputInterface $renderingOutput): void
    {
        $factory = new ControlStructureFactory();
        $conditionStatements = $factory->createStructure($renderingOutput->getContent(), null, null, 2);

        foreach ($conditionStatements as $conditionStatement) {
            $conditionStatement = $this->setCondition($conditionStatement);
            $replaced = str_replace($conditionStatement->getBody(), '', $renderingOutput->getContent());
            $renderingOutput->setContent($replaced);
            $body = $conditionStatement->getCondition()->solve() ? $conditionStatement->getBody() : '';
            $renderingOutput->replace($conditionStatement->getMasked(), $body);
        }

        $renderingOutput->replace(['{% endif %}', '{% else %}'], '');
    }


    public function setCondition(ConditionStatement $conditionStatement): ConditionStatement
    {
        $varFactory = new VarFactory($this->registry, $this->config);
        $condition = ConditionFactory::createCondition($conditionStatement->getTemplateCondition(), $conditionStatement, $varFactory);

        $conditionStatement->setCondition($condition);

        return $conditionStatement;
    }
}
