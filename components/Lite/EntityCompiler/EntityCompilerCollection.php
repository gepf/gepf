<?php

namespace Gepf\Lite\EntityCompiler;

use Gepf\Core\Collection;
use Gepf\Core\Registry\Registry;
use Gepf\Lite\LiteConfig;
use Gepf\Lite\Renderer;
use Psr\Log\LoggerInterface;

class EntityCompilerCollection extends Collection
{
    public function __construct(
        array $compilerClassNames,
        Renderer $renderer,
        LiteConfig $config,
        Registry $registry,
        LoggerInterface $logger,
    )
    {
        $collection = [];
        foreach ($compilerClassNames as $compilerClassName) {
            $collection[] = new $compilerClassName($renderer, $config, $registry, $logger);
        }

        parent::__construct($collection);
    }

    public function current(): EntityCompiler
    {
        return parent::current();
    }
}
