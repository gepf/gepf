<?php

namespace Gepf\Lite\EntityCompiler;

use Gepf\Core\Registry\Registry;
use Gepf\Lite\LiteConfig;
use Gepf\Lite\Renderer;
use Gepf\Lite\RenderingOutput;
use Gepf\Lite\RenderingOutputInterface;
use Gepf\Lite\Template;
use Psr\Log\LoggerInterface;

abstract class EntityCompiler
{
    public function __construct(
        protected Renderer $renderer,
        protected LiteConfig $config,
        protected Registry $registry,
        protected LoggerInterface $logger,
    ) {}

    abstract public function compileToPhp(RenderingOutput $renderingOutput): void;

    abstract public function compileFully(RenderingOutputInterface $renderingOutput): void;


    protected function getStatementContent(string $openingTag, string $closingTag, Template $template): string
    {
        $postContent = $this->getOpeningTagPostContent($openingTag, $template);

        return $this->getEndTagPreContent($postContent, $closingTag);
    }


    protected function getOpeningTagPostContent(string $openingTag, Template $template): string
    {
        $postContent = explode($openingTag, $template->getContent());

        return $postContent[1];
    }


    protected function getEndTagPreContent(string $content, string $endTag): string
    {
        $preContent = explode($endTag, $content);

        return $preContent[0];
    }
}
