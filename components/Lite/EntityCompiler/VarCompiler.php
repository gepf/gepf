<?php

namespace Gepf\Lite\EntityCompiler;

use Gepf\Core\RegEx\RegExFactory;
use Gepf\Lite\Entity\PrintStatement;
use Gepf\Lite\Exception\LiteException;
use Gepf\Lite\Factory\VarFactory;
use Gepf\Lite\LiteConfig;
use Gepf\Lite\RenderingOutput;
use Gepf\Lite\RenderingOutputInterface;

class VarCompiler extends EntityCompiler
{
    public function compileToPhp(RenderingOutput $renderingOutput): void
    {
        $digest = $renderingOutput->getContent();
        $this->renderPhpExpressionsToContent($digest);
        $renderingOutput->setContent($digest);
    }


    public function compileFully(RenderingOutputInterface $renderingOutput): void
    {
        $digest = $renderingOutput->getContent();
        $this->renderVariableValuesToContent($digest, $renderingOutput->getVariables());
        $renderingOutput->setContent($digest);
    }


    private function renderPhpExpressionsToContent(string &$content): void
    {
        foreach ($this->getVariables($content, []) as $variable) {
            $content = str_replace(
                $variable->getStatement()->getMasked(),
                '\' . ' . $variable->getPhpExpression() . ' . \'',
                $content
            );
        }
    }


    public function renderVariableValuesToContent(string &$content, array $inheritedVariables): void
    {
        $processedVariables = [];
        foreach ($this->getVariables($content, $inheritedVariables) as $variable) {
            $statement = $variable->getStatement()->getMasked();
            $value = $variable->getValue();

            if (!is_scalar($value) && $value !== null) {
                throw new LiteException('Value of PrintStatement must be scalar');
            }

            // check value of statement for duplicate detection, in case previous statement has changed the result.
            // Eg <div id="line-1">{{ foo.increase(50) }}</div><div id="line-2">{{ foo.increase(50) }}</div>
            if (isset($processedVariables[$statement]) && $processedVariables[$statement] === $value) {
                continue;
            }

            if (isDevelopment() && !str_contains($content, $variable->getStatement()->getMasked())) {
                throw new LiteException("Previously parsed variable '$statement' could not be injected to rendering output");
            }

            $content = str_replace($statement, $value, $content);

            $processedVariables[$statement] = $value;
        }
    }


    private function getVariables(string $content, array $inheritedVariables): \Generator
    {
        $factory = new VarFactory($this->registry, $this->config);

        foreach (self::getRawVariableExpressions($content, $this->config) as $rawVariableExpression) {
            $statement = new PrintStatement(
                $rawVariableExpression,
                $this->config->getPrintPrefix(),
                $this->config->getPrintSuffix(),
            );

            // TODO adding the variable to the statement is not going anywhere (yet)
            //   in future, this function should not yield variables and parent function
            //   should work with multi-value PrintStatements
            foreach ($statement->getVariableIdentifiers() as $identifier) {
                $variable = $factory->create($statement, $identifier, $inheritedVariables);
                $statement->addVariable($variable);

                yield $variable;
            }
        }
    }


    private static function getRawVariableExpressions(string $compiledContent, LiteConfig $config): array
    {
        $regex = '\s?[a-zA-Z0-9(\/\"\\\\\',\s+)_>.\-\'\[\]]+\s?';

        return RegExFactory::getMatches(
            $config->getRegexPrintPrefix() . $regex . $config->getRegexPrintSuffix(),
            $compiledContent,
        );
    }
}
