<?php

namespace Gepf\Lite\EntityCompiler;

use Gepf\Lite\RenderingOutput;
use Gepf\Lite\RenderingOutputInterface;

class CommentCompiler extends EntityCompiler
{
    public function compileToPhp(RenderingOutput $renderingOutput): void
    {
        // Comments are removed anyway
        $this->compileFully($renderingOutput);
    }

    public function compileFully(RenderingOutputInterface $renderingOutput): void
    {
        $compiled = preg_replace(
            '/' . $this->config->getRegexCommentPrefix() . '[\s\S]*?' . $this->config->getRegexCommentSuffix() . '/',
            '',
            $renderingOutput->getContent(),
        );

        $renderingOutput->setContent($compiled);
    }
}
