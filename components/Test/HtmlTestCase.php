<?php /** @noinspection PhpUnhandledExceptionInspection */

namespace Gepf\Test;

use Gepf\Dom\Crawler;
use Gepf\HttpClient\LocalClient;
use Gepf\HttpClient\Response;
use PHPUnit\Framework\TestCase;

class HtmlTestCase extends TestCase
{
    private Response $response;
    private ?Crawler $crawler;

    protected static LocalClient $client;


    protected function setUp(): void
    {
        self::createLocalClient();
    }


    protected static function createLocalClient(array $options = []): void
    {
        self::$client = new LocalClient(envFile: '.env.test', options: $options);
    }


    public function request(string $path, string $method = 'GET'): void
    {
        $this->setResponse(self::$client->request($path, $method));
    }


    public function form(string $path, array $values): void
    {
        $response = self::$client->request($path, 'POST', $values);
        $this->setResponse($response);
    }


    public function getResponse(): Response
    {
        return $this->response;
    }


    public function setResponse(Response $response): void
    {
        $this->response = $response;
        $this->crawler = new Crawler($response->getBody());
    }


    public function assertResponseIsSuccessful(): void
    {
        $this::assertEquals(200, $this->response->getCode());
        $this::assertEquals(
            $this->safeHeaderComparisonString('text/html; charset=UTF-8'),
            $this->safeHeaderComparisonString($this->response->getContentType()),
        );
    }


    public function assertFormSuccessful(): void
    {
        $this::assertEquals('success', $this->crawler->getMeta('gepf:form-submission'));
    }


    public function assertUserLoggedIn(string $username): void
    {
        $this::assertEquals($username, $this->crawler->getMeta('gepf:user'));
    }


    // to lower, remove whitespaces
    private function safeHeaderComparisonString(string $stringToCompare): string
    {
        return strtolower(preg_replace('/\s+/', '', $stringToCompare));
    }
}
