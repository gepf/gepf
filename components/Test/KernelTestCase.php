<?php

namespace Gepf\Test;

use Gepf\Core\DependencyInjection\Container\Container;
use Gepf\Core\Kernel\Kernel;
use PHPUnit\Framework\TestCase;

class KernelTestCase extends TestCase
{
    protected static Kernel $kernel;
    protected static Container $container;

    protected function setUp(): void
    {
        parent::setUp();
        self::createKernel();
    }

    protected static function createKernel(string $dotEnv = '.env.test'): Kernel
    {
        if (!isset(static::$kernel)) {
            static::$kernel = Kernel::spawn($dotEnv);
        }

        static::$kernel->boot();
        static::$container = static::$kernel->getServiceContainer();

        return static::$kernel;
    }
}
