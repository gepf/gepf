<?php

/**
 * @noinspection PhpUnused
 */

namespace Gepf\Bridge;

use Gepf\Core\Registry\Registry;
use Gepf\Core\Routing\ViewTemplateFile;
use Gepf\Core\View\ViewCompilerInterface;

class Smarty implements ViewCompilerInterface
{
    private \Smarty $smarty;

    public function __construct()
    {
        $this->smarty = new \Smarty();
        $this->smarty->setCompileDir(getTmpDir() . '/compile/templates');
        $this->smarty->setCacheDir(getTmpDir() . '/cache/templates');
    }

    public function compile(Registry $registry, ?ViewTemplateFile $view): string
    {
        if (!$view) {
            throw new \RuntimeException('No template provided for Smarty');
        }

        $this->smarty->setTemplateDir($view->getRoot());

        foreach ($registry->getData()->getRegister() as $index => $var) {
            $this->smarty->assign($index, $var);
        }

        $compileId = str_ends_with($view->getFile()->getPathname(), 'index.html.tpl') ? '../index' : './index';

        $this->smarty->setCompileId($compileId);

        try {
            $html = $this->smarty->fetch($view->getFile()->getPathname(), null, $this->smarty->compile_id);
        } catch (\SmartyException|\Exception $e) {
            $html = 'html_compile_error: ' . $e;
        }

        return $html;
    }

    public function requiresTemplate(): bool
    {
        return true;
    }
}
