<?php

namespace Gepf\Bridge;

use Gepf\Core\FileSystem\File;
use Gepf\Core\FileSystem\VirtualFile;
use Gepf\Core\Util\Text;
use Gepf\Service\Asset\AssetCompilerInterface;
use Gepf\Service\Asset\Item\Asset;
use ScssPhp\ScssPhp\Compiler;
use ScssPhp\ScssPhp\Exception\SassException;
use ScssPhp\ScssPhp\OutputStyle;

class ScssPhp implements AssetCompilerInterface
{
    private Compiler $compiler;

    public function __construct()
    {
        $this->compiler = new Compiler();
    }

    public static function supportedFileTypes(): array
    {
        return [
            AssetCompilerInterface::PRIORITY_MUST => ['scss'],
        ];
    }

    /**
     * @throws SassException
     */
    public function compileAndPersist(Asset $asset): void
    {
        if ($asset->getSources()->count() > 1) {
            throw new \LogicException('CSS package has multiple sources already. Was it compiled before?');
        }

        $groundZeroFile = $asset->getSources()->get(0);

        if (!$groundZeroFile instanceof File) {
            throw new \LogicException('CSS package must be assigned to file, not directory');
        }

        $importDir = Text::pop('/', $groundZeroFile->getPathname());
        $this->compiler->setImportPaths($importDir);
        $this->compiler->setOutputStyle(OutputStyle::COMPRESSED);
        $content = $this->compiler->compileString($groundZeroFile->getContent(), $importDir)->getCss();

        $targetPathname = str_replace(['scss/', '.scss'], ['css/', '.css'], $groundZeroFile->getPathname());
        $virtualFile = new VirtualFile('public/' . $targetPathname);
        $virtualFile->setContent($content);

        $asset->setTarget($virtualFile->persist());
    }

    public function getPostCompileIdentifier(Asset $asset, string $sourceIdentifier): string
    {
        return str_replace(['scss/', '.scss'], ['css/', '.css'], $sourceIdentifier);
    }
}
