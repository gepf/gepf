<?php

namespace Gepf\Bridge\Twig;

use Gepf\Core\DependencyInjection\ConfigurableService;
use Gepf\Core\Registry\Registry;
use Gepf\Core\Routing\ViewTemplateFile;
use Gepf\Core\View\ViewCompilerInterface;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Loader\FilesystemLoader;

class Twig extends ConfigurableService implements ViewCompilerInterface
{
    /**
     * @throws SyntaxError|RuntimeError|LoaderError
     */
    public function compile(Registry $registry, ?ViewTemplateFile $view): string
    {
        if (!$view) {
            throw new \RuntimeException('No template provided for twig');
        }

        $twig = new Environment(new FilesystemLoader($view->getRoot()));
        $twig->addGlobal('request', $registry->getRequest());
        $twig->addGlobal('response', $registry->getResponse());
        $twig->addExtension(new FunctionsExtension($registry->getCallbacks()));
        $twig->enableStrictVariables();

        if (isProduction()) {
            $twig->setCache(getTmpDir() . '/cache/twig');
        }

        if (isDevelopment()) {
            $twig->enableDebug();
        }

        return $twig->render($view->getRelativePath(), $registry->getData()->getRegister());
    }

    public function requiresTemplate(): bool
    {
        return true;
    }
}
