<?php

namespace Gepf\Bridge\Twig;

use Gepf\Core\Registry\CallbackRegistry;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class FunctionsExtension extends AbstractExtension
{
    public function __construct(private readonly CallbackRegistry $registry) {}

    public function getFunctions(): array
    {
        $twigFunctions = [];
        foreach ($this->registry->toArray() as $name => $callback) {
            $twigFunctions[] = new TwigFunction($name, $callback);
        }

        return $twigFunctions;
    }
}
