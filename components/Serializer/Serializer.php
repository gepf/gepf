<?php

namespace Gepf\Serializer;

use Gepf\Serializer\Exception\CircularReferenceException;
use Gepf\Serializer\Exception\MemoryUsageException;
use Gepf\Serializer\Exception\NestingLevelException;

class Serializer
{
    public const CIRCULAR_REFERENCE_MODE_SKIP = 1;
    public const CIRCULAR_REFERENCE_MODE_THROW = 2;
    public const CIRCULAR_REFERENCE_MODE_MARK = 3;

    public const PRIVATE_PROPERTY_MODE_SKIP = 1;
    public const PRIVATE_PROPERTY_MODE_GET = 2;
    public const PRIVATE_PROPERTY_MODE_MARK = 3;

    public const UNINITIALIZED_PROPERTY_MODE_SKIP = 1;
    public const UNINITIALIZED_PROPERTY_MODE_MARK = 2;

    public const NESTING_LEVEL_MODE_SKIP = 1;
    public const NESTING_LEVEL_MODE_THROW = 2;
    public const NESTING_LEVEL_MODE_MARK = 3;

    private int $nestingLevel = 0;
    private array $objectHashes = [];

    public function __construct(
        private readonly int $maxNestingLevel = 9,
        private readonly int $nestingLevelMode = 1,
        private readonly int $circularReferenceMode = 1,
        private readonly int $privatePropertyMode = 1,
        private readonly int $uninitializedPropertyMode = 1,
        private readonly int $maxMemoryUsage = 1048576,
    ) {}


    public function serialize(mixed $subject): array
    {
        $this->walkMixed($subject, $result);

        return $result;
    }

    public function walkMixed(mixed $input, &$output, self $parent = null): void
    {
        if ($parent) {
            $this->nestingLevel = $parent->nestingLevel + 1;
            $this->objectHashes = $parent->objectHashes;

            if ($this->nestingLevel >= $this->maxNestingLevel) {
                if ($this->nestingLevelMode === self::NESTING_LEVEL_MODE_SKIP) {
                    return;
                }

                if ($this->nestingLevelMode === self::NESTING_LEVEL_MODE_THROW) {
                    throw new NestingLevelException("Max nesting level ($this->maxNestingLevel) exceeded");
                }

                if ($this->nestingLevelMode === self::NESTING_LEVEL_MODE_MARK) {
                    $output = '...';
                    return;
                }
            }
        }

        if (memory_get_usage() > $this->maxMemoryUsage) {
            throw new MemoryUsageException("Max memory usage of $this->maxMemoryUsage exceeded");
        }

        if (\is_scalar($input) || $input === null) {
            $output = $input;
            return;
        }

        if (\is_array($input)) {
            $this->walkArray($input, $output);
            return;
        }

        if (\is_object($input)) {
            $objectHash = spl_object_hash($input);
            if (\in_array($objectHash, $this->objectHashes, true)) {
                if ($this->circularReferenceMode === self::CIRCULAR_REFERENCE_MODE_SKIP) {
                    return;
                }

                if ($this->circularReferenceMode === self::CIRCULAR_REFERENCE_MODE_THROW) {
                    throw new CircularReferenceException();
                }

                if ($this->circularReferenceMode === self::CIRCULAR_REFERENCE_MODE_MARK) {
                    $output = "#$objectHash";
                    return;
                }
            }

            $this->objectHashes[] = $objectHash;

            $this->walkObject($input, $output);
            return;
        }

        throw new \RuntimeException('could not detect type');
    }

    private function walkArray(array $array, &$output): void
    {
        $iteration = [];
        foreach ($array as $key => $row) {
            $this->proceedToNextLevel($row, $value);
            $iteration[$key] = $value;
        }
        $output = $iteration;
    }

    private function proceedToNextLevel(mixed $input, &$output): void
    {
        $child = new self(
            maxNestingLevel: $this->maxNestingLevel,
            nestingLevelMode: $this->nestingLevelMode,
            circularReferenceMode: $this->circularReferenceMode,
            privatePropertyMode: $this->privatePropertyMode,
            uninitializedPropertyMode: $this->uninitializedPropertyMode,
            maxMemoryUsage: $this->maxMemoryUsage,
        );
        $child->walkMixed($input, $output, $this);
    }

    private function walkObject(object $object, &$output): void
    {
        $reflectionClass = new \ReflectionClass(\get_class($object));
        $iteration = [];
        foreach ($reflectionClass->getProperties() as $property) {

            if ($property->isPrivate()) {
                switch ($this->privatePropertyMode) {
                    case self::PRIVATE_PROPERTY_MODE_SKIP:
                        continue 2;
                    case self::PRIVATE_PROPERTY_MODE_GET:
                        break;
                    case self::PRIVATE_PROPERTY_MODE_MARK:
                        $iteration[$property->getName()] = 'private';
                }
            }

            $this->accessProperty($property, $object, $iteration);
        }

        $output[$reflectionClass->getName()] = $iteration;
    }


    private function accessProperty($property, $object, &$output): void
    {
        if ($property->isInitialized($object)) {
            $this->proceedToNextLevel($property->getValue($object), $value);

            $output[$property->getName()] = $value;
            return;
        }

        switch ($this->uninitializedPropertyMode) {
            case self::UNINITIALIZED_PROPERTY_MODE_SKIP:
                return;
            case self::UNINITIALIZED_PROPERTY_MODE_MARK:
                $output[$property->getName()] = 'uninitialized';
        }
    }
}
