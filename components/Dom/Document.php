<?php /** @noinspection JSUnresolvedReference */

namespace Gepf\Dom;


use Gepf\Core\RegEx\RegExFactory;

/**
 * The 'I-do-not-care-about-anything'-DOM-Parser
 *
 * It won't care about special chars, html codes ('&nbsp'; etc.), structure,
 * bigger/smaller signs (a > b), doctype declarations...
 * it won't care about anything really.
 */
class Document extends DomEntity
{
    public const GENERAL_NODE_REGEX = "<(?<tag>[\w]+)(?<attributes>[^>]*?)(([\s]*\/>)|(>(?<content>(([^<]*?)|(?R))*)<\/\\1[\s]*>))";
    public const UNESCAPED_VOID_ELEMENTS_REGEX = '<(%s[\w\d]*)\b(?<attributes>[^>]*)[^\/]>';
    public const VOID_ELEMENTS = [
        'area',
        'base',
        'br',
        'col',
        'command',
        'embed',
        'hr',
        'img',
        'input',
        'keygen',
        'link',
        'meta',
        'param',
        'source',
        'track',
        'wbr',
    ];
    public const SENSITIVE_NODE_REGEX = [
        '<!--[\s\S]*?-->',
        '<textarea>[\s\S]*?<\/textarea>',
        '<script>[\s\S]*?<\/script>',
    ];

    /** @var SensitiveNode[] */
    private array $sensitiveNodes;


    public static function parse(string $xhtml): self
    {
        $sensitiveNodes = [];
        foreach (self::replaceAndGetSensitiveNodes($xhtml) as $sensitiveNode) {
            $xhtml = str_replace($sensitiveNode->getXhtml(), $sensitiveNode->getId(), $xhtml);
            $sensitiveNodes[] = $sensitiveNode;
        }

        foreach (self::VOID_ELEMENTS as $voidElement) {
            $test = RegExFactory::getMatches(sprintf(self::UNESCAPED_VOID_ELEMENTS_REGEX, $voidElement), $xhtml);
            if ($test) {
                $newStr = substr_replace($test, '/', -1, 0);
                $xhtml = str_replace($test, $newStr, $xhtml);
            }
        }

        $matches = RegExFactory::getMatches(self::GENERAL_NODE_REGEX, $xhtml);
        $nodes = [];
        foreach ($matches as $match) {
            $nodes[] = Node::parse($match);
        }

        $self = new self($xhtml, $xhtml, $nodes);

        foreach ($sensitiveNodes as $sensitiveNode) {
            $self->addSensitiveNode($sensitiveNode);
        }

        return $self;
    }

    /** @return \Generator<SensitiveNode> */
    private static function replaceAndGetSensitiveNodes(string $xhtml): \Generator
    {
        foreach (self::SENSITIVE_NODE_REGEX as $regex) {
            $matches = RegExFactory::getMatches($regex, $xhtml);
            foreach ($matches as $match) {
                yield new SensitiveNode($match, base64_encode(random_bytes(32)));
            }
        }
    }

    public function addSensitiveNode(SensitiveNode $node): self
    {
        $this->sensitiveNodes[] = $node;

        return $this;
    }


    public function commit(): self
    {
        $this->startCommitment();

        foreach ($this->getNodes() as $node) {
            if (!$node->isOrHasCommitedNodes()) {
                continue;
            }

            $this->setCommitedContent(
                str_replace(
                    $node->getContent(),
                    $node->commitAndGetCommitedContent(),
                    $this->getCommitedContent(),
                ),
            );
        }

        if (isset($this->sensitiveNodes)) {
            $commitedContent = $this->getCommitedContent();
            foreach ($this->getSensitiveNodes() as $sensitiveNode) {
                $commitedContent = str_replace(
                    $sensitiveNode->getId(),
                    $sensitiveNode->getXhtml(),
                    $commitedContent,
                );
            }

            $this->setCommitedContent($commitedContent);
        }

        return $this;
    }

    public function getSensitiveNodes(): array
    {
        return $this->sensitiveNodes;
    }
}
