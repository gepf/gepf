<?php

namespace Gepf\Dom;

readonly class SensitiveNode
{
    public function __construct(
        private string $xhtml,
        private string $id,
    ) {}

    public function getXhtml(): string
    {
        return $this->xhtml;
    }

    public function getId(): string
    {
        return $this->id;
    }
}
