<?php

namespace Gepf\Dom;

use Gepf\Core\RegEx\RegExFactory;

class Node extends DomEntity
{
    public function __construct(
        string $rawText,
        string $content,
        private readonly string $tag,
        private readonly array $attributes,
        private readonly ?Node $parent = null,
    ) {
        parent::__construct($rawText, $content);
    }

    public static function parse(string $raw, Node $parent = null): self
    {
        $captureGroups = RegExFactory::getCaptureGroups(Document::GENERAL_NODE_REGEX, $raw)[0];
        $self = new self(
            $raw,
            $captureGroups['groups']['content'] ?? '',
            $captureGroups['groups']['tag'],
            preg_split('/\s+/', $captureGroups['groups']['attributes']),
            $parent,
        );

        if (isset($captureGroups['groups']['content'])) {
            $matches = RegExFactory::getMatches(Document::GENERAL_NODE_REGEX, $captureGroups['groups']['content']);
            $nodes = [];
            foreach ($matches as $match) {
                $nodes[] = self::parse($match, $self);
            }

            $self->setNodes($nodes);
        }

        return $self;
    }


    public function commitAndGetCommitedContent(): string
    {
        $this->startCommitment();

        foreach ($this->getNodes() as $node) {
            if (!$node->isCommited()) {
                continue;
            }

            $this->setCommitedContent(
                str_replace(
                    $node->getContent(),
                    $node->commitAndGetCommitedContent(),
                    $this->getCommitedContent(),
                ),
            );
        }

        return $this->getCommitedContent();
    }


    public function getTagName(): string
    {
        return $this->tag;
    }


    public function getAttributes(): array
    {
        return $this->attributes;
    }


    public function getParent(): ?Node
    {
        return $this->parent;
    }
}
