<?php

namespace Gepf\Dom;

abstract class DomEntity
{
    private string $commitedContent;

    public function __construct(
        private readonly string $rawText,
        private readonly string $content,
        private array $nodes = [],
    ) {}


    public function getRawText(): string
    {
        return $this->rawText;
    }

    public function setNodes(array $nodes): void
    {
        $this->nodes = $nodes;
    }

    public function getElementsByTagName(string $tagName): \Generator
    {
        foreach ($this->getNodes() as $node) {
            foreach ($node->getElementsByTagName($tagName) as $child) {
                yield $child;
            }

            if ($node->getTagName() === $tagName) {
                yield $node;
            }
        }
    }

    /**
     * @return Node[]
     */
    public function getNodes(): array
    {
        return $this->nodes;
    }

    public function appendText(string $text): void
    {
        $this->startCommitment();
        $this->setCommitedContent($this->getCommitedContent() . htmlspecialchars($text));
    }

    protected function startCommitment(): void
    {
        if (!$this->isCommited()) {
            $this->commitedContent = $this->content;
        }
    }

    public function isCommited(): bool
    {
        return isset($this->commitedContent);
    }

    public function getCommitedContent(): string
    {
        return $this->commitedContent;
    }

    public function setCommitedContent(string $commitedContent): void
    {
        $this->commitedContent = $commitedContent;
    }

    public function prependText(string $text): void
    {
        $this->startCommitment();
        $this->setCommitedContent(htmlspecialchars($text) . $this->getCommitedContent());
    }

    public function appendNode(Node $node): void
    {
        $this->nodes[] = $node;

        $this->startCommitment();
        $this->setCommitedContent($this->getCommitedContent() . $node->getContent());
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function prependNode(Node $node): void
    {
        array_unshift($this->nodes, $node);

        $this->startCommitment();
        $this->setCommitedContent($node->getContent() . $this->getCommitedContent());
    }

    public function appendDocument(Document $document): void
    {
        foreach ($document->getNodes() as $node) {
            $this->nodes[] = $node;
        }

        $this->startCommitment();
        $this->setCommitedContent($this->getCommitedContent() . $document->getContent());
    }

    public function prependDocument(Document $document): void
    {
        array_unshift($this->nodes, ...$document->getNodes());

        $this->startCommitment();
        $this->setCommitedContent($document->getContent() . $this->getCommitedContent());
    }

    public function isOrHasCommitedNodes(): bool
    {
        if ($this->isCommited()) {
            return true;
        }

        foreach ($this->getNodes() as $node) {
            if ($node->isOrHasCommitedNodes()) {
                return true;
            }
        }

        return false;
    }
}
