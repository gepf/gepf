<?php

namespace Gepf\Dom;

use Gepf\Core\Exception\DependencyException;

class Crawler
{
    private Document $dom;

    /**
     * @throws DependencyException
     */
    public function __construct(string $body)
    {
        $this->dom = Document::parse($body);
        // $this->meta = $this->dom->getElementsByTagName('meta');
    }


    public function appendHTMLToTag($source, $tag): void
    {
        $fragment = Document::parse($source);
        $targets = $this->dom->getElementsByTagName($tag);
        foreach ($targets as $target) {
            $target->appendDocument($fragment);
        }
    }


//    public function getMeta(string $key): ?string
//    {
//        foreach ($this->meta as $metaTag) {
//            if ($metaTag->hasAttribute('name') && $metaTag->getAttribute('name') === $key) {
//                return $metaTag->getAttribute('content');
//            }
//        }
//
//        return null;
//    }


    public function getBody(): string
    {
        return $this->dom->commit()->getCommitedContent();
    }
}
