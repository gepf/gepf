<?php

namespace Gepf\Service\Profiler\EventListener;

use Gepf\Core\Environment\Environment;
use Gepf\Core\Event\AbstractEventListener;
use Gepf\Core\Negotiation\Response\Response;
use Gepf\Dom\Crawler;
use Gepf\Service\Profiler\Profiler;

class PrintProfilerListener extends AbstractEventListener
{
    public function __construct(
        private readonly Environment $environment,
        private readonly Profiler $profiler,
        private readonly Response $response,
    ) {}


    public function execute(): void
    {
        if ($this->environment->getServerApi() === 'cli') {
            return;
        }

        if ($this->environment->getMode() !== Environment::MODE_DEV) {
            return;
        }

        if ($this->response->getHeaders()->getContentType() === null) {
            return;
        }

        if ($this->response->getHeaders()->getContentType()->get() !== Response::CONTENT_TYPE_HTML) {
            return;
        }

        $this->profiler->evaluate(RU_START, WALL_CLOCK_START);
        $crawler = new Crawler($this->response->getBody());
        $crawler->appendHTMLToTag($this->profiler->getDump(), 'body');

        $this->response->setBody($crawler->getBody());
    }
}
