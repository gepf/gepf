<?php

namespace Gepf\Service\Profiler;

interface ProfilerInterface
{
    public function evaluate(array $ruStart, float $wallClockStart);
}
