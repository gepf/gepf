<?php

namespace Gepf\Service\Profiler;

use Gepf\Core\DependencyInjection\ConfigurableService;
use Gepf\Core\DependencyInjection\Container\Container;
use Gepf\Core\Event\Event;
use Gepf\Core\Event\EventSubscription;
use Gepf\Core\Kernel\Task\Response\PopulateResponse;
use Gepf\Core\Log\Logger;
use Gepf\Core\Registry\Data;
use Gepf\Core\Registry\Registry;
use Gepf\Lite\Engine as Lite;
use Gepf\Service\Asset\AssetUtils;
use Gepf\Service\Asset\Item\AssetCollection;
use Gepf\Service\Profiler\EventListener\PrintProfilerListener;

class Profiler extends ConfigurableService implements ProfilerInterface
{
    private float $computations;
    private float $systemCalls;
    private float $wallClock;
    private int $memory;

    public function __construct(private readonly Container $container) {}


    public static function getSubscribedEvents(): array
    {
        return [new EventSubscription(PrintProfilerListener::class, PopulateResponse::class, Event::TEMPUS_POST)];
    }


    public static function getAssets(): ?AssetCollection
    {
        return AssetUtils::createAssetsForService(self::class, ['debug-bar.css'], 'profiler');
    }


    public function evaluate(array $ruStart, float $wallClockStart): self
    {
        $ruEnd = getrusage();
        $wallClockEnd = microtime(true);
        $this->computations = round($this->getRuTime($ruEnd, $ruStart, 'u'), 2);
        $this->systemCalls = round($this->getRuTime($ruEnd, $ruStart, 's'), 2);
        $this->wallClock = round(($wallClockEnd - $wallClockStart) * 1000, 2);

        $this->memory = round(memory_get_peak_usage() / 1024); // Alternatively, memory_get_usage()

        return $this;
    }


    private function getRuTime($ruE, $ruS, $index): float
    {
        return
            ($ruE["ru_{$index}time.tv_sec"] * 1000 + $ruE["ru_{$index}time.tv_usec"] / 1000) -
            ($ruS["ru_{$index}time.tv_sec"] * 1000 + $ruS["ru_{$index}time.tv_usec"] / 1000);
    }


    public function getDump(): ?string
    {
        $logger = $this->container->getLogger();

        $data = new Data();
        $data->set('log', $logger instanceof Logger ? $logger->getAll() : null);
        $data->set('templateEngine', $this->container->getViewCompiler()::class);
        $data->set('routes', $this->container->getRouter()->getRoute());
        $data->set('dump', $this->container->getRegistry()->getDump());
        $data->set('bt', getDump($this->container->getResponse()->getBacktrace()));
        $data->set('computations', $this->computations);
        $data->set('systemCalls', $this->systemCalls);
        $data->set('wallClock', $this->wallClock);
        $data->set('memory', $this->memory);
        $data->set('dataRegister', getDump($this->container->getData()->getRegister()));
        $registry = new Registry($data, $this->container->getRegistry()->getAssetRegistry(), $this->container->getRegistry()->getCallbacks());

        try {
            $debugBar = Lite::renderRightAway($registry, __DIR__ . '/Resources/debug-bar.html', $logger);
        } catch (\Exception $e) {
            return '<div id="gepf-profiler">Debug bar broken: ' . $e->getMessage() . ' </div>';
        }

        return $debugBar;
    }
}
