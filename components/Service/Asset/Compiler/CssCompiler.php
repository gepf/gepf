<?php

namespace Gepf\Service\Asset\Compiler;

use Gepf\Core\FileSystem\File;
use Gepf\Service\Asset\AssetCompilerInterface;
use Gepf\Service\Asset\Item\Asset;

/**
 * Simple compiler that checks css file for at_import statements
 * and adds import files to the AssetPackage
 */
class CssCompiler implements AssetCompilerInterface
{
    public static function supportedFileTypes(): array
    {
        return [
            AssetCompilerInterface::PRIORITY_MUST => ['css'],
        ];
    }

    public function compileAndPersist(Asset $asset): void
    {
        if ($asset->getSources()->count() > 1) {
            throw new \LogicException('CSS package has multiple sources already. Was it compiled before?');
        }

        $groundZeroFile = $asset->getSources()->get(0);

        if (!$groundZeroFile instanceof File) {
            throw new \LogicException('CSS package must be assigned to file, not directory');
        }

        $groundZeroCss = $groundZeroFile->openFile();

        foreach ($groundZeroCss as $line) {
            if (str_contains($line, '@import ')) {
                $includePathname = trim(str_replace(['@import', "'", '"', ';'], '', $line));
                $includedFile = new File($groundZeroFile->getPath() . '/' . $includePathname);
                $asset->addSource($includedFile);
            }
        }

        foreach ($asset->getSources() as $key => $source) {
            $targetPath = $asset->getNamespace()
                ? 'public/' . $asset->getNamespace() . '/' . $source->getFilename()
                : 'public/' . $source->getPathname();

            // first source is target qualifier
            // TODO refactor so that target can be file collection
            if ($key === 0) {
                $targetFile = File::createCopyFrom($source->getPathname(), $targetPath);
                $asset->setTarget($targetFile);
            }
        }
    }

    public function getPostCompileIdentifier(Asset $asset, string $sourceIdentifier): string
    {
        return $sourceIdentifier;
    }
}
