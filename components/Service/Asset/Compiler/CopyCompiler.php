<?php

namespace Gepf\Service\Asset\Compiler;

use Gepf\Core\FileSystem\File;
use Gepf\Service\Asset\AssetCompilerInterface;
use Gepf\Service\Asset\Item\Asset;

/**
 * Dummy compiler that does nothing more than copy one file of an asset
 */
class CopyCompiler implements AssetCompilerInterface
{
    public static function supportedFileTypes(): array
    {
        return [
            AssetCompilerInterface::PRIORITY_CAN => ['*'],
            AssetCompilerInterface::PRIORITY_ABSTAIN => ['css', 'scss', 'sass', 'less', 'ts'],
        ];
    }

    public function compileAndPersist(Asset $asset): void
    {
        $targetPath = $asset->getNamespace()
            ? 'public/' . $asset->getNamespace() . '/' . $asset->getSources()->current()->getFilename()
            : 'public/' . $asset->getSources()->current()->getPathname();

        $targetFile = File::createCopyFrom($asset->getSources()->current()->getPathname(), $targetPath);
        $asset->setTarget($targetFile);
    }

    public function getPostCompileIdentifier(Asset $asset, string $sourceIdentifier): string
    {
        return $sourceIdentifier;
    }
}
