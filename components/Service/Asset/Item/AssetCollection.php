<?php

namespace Gepf\Service\Asset\Item;

use Gepf\Core\Collection;

class AssetCollection extends Collection
{
    use AssetCollectionTrait;

    public function current(): AbstractAsset
    {
        return parent::current();
    }
}
