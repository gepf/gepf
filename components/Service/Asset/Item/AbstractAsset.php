<?php

namespace Gepf\Service\Asset\Item;

use Gepf\Core\FileSystem\File;
use Gepf\Core\FileSystem\FileCollection;

abstract class AbstractAsset
{
    public const SERIALIZE_TARGET_KEY = 'target';
    public const SERIALIZE_PATH_KEY = 'path';
    public const SERIALIZE_MODIFIED_KEY = 'modified';

    private FileCollection $sources;
    private File $target;

    public function __construct(
        private readonly ?string $namespace = null,
    ) {
        $this->sources = new FileCollection();
    }

    abstract public static function canDeserializeArray(array $serializedAsset): bool;

    abstract public static function deserialize(array $serializedAsset): self;

    public function getNamespace(): ?string
    {
        return $this->namespace;
    }

    public function getLatestChange(): \DateTimeImmutable
    {
        $sources = $this->getSources()->toArray();

        usort($sources, static function (File $a, File $b) {
            return $a->getModifiedTime() <=> $b->getModifiedTime();
        });

        // TODO test this
        return current($sources)->getModifiedTime();
    }

    public function getSources(): FileCollection
    {
        return $this->sources;
    }

    public function addSource(File $source): void
    {
        $this->sources->addFile($source);
    }

    public function getTarget(): File
    {
        return $this->target;
    }

    public function setTarget(File $target): void
    {
        $this->target = $target;
    }
}
