<?php

namespace Gepf\Service\Asset\Item;

use Gepf\Core\FileSystem\File;
use Gepf\Core\FileSystem\FileCollection;

class Asset extends AbstractAsset
{
    public const SERIALIZE_SOURCE_KEY = 'source';

    public function __construct(File|FileCollection $source, ?string $namespace = null)
    {
        parent::__construct($namespace);

        if ($source instanceof File) {
            $this->addSource($source);
            return;
        }

        foreach ($source as $file) {
            $this->addSource($file);
        }
    }

    public static function canDeserializeArray(array $serializedAsset): bool
    {
        if (!self::validateSerializedArray($serializedAsset)) {

            throw new \RuntimeException('Passed array has wrong structure.' . print_r($serializedAsset, true));
        }

        $targetPath = $serializedAsset[self::SERIALIZE_TARGET_KEY];
        $sourcePath = $serializedAsset[self::SERIALIZE_SOURCE_KEY][0][self::SERIALIZE_PATH_KEY];
        $sourceModified = $serializedAsset[self::SERIALIZE_SOURCE_KEY][0][self::SERIALIZE_MODIFIED_KEY];

        if (!is_readable($targetPath)) {
            return false;
        }

        if (!is_readable($sourcePath)) {
            return false;
        }

        // Existing file modification time not the same as the one passed
        if ((new File($sourcePath))->getModifiedTime()->getTimestamp() !== $sourceModified) {
            return false;
        }

        return true;
    }

    public static function validateSerializedArray(array $serializedAsset): bool
    {
        // TODO automate array structure validation
        if (!isset($serializedAsset[self::SERIALIZE_TARGET_KEY])) {
            return false;
        }

        if (!isset($serializedAsset[self::SERIALIZE_SOURCE_KEY][0][self::SERIALIZE_PATH_KEY])) {
            return false;
        }

        if (!isset($serializedAsset[self::SERIALIZE_SOURCE_KEY][0][self::SERIALIZE_MODIFIED_KEY])) {
            return false;
        }

        return true;
    }

    public static function deserialize(array $serializedAsset): self
    {
        $targetPath = $serializedAsset[self::SERIALIZE_TARGET_KEY];
        $sourcePaths = $serializedAsset[self::SERIALIZE_SOURCE_KEY];

        $sourceFiles = array_map(static fn(array $source) => $source[self::SERIALIZE_PATH_KEY], $sourcePaths);

        $asset = new Asset(FileCollection::createFilesFromStrings($sourceFiles));
        $asset->setTarget(new File($targetPath));

        return $asset;
    }

    public function serialize(): array
    {
        $sourcesArray = array_map(static fn(File $file) => [
            self::SERIALIZE_MODIFIED_KEY => $file->getModifiedTime()->getTimestamp(),
            self::SERIALIZE_PATH_KEY => $file->getPathname(),
        ], $this->getSources()->toArray());

        return [
            self::SERIALIZE_TARGET_KEY => $this->getTarget()->getPathname(),
            self::SERIALIZE_SOURCE_KEY => $sourcesArray,
        ];
    }
}
