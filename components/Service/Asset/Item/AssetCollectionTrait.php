<?php

namespace Gepf\Service\Asset\Item;

trait AssetCollectionTrait
{
    public function addAsset(AbstractAsset $asset): void
    {
        $this->add($asset);
    }
}
