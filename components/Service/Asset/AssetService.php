<?php

namespace Gepf\Service\Asset;

use Gepf\Core\Config\ConfigException;
use Gepf\Core\DependencyInjection\ConfigurableService;
use Gepf\Core\Event\Event;
use Gepf\Core\Event\EventSubscription;
use Gepf\Core\Kernel\Task\Logic\CollectRegisters;
use Gepf\Core\Registry\Registry;
use Gepf\Service\Asset\Command\InstallAssets;
use Gepf\Service\Asset\EventListener\PersistAssetsListener;
use Gepf\Service\Asset\EventListener\RegisterSearchPathAssetsListener;
use Psr\Log\LoggerInterface;

class AssetService extends ConfigurableService
{
    public function __construct(
        private readonly Registry $registry,
        private readonly LoggerInterface $logger,
    ) {}


    public static function getSubscribedEvents(): array
    {
        return [
            new EventSubscription(
                RegisterSearchPathAssetsListener::class,
                CollectRegisters::class,
                Event::TEMPUS_PRE,
            ),
            new EventSubscription(
                PersistAssetsListener::class,
                CollectRegisters::class,
                Event::TEMPUS_POST,
            ),
        ];
    }


    public static function getCallbackNames(): array
    {
        /**
         * @see AssetService::asset
         * @see AssetService::serviceAsset()
         * @see AssetService::buildAsset()
         */
        return ['asset', 'serviceAsset', 'buildAsset'];
    }


    public static function getCommands(): array
    {
        return [InstallAssets::class];
    }


    public function getServiceConfig(): AssetConfig
    {
        if (!$this->serviceConfig instanceof AssetConfig) {
            throw new ConfigException('AssetService requires AssetConfig');
        }

        return $this->serviceConfig;
    }


    public function asset(): callable
    {
        return function (string $identifier) {

            $asset = AssetUtils::createAsset($identifier);
            $manager = new AssetCompilerManager($this->registry->getAssetRegistry(), $this->logger);
            $manager->compileAndCacheSingleAsset($asset);
            $identifier = $manager->getNewIdentifier($asset, $identifier);

            return '/assets/' . $identifier;
        };
    }


    /**
     * Plain path replacement for services or bundles.
     * Services are responsible for asset compilation themselves.
     * @see ConfigurableService::getAssets()
     *
     * This can be done via
     * @see AssetUtils::createAssetsForService
     */
    public function serviceAsset(): callable
    {
        /**
         * @param string $namespace must be the same as namespace defined
         */
        return static function (string $identifier, string $namespace) {
            return '/bundles/' . $namespace . '/' . $identifier;
        };
    }


    public function buildAsset(): callable
    {
        return static function (string $identifier) {
            return '/' . $identifier;
        };
    }
}
