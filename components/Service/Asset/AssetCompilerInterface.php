<?php

namespace Gepf\Service\Asset;

use Gepf\Service\Asset\Item\Asset;

interface AssetCompilerInterface
{
    public const PRIORITY_MUST = 1;
    public const PRIORITY_CAN = 2;
    public const PRIORITY_ABSTAIN = 3;

    /**
     * @see AssetCompilerManager will guess appropriate compiler based on file-extensions
     */
    public static function supportedFileTypes(): array;

    public function compileAndPersist(Asset $asset): void;

    public function getPostCompileIdentifier(Asset $asset, string $sourceIdentifier): string;
}
