<?php

namespace Gepf\Service\Asset;

use Gepf\Bridge\ScssPhp;
use Gepf\Service\Asset\Compiler\CopyCompiler;
use Gepf\Service\Asset\Compiler\CssCompiler;
use Gepf\Service\Asset\Item\AbstractAsset;
use Psr\Log\LoggerInterface;

class AssetCompilerGuesser
{
    private array $knownCompilers = [
        ScssPhp::class,
        CopyCompiler::class,
        CssCompiler::class,
    ];

    public function __construct(private readonly LoggerInterface $logger) {}

    public function guess(AbstractAsset $asset): AssetCompilerInterface
    {
        $extension = $asset->getSources()->get(0)->getExtension();
        $ableCompilers = [];

        /** @var AssetCompilerInterface $compiler */
        foreach ($this->knownCompilers as $compiler) {
            $supported = $compiler::supportedFileTypes();
            if ($this->checkMustCompile($supported, $extension)) {
                return new $compiler();
            }

            if ($this->checkCanCompile($supported, $extension)) {
                $ableCompilers[] = $compiler;
            }

            if ($this->checkWildcardAndAbstain($supported, $extension)) {
                $ableCompilers[] = $compiler;
            }
        }

        if (\count($ableCompilers) > 1) {
            $this->logger->warning('(Asset) more than 1 compilers found for ' . $extension);
        }

        $ableCompiler = current($ableCompilers);

        if (!$ableCompiler) {
            throw new \RuntimeException('(Asset) no eligible compiler found, for ' . $extension);
        }

        return new $ableCompiler();
    }

    private function checkMustCompile(array $supportedExtensions, string $extension): bool
    {
        return $this->check(
            $supportedExtensions,
            AssetCompilerInterface::PRIORITY_MUST,
            $extension
        );
    }

    private function check(array $supportedExtensions, string $supportedExtensionsKey, string $extension): bool
    {
        if (!isset($supportedExtensions[$supportedExtensionsKey])) {
            return false;
        }

        if (!\in_array($extension, $supportedExtensions[$supportedExtensionsKey], true)) {
            return false;
        }

        return true;
    }

    private function checkCanCompile(array $supportedExtensions, string $extension): bool
    {
        return $this->check(
            $supportedExtensions,
            AssetCompilerInterface::PRIORITY_CAN,
            $extension
        );
    }


    private function checkWildcardAndAbstain(array $supportedExtensions, string $extension): bool
    {
        if (!isset($supportedExtensions[AssetCompilerInterface::PRIORITY_CAN])) {
            return false;
        }

        if (!\in_array('*', $supportedExtensions[AssetCompilerInterface::PRIORITY_CAN], true)) {
            return false;
        }

        if (\in_array($extension, $supportedExtensions[AssetCompilerInterface::PRIORITY_ABSTAIN], true)) {
            return false;
        }

        return true;
    }
}
