<?php

namespace Gepf\Service\Asset;

use Gepf\Core\FileSystem\File;
use Gepf\Core\FileSystem\FilePilot;
use Gepf\Core\FileSystem\RecursiveFileCollection;
use Gepf\Core\FileSystem\Util\FileSystem;
use Gepf\Service\Asset\Item\AbstractAsset;
use Gepf\Service\Asset\Item\Asset;
use Gepf\Service\Asset\Item\AssetCollection;

class AssetUtils
{
    /**
     * @param string[] $paths
     */
    public static function createAssetsForService(string $owningService, array $paths, string $namespace): AssetCollection
    {
        if (!isset($_SERVER['DOCUMENT_ROOT'])) {
            throw new \RuntimeException('AssetUtils not working, when DOCUMENT_ROOT is unknown');
        }

        $owningServiceRoot = (new \ReflectionClass($owningService))->getFileName();
        $owningServiceRoot = str_replace($_SERVER['DOCUMENT_ROOT'] . '/', '', $owningServiceRoot);

        $assets = new AssetCollection();
        foreach ($paths as $path) {
            $file = FilePilot::createFromString($owningServiceRoot)->goUp()->goTo("Resources/$path")->getFile();
            $assets->addAsset(new Asset($file, 'bundles/' . $namespace));
        }

        return $assets;
    }


    public static function createAssetsFromDir(string $path): AssetCollection
    {
        $assets = new AssetCollection();
        $dir = new RecursiveFileCollection([$path]);
        foreach ($dir->getFiles() as $file) {
            $assets->addAsset(new Asset($file));
        }

        return $assets;
    }


    public static function createAsset(string $path): Asset
    {
        return new Asset(FileSystem::get('assets/' . $path));
    }


    public static function compareAssetsByPathNames(AbstractAsset $assetA, AbstractAsset $assetB): bool
    {
        $assetAPathNames = array_map(static fn(File $file) => $file->getPathname(), $assetA->getSources()->toArray());

        foreach ($assetB->getSources() as $source) {
            if (!\in_array($source->getPathname(), $assetAPathNames, true)) {
                return false;
            }
        }

        return true;
    }


    public static function compareAssetsByModificationTimes(AbstractAsset $assetA, AbstractAsset $assetB): bool
    {
        $assetAModified = array_map(static fn(File $file) => $file->getModifiedTime()->getTimestamp(), $assetA->getSources()->toArray());
        $assetBModified = array_map(static fn(File $file) => $file->getModifiedTime()->getTimestamp(), $assetB->getSources()->toArray());

        sort($assetAModified);
        sort($assetBModified);

        // TODO pre compilation source collecting
        // For now, there is no system in place, to collect all sources before the actual compiling happens
        // as a workaround, just check if any of the timestamps match
        if (\count($assetAModified) > 1 && \count($assetBModified) === 1 && \in_array($assetBModified[0], $assetAModified, true)) {
            return true;
        }

        return $assetAModified === $assetBModified;
    }


    /**
     * $path used to be $this->query->server->get('PATH_INFO')
     * @noinspection PhpUnused
     */
    public static function checkIfAsset(string $path): ?string
    {
        /** @noinspection PhpComposerExtensionStubsInspection */
        $mime = mime_content_type($path);

        if ($mime === 'text/x-php') {
            return 'Forbidden';
        }

        /**
         * @var array $mimeTypes
         */
        $mimeTypes = require 'config/mimes.php';

        if ($mime === 'text/plain') {
            $pathParts = explode('.', $path);
            $fileExtension = strtolower(array_pop($pathParts));
            if (\array_key_exists($fileExtension, $mimeTypes)) {
                $mime = $mimeTypes[$fileExtension];
            }
        }

        if (\in_array($mime, $mimeTypes, true)) {
            header('Content-Type: ' . $mime);
        }

        return file_get_contents($path);
    }
}