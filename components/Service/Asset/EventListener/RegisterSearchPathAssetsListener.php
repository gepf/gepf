<?php

namespace Gepf\Service\Asset\EventListener;

use Gepf\Core\Event\AbstractEventListener;
use Gepf\Core\Registry\Registry;
use Gepf\Service\Asset\AssetService;
use Gepf\Service\Asset\AssetUtils;

class RegisterSearchPathAssetsListener extends AbstractEventListener
{
    public function __construct(private readonly AssetService $service, private readonly Registry $registry) {}

    public function execute(): void
    {
        $assets = [];
        foreach ($this->service->getServiceConfig()->getAutoInstallFolders() as $folder) {
            foreach (AssetUtils::createAssetsFromDir("assets/$folder") as $asset) {
                $assets[] = $asset;
            }
        }

        $assetRegistry = $this->registry->getAssetRegistry();
        foreach ($assets as $asset) {
            $assetRegistry->addAsset($asset);
        }
    }
}
