<?php

namespace Gepf\Service\Asset\EventListener;

use Gepf\Core\Event\AbstractEventListener;
use Gepf\Core\Registry\Registry;
use Gepf\Service\Asset\AssetCompilerManager;
use Psr\Log\LoggerInterface;

class PersistAssetsListener extends AbstractEventListener
{
    public function __construct(
        private readonly Registry $registry,
        private readonly LoggerInterface $logger,
    ) {}


    public function execute(): void
    {
        $manager = new AssetCompilerManager($this->registry->getAssetRegistry(), $this->logger);
        $manager->compileAndCacheAssetRegistry();
    }
}
