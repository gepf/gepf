<?php

namespace Gepf\Service\Asset;

use Gepf\Cache\Cache;
use Gepf\Cache\CacheItem;
use Gepf\Core\FileSystem\File;
use Gepf\Core\Registry\AssetRegistry;
use Gepf\Service\Asset\Item\AbstractAsset;
use Gepf\Service\Asset\Item\Asset;
use Psr\Log\LoggerInterface;

readonly class AssetCompilerManager
{
    private AssetCompilerGuesser $assetCompilerGuesser;

    public function __construct(
        private AssetRegistry $assetRegistry,
        private LoggerInterface $logger,
    ) {
        $this->assetCompilerGuesser = new AssetCompilerGuesser($logger);
    }


    /**
     * All previously persisted Assets that were added to AssetRegistry are listed
     * in a CacheItem. That list, and the AssetRegistry must be synchronized, so
     * that when iterating either list should get the key for the corresponding
     * item from the other list.
     *
     * Like:
     * foreach sources as key => value {
     *   targets[key] === value
     * }
     */
    public function compileAndCacheAssetRegistry(): void
    {
        $callback = function (CacheItem $item) {

            $persisted = $item->getContent();

            $serializedOutArray = [];
            $vote = 0;
            foreach ($this->assetRegistry as $key => $registeredAsset) {

                // deserialization can fail when modified timestamps do not match
                $assetFromCache = isset($persisted[$key][$registeredAsset::class])
                && Asset::canDeserializeArray($persisted[$key][$registeredAsset::class])
                    ? Asset::deserialize($persisted[$key][$registeredAsset::class])
                    : null;

                // Old entry for asset detected
                if ($assetFromCache) {
                    $this->logger->debug('(Asset) found: ' . $assetFromCache->getSources()->current());
                    $latestChange = $registeredAsset->getLatestChange()->getTimestamp();
                    $this->logger->debug("(Asset) last change at: $latestChange");

                    // Check if target still present
                    if (!file_exists($assetFromCache->getTarget()->getPathname())) {
                        $this->logger->debug("(Asset) target {$assetFromCache->getTarget()->getPathname()} was deleted");
                        $vote++;
                    }

                    // File(s) marked in cache older than assets last modify time detected
                    if (!AssetUtils::compareAssetsByModificationTimes($assetFromCache, $registeredAsset)) {
                        $this->logger->debug("(Asset) file(s) for target {$assetFromCache->getTarget()->getPathname()} are outdated");
                        $vote++;
                    }

                    // Nothing to do
                    if (!$vote) {
                        $this->logger->debug("(Asset) {$assetFromCache->getTarget()} is up to date, nothing to do");
                        $registeredAsset->setTarget(new File($persisted[$key][$registeredAsset::class][AbstractAsset::SERIALIZE_TARGET_KEY]));
                        $serializedOutArray[$key][$registeredAsset::class] = $registeredAsset->serialize();
                        continue;
                    }
                }

                $this->logger->debug("(Asset) okay, {$registeredAsset->getSources()->current()->getPathname()} will be compiled and persisted");

                $compiler = $this->assetCompilerGuesser->guess($registeredAsset);
                $compiler->compileAndPersist($registeredAsset);
                $serializedOutArray[$key][$registeredAsset::class] = $registeredAsset->serialize();
            }

            return $serializedOutArray;
        };

        (new Cache())->get('asset_registry', $callback);
    }


    /**
     * Other than in {@see AssetCompilerManager::compileAndCacheAssetRegistry} the method
     * for compiling a single Asset (as happening in templates via {{ asset('path') }})
     * does not synchronize with a list of all Assets, since not all Assets used in
     * every template can be known. It will check however a separate cached list of
     * Assets to decide whether it was persisted before or not. It cannot delete entries
     * from that list though, meaning that the list might grow over time when not
     * deleted occasionally
     */
    public function compileAndCacheSingleAsset(Asset $asset): void
    {
        $callback = function (CacheItem $item) use ($asset) {

            $persistedSerializedAssets = $item->getContent();
            $replaceKey = null;
            $vote = 'create';
            if ($persistedSerializedAssets) {
                foreach ($persistedSerializedAssets as $key => $persistedSerializedAsset) {

                    $persistedAssetClassName = key($persistedSerializedAsset);

                    if (!is_subclass_of($persistedAssetClassName, AbstractAsset::class)) {
                        throw new \RuntimeException('(Asset) invalid type found in cache: ' . $persistedAssetClassName);
                    }

                    // canDeserializeArray() automatically detects if target and source files still present
                    if (!$persistedAssetClassName::canDeserializeArray($persistedSerializedAsset[$persistedAssetClassName])) {
                        $this->logger->debug(
                            "(Asset) {$persistedSerializedAsset[$persistedAssetClassName][AbstractAsset::SERIALIZE_TARGET_KEY]} described in cache does not exist anymore. Delete.",
                        );
                        unset($persistedSerializedAssets[$key]);
                        continue;
                    }

                    $persistedAsset = $persistedAssetClassName::deserialize($persistedSerializedAsset[$persistedAssetClassName]);

                    // only if persistedAsset and asset have the exact same paths, it will be considered a hit
                    // intersection alone is a miss
                    if (!AssetUtils::compareAssetsByPathNames($persistedAsset, $asset)) {
                        continue;
                    }

                    if (!AssetUtils::compareAssetsByModificationTimes($persistedAsset, $asset)) {
                        $this->logger->debug("(Asset) {$persistedAsset->getTarget()->getPathname()} has been modified. Replace.");
                        $replaceKey = $key;
                        $vote = 'replace';
                        break;
                    }

                    if (AssetUtils::compareAssetsByModificationTimes($persistedAsset, $asset)) {
                        $this->logger->debug("(Asset) {$persistedAsset->getTarget()->getPathname()} up to date, nothing to do.");
                        $vote = 'up_to_date';
                        break;
                    }
                }
            }

            if ($vote !== 'up_to_date') {
                $compiler = $this->assetCompilerGuesser->guess($asset);
                $compiler->compileAndPersist($asset);
                $replaceKey
                    ? $persistedSerializedAssets[$replaceKey][$asset::class] = $asset->serialize()
                    : $persistedSerializedAssets[][$asset::class] = $asset->serialize();
                $this->logger->debug("(Asset) {$asset->getTarget()->getPathname()} was compiled");
            }

            if (!$persistedSerializedAssets) {
                throw new \RuntimeException('(Asset) Compiling failed: ' . $asset->getSources()->current()->getPathname());
            }

            return $persistedSerializedAssets;
        };

        (new Cache())->get('asset_singles', $callback);
    }


    public function getNewIdentifier(Asset $asset, string $oldIdentifier): string
    {
        return $this->assetCompilerGuesser
            ->guess($asset)
            ->getPostCompileIdentifier($asset, $oldIdentifier);
    }
}
