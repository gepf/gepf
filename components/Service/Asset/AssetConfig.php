<?php

namespace Gepf\Service\Asset;

use Gepf\Service\ServiceConfig;

class AssetConfig extends ServiceConfig
{
    public function __construct(private readonly array $autoInstallFolders = [])
    {
        parent::__construct(AssetService::class);
    }

    public function getAutoInstallFolders(): array
    {
        return $this->autoInstallFolders;
    }
}
