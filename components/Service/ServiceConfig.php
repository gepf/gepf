<?php

namespace Gepf\Service;

use Gepf\Core\Event\EventCollection;

/**
 * This object is used in case a service configuration is provided as an array
 */
class ServiceConfig implements ServiceConfigInterface
{
    public function __construct(
        private readonly string $class,
        private readonly array $settings = [],
        private readonly ?string $interface = null,
    ) {}

    public function getClass(): string
    {
        return $this->class;
    }

    public function getName(): string
    {
        $path = explode('\\', $this->getClass());
        $name = array_pop($path);
        $name = preg_replace('#(?<!_)[A-Z]#', '_\0', $name);
        $name = strtolower($name);

        return trim($name, '_');
    }

    public function getSettings(): array
    {
        return $this->settings;
    }

    public function get($key)
    {
        return $this->settings[$key] ?? null;
    }

    public function getInterface(): ?string
    {
        return $this->interface;
    }

    public function getEventSubscribers(): EventCollection
    {
        return new EventCollection();
    }
}
