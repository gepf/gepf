<?php

namespace Gepf\Service\Debug;

use Gepf\Core\DependencyInjection\ConfigurableService;
use Gepf\Core\DependencyInjection\Container\Container;
use Gepf\Service\ServiceConfig;
use Gepf\Service\ServiceConfigInterface;
use ReflectionClass;

class Debug extends ConfigurableService
{
    private const PRIVATE_PROPERTY_PREFIX = 'private';
    private const PROTECTED_PROPERTY_PREFIX = 'protected';
    private const PUBLIC_PROPERTY_PREFIX = 'public';
    private const PARENT_PRIVATE_PROPERTY_PREFIX = 'parent private';

    private const MAX_NESTING_LEVEL = 256;

    private string $dumpCaller = '<div class="Gepf_dump__caller" style="color: grey">%s</div>';
    private string $dumpString = '<span class="Gepf_dump__string" style="color: orange">%s</span>';
    private string $dumpBool = '<span class="Gepf_dump__string" style="color: yellowgreen">%s</span>';
    private string $dumpNull = '<span class="Gepf_dump__null" style="color: palevioletred">%s</span>';
    private string $dumpInteger = '<span class="Gepf_dump__integer" style="color: cornflowerblue">%d</span>';
    private string $dumpArrayKey = '<span style="color: #f44">%s</span><span> => </span>';

    private int $nestingLevel;
    private array $objectHashes;
    private int $initialMemory;
    private int $memoryLimit;


    private string $id;

    private function getDumpContainer(): string
    {
        return '<div id="gepf-profiler-' . $this->getId(
            ) . '" class="Gepf_dump" style="background: #433; color: #ccd; font-family: \'Droid Sans Mono\', monospace;">%s</div>';
    }

    private function getId(): string
    {
        if (!isset($this->id)) {
            $this->id = random_int(10000000, 99999999);
        }

        return $this->id;
    }


    // 'nestingLevel' and 'objectHashes' are here to allow recursion
    public function __construct(int $nestingLevel = 0, array $objectHashes = [], int $initialMemory = null, int $memoryLimit = null)
    {
        $this->nestingLevel = $nestingLevel;
        $this->objectHashes = $objectHashes;
        $this->initialMemory = $initialMemory ?? memory_get_usage();
        $this->memoryLimit = $memoryLimit ?? $this->guessMemoryLimit();
    }


    /**
     * @param ServiceConfig $serviceConfig
     */
    public static function create(Container $container, ServiceConfigInterface $serviceConfig): self
    {
        $initialMemory = $serviceConfig->getSettings()['initialMemory'] ?? null;
        $memoryLimit = $serviceConfig->getSettings()['initialMemory'] ?? null;

        return new self(0, [], $initialMemory, $memoryLimit);
    }


    private function guessMemoryLimit(): int
    {
        $globalLimit = (int)ini_get('memory_limit');

        // if limit is set to unlimited, go with 16MB
        if ($globalLimit === -1) {
            return 16777216;
        }

        // assume dump should stop working if half of allowed memory is already consumed
        return $globalLimit / 2;
    }


    private function checkExcessiveMemoryUsage(): bool
    {
        if (memory_get_usage() > $this->memoryLimit) {
            return true;
        }

        // single dump should not consume more than 1MB
        if (memory_get_usage() - $this->initialMemory > 1048576) {
            return true;
        }

        return false;
    }


    public function dump($mixed, self $parent = null): string
    {
        if ($parent) {
            $this->nestingLevel = $parent->nestingLevel + 1;
            $this->objectHashes = $parent->objectHashes;

            if ($this->nestingLevel >= self::MAX_NESTING_LEVEL) {
                return 'Max nesting level of ' . self::MAX_NESTING_LEVEL . ' reached.';
            }
        }

        if ($this->checkExcessiveMemoryUsage()) {
            return 'excessive memory use... stopping.';
        }

        $htmlOut = '';
        if (!$parent) {

            $collapsableName = "collapsable_{$this->getId()}";
            $toggleName = "toggle_{$this->getId()}";
            $childrenName = "children_{$this->getId()}";

            $script = '<script>
                const ' . $collapsableName . ' = document.querySelectorAll("#gepf-profiler-' . $this->getId() . ' .gepf-profiler-collapsable");
                const ' . $toggleName . ' = [];
                const ' . $childrenName . ' = [];
                for (let i in [...' . $collapsableName . ']) {
                    ' . $toggleName . '[i] = ' . $collapsableName . '[i].querySelector(".gepf-profiler-collapsable--toggle");
                    ' . $childrenName . '[i] = ' . $collapsableName . '[i].querySelector(".gepf-profiler-child");
                    if (' . $childrenName . '[i]) {
                        ' . $toggleName . '[i].addEventListener("click", function() {
                            ' . $toggleName . '[i].classList.toggle("gepf-profiler-collapsable--toggle--closed");
                            ' . $toggleName . '[i].classList.toggle("gepf-profiler-collapsable--toggle--open");
                            ' . $childrenName . '[i].classList.toggle("gepf-profiler-child--hidden");
                        })
                    }
                }
            </script>
            <style>
                .gepf-profiler-child--hidden { display: none; }
                .gepf-profiler-collapsable--toggle--open:before { content: "+"; }
                .gepf-profiler-collapsable--toggle--closed:before { content: "-"; }
            </style>';
        }

        if (\is_string($mixed) && !$mixed) {
            $htmlOut = sprintf($this->dumpNull, '""');
        } elseif (\is_string($mixed)) {
            $htmlOut = sprintf($this->dumpString, htmlspecialchars($mixed));
        } elseif ($mixed === null) {
            $htmlOut = sprintf($this->dumpNull, 'null');
        } elseif (\is_bool($mixed)) {
            $htmlOut = sprintf($this->dumpBool, $mixed ? 'true' : 'false');
        } elseif (\is_int($mixed)) {
            $htmlOut = sprintf($this->dumpInteger, $mixed);
        } elseif (\is_array($mixed) && empty($mixed)) {
            $htmlOut = sprintf($this->dumpNull, '[]');
        } elseif (\is_array($mixed)) {
            $htmlOut = $this->parseArray($mixed);
        } elseif (\is_object($mixed)) {
            $objectHash = spl_object_hash($mixed);
            if (\in_array($objectHash, $this->objectHashes, true)) {
                return 'Circular reference, consider refactoring to weak reference';
            }

            $this->objectHashes[] = spl_object_hash($mixed);
            $htmlOut .= $this->parseObject($mixed);
        }

        if (!$parent) {
            $bt = debug_backtrace();
            $bt = $this->filterBackTrace($bt);

            $caller = $bt[0];
            $htmlCallerOut = sprintf($this->dumpCaller, $caller['file'] . ': ' . $caller['line']);
            $htmlOut = $htmlCallerOut . $htmlOut;
            $htmlOut = sprintf($this->getDumpContainer(), $htmlOut);
            $htmlOut .= $script;
        }

        return $htmlOut;
    }


    private function filterBackTrace($bt)
    {
        if (isset($bt[0]['class']) && $bt[0]['class'] === __CLASS__) {
            array_shift($bt);
            $bt = $this->filterBackTrace($bt);
        }

        return $bt;
    }


    private function getChildDivTag(int $nestingLevel): string
    {
        if ($nestingLevel > 1) {
            return '<div class="gepf-profiler-child gepf-profiler-child--hidden" style="z-index: ' . $this->nestingLevel . '">';
        }

        return '<div class="gepf-profiler-child" style="z-index: ' . $this->nestingLevel . '">';
    }


    private function parseObject(object $object): string
    {
        $reflectionClass = new ReflectionClass(\get_class($object));
        $htmlOut = $reflectionClass->getName();
        $htmlOut .= $this->getChildDivTag($this->nestingLevel) . (new self())->dump(
                $this->dismount($object, $reflectionClass),
                $this,
            ) . '</div>';

        return $htmlOut;
    }


    private function parseArray(array $array): string
    {
        $htmlOut = '';
        foreach ($array as $key => $row) {
            $collapsible = \is_object($row) || \is_array($row);

            $htmlOut .= '<div class="' . ($collapsible ? 'gepf-profiler-collapsable' : 'gepf-profiler-static') . '" style="padding-left: 24px;">';
            $collapseSign = $collapsible ? '<span class="gepf-profiler-collapsable--toggle gepf-profiler-collapsable--toggle--closed"> </span> ' : '&nbsp; ';

            $htmlOut .= sprintf($this->dumpArrayKey, $collapseSign . $key);


            if (\is_array($row)) {
                $htmlOut .= ' array ' . $this->getChildDivTag($this->nestingLevel) . (new self())->dump($row, $this) . '</div>';
            } else {
                $htmlOut .= (new self())->dump($row, $this);
            }

            $htmlOut .= '</div>';
        }

        return $htmlOut;
    }


    public function dismount(object $object, ReflectionClass $reflectionClass): array
    {
        $array = [];

        if ($reflectionClass->getParentClass()) {
            $array['extends'] = $reflectionClass->getParentClass()->getName();
            foreach ($reflectionClass->getParentClass()->getProperties() as $property) {
                if ($property->isPrivate()) {
                    $array[self::PARENT_PRIVATE_PROPERTY_PREFIX . ' ' . $property->getName()] = $this->getPropertyValue(
                        $property,
                        $object,
                    );
                }
            }
        }

        foreach ($reflectionClass->getProperties() as $property) {
            $propertyPrefix = ' - ';
            if ($property->isPrivate()) {
                $propertyPrefix = self::PRIVATE_PROPERTY_PREFIX;
            } elseif ($property->isProtected()) {
                $propertyPrefix = self::PROTECTED_PROPERTY_PREFIX;
            } elseif ($property->isPublic()) {
                $propertyPrefix = self::PUBLIC_PROPERTY_PREFIX;
            }

            $array[$propertyPrefix . ' ' . $property->getName()] = $this->getPropertyValue($property, $object);
        }

        return $array;
    }


    private function getPropertyValue(\ReflectionProperty $property, object $object)
    {
        if ($property->isInitialized($object)) {
            return $property->getValue($object);
        }

        return 'uninitialized';
    }
}
