<?php

namespace Gepf\Service\Security;

interface RoleInterface
{
    public function getName(): string;

    public function getRoutes(): array;

    public function getWorkflows(): array;

    public function getEntityRights(): array;
}
