<?php

namespace Gepf\Service\Security;

use Gepf\Service\RestOrm\Attributes\Type\Char;
use Gepf\Service\RestOrm\Attributes\Type\Enum;
use Gepf\Service\RestOrm\Attributes\Type\Password;
use Gepf\Service\Security\UserProvider\UserProviderEnum;

class Login
{
    #[Char(191)]
    private string $username;

    #[Password]
    private string $password;

    #[Enum(50)]
    private UserProviderEnum $userProvider;
}
