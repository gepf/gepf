<?php

namespace Gepf\Service\Security;

interface UserInterface
{
    public function getUsername(): string;
}
