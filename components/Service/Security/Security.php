<?php

namespace Gepf\Service\Security;

use Gepf\Core\DependencyInjection\ConfigurableService;
use Gepf\Core\Event\Event;
use Gepf\Core\Event\EventSubscription;
use Gepf\Core\Negotiation\Request\Request;
use Gepf\Core\Session\Session;
use Gepf\Core\Util\Store;
use Gepf\Core\Util\StoreException;
use Gepf\Service\Security\AuthMethod\AuthMethodInterface;
use Gepf\Service\Security\EventListener\CheckAuthListener;
use Gepf\Service\Security\UserProvider\UserProviderInterface;
use Psr\Log\LoggerInterface;

class Security extends ConfigurableService
{
    public const DEFAULT_USER_PROVIDER = 'map';
    public const DEFAULT_AUTH_METHOD = 'form-based';

    public const LOGIN_STATUS_STILL_LOGGED_IN = 'still_logged_in';
    public const LOGIN_STATUS_ANONYMOUS = 'anonymous';
    public const LOGIN_STATUS_SUCCESS = 'success';
    public const LOGIN_STATUS_FAILED = 'failed';
    public const LOGIN_STATUS_USER_NONEXISTENT = 'user_nonexistent';
    public const LOGIN_STATUS_EMPTY_USERNAME = 'empty_username';
    public const LOGIN_STATUS_LOGGED_IN_ALREADY = 'logged_in_already';

    private string $authMethod = self::DEFAULT_AUTH_METHOD;
    private string $userProviderType = self::DEFAULT_USER_PROVIDER;
    private AuthMethodInterface $authProvider;
    private UserProviderInterface $userProvider;
    private ?string $username = null;
    private bool $isAuthenticated = false;
    private string $authToken;
    private string $loginStatus;


    public function __construct(
        private readonly Session $session,
        private readonly Request $request,
        private readonly LoggerInterface $logger,
    ) {}


    public static function getSubscribedEvents(): array
    {
        return [new EventSubscription(CheckAuthListener::class, Request::class, Event::TEMPUS_PRE)];
    }


    /**
     * @throws \RuntimeException
     */
    private function createUserProviderFromRequest(): UserProviderInterface
    {
        $this->userProviderType = $this->request->body->get('auth_method') ?? $this->userProviderType;

        if (!isset($this->getServiceConfig()->getSettings()['user_providers'][$this->userProviderType])) {
            throw new \RuntimeException('Demanded user provider not configured');
        }

        $userBackendClassName = $this->getServiceConfig()->getSettings()['user_providers'][$this->userProviderType];

        if (!class_exists($userBackendClassName)) {
            throw new \RuntimeException('Demanded user provider not installed');
        }

        $userBackendProvider = new $userBackendClassName($this->getServiceConfig()->getSettings());

        if (!$userBackendProvider instanceof UserProviderInterface) {
            throw new \RuntimeException('User provider does not implement AuthMethodInterface');
        }

        return $userBackendProvider;
    }


    /**
     * @throws \RuntimeException
     */
    private function createAuthProviderFromRequest(): AuthMethodInterface
    {
        if ($this->request->headers->get('Authorization')) {
            $this->authMethod = $this->request->headers->get('Authorization')->get();
        }

        if (!isset($this->getServiceConfig()->getSettings()['auth_methods'][$this->authMethod])) {
            throw new \RuntimeException('Demanded auth method not configured');
        }

        $authMethodClassName = $this->getServiceConfig()->getSettings()['auth_methods'][$this->authMethod];

        if (!class_exists($authMethodClassName)) {
            throw new \RuntimeException('Demanded auth method not installed');
        }

        return new $authMethodClassName($this->request, $this->session);
    }


    /**
     * TODO have this as event: \Gepf\Core\Kernel\Task\Boilerplate::checkAuth
     */
    public function checkAuth(): void
    {
        $this->userProvider = $this->createUserProviderFromRequest();
        $this->authProvider = $this->createAuthProviderFromRequest();
        $this->authToken = $this->authProvider->getToken();

        try {
            $authToken = Store::get('sessions.' . $this->authToken);
        } catch (StoreException $e) {
            $this->logger->debug($e->getMessage());
            $authToken = null;
        }

        if ($authToken) {
            $this->username = $authToken[AuthMethodInterface::KEY_USERNAME];
            $this->isAuthenticated = true;
            $this->loginStatus = self::LOGIN_STATUS_STILL_LOGGED_IN;

            return;
        }

        if ($this->checkCredentialsProvided()) {
            $this->login();

            return;
        }

        $this->loginStatus = self::LOGIN_STATUS_ANONYMOUS;
    }


    public function login(): bool
    {
        if ($this->isAuthenticated) {
            $this->logger->info('Already logged in user attempted to login');
            $this->loginStatus = self::LOGIN_STATUS_LOGGED_IN_ALREADY;

            return true;
        }

        $username = $this->authProvider->getUsername();
        $pwd = $this->authProvider->getPassword();
        if (!$username) {
            $this->logger->info('Attempt to login with empty username');
            $this->loginStatus = self::LOGIN_STATUS_EMPTY_USERNAME;

            return false;
        }

        $pwdHash = $this->userProvider->getHashForUser($username);
        if (!$pwdHash) {
            $this->logger->info('User not found: ' . $username);
            $this->loginStatus = self::LOGIN_STATUS_USER_NONEXISTENT;

            return false;
        }

        $this->isAuthenticated = $this->checkPassword($pwd, $pwdHash);
        $this->loginStatus = $this->isAuthenticated ? self::LOGIN_STATUS_SUCCESS : self::LOGIN_STATUS_FAILED;

        if ($this->isAuthenticated) {
            $this->username = $username;
            Store::set('sessions.' . $this->authToken, [
                'username' => $username,
                'auth_backend' => $this->userProviderType,
                'auth_method' => 'form-based',
            ]);
        }

        return $this->isAuthenticated;
    }


    public function removeAuth(): void
    {
        Store::delete('sessions.' . $this->session->getId());
    }


    public function checkCredentialsProvided(): bool
    {
        return $this->authProvider->getUsername() !== null || $this->authProvider->getPassword() !== null;
    }


    /*
     * PRIVATE ZONE
     */

    private function checkPassword(string $pwd, string $pwdHash): bool
    {
        // !TODO Research this
        //return hash_equals( $pwdHash, password_hash($pwd,  PASSWORD_DEFAULT) );

        $pwdCheck = crypt($pwd, $pwdHash);
        return hash_equals($pwdCheck, $pwdHash);
    }


    /*
     * GETTERS / SETTERS
     */

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function isAuthenticated(): bool
    {
        return $this->isAuthenticated;
    }

    public function getAvailableUserProviders(): array
    {
        return array_keys($this->getServiceConfig()->getSettings()['user_providers']);
    }

    public function getLoginStatus(): string
    {
        return $this->loginStatus;
    }
}
