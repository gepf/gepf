<?php

namespace Gepf\Service\Security\EventListener;

use Gepf\Core\Event\AbstractEventListener;
use Gepf\Service\Security\Security;

class CheckAuthListener extends AbstractEventListener
{
    public function __construct(
        private readonly Security $security
    ) {}

    public function execute(): void
    {
        $this->security->checkAuth();
    }
}
