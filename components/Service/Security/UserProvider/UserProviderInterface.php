<?php

namespace Gepf\Service\Security\UserProvider;

interface UserProviderInterface
{
    public function getHashForUser(string $username): ?string;
}
