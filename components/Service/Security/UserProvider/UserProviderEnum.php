<?php

namespace Gepf\Service\Security\UserProvider;

use Gepf\Core\Enum;

class UserProviderEnum extends Enum
{
    public const PROVIDER_MAP = 'map';
    public const PROVIDER_PAM = 'pam';


    public static function getAllowedValues(): array
    {
        return [self::PROVIDER_MAP, self::PROVIDER_PAM];
    }
}
