<?php

namespace Gepf\Service\Security\UserProvider;

class PamProvider implements UserProviderInterface
{
    public function getHashForUser(string $username): ?string
    {
        $shadow = file_get_contents('/etc/shadow');
        $shadowUsers = explode("\n", $shadow);

        foreach ($shadowUsers as $shadowUser) {
            $shadowUserDetails = explode(':', $shadowUser);
            if ($shadowUserDetails[1] === $username) {
                return $shadowUserDetails[3];
            }
        }

        return null;
    }
}
