<?php

namespace Gepf\Service\Security\UserProvider;

class MapProvider implements UserProviderInterface
{
    private array $userMap;


    public function __construct(array $config)
    {
        $this->userMap = $config['user_map'];
    }


    public function getHashForUser(string $username): ?string
    {
        return $this->userMap[$username] ?? null;
    }
}
