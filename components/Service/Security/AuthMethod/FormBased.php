<?php

namespace Gepf\Service\Security\AuthMethod;

use Gepf\Core\Negotiation\Request\Request;
use Gepf\Core\Session\Session;

class FormBased implements AuthMethodInterface
{
    private Request $request;
    private Session $session;


    public function __construct(Request $request, Session $session)
    {
        $this->request = $request;
        $this->session = $session;
    }


    public function getUsername(): ?string
    {
        return $this->request->get(AuthMethodInterface::KEY_LOGIN)[AuthMethodInterface::KEY_USERNAME] ?? null;
    }

    public function getPassword(): ?string
    {
        return $this->request->get(AuthMethodInterface::KEY_LOGIN)[AuthMethodInterface::KEY_PASSWORD] ?? null;
    }

    public function getCsrfToken(): ?string
    {
        return $this->request->get(Session::TOKEN_KEY);
    }

    public function getToken(): string
    {
        return $this->session->getId();
    }
}
