<?php

namespace Gepf\Service\Security\AuthMethod;

use Gepf\Core\Negotiation\Request\Request;
use Gepf\Core\Session\Session;

interface AuthMethodInterface
{
    public const KEY_LOGIN = 'login';
    public const KEY_USERNAME = 'username';
    public const KEY_PASSWORD = 'password';

    public function __construct(Request $request, Session $session);

    public function getUsername(): ?string;

    public function getPassword(): ?string;

    public function getCsrfToken(): ?string;

    public function getToken(): string;
}
