<?php

namespace Gepf\Service\User\Role;

use Gepf\Core\Collection;

class RoleCollection extends Collection
{
    public function get(int $key): Role
    {
        return parent::get($key);
    }

    public function current(): Role
    {
        return parent::current();
    }
}
