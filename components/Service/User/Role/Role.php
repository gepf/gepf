<?php

namespace Gepf\Service\User\Role;

use Gepf\Service\User\EntityRightCollection;

class Role
{
    private string $name;
    private array $routes;
    private array $workflows;
    private EntityRightCollection $entityRights;
    private RoleCollection $inheritedRoles;

    private bool $isAdmin;

    public function canRoute(string $route): bool
    {
        return in_array($route, $this->routes, true);
    }
}
