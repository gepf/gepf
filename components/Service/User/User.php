<?php

namespace Gepf\Service\User;

use Gepf\Service\RestOrm\Attributes\Cardinality\ManyToMany;
use Gepf\Service\RestOrm\Attributes\Constraint\Email;
use Gepf\Service\RestOrm\Attributes\Type\Char;
use Gepf\Service\RestOrm\Attributes\Type\Password;
use Gepf\Service\Security\UserInterface;
use Gepf\Service\User\Role\Role;

class User implements UserInterface
{
    #[Char(100)]
    private string $name;

    #[Char(191), Email]
    private string $email;

    #[Password]
    private string $password;

    /**
     * @var Role[]
     */
    #[ManyToMany]
    private array $roles;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    public function getRoles(): array
    {
        return $this->roles;
    }

    public function setRoles(array $roles): void
    {
        $this->roles = $roles;
    }

//	public function canRoute(string $route): bool
//	{
//		return $this->getRole()->canRoute($route);
//	}


    public function getUsername(): string
    {
        return $this->getEmail();
    }
}
