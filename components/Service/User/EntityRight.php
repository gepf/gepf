<?php

namespace Gepf\Service\User;

use Gepf\Service\User\Role\Role;

class EntityRight
{
    private Role $role;

    private string $className;

    private bool $read;
    private bool $create;
    private bool $edit;
    private bool $delete;

    private bool $deleteIfOwned;
    private bool $editIfOwned;
}
