<?php

namespace Gepf\Service\User;

use Gepf\Core\Collection;

class EntityRightCollection extends Collection
{
    public function get(int $key): EntityRight
    {
        return parent::get($key);
    }

    public function current(): EntityRight
    {
        return parent::current();
    }
}
