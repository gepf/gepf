<?php

namespace Gepf\Service;

interface ServiceConfigInterface
{
    public function getClass(): string;

    public function getName(): string;
}
