<?php

namespace Gepf\Service;

use Gepf\Core\Config\ConfigException;
use Gepf\Core\DependencyInjection\ConfigurableService;
use Gepf\Core\FileManager;
use Gepf\Core\Locale;

class Editorial extends ConfigurableService
{
    public const BACKEND_JSON = 'json';

    public function __construct(private readonly FileManager $fileManager, private readonly Locale $locale) {}

    public static function getCallbackNames(): array
    {
        return ['text'];
    }

    public function text(): callable
    {
        $self = $this;
        return static function (string $identifier = 'default') use ($self) {
            return $self->get($identifier);
        };
    }

    /**
     * @throws ConfigException
     */
    public function get(string $domain = 'default'): array
    {
        if ($this->serviceConfig->get('backend') === self::BACKEND_JSON) {
            $fields = $this->fileManager->get('texts/' . $this->locale->getLanguage() . '_' . $domain . '.json');

            return json_decode($fields->getContent(), true, 512, JSON_THROW_ON_ERROR);
        }

        throw new ConfigException('Unknown Editorial backend: ' . $this->getServiceConfig()->get('backend'));
    }
}
