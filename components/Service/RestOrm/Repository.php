<?php

namespace Gepf\Service\RestOrm;

use Gepf\Service\RestOrm\Model\Model;
use Gepf\Service\RestOrm\Model\ModelBuilder;

class Repository
{
    private Model $model;
    private RestOrm $orm;


    public function __construct(string $entityName, RestOrm $orm)
    {
        // Assume, first part of namespace is app- or bundle name
        $token = strtolower(substr($entityName, 0, 2));

        $mb = new ModelBuilder();
        $this->model = $mb->createModel($entityName, $token);
        $this->orm = $orm;
    }


    public function findAll(): array
    {
        $this->orm->getQueryBuilder()->select($this->model->getScalarColumnNames());
        $this->orm->getQueryBuilder()->from($this->model->getName());

        $result = $this->orm->getQueryBuilder()->getResult();

        foreach ($result as $mappedEntity) {
            $this->orm->addQueriedEntity($mappedEntity);
        }

        return $result;
    }
}
