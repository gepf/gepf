<?php

namespace Gepf\Service\RestOrm\Command;

class ShowModel extends AbstractRestOrmCommand
{
    public static function getName(): string
    {
        return 'orm:model:show';
    }

    public function execute(): int
    {
        $appOrBundleName = $this->detectAppOrBundleName();

        /** @noinspection ForgottenDebugOutputInspection */
        print_r($this->restOrm->getMigrationManager()->getDataModelForAppOrBundle($appOrBundleName));

        return 0;
    }
}
