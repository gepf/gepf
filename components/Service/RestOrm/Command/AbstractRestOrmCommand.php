<?php

namespace Gepf\Service\RestOrm\Command;

use Gepf\Core\Command\AbstractCommand;
use Gepf\Core\Command\CommandManager;
use Gepf\Core\Config\Config;
use Gepf\Core\Runtime\Runtime;
use Gepf\Service\RestOrm\RestOrm;

abstract class AbstractRestOrmCommand extends AbstractCommand
{
    public function __construct(
        protected RestOrm $restOrm,
        private readonly Runtime $runtime,
        private readonly Config $config,
    ) {}


    protected function detectAppOrBundleName(): string
    {
        print "available apps \n";

        foreach ($this->runtime->getApps() as $environment) {
            print $environment->getName();
            print "\n";
        }

        $input = CommandManager::getOptionalArgument() ?? CommandManager::getInput('Bundle/App (leave empty for default app)');
        if ($input) {
            return $input;
        }

        if ($this->config->getDefaultApp()) {
            return $this->config->getDefaultApp();
        }

        if ($this->runtime->getSingleApp()) {
            return $this->runtime->getSingleApp()->getName();
        }

        throw new \RuntimeException('No default bundle/app detected');
    }
}
