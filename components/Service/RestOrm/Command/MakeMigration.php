<?php

namespace Gepf\Service\RestOrm\Command;

class MakeMigration extends AbstractRestOrmCommand
{
    public static function getName(): string
    {
        return 'orm:make-migration';
    }

    public function execute(): int
    {
        $this->restOrm->getMigrationManager()->writeOut($this->detectAppOrBundleName());

        return 0;
    }
}
