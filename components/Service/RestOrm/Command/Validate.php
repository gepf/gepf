<?php

namespace Gepf\Service\RestOrm\Command;

class Validate extends AbstractRestOrmCommand
{
    public static function getName(): string
    {
        return 'orm:validate';
    }

    public function execute(): int
    {
        $appOrBundleName = $this->detectAppOrBundleName();

        $migrationManager = $this->restOrm->getMigrationManager();

        if ($migrationManager->validate($appOrBundleName)) {
            print "Schema up to date\n";
            return 0;
        }

        print "Schema not up to date\n";
        print $migrationManager->generateUpMigration($appOrBundleName);

        return 0;
    }
}
