<?php

namespace Gepf\Service\RestOrm\Command;

class MigrateUp extends AbstractRestOrmCommand
{
    public static function getName(): string
    {
        return 'orm:migrate:up';
    }

    public function execute(): int
    {
        $this->restOrm->getMigrationManager()->migrateUp($this->detectAppOrBundleName());

        return 0;
    }
}
