<?php

namespace Gepf\Service\RestOrm\Command;

class ShowPersistedModel extends AbstractRestOrmCommand
{
    public static function getName(): string
    {
        return 'orm:model:show-persisted';
    }

    public function execute(): int
    {
        /** @noinspection ForgottenDebugOutputInspection */
        print_r($this->restOrm->getMigrationManager()->getPersistedDataModel($this->detectAppOrBundleName()));

        return 0;
    }
}
