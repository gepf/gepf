<?php

namespace Gepf\Service\RestOrm\Attributes\Type;

#[\Attribute]
class Integer implements ScalarFieldTypeInterface
{
    public const SQL_TYPE = 'INTEGER';

    public function getSqlType(): string
    {
        return self::SQL_TYPE;
    }
}
