<?php

namespace Gepf\Service\RestOrm\Attributes\Type;

interface LengthTypeInterface
{
    public function getLength(): int;
}
