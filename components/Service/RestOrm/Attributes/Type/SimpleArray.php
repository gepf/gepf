<?php

namespace Gepf\Service\RestOrm\Attributes\Type;

use Gepf\Service\RestOrm\Attributes\FieldTypeInterface;

#[\Attribute]
class SimpleArray implements FieldTypeInterface
{
    public function getSqlType(): string
    {
        return 'TEXT';
    }
}
