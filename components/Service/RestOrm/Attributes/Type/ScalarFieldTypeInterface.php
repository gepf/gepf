<?php

namespace Gepf\Service\RestOrm\Attributes\Type;

use Gepf\Service\RestOrm\Attributes\FieldTypeInterface;

interface ScalarFieldTypeInterface extends FieldTypeInterface
{
    public function getSqlType(): string;
}
