<?php

namespace Gepf\Service\RestOrm\Attributes\Type;

#[\Attribute]
class Char implements ScalarFieldTypeInterface, LengthTypeInterface
{
    public int $length;

    public function __construct(int $length)
    {
        $this->length = $length;
    }

    public function getSqlType(): string
    {
        return "varchar($this->length)";
    }

    public function getLength(): int
    {
        return $this->length;
    }
}
