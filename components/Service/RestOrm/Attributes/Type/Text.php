<?php

namespace Gepf\Service\RestOrm\Attributes\Type;

#[\Attribute]
class Text implements ScalarFieldTypeInterface
{
    public function getSqlType(): string
    {
        return 'TEXT';
    }
}
