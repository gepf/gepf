<?php


namespace Gepf\Service\RestOrm\Attributes\Type;

#[\Attribute]
class Boolean implements ScalarFieldTypeInterface
{
    public function getSqlType(): string
    {
        return 'tinyint(1)';
    }
}
