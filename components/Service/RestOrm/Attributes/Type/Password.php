<?php

namespace Gepf\Service\RestOrm\Attributes\Type;

#[\Attribute]
class Password implements ScalarFieldTypeInterface
{
    public function getSqlType(): string
    {
        return 'varchar(191)';
    }
}
