<?php

namespace Gepf\Service\RestOrm\Attributes\Cardinality;

use Gepf\Service\RestOrm\Attributes\FieldTypeInterface;

#[\Attribute]
class ManyToOne implements FieldTypeInterface
{

}
