<?php

namespace Gepf\Service\RestOrm\Attributes\Cardinality;

use Gepf\Service\RestOrm\Attributes\FieldTypeInterface;

#[\Attribute]
class OneToMany implements FieldTypeInterface
{
    private string $mappingEntity;

    public function __construct(string $mappingEntity)
    {
        $this->mappingEntity = $mappingEntity;
    }

    public function getMappingEntity(): string
    {
        return $this->mappingEntity;
    }
}
