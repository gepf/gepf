<?php

namespace Gepf\Service\RestOrm\Attributes\Cardinality;

use Gepf\Service\RestOrm\Attributes\FieldTypeInterface;

#[\Attribute]
class ManyToMany implements FieldTypeInterface
{

}
