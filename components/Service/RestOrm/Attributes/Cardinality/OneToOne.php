<?php

namespace Gepf\Service\RestOrm\Attributes\Cardinality;

use Gepf\Service\RestOrm\Attributes\FieldTypeInterface;

#[\Attribute]
class OneToOne implements FieldTypeInterface
{
    public const IS_MAPPING = true;

    private bool $mapping;

    public function __construct(bool $mapping = false)
    {
        $this->mapping = $mapping;
    }

    public function isMapping(): bool
    {
        return $this->mapping;
    }
}
