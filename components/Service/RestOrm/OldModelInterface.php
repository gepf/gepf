<?php

namespace Gepf\Service\RestOrm;

interface OldModelInterface
{
    public function getName(): string;

    public function getColumns(): array;

    public function getChildren(): array;
}
