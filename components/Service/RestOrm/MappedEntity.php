<?php

namespace Gepf\Service\RestOrm;

class MappedEntity
{
    private int $id;


    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function isPersisted(): bool
    {
        return isset($this->id);
    }
}
