<?php

namespace Gepf\Service\RestOrm\Model;

use Gepf\Service\RestOrm\Attributes\Type\Integer;

class Relation extends Field
{
    private string $relation;
    private string $originalName;


    public function __construct(string $relation, $name)
    {
        $this->relation = $relation;
        $this->originalName = $name;

        $this->setType(new Integer());
        $this->setScalar(false);

        parent::__construct($name . '_id');
    }


    public function getRelation(): string
    {
        return $this->relation;
    }


    public function getOriginalName(): string
    {
        return $this->originalName;
    }
}
