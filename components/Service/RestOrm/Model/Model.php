<?php

namespace Gepf\Service\RestOrm\Model;

class Model
{
    private string $name;

    /** @var Field[] */
    private array $fields;


    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function toArray(): array
    {
        if (!isset($this->fields)) {
            die($this->getName());
        }

        $columns = [];
        foreach ($this->getFields() as $column) {
            $columns[] = $column->toArray();
        }

        return $columns;
    }


    public function getName(): string
    {
        return $this->name;
    }

    public function hasFields(): bool
    {
        return isset($this->fields) && $this->fields;
    }

    /** @return Field[] */
    public function getFields(): array
    {
        return $this->fields;
    }

    public function addField(Field $column): void
    {
        $this->fields[$column->getName()] = $column;
    }

    public function getScalarColumnNames(): array
    {
        return array_map(static function (Field $column) {
            return $column->getName();
        },
            array_filter($this->getFields(), static function (Field $column) {
                return $column->isScalar();
            }));
    }
}
