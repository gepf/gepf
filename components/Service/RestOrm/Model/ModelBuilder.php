<?php

namespace Gepf\Service\RestOrm\Model;

use Gepf\Core\FileSystem\RecursiveFileCollection;
use Gepf\Core\Util\Naming;
use Gepf\Core\Util\Text;
use Gepf\Service\RestOrm\Attributes\Cardinality\ManyToMany;
use Gepf\Service\RestOrm\Attributes\Cardinality\ManyToOne;
use Gepf\Service\RestOrm\Attributes\Cardinality\OneToMany;
use Gepf\Service\RestOrm\Attributes\Cardinality\OneToOne;
use Gepf\Service\RestOrm\Attributes\Constraint\ConstraintInterface;
use Gepf\Service\RestOrm\Attributes\FieldTypeInterface;
use Gepf\Service\RestOrm\Attributes\Type\ScalarFieldTypeInterface;

class ModelBuilder
{
    /** @var Model[] */
    private array $tables;

    public function toArray(): array
    {
        $tables = [];
        foreach ($this->getTables() as $table) {
            $tables[$table->getName()] = $table->toArray();
        }

        return $tables;
    }


    public function createDataModelForBundle(string $bundle, ?string $prefix = null): array
    {
        return $this->createDataModelFor($bundle, "bundles/$bundle/app/Entity", "bundles\\$bundle\\app", $prefix);
    }


    public function createDataModelForApp(string $app, ?string $prefix = null): array
    {
        $humanizedApp = Text::humanize($app);
        return $this->createDataModelFor($app, "apps/$humanizedApp/Entity", "apps\\$humanizedApp", $prefix);
    }


    public function createDataModelForSingleApp(string $app, ?string $prefix = null): array
    {
        return $this->createDataModelFor($app, 'app/Entity', 'app', $prefix);
    }


    private function createDataModelFor(string $app, string $root, string $transformedRoot, ?string $prefix = null): array
    {
        $prefix = $prefix ?? substr($app, 0, 2);
        $files = new RecursiveFileCollection([$root]);
        foreach ($files->getFiles() as $file) {
            $className = str_replace(
                ['/', '.php', $transformedRoot],
                ['\\', '', Text::humanize($app, '')],
                $file->getPathname()
            );

            $this->addTable($this->createModel($className, $prefix));
            $this->updateReferenceTables($className, $prefix);
        }

        return $this->toArray();
    }


    public function getTables(): array
    {
        return $this->tables;
    }

    public function addTable(Model $table): void
    {
        $this->tables[] = $table;
    }


    public function createModel(string $entityClassName, string $prefix = null): Model
    {
        $reflectedEntity = new \ReflectionClass($entityClassName);
        $classShortName = Text::snakeCase($reflectedEntity->getShortName());

        $table = new Model($prefix ? $prefix . '_' . $classShortName : $classShortName);

        foreach ($reflectedEntity->getProperties() as $property) {
            $column = $this->createColumnFromAttributes($property->getName(), $property->getAttributes(), $property->getType()?->getName());
            if ($column) {
                $column->setNotnull(!$property->getType() || !$property->getType()->allowsNull());
                $table->addField($column);
            }
        }

        if (!$table->hasFields()) {
            throw new ModelException("Entity $entityClassName has no mapped fields");
        }

        return $table;
    }


    private function updateReferenceTables(string $entityClassName, string $prefix = null): void
    {
        $reflectedEntity = new \ReflectionClass($entityClassName);
        $classShortName = Text::snakeCase($reflectedEntity->getShortName());

        foreach ($reflectedEntity->getProperties() as $property) {
            $columnType = $this->getColumnType($property->getAttributes());

            if (!$columnType instanceof ManyToMany) {
                continue;
            }

            $propertyClassName = $property->getType()?->getName();
            if (!$propertyClassName) {
                throw new ModelException('Mapped relations must be type hinted');
            }

            $propertyClassShortName = Naming::underscoreSubject($propertyClassName);

            // check if table exists; If it does exist, it means that it was created on the other side of the relation
            $tableNameA = $prefix ? $prefix . '_' . $classShortName . '_' . $propertyClassShortName : $classShortName . '_' . $propertyClassShortName;
            $tableNameB = $prefix ? $prefix . '_' . $propertyClassShortName . '_' . $classShortName : $propertyClassShortName . '_' . $classShortName;
            $tableExists = array_filter($this->getTables(), static fn(Model $model) => $model->getName() === $tableNameA || $model->getName() === $tableNameB);
            if ($tableExists) {
                continue;
            }

            $table = new Model($tableNameA);

            $relationA = new Relation($entityClassName, $classShortName);
            $relationA->setNotnull(true);
            $relationB = new Relation($propertyClassName, $propertyClassShortName);
            $relationB->setNotnull(true);

            $table->addField($relationA);
            $table->addField($relationB);

            $this->addTable($table);
        }
    }


    /**
     * @param \ReflectionAttribute[] $attributes
     */
    private function createColumnFromAttributes(string $name, array $attributes, string $relation = null): ?Field
    {
        $type = $this->getColumnType($attributes);
        $constraints = $this->getColumnConstraints($attributes);

        if (!$type) {
            return null;
        }

        if ($type instanceof ScalarFieldTypeInterface) {
            $column = new Field($name);
            $column->setType($type);
            $column->setScalar(true);
            $column->setConstraints($constraints);

            return $column;
        }

        if ($type instanceof OneToOne) {

            // Column will be created on mapping side
            if (!$type->isMapping()) {
                return null;
            }

            return new Relation($relation, $name);
        }

        if ($type instanceof ManyToOne) {
            return new Relation($relation, $name);
        }

        if ($type instanceof OneToMany) {
            // Column will be created on 'ManyToOne' side
            return null;
        }

        if ($type instanceof ManyToMany) {
            // Column will be created in reference table
            return null;
        }

        throw new ModelException('Unknown Attribute: ' . \get_class($type));
    }


    /**
     * @throws ModelException
     */
    private function getColumnType(array $attributes): ?FieldTypeInterface
    {
        $types = array_map(static function (\ReflectionAttribute $attribute) {
            return class_exists($attribute->getName()) ? $attribute->newInstance() : null;
        }, $attributes);

        $types = array_filter($types, static function (?object $attribute) {
            return $attribute instanceof FieldTypeInterface;
        });

        if (!$types) {
            return null;
        }

        if (\count($types) > 1) {
            throw new ModelException('Can not define more than one column types for one property');
        }

        $type = current($types);
        if (!$type instanceof FieldTypeInterface) {
            throw new \RuntimeException('Column type should implement ' . FieldTypeInterface::class);
        }

        return $type;
    }

    /**
     * @return ConstraintInterface[]
     */
    private function getColumnConstraints(array $attributes): array
    {
        $constraints = array_map(static function (\ReflectionAttribute $attribute) {
            return class_exists($attribute->getName()) ? $attribute->newInstance() : null;
        }, $attributes);

        return array_filter($constraints, static function (?object $attribute) {
            return $attribute instanceof ConstraintInterface;
        });
    }
}
