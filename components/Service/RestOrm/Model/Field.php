<?php

namespace Gepf\Service\RestOrm\Model;

use Gepf\Service\RestOrm\Attributes\Type\ScalarFieldTypeInterface;

class Field
{
    private string $name;
    private ScalarFieldTypeInterface $type;
    private bool $notnull;
    private bool $scalar;
    private array $constraints;


    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function toArray(): array
    {
        return [
            'name' => $this->getName(),
            'type' => $this->getType(),
            'notnull' => $this->isNotnull(),
        ];
    }


    public function getName(): string
    {
        return $this->name;
    }

    public function getType(): ScalarFieldTypeInterface
    {
        return $this->type;
    }

    public function setType(ScalarFieldTypeInterface $type): void
    {
        $this->type = $type;
    }

    public function isNotnull(): bool
    {
        return $this->notnull;
    }

    public function setNotnull(bool $notnull): void
    {
        $this->notnull = $notnull;
    }

    public function isScalar(): bool
    {
        return $this->scalar;
    }

    public function setScalar(bool $scalar): void
    {
        $this->scalar = $scalar;
    }

    public function getConstraints(): array
    {
        return $this->constraints;
    }

    public function setConstraints(array $constraints): void
    {
        $this->constraints = $constraints;
    }
}
