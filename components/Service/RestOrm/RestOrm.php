<?php

namespace Gepf\Service\RestOrm;

use Gepf\Core\DependencyInjection\ConfigurableService;
use Gepf\Core\Runtime\Runtime;
use Gepf\Core\Util\Property;
use Gepf\Service\RestOrm\Command\MakeMigration;
use Gepf\Service\RestOrm\Command\MigrateUp;
use Gepf\Service\RestOrm\Command\ShowModel;
use Gepf\Service\RestOrm\Command\ShowPersistedModel;
use Gepf\Service\RestOrm\Command\Validate;
use Gepf\Service\RestOrm\Migration\MigrationManager;
use Gepf\Service\RestOrm\Model\Model;
use Gepf\Service\RestOrm\Model\ModelBuilder;
use Gepf\Service\Sql\Query\QueryBuilder;
use Gepf\Service\Sql\SqlManager;

class RestOrm extends ConfigurableService
{
    private SqlManager $sqlManager;
    private MigrationManager $migrationManager;
    private QueryBuilder $queryBuilder;
    private array $queriedEntities;


    public function __construct(SqlManager $sqlManager, Runtime $runtime)
    {
        $this->sqlManager = $sqlManager;
        $this->migrationManager = new MigrationManager($sqlManager, $runtime);
        $this->queryBuilder = new QueryBuilder($this->getSqlManager());
    }


    public static function getCommands(): array
    {
        return [
            MakeMigration::class,
            MigrateUp::class,
            ShowModel::class,
            ShowPersistedModel::class,
            Validate::class,
        ];
    }


    /**
     * @noinspection PhpUnused
     */
    public function getRepository(string $entityClassName): Repository
    {
        return new Repository($entityClassName, $this);
    }


    public function getSqlManager(): SqlManager
    {
        return $this->sqlManager;
    }

    public function getMigrationManager(): MigrationManager
    {
        return $this->migrationManager;
    }

    public function getQueryBuilder(): QueryBuilder
    {
        return $this->queryBuilder;
    }

    public function addQueriedEntity(MappedEntity $entity): void
    {
        $identifier = $entity::class . ':' . $entity->getId();
        if (isset($this->queriedEntities[$identifier])) {
            throw new OrmException('Trying to query entity that was previously queried.');
        }

        $this->queriedEntities[$identifier] = $entity;
    }


    /** @noinspection PhpUnhandledExceptionInspection */
    public function persist(MappedEntity $entity): void
    {
        // Assume, first part of namespace is app- or bundle name
        $token = strtolower(substr($entity::class, 0, 2));

        $mb = new ModelBuilder();
        $model = $mb->createModel($entity::class, $token);
        $values = $this->mapEntityToValues($entity, $model);

        // CREATE NEW
        if (!$entity->isPersisted()) {
            $this->getQueryBuilder()->insert($model->getName());
            $this->getQueryBuilder()->values($values);

            $id = $this->getQueryBuilder()->getSingleResult();

            if (!$id) {
                throw new OrmException('Could not persist entity: ' . $entity::class);
            }

            $entity->setId($id);

            return;
        }

        $identifier = $entity::class . ':' . $entity->getId();
        if (!isset($this->queriedEntities[$identifier]) && $entity->getId()) {
            throw new OrmException('Trying to persist existing entity that was not registered in ORM previously.');
        }
    }


    private function mapEntityToValues(MappedEntity $entity, Model $model): array
    {
        $values = [];
        foreach ($model->getFields() as $field) {

            // TODO implement forms for collection types
            if (!$field->isScalar()) {
                continue;
            }

            $fieldName = $field->getName();
            $values[$fieldName] = Property::access($entity, $fieldName);
        }

        return $values;
    }
}
