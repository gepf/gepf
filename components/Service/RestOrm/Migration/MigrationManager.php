<?php

namespace Gepf\Service\RestOrm\Migration;

use DateTimeImmutable;
use Gepf\Core\FileSystem\File;
use Gepf\Core\FileSystem\RecursiveFileCollection;
use Gepf\Core\Runtime\Runtime;
use Gepf\Service\RestOrm\Model\ModelBuilder;
use Gepf\Service\Sql\SqlManager;

readonly class MigrationManager
{
    public function __construct(
        private SqlManager $queryBuilder,
        private Runtime $runtime,
    ) {}


    public function getPersistedTableNames(string $bundle = null, string $token = null): array
    {
        return array_keys($this->getPersistedDataModel($bundle, $token));
    }


    public function getDefinedTableNames(string $bundle): array
    {
        return array_keys($this->getDataModelForAppOrBundle($bundle));
    }


    public function getTablesToDelete(string $bundle): array
    {
        return array_diff($this->getPersistedTableNames($bundle), $this->getDefinedTableNames($bundle));
    }


    public function getTablesToCreate(string $bundle): array
    {
        return array_diff($this->getDefinedTableNames($bundle), $this->getPersistedTableNames($bundle));
    }


    public function getTablesToUpdate(string $bundle): array
    {
        $commonTables = array_intersect($this->getDefinedTableNames($bundle), $this->getPersistedTableNames($bundle));
        $model = $this->getDataModelForAppOrBundle($bundle);
        $persisted = $this->getPersistedDataModel($bundle);

        $tablesToUpdate = [];
        foreach ($commonTables as $table) {
            if ($model[$table] === $persisted[$table]) {
                continue;
            }

            $tablesToUpdate[] = $table;
        }

        return $tablesToUpdate;
    }


    public function validate(string $bundle): bool
    {
        return !$this->getTablesToCreate($bundle) && !$this->getTablesToDelete($bundle) && !$this->getTablesToUpdate($bundle);
    }


    public function generateUpMigration(string $bundle): string
    {
        $out = '';
        foreach ($this->getTablesToDelete($bundle) as $table) {
            $out .= "DROP TABLE $table;\n";
        }

        $out .= $this->generateCreateStatement($this->getTablesToCreate($bundle), $this->getDataModelForAppOrBundle($bundle));

        return $out;
    }


    public function generateDownMigration(string $bundle): string
    {
        $out = '';
        foreach ($this->getTablesToCreate($bundle) as $table) {
            $out .= "DROP TABLE $table;\n";
        }

        $out .= $this->generateCreateStatement($this->getTablesToDelete($bundle), $this->getPersistedDataModel());

        return $out;
    }


    private function generateCreateStatement(array $tablesNames, array $model): string
    {
        $out = '';
        foreach ($tablesNames as $table) {
            $out .= "CREATE TABLE $table (\n";
            $columnSql = [];
            foreach ($model[$table] as $column) {
                $columnSql[] = '    ' . $column['name'] . ' ' . $column['type'] . ($column['notnull'] ? ' NOT NULL' : '');
            }
            $out .= implode(",\n", $columnSql);
            $out .= "\n);\n";
        }

        return $out;
    }


    public function writeOut(string $bundle): bool
    {
        $date = (new DateTimeImmutable())->format('YmdHis');
        file_put_contents("bundles/$bundle/migrations/$date-up.sql", $this->generateUpMigration($bundle));
        file_put_contents("bundles/$bundle/migrations/$date-down.sql", $this->generateDownMigration($bundle));

        return true;
    }


    public function getDataModelForAppOrBundle(string $bundle): array
    {
        $applicationType = $this->runtime->getMode();
        $modelBuilder = new ModelBuilder();

        return match ($applicationType) {
            Runtime::MODE_APP => $modelBuilder->createDataModelForSingleApp($bundle),
            Runtime::MODE_VENDOR => $modelBuilder->createDataModelForApp($bundle),
            Runtime::MODE_BUNDLE => $modelBuilder->createDataModelForBundle($bundle),
            default => [],
        };
    }


    public function getPersistedDataModel(string $bundle = null, string $token = null): array
    {
        $token = $token ?? substr($bundle, 0, 2);
        $schema = $this->queryBuilder->getSchema();

        if ($bundle) {
            $schema = array_filter($schema, static function ($key) use ($token) {
                return str_starts_with($key, $token . '_');
            }, ARRAY_FILTER_USE_KEY);
        }

        foreach ($schema as &$table) {
            $table = array_map(static function (array $column) {
                // Remove unsupported features from columns
                unset($column['cid'], $column['dflt_value'], $column['pk']);

                // Sqlite provides 'notnull' as integer field
                $column['notnull'] = (bool)$column['notnull'];

                return $column;
            }, $table);
        }

        return $schema;
    }


    public function migrateUp(string $bundle, string $token = null): bool
    {
        $token = $token ?? substr($bundle, 0, 2);

        $this->queryBuilder->rawQuery('CREATE TABLE IF NOT EXISTS migration (number int(14) NOT NULL, app char(8) NOT NULL)');
        $executedMigrations = $this->queryBuilder->rawQuery("SELECT number FROM migration WHERE app = '$token'");
        $executedMigrations = array_map(static function (array $row) {
            return $row['number'];
        }, $executedMigrations);
        $foundMigrations = new RecursiveFileCollection(["bundles/$bundle/migrations"]);

        $upMigrations = array_filter($foundMigrations->getFiles(), static function (File $file) {
            return str_contains($file->getFilename(), 'up');
        });

        usort($upMigrations, static function (File $a, File $b) {
            return $a->getFilename() <=> $b->getFilename();
        });

        foreach ($upMigrations as $upMigration) {
            $number = (int)str_replace('-up', '', $upMigration->getFilename());

            if (\in_array($number, $executedMigrations, true)) {
                continue;
            }

            $this->queryBuilder->rawExec($upMigration->getContent());
            $this->queryBuilder->rawQuery("INSERT INTO migration VALUES ('$number', '$token')");
        }

        return true;
    }
}
