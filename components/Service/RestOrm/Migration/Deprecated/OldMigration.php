<?php

namespace Gepf\Service\RestOrm\Migration\Deprecated;

use Gepf\Core\FileSystem\Util\Directory;
use Gepf\Service\RestOrm\OldAbstractOldModel;
use Gepf\Service\RestOrm\OldModelInterface;
use Gepf\Service\Sql\SqlManager;

class OldMigration
{
    private SqlManager $queryBuilder;


    public function __construct(SqlManager $queryBuilder)
    {
        $this->queryBuilder = $queryBuilder;
    }


    public function compare(): array
    {
        $models = $this->getModels();
        $tables = $this->queryBuilder->getSchema();

        return $this->compareSchemas($models, $tables);
    }


    public function getModels(): array
    {
        foreach (Directory::list('apps') as $path) {
            if (is_file($path)) {
                /** @noinspection PhpIncludeInspection */
                require $path;
            }
        }

        $models = [];
        foreach (get_declared_classes() as $className) {
            if ($className !== OldAbstractOldModel::class && in_array(OldModelInterface::class, class_implements($className), true)) {
                $models[] = new $className();
            }
        }

        return $models;
    }


    /**
     * @param OldModelInterface[] $models
     */
    private function compareSchemas(array $models, array $tables): array
    {
        $migrationSuggestions = [];
        foreach ($models as $n => $model) {

            $modelName = $model->getName();

            if (!isset($tables[$modelName])) {

                $columnStatements = ['id INTEGER PRIMARY KEY AUTOINCREMENT'];
                foreach ($model->getColumns() as $column => $properties) {
                    $columnStatements[] = $column . ' ' . implode(' ', $properties);
                }

                foreach ($model->getChildren() as $childProperties) {

                    if ($childProperties['cardinality'] === 'many_to_many') {
                        // Referring tables will be checked in a different step
                        continue;
                    }

                    if ($childProperties['cardinality'] === 'one_to_many') {
                        // Foreign must be created on inverting side
                        continue;
                    }

                    /** @var OldModelInterface $child */
                    $child = new $childProperties['model']();

                    $columnStatements[] = "FOREIGN KEY({$childProperties['reference']}) REFERENCES {$child->getName()}(id)";
                }

                $migrationSuggestions[$n]['up'] = "CREATE TABLE $modelName (" . implode(', ', $columnStatements) . ')';
                $migrationSuggestions[$n]['down'] = "DROP TABLE $modelName";
            }
        }

        return $migrationSuggestions;
    }
}
