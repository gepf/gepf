<?php

namespace Gepf\Service\Sql;

use Gepf\Core\Config\ConfigException;
use Gepf\Core\DependencyInjection\ConfigurableService;
use Gepf\Core\DependencyInjection\Container\Container;
use Gepf\Service\ServiceConfigInterface;
use Gepf\Service\Sql\Driver\DriverInterface;
use Gepf\Service\Sql\Driver\MySqlI;
use Gepf\Service\Sql\Driver\Sqlite;
use Psr\Log\LoggerInterface;

class SqlManager extends ConfigurableService
{
    private SqlConfig $sqlConfig;
    private DriverInterface $sqlDriver;


    public function __construct(private readonly LoggerInterface $logger) {}

    public function setConfig(ServiceConfigInterface $serviceConfig): void
    {
        if (!$serviceConfig instanceof SqlConfig) {
            throw new ConfigException('SqlManager requires SqlConfig');
        }
        $this->sqlConfig = $serviceConfig;
    }

    /**
     * @param SqlConfig $serviceConfig
     */
    public static function create(Container $container, ServiceConfigInterface $serviceConfig): self
    {
        return new self($container->getLogger(), $serviceConfig);
    }


    public function getSchema(): array
    {
        return $this->getSqlDriver()->getSchema();
    }


    public function rawQuery(string $query): array
    {
        return $this->getSqlDriver()->query($query);
    }


    public function rawExec(string $query): bool
    {
        return $this->getSqlDriver()->exec($query);
    }


    protected function getSqlDriver(): DriverInterface
    {
        if (!isset($this->sqlDriver)) {
            try {
                $this->sqlDriver = $this->createSqlDriver();
            } catch (ConfigException $e) {
                $this->logger->error($e);
            }
        }

        return $this->sqlDriver;
    }


    /**
     * @throws ConfigException
     * @noinspection PhpComposerExtensionStubsInspection
     */
    private function createSqlDriver(): DriverInterface
    {
        $driver = $this->sqlConfig->getDriver() ?? \SQLite3::class;

        if ($driver === \SQLite3::class) {
            return new Sqlite($this->sqlConfig);
        }

        if ($driver === \mysqli::class) {
            return new MySqlI($this->sqlConfig);
        }

        throw new ConfigException('Unknown database driver configured: ' . $driver);
    }
}
