<?php

namespace Gepf\Service\Sql\Query;

class WhereClause
{
    public const COMPARISON_OPERATOR_IS = '=';
    public const COMPARISON_OPERATOR_IS_NOT = '!=';
    public const COMPARISON_OPERATOR_BIGGER_THAN = '>';
    public const COMPARISON_OPERATOR_SMALLER_THAN = '<';

    private string $column;
    private null|string|Query $value;
    private int $comparisonOperator;

    /**
     * @var WhereClause[]|WhereClause
     */
    private array|WhereClause $and;

    /**
     * @var WhereClause[]|WhereClause
     */
    private array|WhereClause $or;

    public function getColumn(): string
    {
        return $this->column;
    }

    public function setColumn(string $column): void
    {
        $this->column = $column;
    }

    public function getValue(): Query|string|null
    {
        return $this->value;
    }

    public function setValue(Query|string|null $value): void
    {
        $this->value = $value;
    }

    public function getComparisonOperator(): int
    {
        return $this->comparisonOperator;
    }

    public function setComparisonOperator(int $comparisonOperator): void
    {
        $this->comparisonOperator = $comparisonOperator;
    }

    public function getAnd(): array|WhereClause
    {
        return $this->and;
    }

    public function setAnd(array|WhereClause $and): void
    {
        $this->and = $and;
    }

    public function getOr(): array|WhereClause
    {
        return $this->or;
    }

    public function setOr(array|WhereClause $or): void
    {
        $this->or = $or;
    }
}
