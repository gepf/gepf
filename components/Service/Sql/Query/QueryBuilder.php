<?php /** @noinspection PhpUnhandledExceptionInspection */

namespace Gepf\Service\Sql\Query;

use Gepf\Service\Sql\SqlException;
use Gepf\Service\Sql\SqlManager;

class QueryBuilder
{
    private Query $query;
    private SqlManager $sqlManager;

    public function __construct(SqlManager $sqlManager)
    {
        $this->query = new Query();
        $this->sqlManager = $sqlManager;
    }

    public function select(array $columns): void
    {
        $this->query->setType(Query::TYPE_SELECT);
        $this->query->setColumns($columns);
    }

    public function insert(string $table): void
    {
        $this->query->setType(Query::TYPE_INSERT);
        $this->query->setTable($table);
    }

    public function from(string $table): void
    {
        $this->query->setTable($table);
    }

    public function values(array $values): void
    {
        $typedValues = [];
        foreach ($values as $key => $value) {
            if ($value instanceof Value) {
                $typedValues[] = $value;
                continue;
            }

            if (is_string($key) && is_scalar($value)) {
                $typedValues[] = new Value($key, $value);
                continue;
            }

            throw new SqlException('Invalid parameters provided');
        }

        $this->query->setValues($typedValues);
    }

//	public function getQuery(): Query
//	{
//		return $this->query;
//	}

    public function getRawQuery(): string
    {
        return match ($this->query->getType()) {
            Query::TYPE_SELECT => 'SELECT ' . $this->query->getColumnsAsString()
                . ' FROM ' . $this->query->getTable()
//				. ' '. $this->innerJoinStatement
//				. ' '. $this->leftJoinStatement
//				. ' '. $this->whereStatement
//				. ' '. $this->orderStatement
//				. ' '. $this->limitStatement
//				. ' '. $this->offsetStatement
        ,
            Query::TYPE_INSERT => "INSERT INTO {$this->query->getTable()} VALUES ({$this->getValuesExpr()})",
            Query::TYPE_UPDATE => "UPDATE {$this->query->getTable()} SET ({$this->getValuesExpr()}) WHERE {$this->getWhereExpr()}",
            Query::TYPE_DELETE => "DELETE FROM {$this->query->getTable()} WHERE {$this->getWhereExpr()}",
            default => throw new SqlException("Unknown query type: {$this->query->getType()}"),
        };
    }

    public function getSingleResult(): int
    {
        $query = $this->getRawQuery();
        $result = $this->sqlManager->rawQuery($query);

        if (count($result) > 1) {
            throw new SqlException('More than one rows returned');
        }

        return current($result);
    }

    public function getResult(): array
    {
        $query = $this->getRawQuery();
        $result = $this->sqlManager->rawQuery($query);

        $sanitizedRows = [];
        foreach ($result as $row) {
            $key = key($row);
            $sanitizedRows[] = $row[$key];
        }

        return $sanitizedRows;
    }

    private function getValuesExpr(): string
    {
        return implode(
            ', ',
            array_map(static function (Value $value) {
                return "{$value->getKey()} = '{$value->getContent()}'";
            }, $this->query->getValues()),
        );
    }

    private function getWhereExpr(): string
    {
        return 'test';
    }
}
