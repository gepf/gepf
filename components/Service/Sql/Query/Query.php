<?php

namespace Gepf\Service\Sql\Query;

class Query
{
    public const TYPE_SELECT = 1;
    public const TYPE_INSERT = 2;
    public const TYPE_UPDATE = 3;
    public const TYPE_DELETE = 4;

    public const TYPES = [
        self::TYPE_SELECT,
        self::TYPE_INSERT,
        self::TYPE_UPDATE,
        self::TYPE_DELETE,
    ];

    private int $type;
    private array $columns;
    private string $table;

    /**
     * @var Value[]
     */
    private array $values;

    /** @var WhereClause[] */
    private array $where;
    private ?string $orderBy = null;
    private ?int $offset = null;
    private ?int $limit = null;


    public function getType(): int
    {
        return $this->type;
    }

    public function setType(int $type): void
    {
        $this->type = $type;
    }

    public function getColumns(): array
    {
        return $this->columns;
    }

    public function getColumnsAsString(): string
    {
        return implode(' ,', $this->getColumns());
    }

    public function setColumns(array $columns): void
    {
        $this->columns = $columns;
    }

    public function getTable(): string
    {
        return $this->table;
    }

    public function setTable(string $table): void
    {
        $this->table = $table;
    }

    public function getValues(): array
    {
        return $this->values;
    }

    public function setValues(array $values): void
    {
        $this->values = $values;
    }

    public function getWhere(): array
    {
        return $this->where;
    }

    public function setWhere(array $where): void
    {
        $this->where = $where;
    }

    public function getOrderBy(): ?string
    {
        return $this->orderBy;
    }

    public function setOrderBy(?string $orderBy): void
    {
        $this->orderBy = $orderBy;
    }

    public function getOffset(): ?int
    {
        return $this->offset;
    }

    public function setOffset(?int $offset): void
    {
        $this->offset = $offset;
    }

    public function getLimit(): ?int
    {
        return $this->limit;
    }

    public function setLimit(?int $limit): void
    {
        $this->limit = $limit;
    }
}
