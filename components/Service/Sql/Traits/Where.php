<?php

namespace Gepf\Service\Sql\Traits;

use core\Exception_handler;

trait     Where
{


    public function where($param, $value = false, $uniq = false, $comp = '=')
    {

        if ($param === true) {
            $this->where = true;
            return $this;
        } elseif (!strict($param)) {
            $this->where = '__unset';
            return $this;
        } elseif (is_array($param)) {
            if (Arr::isAssoc($param)) {
                foreach ($param as $col => $val) {
                    $clauses[] = array($col, $val);
                }
            } else {
                if (is_array($param[0]) || is_numeric($param[0])) {
                    $clauses = $param;
                    if ($comp === '=') {
                        $this->forceId = true;
                    }
                } else {
                    $clauses[0] = $param;
                    if (Arr::get(3, $param) === '=') {
                        $this->forceId = true;
                    }
                }
            }
        } else {
            $clauses[0] = array($param, $value, $uniq, $comp);
        }


        foreach ($clauses as $clause) {

            if (!is_array($clause)) {
                $tmp = $clause;
                unset($clause);
                $clause[0] = $tmp;
            }

            if (!isset($clause[2])) {
                $clause[2] = false;
            }
            if (!isset($clause[3])) {
                $clause[3] = '=';
            }

            if (
                !is_numeric($clause[0]) &&
                (!isset($clause[1]) || $clause[1] === false) &&
                count(explode(' ', $clause[0])) === 1
            ) {
                throw new Exception_handler('Single word passed to sql->where: ' . $clause[0]);
                $this->where = '__unset';
                return $this;
            } elseif (is_numeric($clause[0])) {
                $col = $this->table ? $this->table . '.id' : 'id';
                $this->where[] = array(
                    'col' => 'id',
                    'val' => $clause[0],
                    'uniq' => true,
                    'comp' => '=',
                    'stmt' => $col . ' = ' . $clause[0],
                );
            } elseif (empty($clause[0])) {
                $this->where[] = array(
                    'uniq' => false,
                    'stmt' => false,
                );
                throw new Exception_handler('Empty value passed to DB->where');
            } elseif (!isset($clause[1]) || $clause[1] === false) {
                $this->where[] = array(
                    'col' => '__IN_STATEMENT',
                    'val' => '__IN_STATEMENT',
                    'uniq' => $clause[2],
                    'comp' => '__IN_STATEMENT',
                    'stmt' => $clause[0],
                );
            } else {

                $val = '"' . $clause[1] . '"';
                $comp = $clause[3];

                $this->where[] = array(
                    'col' => $clause[0],
                    'val' => $val,
                    'uniq' => $clause[2],
                    'comp' => $comp,
                    'stmt' => $clause[0] . ' ' . $comp . ' ' . $val,
                );
            }
        }

        return $this;
    }


}
