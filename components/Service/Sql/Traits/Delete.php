<?php

namespace Gepf\Service\Sql\Traits;

use Gepf\Service\Sql\SqlException;

trait Delete
{
    /**
     * @throws SqlException
     */
    public function delete($table = false, $where = false)
    {
        if ($table) {
            $this->setTable($table);
        }
        if ($where) {
            $this->where($where);
        }

        if (!$this->where) {
            throw new SqlException('No param given for sql->delete.');
        }
        if (!$this->table) {
            throw new SqlException('No table given for sql->delete.');
        }

        $this->where_stmt = $this->buildWhereStmt($this->where);

        $this->query = 'DELETE FROM ' . $this->table . ' WHERE ' . $this->where_stmt;
        $this->result = mysqli_query($this->conn, $this->query);
        $result = mysqli_affected_rows($this->conn);

        if ($this->verbose) {
            Arr::dump($this->query);
        }

        $this->reset();
        return $result;
    }
}
