<?php

namespace Gepf\Service\Sql\Traits;

trait Select
{
    public function getResult(): array
    {
        $this->buildStatements();

        $query =
            ' SELECT ' . $this->columnsStatement .
            ' FROM ' . $this->table .
            ' ' . $this->innerJoinStatement .
            ' ' . $this->leftJoinStatement .
            ' ' . $this->whereStatement .
            ' ' . $this->orderStatement .
            ' ' . $this->limitStatement .
            ' ' . $this->offsetStatement;

        $sqlDriver = $this->getSqlDriver();
        $this->result = $sqlDriver->query($query);

        /** SANITIZE */

        if (str_contains($this->columnsStatement, ',') || str_contains($this->columnsStatement, '*')) {
            return $this->result;
        }

        $sanitizedRows = [];
        foreach ($this->result as $row) {
            $key = key($row);
            $sanitizedRows[] = $row[$key];
        }

        return $sanitizedRows;
    }
}
