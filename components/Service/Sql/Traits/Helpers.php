<?php

namespace Gepf\Service\Sql\Traits;

use core\Exception_handler;

trait Helpers
{
    public function getRowCount($table = false)
    {

        if (!$table) {
            $table = $this->table;
        }
        if (!$table) {
            return false;
        }

        $query = 'SHOW TABLE STATUS WHERE name = "' . $table . '"';

        $result = mysqli_query($this->conn, $query);
        $result = mysqli_fetch_array($result, MYSQLI_ASSOC)['Rows'];

        return $result;
    }


    public function plain_query($promt)
    {

        $result = mysqli_query($this->conn, $promt);

        if ($this->conn->errno) {
            throw new Exception_handler($this->conn->error);
        }

        if (!$result) {
            return false;
        } else {

            while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
                $rows[] = $row;
            }

            if (!empty($rows)) {
                return $rows;
            } else {
                return false;
            }
        }
    }
}
