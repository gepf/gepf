<?php

namespace Gepf\Service\Sql\Traits;

trait Statements
{
    private function buildStatements(): void
    {
        $this->columnsStatement = implode(', ', $this->columns);

        $this->whereStatement = $this->buildWhereStmt($this->whereClauses);
        $this->orderStatement = $this->orderBy ? 'ORDER BY ' . $this->orderBy : '';
        $this->offsetStatement = $this->offset ? 'OFFSET ' . $this->offset : '';
        $this->limitStatement = $this->limit ? 'LIMIT ' . $this->limit : '';

        $this->innerJoinStatement = $this->innerJoin ?
            'INNER JOIN ' . $this->innerJoin['table']
            . ' ON ' . $this->table . '.id = '
            . $this->innerJoin['table'] . '.' . $this->innerJoin['column']
            : '';

        $this->leftJoinStatement = $this->leftJoin ?
            'LEFT JOIN ' . $this->leftJoin['table']
            . ' ON ' . $this->table . '.' . $this->leftJoin['on'] . ' = ' . $this->leftJoin['table'] . '.id'
            : '';
    }


    // TODO Where clause must be object
    private function buildWhereStmt(?array $whereClauses = null): string
    {
        if (!$whereClauses) {
            return '';
        }

        $whereStatement = '';
        foreach ($whereClauses as $whereClause) {
            $whereStatement .= $whereClause['stmt'];
        }

        return $whereStatement;
    }
}
