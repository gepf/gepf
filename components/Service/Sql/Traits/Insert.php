<?php

namespace Gepf\Service\Sql\Traits;

use Gepf\Service\Sql\SqlException;

trait Insert
{
    public function insert()
    {
        //!TODO if (isset ($this->columns)) checkColumnMismatch();

        $values = implode(', ', $this->values);
        $columns = implode(', ', $this->columns);
        $columns = ' (id, ' . $columns . ')';

        $this->query = 'INSERT INTO ' . $this->table . $columns . ' VALUES (NULL,' . $values . ')';
        $this->result = mysqli_query($this->conn, $this->query);

        if ($this->conn->errno) {
            throw new SqlException($this->conn->error . '. ' . $this->query);
            $return = false;
        } else {
            $return = mysqli_insert_id($this->conn);
            if (empty($return)) {
                $return = false;
            }
        }

        // TODO log->debug($this->query)

        // $this->reset();

        return $return;
    }
}
