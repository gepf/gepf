<?php

namespace Gepf\Service\Sql\Traits;

trait Options
{
    public function addColumn(string $column): self
    {
        $this->columns[] = $column;

        return $this;
    }

    public function setColumns(array $columns): self
    {
        $this->columns = $columns;

        return $this;
    }


    public function setTable(string $table): self
    {
        $this->table = $table;

        return $this;
    }


    /**
     * TODO column should be column object, to make value validation possible
     *
     * @throws \Exception
     */
    public function addValue(/*?scalar*/ $value, ?string $column = null): self
    {
        if ($value !== null && !is_scalar($value)) {
            throw new \Exception('Value for query builder must be scalar');
        }

        $escapedValue = is_string($value) ? $this->sqlResource->real_escape_string($value) : $value;

        if ($column) {
            $this->values[$column] = '`' . $escapedValue . '`';

            return $this;
        }

        $this->values[] = '`' . $escapedValue . '`';

        return $this;
    }


    public function setOrderBy(string $column): self
    {
        $this->orderBy = $column;

        return $this;
    }


//	public function setDescending(): self
//	{
//		$desc = ' DESC';
//		$this->desc = true;
//
//		return $this;
//	}


    public function setOffset(int $offset): self
    {
        $this->offset = $offset;

        return $this;
    }


    public function setLimit(int $limit): self
    {
        $this->limit = $limit;

        return $this;
    }


    public function setInnerJoin($param): self
    {
        $this->innerJoin = $param;

        return $this;
    }


    public function setLeftJoin(string $table, string $on, array $columns = null): self
    {
        $this->leftJoin['table'] = $table;
        $this->leftJoin['on'] = $on;
        $this->leftJoin['cols'] = $columns;

        return $this;
    }
}
