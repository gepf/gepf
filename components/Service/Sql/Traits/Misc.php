<?php

namespace Gepf\Service\Sql\Traits;

trait Misc
{
    public function checkUnique()
    {
        if (!$this->table) {
            die('no table specified');
        }
        if (!$this->where) {
            die('no where clause specified');
        }

        foreach ($this->where as $where_clause) {
            $query =
                'SELECT * FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS ' .
                "WHERE TABLE_NAME    = '$this->table' " .
                "AND CONSTRAINT_NAME = '" . $where_clause['col'] . "' " .
                "AND CONSTRAINT_TYPE = 'UNIQUE'";
            $result = mysqli_query($this->conn, $query);
            $return = mysqli_fetch_array($result, MYSQLI_ASSOC);
        }

        return $return;
    }
}
