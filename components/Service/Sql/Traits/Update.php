<?php

namespace Gepf\Service\Sql\Traits;

//use       Arr;

use core\Exception_handler;

trait Update
{
    public function update($where = false, $table = false)
    {
        if ($table) {
            $this->setTable($table);
        }
        if ($where) {
            $this->where($where);
        }

        if (!$this->columns) {
            throw new Exception_handler('No columns given. Use Sql->val');
        }
        if (!$this->values) {
            throw new Exception_handler('No values given. Use Sql->val');
        }
        if (!$this->where) {
            throw new Exception_handler('No param given for sql->update');
        }
        if (!$this->table) {
            throw new Exception_handler('No table given for sql->update');
        }

        $this->where_stmt = $this->buildWhereStmt($this->where);

        foreach ($this->columns as $key => $column) {
            $sets[] = $column . ' = ' . $this->values[$key];
        }
        $set = implode(',', $sets);

        $query = 'UPDATE ' . $this->table . ' SET ' . $set . ' ' . $this->where_stmt;
        $result = mysqli_query($this->conn, $query);


        if ($this->verbose) {
            Arr::dump($query);
        }
        $this->reset();
        return $result;
    }
}
