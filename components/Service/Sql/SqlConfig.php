<?php

namespace Gepf\Service\Sql;

use Gepf\Service\ServiceConfig;

class SqlConfig extends ServiceConfig
{
    private ?string $driver;
    private ?string $sqliteSourceFile = null;

    public function __construct(string $driver = null)
    {
        parent::__construct(SqlManager::class);
        $this->driver = $driver;
    }

    public function getName(): string
    {
        return 'sql';
    }

    public function getDriver(): ?string
    {
        return $this->driver;
    }

    public function getSqliteSourceFile(): ?string
    {
        return $this->sqliteSourceFile;
    }

    /**
     * Can be set inside application/bundle
     */
    public function setSqliteSourceFile(?string $sqliteSourceFile): void
    {
        $this->sqliteSourceFile = $sqliteSourceFile;
    }
}
