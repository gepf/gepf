<?php

namespace Gepf\Service\Sql\Schema;

class Relation
{
    public const CARDINALITY_MANY_TO_MANY = 1;
    public const CARDINALITY_ONE_TO_ONE = 2;
    public const CARDINALITY_ONE_TO_MANY = 3;
    public const CARDINALITY_MANY_TO_ONE = 4;

    private string $model;
    private int $cardinality;
    private ?string $reference;


    public static function create(): self
    {
        return new self();
    }


    public function getModel(): string
    {
        return $this->model;
    }

    public function setModel(string $model): self
    {
        $this->model = $model;

        return $this;
    }

    public function getCardinality(): int
    {
        return $this->cardinality;
    }

    public function setCardinality(int $cardinality): self
    {
        $this->cardinality = $cardinality;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(?string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }
}
