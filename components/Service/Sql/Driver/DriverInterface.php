<?php

namespace Gepf\Service\Sql\Driver;

interface DriverInterface
{
    public function query(string $queryStatement): array;

    public function exec(string $queryStatement): bool;

    public function getSchema(): array;
}
