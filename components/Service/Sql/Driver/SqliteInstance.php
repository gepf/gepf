<?php

namespace Gepf\Service\Sql\Driver;

use Gepf\Core\Exception\DependencyException;
use Gepf\Core\FileSystem\Util\Directory;
use Gepf\Service\Sql\SqlConfig;

class SqliteInstance
{
    private static \SQLite3 $sqlResource;

    /**
     * @throws DependencyException
     */
    public static function get(SqlConfig $sqlConfig): \SQLite3
    {
        if (isset(self::$sqlResource)) {
            return self::$sqlResource;
        }

        if (!class_exists(\SQLite3::class)) {
            throw new DependencyException('SQLite3', DependencyException::TYPE_EXTENSION);
        }

        $file = $sqlConfig->getSqliteSourceFile() ?? getVarDir() . '/sqlite/Gepf.sqlite';
        Directory::ensureForFile($file);

        self::$sqlResource = new \SQLite3($file);

        return self::$sqlResource;
    }
}
