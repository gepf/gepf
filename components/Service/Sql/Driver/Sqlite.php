<?php

namespace Gepf\Service\Sql\Driver;

use Gepf\Core\Exception\DependencyException;
use Gepf\Service\Sql\SqlException;

class Sqlite extends AbstractDriver implements DriverInterface
{
    /**
     * @throws DependencyException|SqlException
     */
    public function query(string $queryStatement): array
    {
        if (!isset($this->sqlInstance)) {
            $this->sqlInstance = SqliteInstance::get($this->serviceConfig);
        }

        $result = $this->sqlInstance->query($queryStatement);

        if (!$result) {
            throw new SqlException($this->sqlInstance->lastErrorMsg());
        }

        $rows = [];
        while ($row = $result->fetchArray(SQLITE3_ASSOC)) {
            $rows[] = $row;
        }

        return $rows;
    }

    /**
     * @throws SqlException|DependencyException
     */
    public function exec(string $queryStatement): bool
    {
        if (!isset($this->sqlInstance)) {
            $this->sqlInstance = SqliteInstance::get($this->serviceConfig);
        }

        $result = $this->sqlInstance->exec($queryStatement);

        if (!$result) {
            throw new SqlException($this->sqlInstance->lastErrorMsg());
        }

        return $result;
    }

    /**
     * @throws DependencyException|SqlException
     */
    public function getSchema(): array
    {
        $tables = $this->query("SELECT name FROM sqlite_master WHERE type = 'table' AND name NOT LIKE 'sqlite_%';");

        $schema = [];
        foreach ($tables as $table) {
            $schema[$table['name']] = $this->query("PRAGMA table_info({$table['name']});");
        }

        return $schema;
    }
}
