<?php

namespace Gepf\Service\Sql\Driver;

use Gepf\Core\Config\ConfigException;
use Gepf\Core\Exception\DependencyException;
use Gepf\Service\Sql\SqlConfig;
use Gepf\Service\Sql\SqlException;

class MySqlI extends AbstractDriver implements DriverInterface
{
    /**
     * @throws ConfigException
     */
    public function __construct(SqlConfig $serviceConfig)
    {
        parent::__construct($serviceConfig);
        self::validateConfigForMySql($serviceConfig);
    }

    /**
     * @throws ConfigException
     */
    public static function validateConfigForMySql(SqlConfig $serviceConfig): void
    {
        if (!isset($serviceConfig['user'])) {
            throw new ConfigException('MySql user not set');
        }

        if (!isset($serviceConfig['password'])) {
            throw new ConfigException('MySql user password not set');
        }
    }

    /**
     * @throws DependencyException
     * @throws SqlException
     */
    public function query(string $queryStatement): array
    {
        if (!isset($this->sqlInstance)) {
            $this->sqlInstance = MySqlIInstance::get($this->serviceConfig);
        }

        if (!$this->sqlInstance) {
            return [];
        }

        $result = \mysqli_query($this->sqlInstance, $queryStatement);

        if ($this->sqlInstance->errno) {
            throw new SqlException($this->sqlInstance->error . '. ' . $queryStatement);
        }

        if (mysqli_num_rows($result) === 0) {
            return [];
        }

        $rows = [];
        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            $rows[] = $row;
        }

        return $rows;
    }


    /**
     * @throws DependencyException
     * @throws SqlException
     */
    public function getSchema(): array
    {
        $database = $this->serviceConfig['db'] ?? $this->serviceConfig['user'];

        $query = 'SELECT `TABLE_NAME`, `ORDINAL_POSITION`, `COLUMN_NAME`, `COLUMN_TYPE`, `COLUMN_KEY`, `IS_NULLABLE`, `EXTRA`
			FROM INFORMATION_SCHEMA.COLUMNS
			WHERE TABLE_SCHEMA = ' . $database;

        return $this->query($query);


//		$tables = [];
//		$n = 0;
//		foreach ($schema as $column) {
//
//			$colDef['name']  = $column['COLUMN_NAME'];
//			$colDef['pos']   = $column['ORDINAL_POSITION'];
//			$colDef['type']  = $column['COLUMN_TYPE'];
//			#$colDef['key']   = $column['COLUMN_KEY'];
//			$colDef['null']  = $column['IS_NULLABLE'];
//			$colDef['extra'] = $column['EXTRA'];
//
//			if (!isset($table)) {
//				$n = 0;
//			} elseif ( Collection::get($table, 'name') !== $column['TABLE_NAME'] ) {
//				$n++;
//				unset($table['columns']);
//				unset($colsStmt);
//			}
//
//			$table['name']      = $column['TABLE_NAME'];
//			$table['columns'][] = $colDef;
//
//			$colNull  = $colDef['null'] === 'YES' ? '' : 'NOT NULL ';
//			$colStmt  =
//				$colDef['name'].' '.
//				$colDef['type'].' '.
//				$colNull.
//				$colDef['extra']
//			;
//			$colsStmt = isset($colsStmt) ? $colsStmt.', '.$colStmt : $colStmt;
//
//			$table['stmt'] =
//				'CREATE '.$table['name'].' ('.$colsStmt.') PRIMARY KEY (id);'
//			;
//
//			$tables[$n] = $table;
//
//		}
//
//		return $tables;
    }

    public function exec(string $queryStatement): bool
    {
        // TODO: Implement exec() method.
        return false;
    }
}
