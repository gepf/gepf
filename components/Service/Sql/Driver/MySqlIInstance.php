<?php

namespace Gepf\Service\Sql\Driver;

use Gepf\Core\Exception\DependencyException;
use Gepf\Service\Sql\SqlConfig;

class MySqlIInstance
{
    private static \mysqli $sqlResource;

    /**
     * @throws DependencyException
     */
    public static function get(SqlConfig $serviceConfig): \mysqli
    {
        if (isset(self::$sqlResource)) {
            return self::$sqlResource;
        }

        if (!class_exists(\mysqli::class)) {
            throw new DependencyException('SQLite3 is not installed');
        }

        self::$sqlResource = new \mysqli(
            $serviceConfig['host'] ?? 'localhost',
            $serviceConfig['user'],
            $serviceConfig['password'],
            $serviceConfig['database'] ?? $serviceConfig['user'],
        );
        self::$sqlResource->set_charset('utf8');

        return self::$sqlResource;
    }
}
