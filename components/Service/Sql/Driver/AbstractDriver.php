<?php

namespace Gepf\Service\Sql\Driver;

use Gepf\Service\Sql\SqlConfig;

abstract class AbstractDriver
{
    protected SqlConfig $serviceConfig;

    /**
     * @var MySqlIInstance|SqliteInstance
     */
    protected $sqlInstance;


    public function __construct(SqlConfig $serviceConfig)
    {
        $this->serviceConfig = $serviceConfig;
    }
}
