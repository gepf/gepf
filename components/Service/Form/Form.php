<?php

namespace Gepf\Service\Form;

use Gepf\Core\Negotiation\Request\Request;
use Gepf\Core\Util\Property;
use Gepf\Service\RestOrm\Attributes\Type\Boolean;
use Gepf\Service\RestOrm\Attributes\Type\Char;
use Gepf\Service\RestOrm\Attributes\Type\LengthTypeInterface;
use Gepf\Service\RestOrm\Attributes\Type\Password;
use Gepf\Service\RestOrm\MappedEntity;
use Gepf\Service\RestOrm\Model\Field as MappedField;
use Gepf\Service\RestOrm\Model\Model;
use Gepf\Service\RestOrm\Model\ModelBuilder;

class Form
{
    // $id is the name of the form. It is called 'id' and not 'name' to prevent name collisions
    private string $id;
    private bool $isSubmitted = false;
    private ?MappedEntity $entity = null;
    private ?Model $model;
    private FieldCollection $fields;

    public function __construct(string $name)
    {
        $this->id = $name;
        $this->fields = new FieldCollection();
    }

    public function __call(string $field, array $args)
    {
        return $this->fields->get($field);
    }

    public static function createFromModel(Model $model): self
    {
        $form = new self($model->getName());
        $form->setModel($model);

        return $form;
    }

    public static function createFromEntityClass(string $className): self
    {
        $modelBuilder = new ModelBuilder();
        /** @noinspection PhpUnhandledExceptionInspection */
        $entityModel = $modelBuilder->createModel($className);

        $form = new self($entityModel->getName());
        $form->setModel($entityModel);

        return $form;
    }

    public static function createFromEntity(MappedEntity $entity): self
    {
        $form = self::createFromEntityClass($entity::class);
        $form->setEntity($entity);

        return $form;
    }

    public function handleRequest(Request $request): void
    {
        $this->isSubmitted = $request->method === 'POST';

        if (!$this->entity) {
            throw new FormException('Can not handle request for form without entity');
        }

        $this->mapFormDataToEntity($request);
    }

    public function addAllMapped(): void
    {
        foreach ($this->model->getFields() as $mappedField) {

            // TODO implement forms for collection types
            if (!$mappedField->isScalar()) {
                continue;
            }

            $parsedAttributes = $this->parseAttributes([], $mappedField);
            $field = Field::create($this, $mappedField->getName(), $parsedAttributes);
            $this->fields->set($mappedField->getName(), $field);
        }
    }

    public function add(string $key, ...$attributes): void
    {
        $columnDefinition = $this->model->getFields()[$key] ?? null;

        if ($this->model && !$columnDefinition) {
            throw new FormException('Model defined, but has no column matching field name: ' . $key);
        }

        $parsedAttributes = $this->parseAttributes($attributes, $columnDefinition);

        $field = Field::create($this, $key, $parsedAttributes);

        $this->fields->set($key, $field);
    }

    public function setEntity(MappedEntity $entity): void
    {
        $this->entity = $entity;
    }

    private function setModel(Model $model): void
    {
        $this->model = $model;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function isSubmitted(): bool
    {
        return $this->isSubmitted;
    }

    public function getFields(): array
    {
        return $this->fields->toArray();
    }


    private function parseAttributes(array $declaredAttributes, ?MappedField $columnDefinitions = null): array
    {
        if (!$columnDefinitions) {
            return $declaredAttributes;
        }

        $columnAttributes = $this->createAttributesFromColumn($columnDefinitions);

        return array_merge($declaredAttributes, $columnAttributes);
    }


    // TODO refactor to helper class
    private function createAttributesFromColumn(MappedField $columnDefinition): array
    {
        $type = $columnDefinition->getType();
        $attributes = [];

        if ($type instanceof LengthTypeInterface) {
            $attributes['length'] = $type->getLength();
        }

        if ($type instanceof Char) {
            $attributes['type'] = 'text';
        }

        if ($type instanceof Boolean) {
            $attributes['type'] = 'checkbox';
        }

        if ($type instanceof Password) {
            $attributes['type'] = 'password';
        }

        if ($columnDefinition->isNotnull()) {
            $attributes['required'] = 'true';
        }

        return $attributes;
    }


    private function mapFormDataToEntity(Request $request): void
    {
        if (!$request->getBody()->getAll()) {
            return;
        }

        if (!$request->get($this->getId())) {
            return;
        }

        $formValues = $request->get($this->model->getName());
        foreach ($this->model->getFields() as $field) {

            // TODO implement forms for collection types
            if (!$field->isScalar()) {
                continue;
            }

            $value = $formValues[$field->getName()];
            Property::setValue($this->entity, $field->getName(), $value);
        }
    }
}
