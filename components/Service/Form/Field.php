<?php

namespace Gepf\Service\Form;

use Gepf\Core\Exception\WrongReferenceException;

class Field
{
    public const ATTRIBUTE_TYPE = 'type';

    /** @see Form */
    private \WeakReference $form;

    private string $label;
    private string $type;
    private string $name;
    private array $attributes;


    public static function create(Form $form, string $column, array $attributes): self
    {
        $name = "{$form->getId()}[$column]";

        $field = new self();
        $field->setForm($form);
        $field->setLabel($column);
        $field->setName($name);
        $field->setType($attributes['type'] ?? 'text');

        return $field;
    }


    public function getLabel(): string
    {
        return $this->label;
    }

    public function setLabel(string $label): void
    {
        $this->label = $label;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @throws WrongReferenceException
     */
    public function getForm(): Form
    {
        $form = $this->form->get();

        if (!$form instanceof Form) {
            throw new WrongReferenceException();
        }

        return $form;
    }

    public function setForm(Form $form): void
    {
        $this->form = \WeakReference::create($form);
    }
}
