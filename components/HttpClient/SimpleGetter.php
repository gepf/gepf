<?php

namespace Gepf\HttpClient;

use Gepf\Core\Exception\DependencyException;

class SimpleGetter
{
    private bool $useCurl;

    /**
     * @throws DependencyException
     */
    public function __construct()
    {
        $urlFopen = (bool)ini_get('allow_url_fopen');
        if (!$urlFopen) {
            if (!extension_loaded('curl')) {
                throw new DependencyException('curl', DependencyException::TYPE_EXTENSION);
            }

            $this->useCurl = true;
        }
    }

    /**
     * @throws HttpException
     */
    public function request(string $path): string
    {
        if ($this->useCurl) {
            $httpClient = new HttpClient();
            $response = $httpClient->request($path);

            return $response->getBody();
        }

        return file_get_contents($path);
    }
}
