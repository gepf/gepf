<?php

namespace Gepf\HttpClient;

use Gepf\Core\Exception\DependencyException;

class HttpClient
{
    /**
     * @throws DependencyException
     */
    public function __construct()
    {
        if (!\extension_loaded('curl')) {
            throw new DependencyException('curl', DependencyException::TYPE_EXTENSION);
        }
    }


    /**
     * @throws HttpException
     */
    public function request(string $path, string $method = 'GET', array $parameters = [], array $headers = [], array $cookies = []): Response
    {
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $path);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $this->setCurlMethod($curl, $method);

        if ($parameters) {
            curl_setopt($curl, CURLOPT_POSTFIELDS, $parameters);
        }

        if (!isset($headers['Accept'])) {
            $headers['Accept'] = 'text/html';
        }
        foreach ($headers as $key => $value) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, [$key . ': ' . $value]);
        }

        foreach ($cookies as $key => $value) {
            curl_setopt($curl, CURLOPT_COOKIE, $key . '=' . $value);
        }

        $body = curl_exec($curl);
        $info = curl_getinfo($curl);
        curl_close($curl);

        if (!$info['content_type']) {
            throw new HttpException('Server did not send valid Content-Type: header');
        }

        if ($body === false) {
            throw new HttpException("curl returned false, for '$path'");
        }

        $response = new Response();
        $response->setBody($body);
        $response->setCode($info['http_code']);
        $response->setContentType($info['content_type']);

        return $response;
    }


    private function setCurlMethod($curl, string $method): void
    {
        if ($method === 'GET') {
            return;
        }

        if ($method === 'POST') {
            curl_setopt($curl, CURLOPT_POST, true);
            return;
        }

        if ($method === 'PUT') {
            curl_setopt($curl, CURLOPT_PUT, true);
            return;
        }

        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
    }
}
