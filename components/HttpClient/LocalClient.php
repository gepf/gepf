<?php

namespace Gepf\HttpClient;

class LocalClient extends HttpClient
{
    private string $host;
    private string $fqdn;

    public function __construct(
        int $port = 8888,
        private readonly string $envFile = '.env',
        private readonly int $serverSpawnRetry = 99,
        private readonly array $options = [],
    ) {
        parent::__construct();

        $this->host = "localhost:$port";
        $this->fqdn = "http://localhost:$port";
    }


    public function request(string $path, string $method = 'GET', array $parameters = [], array $headers = [], array $cookies = []): Response
    {

        $options = implode(
            ' ',
            array_map(
                static fn($item, $key) => "$key=$item",
                $this->options,
                array_keys($this->options),
            ),
        );

        $pid = exec("GEPF_ENV_FILE=$this->envFile $options php -S $this->host > /dev/null 2>&1 & echo $!");

        if (!$this->probeSpawn($this->fqdn, $this->serverSpawnRetry)) {
            throw new HttpException('Could not spawn local server');
        }

        $result = parent::request($this->fqdn . $path, $method, $parameters, $headers, $cookies);

        exec('kill ' . $pid);

        return $result;
    }


    private function probeSpawn(string $host, int $serverSpawnRetry): bool
    {
        usleep(1000);
        try {
            $probe = parent::request("$host/__ping");
        } catch (HttpException) {
            $probe = false;
        }

        if ($probe) {
            return true;
        }

        if ($serverSpawnRetry > 0) {
            return $this->probeSpawn($host, $serverSpawnRetry - 1);
        }

        return false;
    }
}
