<?php

namespace Gepf\Cache;

class UninitializedCacheItem extends CacheItem
{
    public function __construct()
    {
        parent::__construct(
            content: null,
            createdAt: new \DateTimeImmutable(),
            phpStrategy: null,
            valid: false,
        );
    }
}
