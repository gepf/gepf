<?php

namespace Gepf\Cache;

use Gepf\Core\FileSystem\FileCollection;

class CacheInvalidator
{
    private ?int $expiresAfter = null;
    private FileCollection $origins;
    private ?\DateTimeImmutable $expiresAt = null;
    private ?\Closure $customCallback = null;

    public function invalidate(CacheItem $cacheItem): bool
    {
        $result = $this->iterateOrigins($cacheItem);
        $cacheItem->setValid($result);

        return $result;
    }

    public function getOrigins(): FileCollection
    {
        return $this->origins;
    }

    public function setOrigins(FileCollection $origins): void
    {
        $this->origins = $origins;
    }

    public function getExpiresAt(): ?\DateTimeImmutable
    {
        return $this->expiresAt;
    }

    public function setExpiresAt($expiresAt): void
    {
        $this->expiresAt = $expiresAt;
    }

    public function getExpiresAfter(): ?int
    {
        return $this->expiresAfter;
    }

    public function setExpiresAfter($expiresAfter): void
    {
        $this->expiresAfter = $expiresAfter;
    }

    public function getCustomCallback(): ?\Closure
    {
        return $this->customCallback;
    }

    public function setCustomCallback(\Closure $customCallback): void
    {
        $this->customCallback = $customCallback;
    }


    private function iterateOrigins(CacheItem $cacheItem): bool
    {
        /**
         * Invalidated previously is possible
         */
        if ($cacheItem->isValid() === false) {
            return false;
        }

        foreach ($this->getOrigins() as $origin) {

            if (!$origin->isReadable()) {
                return false;
            }

            if ($this->getExpiresAt() && $this->getExpiresAt() < new \DateTimeImmutable('now')) {
                return false;
            }

            if ($this->getExpiresAfter()) {
                $timeDifference = $origin->getModifiedTime()->getTimestamp() - $cacheItem->getCreatedAt()->getTimestamp();
                if ($timeDifference > $this->getExpiresAfter()) {
                    return false;
                }
            }

            if ($this->getCustomCallback() && $this->getCustomCallback()() === false) {
                return false;
            }
        }

        return true;
    }
}
