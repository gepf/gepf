<?php

/*
 * PhpFileAdapter will cache content in php files which will be required natively.
 * That way, those contents will be stored indirectly by the opcache - wich is faster than
 * having a file with traditional structuring (such as json) included via file_get_contents()
 *
 * File structure like:
 * return [
 *     0 => <time()>
 *     1 => 'Lorem Ipsum'
 * ]
 *
 * return value will end up in [$content, $created], when required as php file
 */

namespace Gepf\Cache\Adapter;

use Gepf\Cache\CacheInterface;
use Gepf\Cache\CacheInvalidator;
use Gepf\Cache\CacheItem;
use Gepf\Cache\Exception\CacheException;
use Gepf\Cache\UninitializedCacheItem;
use Gepf\Core\FileSystem\Util\TmpFileSystem;

class PhpFileAdapter implements CacheInterface
{
    public const STRATEGY_KEY = 0;
    public const CREATED_KEY = 1;
    public const CONTENT_KEY = 2;

    public const STRATEGY_STRING = 'string';
    public const STRATEGY_NUMERIC = 'numeric';
    public const STRATEGY_VAR_EXPORT = 'var_export';        // Slowest encoding, but decoding not necessary
    public const STRATEGY_JSON = 'json';                    // Overall fastest performance
    public const STRATEGY_SERIALIZATION = 'serialization';  // Slower than json, but easier to instantiate objects

    public function __construct(private readonly string $strategy = self::STRATEGY_VAR_EXPORT) {}

    public function get(string $key, callable $callback, CacheInvalidator $invalidator = null): mixed
    {
        $path = '/cache/' . str_replace('.', '/', $key) . '.php';

        $hitOrMissItem = $this->hitOrMiss($path);
        $hitOrMissItem->deserialize($this->getDeserializeCallback());
        if ($invalidator && $invalidator->invalidate($hitOrMissItem)) {
            return $hitOrMissItem->getContent();
        }

        /*
         * Since this is a contract, invalidation happens inside the callback.
         * So it is not clear, whether this is a heavy operation or not
         */
        $resultItem = $callback($hitOrMissItem);

        /*
         * Callback can return anything. It is allowed to return an already instantiated
         * CacheItem though, which will allow for easier new != old comparison
         */
        $result = $resultItem instanceof CacheItem ? $resultItem->getContent() : $resultItem;
        $strategy = $this->guessStrategy($result);

        /*
         * Ensure resultItem is an item. Item must be deserialized
         */
        $resultItem = $resultItem instanceof CacheItem ? $resultItem : new CacheItem(
            content: $resultItem,
            createdAt: new \DateTimeImmutable(),
            phpStrategy: $strategy,
        );

        if ($this->checkResultSame($resultItem, $hitOrMissItem, $strategy)) {
            return $resultItem->getContent();
        }

        $cachedFile = '<?php return ['
            . self::STRATEGY_KEY . " => '$strategy', "
            . self::CREATED_KEY . ' => ' . time() . ', '
            . self::CONTENT_KEY . ' => ' . $this->serialize($result, $strategy)
            . '];';

        TmpFileSystem::put($path, $cachedFile);

        return $result;
    }

    private function guessStrategy(mixed $content): string
    {
        if (\is_string($content)) {
            return self::STRATEGY_STRING;
        }

        if (is_scalar($content) || $content === null) {
            return self::STRATEGY_NUMERIC;
        }

        switch ($this->strategy) {
            case self::STRATEGY_VAR_EXPORT:
                return self::STRATEGY_VAR_EXPORT;
            case self::STRATEGY_JSON:
                return self::STRATEGY_JSON;
            case self::STRATEGY_SERIALIZATION:
                return self::STRATEGY_SERIALIZATION;
        }

        throw new CacheException('Unknown serialization strategy ' . $this->strategy);
    }

    private function serialize(mixed $content, string $strategy): mixed
    {
        switch ($strategy) {
            case self::STRATEGY_STRING:
                return $this->mask($content);
            case self::STRATEGY_NUMERIC:
                return $content ?? 'null';
            case self::STRATEGY_VAR_EXPORT:
                return var_export($content, true);
            case self::STRATEGY_JSON:
                return $this->mask(json_encode($content, JSON_THROW_ON_ERROR));
            case self::STRATEGY_SERIALIZATION:
                return $this->mask(serialize($content));
        }

        throw new CacheException('Unknown serialization strategy ' . $this->strategy);
    }

    private function deserialize(mixed $serialized, string $strategy): mixed
    {
        switch ($strategy) {
            case self::STRATEGY_STRING:
            case self::STRATEGY_NUMERIC:
            case self::STRATEGY_VAR_EXPORT:
                return $serialized;
            case self::STRATEGY_JSON:
                return json_decode($serialized, false, 512, JSON_THROW_ON_ERROR);
            case self::STRATEGY_SERIALIZATION:
                return unserialize($serialized, [false]);
        }

        throw new CacheException('Unknown serialization strategy ' . $this->strategy);
    }

    private function hitOrMiss(string $path): CacheItem
    {
        if (TmpFileSystem::exists($path)) {
            $hit = TmpFileSystem::nativelyRequirePhpFileContent($path);
            /** @noinspection PhpUnhandledExceptionInspection */
            return new CacheItem(
                content: $hit[self::CONTENT_KEY],
                createdAt: new \DateTimeImmutable('@' . $hit[self::CREATED_KEY]),
                phpStrategy: $hit[self::STRATEGY_KEY],
                serialized: $hit[self::STRATEGY_KEY] === self::STRATEGY_JSON || $hit[self::STRATEGY_KEY] === self::STRATEGY_SERIALIZATION,
            );
        }

        return new UninitializedCacheItem();
    }

    private function checkResultSame(CacheItem $resultItem, CacheItem $hitOrMissItem, string $strategy): bool
    {
        if ($hitOrMissItem instanceof UninitializedCacheItem) {
            return false;
        }

        if ($strategy !== $hitOrMissItem->getPhpStrategy()) {
            return false;
        }

        if ($resultItem->getContent() !== $hitOrMissItem->getContent()) {
            return false;
        }

        return true;
    }

    private function mask(string $string): string
    {
        return '\'' . $string . '\'';
    }

    private function getDeserializeCallback(): \Closure
    {
        return function (mixed $serialized, string $strategy) {
            return $this->deserialize($serialized, $strategy);
        };
    }
}
