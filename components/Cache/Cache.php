<?php

namespace Gepf\Cache;

use Gepf\Cache\Adapter\PhpFileAdapter;

class Cache implements CacheInterface
{
    /**
     * TODO implement cache pool; implement reflector to choose appropriate adapter
     */
    public function get(string $key, callable $callback, CacheInvalidator $invalidator = null): mixed
    {
        return (new PhpFileAdapter())->get($key, $callback, $invalidator);
    }
}
