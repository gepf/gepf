<?php

namespace Gepf\Cache;

use Gepf\Core\FileSystem\FileCollection;

class CacheInvalidatorFactory
{
    public static function createCustomCallback(callable $callable): CacheInvalidator
    {
        $invalidator = new CacheInvalidator();
        $invalidator->setCustomCallback($callable);

        return $invalidator;
    }

    public static function createExpiresAt(\DateTimeImmutable $dateTime, FileCollection $checkAlso): CacheInvalidator
    {
        $invalidator = new CacheInvalidator();
        $invalidator->setExpiresAt($dateTime);
        $invalidator->setOrigins($checkAlso);

        return $invalidator;
    }

    public static function createExpiresAfter(int $seconds, FileCollection $checkAlso): CacheInvalidator
    {
        $invalidator = new CacheInvalidator();
        $invalidator->setExpiresAfter($seconds);
        $invalidator->setOrigins($checkAlso);

        return $invalidator;
    }
}
