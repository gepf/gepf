<?php

namespace Gepf\Cache;

/**
 * If cache item is instantiated as empty item, it's phpStrategy must remain
 * empty. The cache adapter will guess the strategy later, based on content
 * and arguments. It will then write the strategy in the 'real' cache from
 * which it can be instantiated.
 */
class CacheItem
{
    /**
     * @param string|null $phpStrategy for example json, var_export, etc
     * @param bool|null $valid null means undecided
     */
    public function __construct(
        private mixed $content,
        private readonly \DateTimeImmutable $createdAt,
        private readonly ?string $phpStrategy,
        private bool $serialized = false,
        private ?bool $valid = null,
    ) {}

    public function getContent()
    {
        if ($this->isSerialized()) {
            throw new \RuntimeException(
                'Attempted to retrieve serialized cache content. Content must be deserialized first'
            );
        }

        return $this->content;
    }

    public function setContent(mixed $content): void
    {
        $this->content = $content;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function isSerialized(): bool
    {
        return $this->serialized;
    }

    public function setSerialized(bool $serialized): void
    {
        $this->serialized = $serialized;
    }

    public function isValid(): bool
    {
        return $this->valid;
    }

    public function setValid(bool $valid): void
    {
        $this->valid = $valid;
    }

    public function getPhpStrategy(): string
    {
        return $this->phpStrategy;
    }

//    public function needsSerialization(): bool
//    {
//        if ($this->isSerialized()) {
//            return false;
//        }
//
//        if (\in_array($this->getPhpStrategy(), [
//            PhpFileAdapter::STRATEGY_JSON,
//            PhpFileAdapter::STRATEGY_SERIALIZATION
//        ], true)) {
//            return true;
//        }
//
//        return false;
//    }

    public function deserialize(callable $callback): void
    {
        if (!$this->isSerialized()) {
            return;
        }

        $result = $callback($this->content, $this->getPhpStrategy());

        $this->setContent($result);
        $this->setSerialized(false);
    }
}
