<?php

namespace Gepf\Cache;

interface CacheInterface
{
    public function get(string $key, callable $callback, CacheInvalidator $invalidator = null): mixed;

//    // TODO Implement these functions in the adapters
//    public function clear();
//    public function clearOlderThan(\DateTimeImmutable $dateTime);
//    public function invalidateAll();
}
